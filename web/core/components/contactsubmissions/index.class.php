<?php
/**
 * @package contactsubmissions
 * @subpackage controllers
 */
require_once dirname(__FILE__) . '/model/contactsubmissions/contactsubmissions.class.php';
abstract class ContactSubmissionsManagerController extends modExtraManagerController {
    /** @var ContactSubmissions $contactsubmissions */
    public $contactsubmissions;
    public function initialize() {
        $this->contactsubmissions = new ContactSubmissions($this->modx);

        $this->addCss($this->contactsubmissions->config['cssUrl'].'mgr.css');
        $this->addJavascript($this->contactsubmissions->config['jsUrl'].'mgr/contactsubmissions.js');
        $this->addHtml('<script type="text/javascript">
        Ext.onReady(function() {
            ContactSubmissions.config = '.$this->modx->toJSON($this->contactsubmissions->config).';
        });
        </script>');
        return parent::initialize();
    }
    public function getLanguageTopics() {
        return array('contactsubmissions:default');
    }
    public function checkPermissions() { return true;}
}
/**
 * @package contactsubmissions
 * @subpackage controllers
 */
class IndexManagerController extends ContactSubmissionsManagerController {
    public static function getDefaultController() { return 'home'; }
}