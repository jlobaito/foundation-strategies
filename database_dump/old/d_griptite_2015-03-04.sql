# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.30)
# Database: d_griptite
# Generation Time: 2015-03-04 18:16:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table modx_access_actiondom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_actiondom`;

CREATE TABLE `modx_access_actiondom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_actions`;

CREATE TABLE `modx_access_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_category`;

CREATE TABLE `modx_access_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_context
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_context`;

CREATE TABLE `modx_access_context` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_context` WRITE;
/*!40000 ALTER TABLE `modx_access_context` DISABLE KEYS */;

INSERT INTO `modx_access_context` (`id`, `target`, `principal_class`, `principal`, `authority`, `policy`)
VALUES
	(1,'web','modUserGroup',0,9999,3),
	(2,'mgr','modUserGroup',1,0,2),
	(3,'web','modUserGroup',1,0,2),
	(8,'mgr','modUserGroup',4,9,12),
	(6,'web','modUserGroup',4,9999,12),
	(9,'web','modUserGroup',5,9999,14),
	(10,'mgr','modUserGroup',5,9999,14);

/*!40000 ALTER TABLE `modx_access_context` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_elements`;

CREATE TABLE `modx_access_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_media_source
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_media_source`;

CREATE TABLE `modx_access_media_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_media_source` WRITE;
/*!40000 ALTER TABLE `modx_access_media_source` DISABLE KEYS */;

INSERT INTO `modx_access_media_source` (`id`, `target`, `principal_class`, `principal`, `authority`, `policy`, `context_key`)
VALUES
	(9,'2','modUserGroup',4,9,8,'mgr'),
	(18,'1','modUserGroup',4,0,8,''),
	(10,'3','modUserGroup',4,0,8,''),
	(17,'1','modUserGroup',1,0,8,''),
	(13,'4','modUserGroup',5,0,8,'mgr'),
	(14,'5','modUserGroup',5,0,8,'mgr'),
	(22,'6','modUserGroup',5,9,8,''),
	(19,'1','modUserGroup',5,0,8,''),
	(23,'6','modUserGroup',1,9,8,'');

/*!40000 ALTER TABLE `modx_access_media_source` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_menus`;

CREATE TABLE `modx_access_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_permissions`;

CREATE TABLE `modx_access_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `value` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `template` (`template`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_permissions` WRITE;
/*!40000 ALTER TABLE `modx_access_permissions` DISABLE KEYS */;

INSERT INTO `modx_access_permissions` (`id`, `template`, `name`, `description`, `value`)
VALUES
	(1,1,'about','perm.about_desc',1),
	(2,1,'access_permissions','perm.access_permissions_desc',1),
	(3,1,'actions','perm.actions_desc',1),
	(4,1,'change_password','perm.change_password_desc',1),
	(5,1,'change_profile','perm.change_profile_desc',1),
	(6,1,'charsets','perm.charsets_desc',1),
	(7,1,'class_map','perm.class_map_desc',1),
	(8,1,'components','perm.components_desc',1),
	(9,1,'content_types','perm.content_types_desc',1),
	(10,1,'countries','perm.countries_desc',1),
	(11,1,'create','perm.create_desc',1),
	(12,1,'credits','perm.credits_desc',1),
	(13,1,'customize_forms','perm.customize_forms_desc',1),
	(14,1,'dashboards','perm.dashboards_desc',1),
	(15,1,'database','perm.database_desc',1),
	(16,1,'database_truncate','perm.database_truncate_desc',1),
	(17,1,'delete_category','perm.delete_category_desc',1),
	(18,1,'delete_chunk','perm.delete_chunk_desc',1),
	(19,1,'delete_context','perm.delete_context_desc',1),
	(20,1,'delete_document','perm.delete_document_desc',1),
	(21,1,'delete_eventlog','perm.delete_eventlog_desc',1),
	(22,1,'delete_plugin','perm.delete_plugin_desc',1),
	(23,1,'delete_propertyset','perm.delete_propertyset_desc',1),
	(24,1,'delete_snippet','perm.delete_snippet_desc',1),
	(25,1,'delete_template','perm.delete_template_desc',1),
	(26,1,'delete_tv','perm.delete_tv_desc',1),
	(27,1,'delete_role','perm.delete_role_desc',1),
	(28,1,'delete_user','perm.delete_user_desc',1),
	(29,1,'directory_chmod','perm.directory_chmod_desc',1),
	(30,1,'directory_create','perm.directory_create_desc',1),
	(31,1,'directory_list','perm.directory_list_desc',1),
	(32,1,'directory_remove','perm.directory_remove_desc',1),
	(33,1,'directory_update','perm.directory_update_desc',1),
	(34,1,'edit_category','perm.edit_category_desc',1),
	(35,1,'edit_chunk','perm.edit_chunk_desc',1),
	(36,1,'edit_context','perm.edit_context_desc',1),
	(37,1,'edit_document','perm.edit_document_desc',1),
	(38,1,'edit_locked','perm.edit_locked_desc',1),
	(39,1,'edit_plugin','perm.edit_plugin_desc',1),
	(40,1,'edit_propertyset','perm.edit_propertyset_desc',1),
	(41,1,'edit_role','perm.edit_role_desc',1),
	(42,1,'edit_snippet','perm.edit_snippet_desc',1),
	(43,1,'edit_template','perm.edit_template_desc',1),
	(44,1,'edit_tv','perm.edit_tv_desc',1),
	(45,1,'edit_user','perm.edit_user_desc',1),
	(46,1,'element_tree','perm.element_tree_desc',1),
	(47,1,'empty_cache','perm.empty_cache_desc',1),
	(48,1,'error_log_erase','perm.error_log_erase_desc',1),
	(49,1,'error_log_view','perm.error_log_view_desc',1),
	(50,1,'export_static','perm.export_static_desc',1),
	(51,1,'file_create','perm.file_create_desc',1),
	(52,1,'file_list','perm.file_list_desc',1),
	(53,1,'file_manager','perm.file_manager_desc',1),
	(54,1,'file_remove','perm.file_remove_desc',1),
	(55,1,'file_tree','perm.file_tree_desc',1),
	(56,1,'file_update','perm.file_update_desc',1),
	(57,1,'file_upload','perm.file_upload_desc',1),
	(58,1,'file_view','perm.file_view_desc',1),
	(59,1,'flush_sessions','perm.flush_sessions_desc',1),
	(60,1,'frames','perm.frames_desc',1),
	(61,1,'help','perm.help_desc',1),
	(62,1,'home','perm.home_desc',1),
	(63,1,'import_static','perm.import_static_desc',1),
	(64,1,'languages','perm.languages_desc',1),
	(65,1,'lexicons','perm.lexicons_desc',1),
	(66,1,'list','perm.list_desc',1),
	(67,1,'load','perm.load_desc',1),
	(68,1,'logout','perm.logout_desc',1),
	(69,1,'logs','perm.logs_desc',1),
	(70,1,'menu_reports','perm.menu_reports_desc',1),
	(71,1,'menu_security','perm.menu_security_desc',1),
	(72,1,'menu_site','perm.menu_site_desc',1),
	(73,1,'menu_support','perm.menu_support_desc',1),
	(74,1,'menu_system','perm.menu_system_desc',1),
	(75,1,'menu_tools','perm.menu_tools_desc',1),
	(76,1,'menu_user','perm.menu_user_desc',1),
	(77,1,'menus','perm.menus_desc',1),
	(78,1,'messages','perm.messages_desc',1),
	(79,1,'namespaces','perm.namespaces_desc',1),
	(80,1,'new_category','perm.new_category_desc',1),
	(81,1,'new_chunk','perm.new_chunk_desc',1),
	(82,1,'new_context','perm.new_context_desc',1),
	(83,1,'new_document','perm.new_document_desc',1),
	(84,1,'new_static_resource','perm.new_static_resource_desc',1),
	(85,1,'new_symlink','perm.new_symlink_desc',1),
	(86,1,'new_weblink','perm.new_weblink_desc',1),
	(87,1,'new_document_in_root','perm.new_document_in_root_desc',1),
	(88,1,'new_plugin','perm.new_plugin_desc',1),
	(89,1,'new_propertyset','perm.new_propertyset_desc',1),
	(90,1,'new_role','perm.new_role_desc',1),
	(91,1,'new_snippet','perm.new_snippet_desc',1),
	(92,1,'new_template','perm.new_template_desc',1),
	(93,1,'new_tv','perm.new_tv_desc',1),
	(94,1,'new_user','perm.new_user_desc',1),
	(95,1,'packages','perm.packages_desc',1),
	(96,1,'policy_delete','perm.policy_delete_desc',1),
	(97,1,'policy_edit','perm.policy_edit_desc',1),
	(98,1,'policy_new','perm.policy_new_desc',1),
	(99,1,'policy_save','perm.policy_save_desc',1),
	(100,1,'policy_view','perm.policy_view_desc',1),
	(101,1,'policy_template_delete','perm.policy_template_delete_desc',1),
	(102,1,'policy_template_edit','perm.policy_template_edit_desc',1),
	(103,1,'policy_template_new','perm.policy_template_new_desc',1),
	(104,1,'policy_template_save','perm.policy_template_save_desc',1),
	(105,1,'policy_template_view','perm.policy_template_view_desc',1),
	(106,1,'property_sets','perm.property_sets_desc',1),
	(107,1,'providers','perm.providers_desc',1),
	(108,1,'publish_document','perm.publish_document_desc',1),
	(109,1,'purge_deleted','perm.purge_deleted_desc',1),
	(110,1,'remove','perm.remove_desc',1),
	(111,1,'remove_locks','perm.remove_locks_desc',1),
	(112,1,'resource_duplicate','perm.resource_duplicate_desc',1),
	(113,1,'resourcegroup_delete','perm.resourcegroup_delete_desc',1),
	(114,1,'resourcegroup_edit','perm.resourcegroup_edit_desc',1),
	(115,1,'resourcegroup_new','perm.resourcegroup_new_desc',1),
	(116,1,'resourcegroup_resource_edit','perm.resourcegroup_resource_edit_desc',1),
	(117,1,'resourcegroup_resource_list','perm.resourcegroup_resource_list_desc',1),
	(118,1,'resourcegroup_save','perm.resourcegroup_save_desc',1),
	(119,1,'resourcegroup_view','perm.resourcegroup_view_desc',1),
	(120,1,'resource_quick_create','perm.resource_quick_create_desc',1),
	(121,1,'resource_quick_update','perm.resource_quick_update_desc',1),
	(122,1,'resource_tree','perm.resource_tree_desc',1),
	(123,1,'save','perm.save_desc',1),
	(124,1,'save_category','perm.save_category_desc',1),
	(125,1,'save_chunk','perm.save_chunk_desc',1),
	(126,1,'save_context','perm.save_context_desc',1),
	(127,1,'save_document','perm.save_document_desc',1),
	(128,1,'save_plugin','perm.save_plugin_desc',1),
	(129,1,'save_propertyset','perm.save_propertyset_desc',1),
	(130,1,'save_role','perm.save_role_desc',1),
	(131,1,'save_snippet','perm.save_snippet_desc',1),
	(132,1,'save_template','perm.save_template_desc',1),
	(133,1,'save_tv','perm.save_tv_desc',1),
	(134,1,'save_user','perm.save_user_desc',1),
	(135,1,'search','perm.search_desc',1),
	(136,1,'settings','perm.settings_desc',1),
	(137,1,'source_save','perm.source_save_desc',1),
	(138,1,'source_delete','perm.source_delete_desc',1),
	(139,1,'source_edit','perm.source_edit_desc',1),
	(140,1,'source_view','perm.source_view_desc',1),
	(141,1,'sources','perm.sources_desc',1),
	(142,1,'steal_locks','perm.steal_locks_desc',1),
	(143,1,'tree_show_element_ids','perm.tree_show_element_ids_desc',1),
	(144,1,'tree_show_resource_ids','perm.tree_show_resource_ids_desc',1),
	(145,1,'undelete_document','perm.undelete_document_desc',1),
	(146,1,'unpublish_document','perm.unpublish_document_desc',1),
	(147,1,'unlock_element_properties','perm.unlock_element_properties_desc',1),
	(148,1,'usergroup_delete','perm.usergroup_delete_desc',1),
	(149,1,'usergroup_edit','perm.usergroup_edit_desc',1),
	(150,1,'usergroup_new','perm.usergroup_new_desc',1),
	(151,1,'usergroup_save','perm.usergroup_save_desc',1),
	(152,1,'usergroup_user_edit','perm.usergroup_user_edit_desc',1),
	(153,1,'usergroup_user_list','perm.usergroup_user_list_desc',1),
	(154,1,'usergroup_view','perm.usergroup_view_desc',1),
	(155,1,'view','perm.view_desc',1),
	(156,1,'view_category','perm.view_category_desc',1),
	(157,1,'view_chunk','perm.view_chunk_desc',1),
	(158,1,'view_context','perm.view_context_desc',1),
	(159,1,'view_document','perm.view_document_desc',1),
	(160,1,'view_element','perm.view_element_desc',1),
	(161,1,'view_eventlog','perm.view_eventlog_desc',1),
	(162,1,'view_offline','perm.view_offline_desc',1),
	(163,1,'view_plugin','perm.view_plugin_desc',1),
	(164,1,'view_propertyset','perm.view_propertyset_desc',1),
	(165,1,'view_role','perm.view_role_desc',1),
	(166,1,'view_snippet','perm.view_snippet_desc',1),
	(167,1,'view_sysinfo','perm.view_sysinfo_desc',1),
	(168,1,'view_template','perm.view_template_desc',1),
	(169,1,'view_tv','perm.view_tv_desc',1),
	(170,1,'view_user','perm.view_user_desc',1),
	(171,1,'view_unpublished','perm.view_unpublished_desc',1),
	(172,1,'workspaces','perm.workspaces_desc',1),
	(173,2,'add_children','perm.add_children_desc',1),
	(174,2,'copy','perm.copy_desc',1),
	(175,2,'create','perm.create_desc',1),
	(176,2,'delete','perm.delete_desc',1),
	(177,2,'list','perm.list_desc',1),
	(178,2,'load','perm.load_desc',1),
	(179,2,'move','perm.move_desc',1),
	(180,2,'publish','perm.publish_desc',1),
	(181,2,'remove','perm.remove_desc',1),
	(182,2,'save','perm.save_desc',1),
	(183,2,'steal_lock','perm.steal_lock_desc',1),
	(184,2,'undelete','perm.undelete_desc',1),
	(185,2,'unpublish','perm.unpublish_desc',1),
	(186,2,'view','perm.view_desc',1),
	(187,3,'load','perm.load_desc',1),
	(188,3,'list','perm.list_desc',1),
	(189,3,'view','perm.view_desc',1),
	(190,3,'save','perm.save_desc',1),
	(191,3,'remove','perm.remove_desc',1),
	(192,4,'add_children','perm.add_children_desc',1),
	(193,4,'create','perm.create_desc',1),
	(194,4,'copy','perm.copy_desc',1),
	(195,4,'delete','perm.delete_desc',1),
	(196,4,'list','perm.list_desc',1),
	(197,4,'load','perm.load_desc',1),
	(198,4,'remove','perm.remove_desc',1),
	(199,4,'save','perm.save_desc',1),
	(200,4,'view','perm.view_desc',1),
	(201,5,'create','perm.create_desc',1),
	(202,5,'copy','perm.copy_desc',1),
	(203,5,'list','perm.list_desc',1),
	(204,5,'load','perm.load_desc',1),
	(205,5,'remove','perm.remove_desc',1),
	(206,5,'save','perm.save_desc',1),
	(207,5,'view','perm.view_desc',1),
	(208,6,'load','perm.load_desc',1),
	(209,6,'list','perm.list_desc',1),
	(210,6,'view','perm.view_desc',1),
	(211,6,'save','perm.save_desc',1),
	(212,6,'remove','perm.remove_desc',1),
	(213,6,'view_unpublished','perm.view_unpublished_desc',1),
	(214,6,'copy','perm.copy_desc',1),
	(215,7,'resource_tree','perm.resource_tree_desc',1),
	(216,7,'resource_quick_update','perm.resource_quick_update_desc',1),
	(217,7,'resource_quick_create','perm.resource_quick_create_desc',1),
	(218,7,'resource_duplicate','perm.resource_duplicate_desc',1),
	(219,7,'resourcegroup_view','perm.resourcegroup_view_desc',1),
	(220,7,'resourcegroup_save','perm.resourcegroup_save_desc',1),
	(221,7,'resourcegroup_resource_list','perm.resourcegroup_resource_list_desc',1),
	(222,7,'resourcegroup_resource_edit','perm.resourcegroup_resource_edit_desc',1),
	(223,7,'resourcegroup_new','perm.resourcegroup_new_desc',1),
	(224,7,'resourcegroup_edit','perm.resourcegroup_edit_desc',1),
	(225,7,'resourcegroup_delete','perm.resourcegroup_delete_desc',1),
	(226,7,'remove_locks','perm.remove_locks_desc',1),
	(227,7,'remove','perm.remove_desc',1),
	(228,7,'purge_deleted','perm.purge_deleted_desc',1),
	(229,7,'publish_document','perm.publish_document_desc',1),
	(230,7,'providers','perm.providers_desc',1),
	(231,7,'property_sets','perm.property_sets_desc',1),
	(232,7,'policy_view','perm.policy_view_desc',1),
	(233,7,'policy_template_view','perm.policy_template_view_desc',1),
	(234,7,'policy_template_save','perm.policy_template_save_desc',1),
	(235,7,'policy_template_new','perm.policy_template_new_desc',1),
	(236,7,'policy_template_edit','perm.policy_template_edit_desc',1),
	(237,7,'policy_template_delete','perm.policy_template_delete_desc',1),
	(238,7,'policy_save','perm.policy_save_desc',1),
	(239,7,'policy_new','perm.policy_new_desc',1),
	(240,7,'policy_edit','perm.policy_edit_desc',1),
	(241,7,'policy_delete','perm.policy_delete_desc',1),
	(242,7,'packages','perm.packages_desc',1),
	(243,7,'new_weblink','perm.new_weblink_desc',1),
	(244,7,'new_user','perm.new_user_desc',1),
	(245,7,'new_tv','perm.new_tv_desc',1),
	(246,7,'new_template','perm.new_template_desc',1),
	(247,7,'new_symlink','perm.new_symlink_desc',1),
	(248,7,'new_static_resource','perm.new_static_resource_desc',1),
	(249,7,'new_snippet','perm.new_snippet_desc',1),
	(250,7,'new_role','perm.new_role_desc',1),
	(251,7,'new_propertyset','perm.new_propertyset_desc',1),
	(252,7,'new_plugin','perm.new_plugin_desc',1),
	(253,7,'new_document_in_root','perm.new_document_in_root_desc',1),
	(254,7,'new_document','perm.new_document_desc',1),
	(255,7,'new_context','perm.new_context_desc',1),
	(256,7,'new_chunk','perm.new_chunk_desc',1),
	(257,7,'new_category','perm.new_category_desc',1),
	(258,7,'namespaces','perm.namespaces_desc',1),
	(259,7,'messages','perm.messages_desc',1),
	(260,7,'menu_user','perm.menu_user_desc',1),
	(261,7,'menu_tools','perm.menu_tools_desc',1),
	(262,7,'menu_system','perm.menu_system_desc',1),
	(263,7,'menu_support','perm.menu_support_desc',1),
	(264,7,'menu_site','perm.menu_site_desc',1),
	(265,7,'menu_security','perm.menu_security_desc',1),
	(266,7,'menu_reports','perm.menu_reports_desc',1),
	(267,7,'menus','perm.menus_desc',1),
	(268,7,'logs','perm.logs_desc',1),
	(269,7,'logout','perm.logout_desc',1),
	(270,7,'load','perm.load_desc',1),
	(271,7,'list','perm.list_desc',1),
	(272,7,'lexicons','perm.lexicons_desc',1),
	(273,7,'languages','perm.languages_desc',1),
	(274,7,'import_static','perm.import_static_desc',1),
	(275,7,'home','perm.home_desc',1),
	(276,7,'help','perm.help_desc',1),
	(277,7,'frames','perm.frames_desc',1),
	(278,7,'flush_sessions','perm.flush_sessions_desc',1),
	(279,7,'file_view','perm.file_view_desc',1),
	(280,7,'file_upload','perm.file_upload_desc',1),
	(281,7,'file_update','perm.file_update_desc',1),
	(282,7,'file_tree','perm.file_tree_desc',1),
	(283,7,'file_remove','perm.file_remove_desc',1),
	(284,7,'file_manager','perm.file_manager_desc',1),
	(285,7,'file_list','perm.file_list_desc',1),
	(286,7,'file_create','perm.file_create_desc',1),
	(287,7,'export_static','perm.export_static_desc',1),
	(288,7,'error_log_view','perm.error_log_view_desc',1),
	(289,7,'error_log_erase','perm.error_log_erase_desc',1),
	(290,7,'empty_cache','perm.empty_cache_desc',1),
	(291,7,'element_tree','perm.element_tree_desc',1),
	(292,7,'edit_user','perm.edit_user_desc',1),
	(293,7,'edit_tv','perm.edit_tv_desc',1),
	(294,7,'edit_template','perm.edit_template_desc',1),
	(295,7,'edit_snippet','perm.edit_snippet_desc',1),
	(296,7,'edit_role','perm.edit_role_desc',1),
	(297,7,'edit_propertyset','perm.edit_propertyset_desc',1),
	(298,7,'edit_plugin','perm.edit_plugin_desc',1),
	(299,7,'edit_locked','perm.edit_locked_desc',1),
	(300,7,'edit_document','perm.edit_document_desc',1),
	(301,7,'edit_context','perm.edit_context_desc',1),
	(302,7,'edit_chunk','perm.edit_chunk_desc',1),
	(303,7,'edit_category','perm.edit_category_desc',1),
	(304,7,'directory_update','perm.directory_update_desc',1),
	(305,7,'directory_remove','perm.directory_remove_desc',1),
	(306,7,'directory_list','perm.directory_list_desc',1),
	(307,7,'directory_create','perm.directory_create_desc',1),
	(308,7,'directory_chmod','perm.directory_chmod_desc',1),
	(309,7,'delete_user','perm.delete_user_desc',1),
	(310,7,'delete_tv','perm.delete_tv_desc',1),
	(311,7,'delete_template','perm.delete_template_desc',1),
	(312,7,'delete_snippet','perm.delete_snippet_desc',1),
	(313,7,'delete_role','perm.delete_role_desc',1),
	(314,7,'delete_propertyset','perm.delete_propertyset_desc',1),
	(315,7,'delete_plugin','perm.delete_plugin_desc',1),
	(316,7,'delete_eventlog','perm.delete_eventlog_desc',1),
	(317,7,'delete_document','perm.delete_document_desc',1),
	(318,7,'delete_context','perm.delete_context_desc',1),
	(319,7,'delete_chunk','perm.delete_chunk_desc',1),
	(320,7,'delete_category','perm.delete_category_desc',1),
	(321,7,'database_truncate','perm.database_truncate_desc',1),
	(322,7,'database','perm.database_desc',1),
	(323,7,'dashboards','perm.dashboards_desc',1),
	(324,7,'customize_forms','perm.customize_forms_desc',1),
	(325,7,'credits','perm.credits_desc',1),
	(326,7,'create','perm.create_desc',1),
	(327,7,'countries','perm.countries_desc',1),
	(328,7,'content_types','perm.content_types_desc',1),
	(329,7,'components','perm.components_desc',1),
	(330,7,'class_map','perm.class_map_desc',1),
	(331,7,'charsets','perm.charsets_desc',1),
	(332,7,'change_profile','perm.change_profile_desc',1),
	(333,7,'change_password','perm.change_password_desc',1),
	(334,7,'actions','perm.actions_desc',1),
	(335,7,'access_permissions','perm.access_permissions_desc',1),
	(336,7,'about','perm.about_desc',1),
	(337,7,'save','perm.save_desc',1),
	(338,7,'save_category','perm.save_category_desc',1),
	(339,7,'save_chunk','perm.save_chunk_desc',1),
	(340,7,'save_context','perm.save_context_desc',1),
	(341,7,'save_document','perm.save_document_desc',1),
	(342,7,'save_plugin','perm.save_plugin_desc',1),
	(343,7,'save_propertyset','perm.save_propertyset_desc',1),
	(344,7,'save_role','perm.save_role_desc',1),
	(345,7,'save_snippet','perm.save_snippet_desc',1),
	(346,7,'save_template','perm.save_template_desc',1),
	(347,7,'save_tv','perm.save_tv_desc',1),
	(348,7,'save_user','perm.save_user_desc',1),
	(349,7,'search','perm.search_desc',1),
	(350,7,'settings','perm.settings_desc',1),
	(351,7,'sources','perm.sources_desc',1),
	(352,7,'source_delete','perm.source_delete_desc',1),
	(353,7,'source_edit','perm.source_edit_desc',1),
	(354,7,'source_save','perm.source_save_desc',1),
	(355,7,'source_view','perm.source_view_desc',1),
	(356,7,'steal_locks','perm.steal_locks_desc',1),
	(357,7,'tree_show_element_ids','perm.tree_show_element_ids_desc',1),
	(358,7,'tree_show_resource_ids','perm.tree_show_resource_ids_desc',1),
	(359,7,'undelete_document','perm.undelete_document_desc',1),
	(360,7,'unlock_element_properties','perm.unlock_element_properties_desc',1),
	(361,7,'unpublish_document','perm.unpublish_document_desc',1),
	(362,7,'usergroup_delete','perm.usergroup_delete_desc',1),
	(363,7,'usergroup_edit','perm.usergroup_edit_desc',1),
	(364,7,'usergroup_new','perm.usergroup_new_desc',1),
	(365,7,'usergroup_save','perm.usergroup_save_desc',1),
	(366,7,'usergroup_user_edit','perm.usergroup_user_edit_desc',1),
	(367,7,'usergroup_user_list','perm.usergroup_user_list_desc',1),
	(368,7,'usergroup_view','perm.usergroup_view_desc',1),
	(369,7,'view','perm.view_desc',1),
	(370,7,'view_category','perm.view_category_desc',1),
	(371,7,'view_chunk','perm.view_chunk_desc',1),
	(372,7,'view_context','perm.view_context_desc',1),
	(373,7,'view_document','perm.view_document_desc',1),
	(374,7,'view_element','perm.view_element_desc',1),
	(375,7,'view_eventlog','perm.view_eventlog_desc',1),
	(376,7,'view_offline','perm.view_offline_desc',1),
	(377,7,'view_plugin','perm.view_plugin_desc',1),
	(378,7,'view_propertyset','perm.view_propertyset_desc',1),
	(379,7,'view_role','perm.view_role_desc',1),
	(380,7,'view_snippet','perm.view_snippet_desc',1),
	(381,7,'view_sysinfo','perm.view_sysinfo_desc',1),
	(382,7,'view_template','perm.view_template_desc',1),
	(383,7,'view_tv','perm.view_tv_desc',1),
	(384,7,'view_unpublished','perm.view_unpublished_desc',1),
	(385,7,'view_user','perm.view_user_desc',1),
	(386,7,'workspaces','perm.workspaces_desc',1);

/*!40000 ALTER TABLE `modx_access_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_policies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_policies`;

CREATE TABLE `modx_access_policies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `parent` int(10) unsigned NOT NULL DEFAULT '0',
  `template` int(10) unsigned NOT NULL DEFAULT '0',
  `class` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  `lexicon` varchar(255) NOT NULL DEFAULT 'permissions',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent` (`parent`),
  KEY `class` (`class`),
  KEY `template` (`template`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_policies` WRITE;
/*!40000 ALTER TABLE `modx_access_policies` DISABLE KEYS */;

INSERT INTO `modx_access_policies` (`id`, `name`, `description`, `parent`, `template`, `class`, `data`, `lexicon`)
VALUES
	(1,'Resource','MODX Resource Policy with all attributes.',0,2,'','{\"add_children\":true,\"create\":true,\"copy\":true,\"delete\":true,\"list\":true,\"load\":true,\"move\":true,\"publish\":true,\"remove\":true,\"save\":true,\"steal_lock\":true,\"undelete\":true,\"unpublish\":true,\"view\":true}','permissions'),
	(2,'Administrator','Context administration policy with all permissions.',0,1,'','{\"about\":true,\"access_permissions\":true,\"actions\":true,\"change_password\":true,\"change_profile\":true,\"charsets\":true,\"class_map\":true,\"components\":true,\"content_types\":true,\"countries\":true,\"create\":true,\"credits\":true,\"customize_forms\":true,\"dashboards\":true,\"database\":true,\"database_truncate\":true,\"delete_category\":true,\"delete_chunk\":true,\"delete_context\":true,\"delete_document\":true,\"delete_eventlog\":true,\"delete_plugin\":true,\"delete_propertyset\":true,\"delete_role\":true,\"delete_snippet\":true,\"delete_template\":true,\"delete_tv\":true,\"delete_user\":true,\"directory_chmod\":true,\"directory_create\":true,\"directory_list\":true,\"directory_remove\":true,\"directory_update\":true,\"edit_category\":true,\"edit_chunk\":true,\"edit_context\":true,\"edit_document\":true,\"edit_locked\":true,\"edit_plugin\":true,\"edit_propertyset\":true,\"edit_role\":true,\"edit_snippet\":true,\"edit_template\":true,\"edit_tv\":true,\"edit_user\":true,\"element_tree\":true,\"empty_cache\":true,\"error_log_erase\":true,\"error_log_view\":true,\"export_static\":true,\"file_create\":true,\"file_list\":true,\"file_manager\":true,\"file_remove\":true,\"file_tree\":true,\"file_update\":true,\"file_upload\":true,\"file_view\":true,\"flush_sessions\":true,\"frames\":true,\"help\":true,\"home\":true,\"import_static\":true,\"languages\":true,\"lexicons\":true,\"list\":true,\"load\":true,\"logout\":true,\"logs\":true,\"menus\":true,\"menu_reports\":true,\"menu_security\":true,\"menu_site\":true,\"menu_support\":true,\"menu_system\":true,\"menu_tools\":true,\"menu_user\":true,\"messages\":true,\"namespaces\":true,\"new_category\":true,\"new_chunk\":true,\"new_context\":true,\"new_document\":true,\"new_document_in_root\":true,\"new_plugin\":true,\"new_propertyset\":true,\"new_role\":true,\"new_snippet\":true,\"new_static_resource\":true,\"new_symlink\":true,\"new_template\":true,\"new_tv\":true,\"new_user\":true,\"new_weblink\":true,\"packages\":true,\"policy_delete\":true,\"policy_edit\":true,\"policy_new\":true,\"policy_save\":true,\"policy_template_delete\":true,\"policy_template_edit\":true,\"policy_template_new\":true,\"policy_template_save\":true,\"policy_template_view\":true,\"policy_view\":true,\"property_sets\":true,\"providers\":true,\"publish_document\":true,\"purge_deleted\":true,\"remove\":true,\"remove_locks\":true,\"resource_duplicate\":true,\"resourcegroup_delete\":true,\"resourcegroup_edit\":true,\"resourcegroup_new\":true,\"resourcegroup_resource_edit\":true,\"resourcegroup_resource_list\":true,\"resourcegroup_save\":true,\"resourcegroup_view\":true,\"resource_quick_create\":true,\"resource_quick_update\":true,\"resource_tree\":true,\"save\":true,\"save_category\":true,\"save_chunk\":true,\"save_context\":true,\"save_document\":true,\"save_plugin\":true,\"save_propertyset\":true,\"save_role\":true,\"save_snippet\":true,\"save_template\":true,\"save_tv\":true,\"save_user\":true,\"search\":true,\"settings\":true,\"sources\":true,\"source_delete\":true,\"source_edit\":true,\"source_save\":true,\"source_view\":true,\"steal_locks\":true,\"tree_show_element_ids\":true,\"tree_show_resource_ids\":true,\"undelete_document\":true,\"unlock_element_properties\":true,\"unpublish_document\":true,\"usergroup_delete\":true,\"usergroup_edit\":true,\"usergroup_new\":true,\"usergroup_save\":true,\"usergroup_user_edit\":true,\"usergroup_user_list\":true,\"usergroup_view\":true,\"view\":true,\"view_category\":true,\"view_chunk\":true,\"view_context\":true,\"view_document\":true,\"view_element\":true,\"view_eventlog\":true,\"view_offline\":true,\"view_plugin\":true,\"view_propertyset\":true,\"view_role\":true,\"view_snippet\":true,\"view_sysinfo\":true,\"view_template\":true,\"view_tv\":true,\"view_unpublished\":true,\"view_user\":true,\"workspaces\":true}','permissions'),
	(3,'Load Only','A minimal policy with permission to load an object.',0,3,'','{\"load\":true}','permissions'),
	(4,'Load, List and View','Provides load, list and view permissions only.',0,3,'','{\"load\":true,\"list\":true,\"view\":true}','permissions'),
	(5,'Object','An Object policy with all permissions.',0,3,'','{\"load\":true,\"list\":true,\"view\":true,\"save\":true,\"remove\":true}','permissions'),
	(6,'Element','MODX Element policy with all attributes.',0,4,'','{\"add_children\":true,\"create\":true,\"delete\":true,\"list\":true,\"load\":true,\"remove\":true,\"save\":true,\"view\":true,\"copy\":true}','permissions'),
	(7,'Content Editor','Context administration policy with limited, content-editing related Permissions, but no publishing.',0,1,'','{\"change_profile\":true,\"class_map\":true,\"countries\":true,\"edit_document\":true,\"frames\":true,\"help\":true,\"home\":true,\"load\":true,\"list\":true,\"logout\":true,\"menu_reports\":true,\"menu_site\":true,\"menu_support\":true,\"menu_tools\":true,\"menu_user\":true,\"resource_duplicate\":true,\"resource_tree\":true,\"save_document\":true,\"source_view\":true,\"tree_show_resource_ids\":true,\"view\":true,\"view_document\":true,\"new_document\":true,\"delete_document\":true}','permissions'),
	(8,'Media Source Admin','Media Source administration policy.',0,5,'','{\"create\":true,\"copy\":true,\"load\":true,\"list\":true,\"save\":true,\"remove\":true,\"view\":true}','permissions'),
	(9,'Media Source User','Media Source user policy, with basic viewing and using - but no editing - of Media Sources.',0,5,'','{\"load\":true,\"list\":true,\"view\":true}','permissions'),
	(10,'Developer','Context administration policy with most Permissions except Administrator and Security functions.',0,0,'','{\"about\":true,\"change_password\":true,\"change_profile\":true,\"charsets\":true,\"class_map\":true,\"components\":true,\"content_types\":true,\"countries\":true,\"create\":true,\"credits\":true,\"customize_forms\":true,\"dashboards\":true,\"database\":true,\"delete_category\":true,\"delete_chunk\":true,\"delete_context\":true,\"delete_document\":true,\"delete_eventlog\":true,\"delete_plugin\":true,\"delete_propertyset\":true,\"delete_snippet\":true,\"delete_template\":true,\"delete_tv\":true,\"delete_role\":true,\"delete_user\":true,\"directory_chmod\":true,\"directory_create\":true,\"directory_list\":true,\"directory_remove\":true,\"directory_update\":true,\"edit_category\":true,\"edit_chunk\":true,\"edit_context\":true,\"edit_document\":true,\"edit_locked\":true,\"edit_plugin\":true,\"edit_propertyset\":true,\"edit_role\":true,\"edit_snippet\":true,\"edit_template\":true,\"edit_tv\":true,\"edit_user\":true,\"element_tree\":true,\"empty_cache\":true,\"error_log_erase\":true,\"error_log_view\":true,\"export_static\":true,\"file_create\":true,\"file_list\":true,\"file_manager\":true,\"file_remove\":true,\"file_tree\":true,\"file_update\":true,\"file_upload\":true,\"file_view\":true,\"frames\":true,\"help\":true,\"home\":true,\"import_static\":true,\"languages\":true,\"lexicons\":true,\"list\":true,\"load\":true,\"logout\":true,\"logs\":true,\"menu_reports\":true,\"menu_site\":true,\"menu_support\":true,\"menu_system\":true,\"menu_tools\":true,\"menu_user\":true,\"menus\":true,\"messages\":true,\"namespaces\":true,\"new_category\":true,\"new_chunk\":true,\"new_context\":true,\"new_document\":true,\"new_static_resource\":true,\"new_symlink\":true,\"new_weblink\":true,\"new_document_in_root\":true,\"new_plugin\":true,\"new_propertyset\":true,\"new_role\":true,\"new_snippet\":true,\"new_template\":true,\"new_tv\":true,\"new_user\":true,\"packages\":true,\"property_sets\":true,\"providers\":true,\"publish_document\":true,\"purge_deleted\":true,\"remove\":true,\"resource_duplicate\":true,\"resource_quick_create\":true,\"resource_quick_update\":true,\"resource_tree\":true,\"save\":true,\"save_category\":true,\"save_chunk\":true,\"save_context\":true,\"save_document\":true,\"save_plugin\":true,\"save_propertyset\":true,\"save_snippet\":true,\"save_template\":true,\"save_tv\":true,\"save_user\":true,\"search\":true,\"settings\":true,\"source_delete\":true,\"source_edit\":true,\"source_save\":true,\"source_view\":true,\"sources\":true,\"tree_show_element_ids\":true,\"tree_show_resource_ids\":true,\"undelete_document\":true,\"unpublish_document\":true,\"unlock_element_properties\":true,\"view\":true,\"view_category\":true,\"view_chunk\":true,\"view_context\":true,\"view_document\":true,\"view_element\":true,\"view_eventlog\":true,\"view_offline\":true,\"view_plugin\":true,\"view_propertyset\":true,\"view_role\":true,\"view_snippet\":true,\"view_sysinfo\":true,\"view_template\":true,\"view_tv\":true,\"view_user\":true,\"view_unpublished\":true,\"workspaces\":true}','permissions'),
	(11,'Context','A standard Context policy that you can apply when creating Context ACLs for basic read/write and view_unpublished access within a Context.',0,6,'','{\"load\":true,\"list\":true,\"view\":true,\"save\":true,\"remove\":true,\"copy\":true,\"view_unpublished\":true}','permissions'),
	(12,'SubAdmins','Context sub administration policy with some permissions.',0,1,'','{\"about\":false,\"access_permissions\":false,\"actions\":false,\"change_password\":true,\"change_profile\":true,\"charsets\":false,\"class_map\":false,\"components\":true,\"content_types\":true,\"countries\":true,\"create\":true,\"credits\":false,\"customize_forms\":false,\"dashboards\":true,\"database\":false,\"database_truncate\":false,\"delete_category\":false,\"delete_chunk\":false,\"delete_context\":false,\"delete_document\":true,\"delete_eventlog\":true,\"delete_plugin\":false,\"delete_propertyset\":false,\"delete_role\":false,\"delete_snippet\":false,\"delete_template\":false,\"delete_tv\":false,\"delete_user\":false,\"directory_chmod\":true,\"directory_create\":true,\"directory_list\":true,\"directory_remove\":false,\"directory_update\":true,\"edit_category\":false,\"edit_chunk\":false,\"edit_context\":false,\"edit_document\":true,\"edit_locked\":false,\"edit_plugin\":false,\"edit_propertyset\":true,\"edit_role\":false,\"edit_snippet\":false,\"edit_template\":false,\"edit_tv\":false,\"edit_user\":false,\"element_tree\":false,\"empty_cache\":true,\"error_log_erase\":false,\"error_log_view\":false,\"export_static\":false,\"file_create\":true,\"file_list\":true,\"file_manager\":true,\"file_remove\":true,\"file_tree\":true,\"file_update\":true,\"file_upload\":true,\"file_view\":true,\"flush_sessions\":false,\"frames\":true,\"help\":true,\"home\":true,\"import_static\":false,\"languages\":false,\"lexicons\":false,\"list\":true,\"load\":true,\"logout\":true,\"logs\":false,\"menus\":true,\"menu_reports\":false,\"menu_security\":false,\"menu_site\":true,\"menu_support\":false,\"menu_system\":false,\"menu_tools\":false,\"menu_user\":true,\"messages\":true,\"namespaces\":false,\"new_category\":false,\"new_chunk\":false,\"new_context\":true,\"new_document\":true,\"new_document_in_root\":true,\"new_plugin\":false,\"new_propertyset\":false,\"new_role\":false,\"new_snippet\":false,\"new_static_resource\":true,\"new_symlink\":true,\"new_template\":false,\"new_tv\":false,\"new_user\":false,\"new_weblink\":true,\"packages\":false,\"policy_delete\":false,\"policy_edit\":false,\"policy_new\":false,\"policy_save\":false,\"policy_template_delete\":false,\"policy_template_edit\":false,\"policy_template_new\":false,\"policy_template_save\":false,\"policy_template_view\":false,\"policy_view\":false,\"property_sets\":false,\"providers\":false,\"publish_document\":true,\"purge_deleted\":true,\"remove\":true,\"remove_locks\":true,\"resourcegroup_delete\":true,\"resourcegroup_edit\":true,\"resourcegroup_new\":true,\"resourcegroup_resource_edit\":true,\"resourcegroup_resource_list\":true,\"resourcegroup_save\":true,\"resourcegroup_view\":false,\"resource_duplicate\":true,\"resource_quick_create\":true,\"resource_quick_update\":true,\"resource_tree\":true,\"save\":true,\"save_category\":false,\"save_chunk\":false,\"save_context\":false,\"save_document\":true,\"save_plugin\":false,\"save_propertyset\":false,\"save_role\":false,\"save_snippet\":false,\"save_template\":false,\"save_tv\":false,\"save_user\":false,\"search\":true,\"settings\":false,\"sources\":false,\"source_delete\":true,\"source_edit\":true,\"source_save\":true,\"source_view\":true,\"steal_locks\":true,\"tree_show_element_ids\":false,\"tree_show_resource_ids\":true,\"undelete_document\":true,\"unlock_element_properties\":false,\"unpublish_document\":true,\"usergroup_delete\":false,\"usergroup_edit\":false,\"usergroup_new\":false,\"usergroup_save\":false,\"usergroup_user_edit\":false,\"usergroup_user_list\":false,\"usergroup_view\":false,\"view\":true,\"view_category\":false,\"view_chunk\":false,\"view_context\":false,\"view_document\":true,\"view_element\":false,\"view_eventlog\":false,\"view_offline\":true,\"view_plugin\":false,\"view_propertyset\":true,\"view_role\":false,\"view_snippet\":false,\"view_sysinfo\":true,\"view_template\":false,\"view_tv\":false,\"view_unpublished\":true,\"view_user\":false,\"workspaces\":false}','permissions'),
	(13,'Duplicate of SubAdmins','Context sub administration policy with some permissions.',0,1,'','{\"about\":false,\"access_permissions\":false,\"actions\":false,\"change_password\":true,\"change_profile\":true,\"charsets\":false,\"class_map\":false,\"components\":true,\"content_types\":true,\"countries\":true,\"create\":true,\"credits\":false,\"customize_forms\":false,\"dashboards\":true,\"database\":false,\"database_truncate\":false,\"delete_category\":false,\"delete_chunk\":false,\"delete_context\":false,\"delete_document\":true,\"delete_eventlog\":false,\"delete_plugin\":false,\"delete_propertyset\":false,\"delete_role\":false,\"delete_snippet\":false,\"delete_template\":false,\"delete_tv\":false,\"delete_user\":false,\"directory_chmod\":true,\"directory_create\":true,\"directory_list\":true,\"directory_remove\":false,\"directory_update\":true,\"edit_category\":false,\"edit_chunk\":false,\"edit_context\":false,\"edit_document\":true,\"edit_locked\":false,\"edit_plugin\":false,\"edit_propertyset\":true,\"edit_role\":false,\"edit_snippet\":false,\"edit_template\":false,\"edit_tv\":false,\"edit_user\":false,\"element_tree\":false,\"empty_cache\":true,\"error_log_erase\":false,\"error_log_view\":false,\"export_static\":false,\"file_create\":true,\"file_list\":true,\"file_manager\":true,\"file_remove\":true,\"file_tree\":true,\"file_update\":true,\"file_upload\":true,\"file_view\":true,\"flush_sessions\":false,\"frames\":true,\"help\":true,\"home\":true,\"import_static\":false,\"languages\":false,\"lexicons\":false,\"list\":true,\"load\":true,\"logout\":true,\"logs\":false,\"menus\":true,\"menu_reports\":false,\"menu_security\":false,\"menu_site\":true,\"menu_support\":false,\"menu_system\":false,\"menu_tools\":false,\"menu_user\":true,\"messages\":true,\"namespaces\":false,\"new_category\":false,\"new_chunk\":false,\"new_context\":true,\"new_document\":true,\"new_document_in_root\":true,\"new_plugin\":false,\"new_propertyset\":false,\"new_role\":false,\"new_snippet\":false,\"new_static_resource\":true,\"new_symlink\":true,\"new_template\":false,\"new_tv\":false,\"new_user\":false,\"new_weblink\":true,\"packages\":false,\"policy_delete\":false,\"policy_edit\":false,\"policy_new\":false,\"policy_save\":false,\"policy_template_delete\":false,\"policy_template_edit\":false,\"policy_template_new\":false,\"policy_template_save\":false,\"policy_template_view\":false,\"policy_view\":false,\"property_sets\":false,\"providers\":false,\"publish_document\":true,\"purge_deleted\":true,\"remove\":true,\"remove_locks\":true,\"resourcegroup_delete\":true,\"resourcegroup_edit\":true,\"resourcegroup_new\":true,\"resourcegroup_resource_edit\":true,\"resourcegroup_resource_list\":true,\"resourcegroup_save\":true,\"resourcegroup_view\":false,\"resource_duplicate\":true,\"resource_quick_create\":true,\"resource_quick_update\":true,\"resource_tree\":true,\"save\":true,\"save_category\":false,\"save_chunk\":false,\"save_context\":false,\"save_document\":true,\"save_plugin\":false,\"save_propertyset\":false,\"save_role\":false,\"save_snippet\":false,\"save_template\":false,\"save_tv\":false,\"save_user\":false,\"search\":true,\"settings\":false,\"sources\":false,\"source_delete\":true,\"source_edit\":true,\"source_save\":true,\"source_view\":true,\"steal_locks\":true,\"tree_show_element_ids\":false,\"tree_show_resource_ids\":true,\"undelete_document\":true,\"unlock_element_properties\":false,\"unpublish_document\":true,\"usergroup_delete\":false,\"usergroup_edit\":false,\"usergroup_new\":false,\"usergroup_save\":false,\"usergroup_user_edit\":false,\"usergroup_user_list\":false,\"usergroup_view\":false,\"view\":true,\"view_category\":false,\"view_chunk\":false,\"view_context\":false,\"view_document\":true,\"view_element\":false,\"view_eventlog\":false,\"view_offline\":true,\"view_plugin\":false,\"view_propertyset\":true,\"view_role\":false,\"view_snippet\":false,\"view_sysinfo\":true,\"view_template\":false,\"view_tv\":false,\"view_unpublished\":true,\"view_user\":false,\"workspaces\":false}',''),
	(14,'- Client Content Editor','Context administration policy with reduced permissions.',0,7,'','{\"about\":false,\"access_permissions\":true,\"actions\":false,\"change_password\":true,\"change_profile\":true,\"charsets\":false,\"class_map\":true,\"components\":true,\"content_types\":false,\"countries\":false,\"create\":true,\"credits\":false,\"customize_forms\":false,\"dashboards\":false,\"database\":false,\"database_truncate\":false,\"delete_category\":false,\"delete_chunk\":false,\"delete_context\":false,\"delete_document\":true,\"delete_eventlog\":false,\"delete_plugin\":false,\"delete_propertyset\":false,\"delete_role\":false,\"delete_snippet\":false,\"delete_template\":false,\"delete_tv\":false,\"delete_user\":true,\"directory_chmod\":true,\"directory_create\":true,\"directory_list\":true,\"directory_remove\":true,\"directory_update\":true,\"edit_category\":false,\"edit_chunk\":false,\"edit_context\":false,\"edit_document\":true,\"edit_locked\":true,\"edit_plugin\":false,\"edit_propertyset\":false,\"edit_role\":false,\"edit_snippet\":false,\"edit_template\":false,\"edit_tv\":true,\"edit_user\":true,\"element_tree\":false,\"empty_cache\":true,\"error_log_erase\":false,\"error_log_view\":false,\"export_static\":false,\"file_create\":true,\"file_list\":true,\"file_manager\":true,\"file_remove\":true,\"file_tree\":true,\"file_update\":true,\"file_upload\":true,\"file_view\":true,\"flush_sessions\":false,\"frames\":true,\"help\":false,\"home\":true,\"import_static\":false,\"languages\":false,\"lexicons\":false,\"list\":true,\"load\":true,\"logout\":true,\"logs\":false,\"menus\":false,\"menu_reports\":false,\"menu_security\":false,\"menu_site\":true,\"menu_support\":false,\"menu_system\":false,\"menu_tools\":false,\"menu_user\":true,\"messages\":true,\"namespaces\":false,\"new_category\":false,\"new_chunk\":false,\"new_context\":false,\"new_document\":true,\"new_document_in_root\":true,\"new_plugin\":false,\"new_propertyset\":false,\"new_role\":false,\"new_snippet\":false,\"new_static_resource\":true,\"new_symlink\":true,\"new_template\":false,\"new_tv\":false,\"new_user\":true,\"new_weblink\":true,\"packages\":false,\"policy_delete\":false,\"policy_edit\":false,\"policy_new\":false,\"policy_save\":false,\"policy_template_delete\":false,\"policy_template_edit\":false,\"policy_template_new\":false,\"policy_template_save\":false,\"policy_template_view\":false,\"policy_view\":false,\"property_sets\":false,\"providers\":false,\"publish_document\":true,\"purge_deleted\":true,\"remove\":true,\"remove_locks\":true,\"resourcegroup_delete\":false,\"resourcegroup_edit\":false,\"resourcegroup_new\":false,\"resourcegroup_resource_edit\":false,\"resourcegroup_resource_list\":false,\"resourcegroup_save\":false,\"resourcegroup_view\":false,\"resource_duplicate\":true,\"resource_quick_create\":true,\"resource_quick_update\":true,\"resource_tree\":true,\"save\":true,\"save_category\":false,\"save_chunk\":false,\"save_context\":false,\"save_document\":true,\"save_plugin\":false,\"save_propertyset\":false,\"save_role\":false,\"save_snippet\":false,\"save_template\":false,\"save_tv\":true,\"save_user\":true,\"search\":true,\"settings\":false,\"sources\":true,\"source_delete\":false,\"source_edit\":false,\"source_save\":false,\"source_view\":true,\"steal_locks\":true,\"tree_show_element_ids\":false,\"tree_show_resource_ids\":true,\"undelete_document\":true,\"unlock_element_properties\":false,\"unpublish_document\":true,\"usergroup_delete\":false,\"usergroup_edit\":false,\"usergroup_new\":false,\"usergroup_save\":false,\"usergroup_user_edit\":false,\"usergroup_user_list\":false,\"usergroup_view\":false,\"view\":true,\"view_category\":true,\"view_chunk\":true,\"view_context\":false,\"view_document\":true,\"view_element\":false,\"view_eventlog\":false,\"view_offline\":true,\"view_plugin\":false,\"view_propertyset\":false,\"view_role\":false,\"view_snippet\":false,\"view_sysinfo\":false,\"view_template\":false,\"view_tv\":true,\"view_unpublished\":true,\"view_user\":false,\"workspaces\":false}','');

/*!40000 ALTER TABLE `modx_access_policies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_policy_template_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_policy_template_groups`;

CREATE TABLE `modx_access_policy_template_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_policy_template_groups` WRITE;
/*!40000 ALTER TABLE `modx_access_policy_template_groups` DISABLE KEYS */;

INSERT INTO `modx_access_policy_template_groups` (`id`, `name`, `description`)
VALUES
	(1,'Admin','All admin policy templates.'),
	(2,'Object','All Object-based policy templates.'),
	(3,'Resource','All Resource-based policy templates.'),
	(4,'Element','All Element-based policy templates.'),
	(5,'MediaSource','All Media Source-based policy templates.');

/*!40000 ALTER TABLE `modx_access_policy_template_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_policy_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_policy_templates`;

CREATE TABLE `modx_access_policy_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_group` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext,
  `lexicon` varchar(255) NOT NULL DEFAULT 'permissions',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_policy_templates` WRITE;
/*!40000 ALTER TABLE `modx_access_policy_templates` DISABLE KEYS */;

INSERT INTO `modx_access_policy_templates` (`id`, `template_group`, `name`, `description`, `lexicon`)
VALUES
	(1,1,'AdministratorTemplate','Context administration policy template with all permissions.','permissions'),
	(2,3,'ResourceTemplate','Resource Policy Template with all attributes.','permissions'),
	(3,2,'ObjectTemplate','Object Policy Template with all attributes.','permissions'),
	(4,4,'ElementTemplate','Element Policy Template with all attributes.','permissions'),
	(5,5,'MediaSourceTemplate','Media Source Policy Template with all attributes.','permissions'),
	(6,2,'ContextTemplate','Context Policy Template with all attributes.','permissions'),
	(7,1,'- Client Content Editor','Context administration policy template with all permissions.','permissions');

/*!40000 ALTER TABLE `modx_access_policy_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_resource_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_resource_groups`;

CREATE TABLE `modx_access_resource_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_resources`;

CREATE TABLE `modx_access_resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_templatevars
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_templatevars`;

CREATE TABLE `modx_access_templatevars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_actiondom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_actiondom`;

CREATE TABLE `modx_actiondom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `set` int(11) NOT NULL DEFAULT '0',
  `action` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `xtype` varchar(100) NOT NULL DEFAULT '',
  `container` varchar(255) NOT NULL DEFAULT '',
  `rule` varchar(100) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `constraint` varchar(255) NOT NULL DEFAULT '',
  `constraint_field` varchar(100) NOT NULL DEFAULT '',
  `constraint_class` varchar(100) NOT NULL DEFAULT '',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `for_parent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `set` (`set`),
  KEY `action` (`action`),
  KEY `name` (`name`),
  KEY `active` (`active`),
  KEY `for_parent` (`for_parent`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_actiondom` WRITE;
/*!40000 ALTER TABLE `modx_actiondom` DISABLE KEYS */;

INSERT INTO `modx_actiondom` (`id`, `set`, `action`, `name`, `description`, `xtype`, `container`, `rule`, `value`, `constraint`, `constraint_field`, `constraint_class`, `active`, `for_parent`, `rank`)
VALUES
	(5,1,'resource/create','searchable',NULL,'','modx-panel-resource','fieldVisible','0','','','modResource',1,1,5),
	(6,1,'resource/create','cacheable',NULL,'','modx-panel-resource','fieldVisible','0','','','modResource',1,1,5),
	(7,1,'resource/create','pagetitle',NULL,'','modx-panel-resource','fieldTitle','Content Heading (required)','','','modResource',1,1,4),
	(8,1,'resource/create','longtitle',NULL,'','modx-panel-resource','fieldTitle','SEO Page Title','','','modResource',1,1,4),
	(9,1,'resource/create','description',NULL,'','modx-panel-resource','fieldTitle','SEO Meta Decription','','','modResource',1,1,4),
	(10,1,'resource/create','introtext',NULL,'','modx-panel-resource','fieldVisible','0','','','modResource',1,1,5),
	(11,1,'resource/create','alias',NULL,'','modx-panel-resource','fieldTitle','Page URL','','','modResource',1,1,4),
	(12,1,'resource/create','menutitle',NULL,'','modx-panel-resource','fieldTitle','Site Navigation Title','','','modResource',1,1,4),
	(27,3,'resource/update','alias',NULL,'','modx-panel-resource','fieldTitle','Page URL','','','modResource',1,0,4),
	(26,3,'resource/update','introtext',NULL,'','modx-panel-resource','fieldVisible','0','','','modResource',1,0,5),
	(25,3,'resource/update','description',NULL,'','modx-panel-resource','fieldTitle','SEO Meta Decription','','','modResource',1,0,4),
	(24,3,'resource/update','longtitle',NULL,'','modx-panel-resource','fieldTitle','SEO Page Title','','','modResource',1,0,4),
	(23,3,'resource/update','pagetitle',NULL,'','modx-panel-resource','fieldTitle','Content Heading (required)','','','modResource',1,0,4),
	(22,3,'resource/update','cacheable',NULL,'','modx-panel-resource','fieldVisible','0','','','modResource',1,0,5),
	(21,3,'resource/update','searchable',NULL,'','modx-panel-resource','fieldVisible','0','','','modResource',1,0,5),
	(28,3,'resource/update','menutitle',NULL,'','modx-panel-resource','fieldTitle','Site Navigation Title','','','modResource',1,0,4);

/*!40000 ALTER TABLE `modx_actiondom` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_actions`;

CREATE TABLE `modx_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) NOT NULL DEFAULT 'core',
  `controller` varchar(255) NOT NULL,
  `haslayout` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `lang_topics` text NOT NULL,
  `assets` text NOT NULL,
  `help_url` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `namespace` (`namespace`),
  KEY `controller` (`controller`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_actions` WRITE;
/*!40000 ALTER TABLE `modx_actions` DISABLE KEYS */;

INSERT INTO `modx_actions` (`id`, `namespace`, `controller`, `haslayout`, `lang_topics`, `assets`, `help_url`)
VALUES
	(1,'core','welcome',1,'welcome,configcheck','',''),
	(2,'core','system',0,'','',''),
	(3,'core','browser',0,'file','',''),
	(4,'core','context/create',1,'context,setting,access,policy,user','','Contexts'),
	(5,'core','context/update',1,'context,setting,access,policy,user','','Contexts'),
	(6,'core','context/view',1,'context','','Contexts'),
	(7,'core','element',1,'element','',''),
	(8,'core','element/chunk',1,'chunk,category,propertyset,element','','Chunks'),
	(9,'core','element/chunk/create',1,'chunk,category,propertyset,element','','Chunks'),
	(10,'core','element/chunk/update',1,'chunk,category,propertyset,element','','Chunks'),
	(11,'core','element/plugin',1,'plugin,category,system_events,propertyset,element','','Plugins'),
	(12,'core','element/plugin/create',1,'plugin,category,system_events,propertyset,element','','Plugins'),
	(13,'core','element/plugin/update',1,'plugin,category,system_events,propertyset,element','','Plugins'),
	(14,'core','element/snippet',1,'snippet,propertyset,element','','Snippets'),
	(15,'core','element/snippet/create',1,'snippet,propertyset,element','','Snippets'),
	(16,'core','element/snippet/update',1,'snippet,propertyset,element','','Snippets'),
	(17,'core','element/template',1,'template,propertyset,element','','Templates'),
	(18,'core','element/template/create',1,'template,propertyset,element','','Templates'),
	(19,'core','element/template/update',1,'template,propertyset,element','','Templates'),
	(20,'core','element/template/tvsort',1,'template,tv,propertyset,element','',''),
	(21,'core','element/tv',1,'tv,propertyset,element','','Template+Variables'),
	(22,'core','element/tv/create',1,'tv,tv_widget,propertyset,element','','Template+Variables'),
	(23,'core','element/tv/update',1,'tv,tv_widget,propertyset,element','','Template+Variables'),
	(24,'core','element/view',1,'element','',''),
	(25,'core','resource',1,'','',''),
	(26,'core','security/usergroup/create',1,'user,access,policy,context','','User+Groups'),
	(27,'core','security/usergroup/update',1,'user,access,policy,context','','User+Groups'),
	(28,'core','resource/data',1,'resource','','Resource'),
	(29,'core','resource/empty_recycle_bin',1,'resource','',''),
	(30,'core','resource/update',1,'resource','','Resource'),
	(31,'core','security',1,'user','',''),
	(32,'core','security/role',1,'user','','Roles'),
	(33,'core','security/user/create',1,'user,setting,access','','Users'),
	(34,'core','security/user/update',1,'user,setting,access','','Users'),
	(35,'core','security/login',1,'login','',''),
	(36,'core','system/refresh_site',1,'','',''),
	(37,'core','system/phpinfo',1,'','',''),
	(38,'core','resource/tvs',0,'','',''),
	(39,'core','system/file',1,'file','',''),
	(40,'core','system/file/edit',1,'file','',''),
	(41,'core','security/access/policy/update',1,'user,policy','','Policies'),
	(42,'core','workspaces/package/view',1,'workspace,namespace','','Package+Management'),
	(43,'core','security/access/policy/template/update',1,'user,policy','','PolicyTemplates'),
	(44,'core','security/forms/profile/update',1,'formcustomization,user,access,policy','','Form+Customization+Profiles'),
	(45,'core','security/forms/set/update',1,'formcustomization,user,access,policy','','Form+Customization+Sets'),
	(46,'core','system/dashboards/update',1,'dashboards,user','','Dashboards'),
	(47,'core','system/dashboards/create',1,'dashboards,user','','Dashboards'),
	(48,'core','system/dashboards/widget/update',1,'dashboards,user','','Dashboard+Widgets'),
	(49,'core','system/dashboards/widget/create',1,'dashboards,user','','Dashboard+Widgets'),
	(50,'core','source/create',1,'sources,namespace','','Media+Sources'),
	(51,'core','source/update',1,'sources,namespace','','Media+Sources'),
	(52,'core','system/file/create',1,'file','',''),
	(53,'core','system/dashboards',1,'about','','Dashboards'),
	(54,'core','search',1,'','',''),
	(55,'core','resource/create',1,'resource','','Resource'),
	(56,'core','security/user',1,'user','','Users'),
	(57,'core','security/permission',1,'user,access,policy','','Security'),
	(58,'core','security/resourcegroup/index',1,'resource,user,access','','Resource+Groups'),
	(59,'core','security/forms',1,'formcustomization,user,access,policy','','Customizing+The+Manager'),
	(60,'core','system/import',1,'import','',''),
	(61,'core','system/import/html',1,'import','',''),
	(62,'core','element/propertyset/index',1,'element,category,propertyset','','Properties+and+Property+Sets'),
	(63,'core','source/index',1,'sources,namespace','','Media+Sources'),
	(64,'core','resource/site_schedule',1,'resource','',''),
	(65,'core','system/logs/index',1,'manager_log','',''),
	(66,'core','system/event',1,'system_events','',''),
	(67,'core','system/info',1,'system_info','',''),
	(68,'core','help',1,'about','',''),
	(69,'core','workspaces',1,'workspace','','Package+Management'),
	(70,'core','system/settings',1,'setting','','Settings'),
	(71,'core','workspaces/lexicon',1,'package_builder,lexicon,namespace','','Internationalization'),
	(72,'core','system/contenttype',1,'content_type','','Content+Types'),
	(73,'core','context',1,'context','','Contexts'),
	(74,'core','system/action',1,'action,menu,namespace','','Actions+and+Menus'),
	(75,'core','workspaces/namespace',1,'workspace,package_builder,lexicon,namespace','','Namespaces'),
	(76,'core','security/profile',1,'user','',''),
	(77,'core','security/message',1,'messages','',''),
	(78,'contactsubmissions','index',1,'contactsubmissions:default','',''),
	(79,'Help','index',1,'Help:default','',''),
	(80,'migx','index',0,'example:default','','');

/*!40000 ALTER TABLE `modx_actions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_actions_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_actions_fields`;

CREATE TABLE `modx_actions_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT 'field',
  `tab` varchar(255) NOT NULL DEFAULT '',
  `form` varchar(255) NOT NULL DEFAULT '',
  `other` varchar(255) NOT NULL DEFAULT '',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `action` (`action`),
  KEY `type` (`type`),
  KEY `tab` (`tab`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_actions_fields` WRITE;
/*!40000 ALTER TABLE `modx_actions_fields` DISABLE KEYS */;

INSERT INTO `modx_actions_fields` (`id`, `action`, `name`, `type`, `tab`, `form`, `other`, `rank`)
VALUES
	(598,'resource/create','searchable','field','modx-page-settings-right-box-left','modx-panel-resource','',1),
	(586,'resource/create','modx-page-settings-left','tab','','modx-panel-resource','',4),
	(585,'resource/create','modx-page-settings','tab','','modx-panel-resource','',3),
	(584,'resource/create','published','field','modx-resource-main-right','modx-panel-resource','',5),
	(583,'resource/create','hidemenu','field','modx-resource-main-right','modx-panel-resource','',4),
	(581,'resource/create','menutitle','field','modx-resource-main-right','modx-panel-resource','',2),
	(580,'resource/create','alias','field','modx-resource-main-right','modx-panel-resource','',1),
	(579,'resource/create','template','field','modx-resource-main-right','modx-panel-resource','',0),
	(577,'resource/create','introtext','field','modx-resource-main-left','modx-panel-resource','',4),
	(576,'resource/create','description','field','modx-resource-main-left','modx-panel-resource','',3),
	(575,'resource/create','longtitle','field','modx-resource-main-left','modx-panel-resource','',2),
	(572,'resource/create','modx-resource-main-left','tab','','modx-panel-resource','',1),
	(571,'resource/create','modx-resource-settings','tab','','modx-panel-resource','',0),
	(570,'resource/update','modx-resource-content','field','modx-resource-content','modx-panel-resource','',0),
	(568,'resource/update','modx-panel-resource-tv','tab','','modx-panel-resource','tv',8),
	(567,'resource/update','deleted','field','modx-page-settings-right-box-right','modx-panel-resource','',2),
	(566,'resource/update','syncsite','field','modx-page-settings-right-box-right','modx-panel-resource','',1),
	(563,'resource/update','uri','field','modx-page-settings-right-box-left','modx-panel-resource','',4),
	(562,'resource/update','uri_override','field','modx-page-settings-right-box-left','modx-panel-resource','',3),
	(561,'resource/update','richtext','field','modx-page-settings-right-box-left','modx-panel-resource','',2),
	(557,'resource/update','unpub_date','field','modx-page-settings-right','modx-panel-resource','',2),
	(556,'resource/update','pub_date','field','modx-page-settings-right','modx-panel-resource','',1),
	(555,'resource/update','publishedon','field','modx-page-settings-right','modx-panel-resource','',0),
	(554,'resource/update','modx-page-settings-right','tab','','modx-panel-resource','',5),
	(553,'resource/update','menuindex','field','modx-page-settings-left','modx-panel-resource','',4),
	(543,'resource/update','menutitle','field','modx-resource-main-right','modx-panel-resource','',2),
	(542,'resource/update','alias','field','modx-resource-main-right','modx-panel-resource','',1),
	(537,'resource/update','longtitle','field','modx-resource-main-left','modx-panel-resource','',2),
	(536,'resource/update','pagetitle','field','modx-resource-main-left','modx-panel-resource','',1),
	(534,'resource/update','modx-resource-main-left','tab','','modx-panel-resource','',1),
	(597,'resource/create','isfolder','field','modx-page-settings-right-box-left','modx-panel-resource','',0),
	(596,'resource/create','modx-page-settings-right-box-left','tab','','modx-panel-resource','',6),
	(595,'resource/create','unpub_date','field','modx-page-settings-right','modx-panel-resource','',2),
	(594,'resource/create','pub_date','field','modx-page-settings-right','modx-panel-resource','',1),
	(593,'resource/create','publishedon','field','modx-page-settings-right','modx-panel-resource','',0),
	(592,'resource/create','modx-page-settings-right','tab','','modx-panel-resource','',5),
	(591,'resource/create','menuindex','field','modx-page-settings-left','modx-panel-resource','',4),
	(590,'resource/create','content_dispo','field','modx-page-settings-left','modx-panel-resource','',3),
	(589,'resource/create','content_type','field','modx-page-settings-left','modx-panel-resource','',2),
	(588,'resource/create','class_key','field','modx-page-settings-left','modx-panel-resource','',1),
	(587,'resource/create','parent-cmb','field','modx-page-settings-left','modx-panel-resource','',0),
	(582,'resource/create','link_attributes','field','modx-resource-main-right','modx-panel-resource','',3),
	(578,'resource/create','modx-resource-main-right','tab','','modx-panel-resource','',2),
	(574,'resource/create','pagetitle','field','modx-resource-main-left','modx-panel-resource','',1),
	(573,'resource/create','id','field','modx-resource-main-left','modx-panel-resource','',0),
	(569,'resource/update','modx-resource-access-permissions','tab','','modx-panel-resource','',9),
	(533,'resource/update','modx-resource-settings','tab','','modx-panel-resource','',0),
	(565,'resource/update','cacheable','field','modx-page-settings-right-box-right','modx-panel-resource','',0),
	(564,'resource/update','modx-page-settings-right-box-right','tab','','modx-panel-resource','',7),
	(560,'resource/update','searchable','field','modx-page-settings-right-box-left','modx-panel-resource','',1),
	(559,'resource/update','isfolder','field','modx-page-settings-right-box-left','modx-panel-resource','',0),
	(558,'resource/update','modx-page-settings-right-box-left','tab','','modx-panel-resource','',6),
	(552,'resource/update','content_dispo','field','modx-page-settings-left','modx-panel-resource','',3),
	(551,'resource/update','content_type','field','modx-page-settings-left','modx-panel-resource','',2),
	(550,'resource/update','class_key','field','modx-page-settings-left','modx-panel-resource','',1),
	(549,'resource/update','parent-cmb','field','modx-page-settings-left','modx-panel-resource','',0),
	(548,'resource/update','modx-page-settings-left','tab','','modx-panel-resource','',4),
	(547,'resource/update','modx-page-settings','tab','','modx-panel-resource','',3),
	(546,'resource/update','published','field','modx-resource-main-right','modx-panel-resource','',5),
	(545,'resource/update','hidemenu','field','modx-resource-main-right','modx-panel-resource','',4),
	(544,'resource/update','link_attributes','field','modx-resource-main-right','modx-panel-resource','',3),
	(541,'resource/update','template','field','modx-resource-main-right','modx-panel-resource','',0),
	(540,'resource/update','modx-resource-main-right','tab','','modx-panel-resource','',2),
	(539,'resource/update','introtext','field','modx-resource-main-left','modx-panel-resource','',4),
	(538,'resource/update','description','field','modx-resource-main-left','modx-panel-resource','',3),
	(535,'resource/update','id','field','modx-resource-main-left','modx-panel-resource','',0),
	(599,'resource/create','richtext','field','modx-page-settings-right-box-left','modx-panel-resource','',2),
	(600,'resource/create','uri_override','field','modx-page-settings-right-box-left','modx-panel-resource','',3),
	(601,'resource/create','uri','field','modx-page-settings-right-box-left','modx-panel-resource','',4),
	(602,'resource/create','modx-page-settings-right-box-right','tab','','modx-panel-resource','',7),
	(603,'resource/create','cacheable','field','modx-page-settings-right-box-right','modx-panel-resource','',0),
	(604,'resource/create','syncsite','field','modx-page-settings-right-box-right','modx-panel-resource','',1),
	(605,'resource/create','deleted','field','modx-page-settings-right-box-right','modx-panel-resource','',2),
	(606,'resource/create','modx-panel-resource-tv','tab','','modx-panel-resource','tv',8),
	(607,'resource/create','modx-resource-access-permissions','tab','','modx-panel-resource','',9),
	(608,'resource/create','modx-resource-content','field','modx-resource-content','modx-panel-resource','',0);

/*!40000 ALTER TABLE `modx_actions_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_active_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_active_users`;

CREATE TABLE `modx_active_users` (
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `id` int(10) DEFAULT NULL,
  `action` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_categories`;

CREATE TABLE `modx_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned DEFAULT '0',
  `category` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`parent`,`category`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_categories` WRITE;
/*!40000 ALTER TABLE `modx_categories` DISABLE KEYS */;

INSERT INTO `modx_categories` (`id`, `parent`, `category`)
VALUES
	(1,0,'Breadcrumbs'),
	(2,0,'Utils'),
	(3,0,'Layout'),
	(4,0,'FormIt'),
	(5,0,'MIGX'),
	(6,0,'Homepage'),
	(7,6,'About'),
	(8,6,'Services'),
	(9,0,'Appearance');

/*!40000 ALTER TABLE `modx_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_categories_closure
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_categories_closure`;

CREATE TABLE `modx_categories_closure` (
  `ancestor` int(10) unsigned NOT NULL DEFAULT '0',
  `descendant` int(10) unsigned NOT NULL DEFAULT '0',
  `depth` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ancestor`,`descendant`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_categories_closure` WRITE;
/*!40000 ALTER TABLE `modx_categories_closure` DISABLE KEYS */;

INSERT INTO `modx_categories_closure` (`ancestor`, `descendant`, `depth`)
VALUES
	(1,1,0),
	(0,1,0),
	(2,2,0),
	(0,2,0),
	(3,3,0),
	(0,3,0),
	(4,4,0),
	(0,4,0),
	(5,5,0),
	(0,5,0),
	(6,6,0),
	(0,6,0),
	(7,7,0),
	(6,7,1),
	(0,7,0),
	(8,8,0),
	(6,8,1),
	(0,8,0),
	(9,9,0),
	(0,9,0);

/*!40000 ALTER TABLE `modx_categories_closure` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_class_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_class_map`;

CREATE TABLE `modx_class_map` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(120) NOT NULL DEFAULT '',
  `parent_class` varchar(120) NOT NULL DEFAULT '',
  `name_field` varchar(255) NOT NULL DEFAULT 'name',
  `path` tinytext,
  `lexicon` varchar(255) NOT NULL DEFAULT 'core:resource',
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`),
  KEY `parent_class` (`parent_class`),
  KEY `name_field` (`name_field`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_class_map` WRITE;
/*!40000 ALTER TABLE `modx_class_map` DISABLE KEYS */;

INSERT INTO `modx_class_map` (`id`, `class`, `parent_class`, `name_field`, `path`, `lexicon`)
VALUES
	(1,'modDocument','modResource','pagetitle','','core:resource'),
	(2,'modWebLink','modResource','pagetitle','','core:resource'),
	(3,'modSymLink','modResource','pagetitle','','core:resource'),
	(4,'modStaticResource','modResource','pagetitle','','core:resource'),
	(5,'modTemplate','modElement','templatename','','core:resource'),
	(6,'modTemplateVar','modElement','name','','core:resource'),
	(7,'modChunk','modElement','name','','core:resource'),
	(8,'modSnippet','modElement','name','','core:resource'),
	(9,'modPlugin','modElement','name','','core:resource');

/*!40000 ALTER TABLE `modx_class_map` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_content_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_content_type`;

CREATE TABLE `modx_content_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` tinytext,
  `mime_type` tinytext,
  `file_extensions` tinytext,
  `headers` mediumtext,
  `binary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_content_type` WRITE;
/*!40000 ALTER TABLE `modx_content_type` DISABLE KEYS */;

INSERT INTO `modx_content_type` (`id`, `name`, `description`, `mime_type`, `file_extensions`, `headers`, `binary`)
VALUES
	(1,'HTML','HTML content','text/html','',NULL,0),
	(2,'XML','XML content','text/xml','.xml',NULL,0),
	(3,'text','plain text content','text/plain','.txt',NULL,0),
	(4,'CSS','CSS content','text/css','.css',NULL,0),
	(5,'javascript','javascript content','text/javascript','.js',NULL,0),
	(6,'RSS','For RSS feeds','application/rss+xml','.rss',NULL,0),
	(7,'JSON','JSON','application/json','.js',NULL,0),
	(8,'PDF','PDF Files','application/pdf','.pdf',NULL,1);

/*!40000 ALTER TABLE `modx_content_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_context
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_context`;

CREATE TABLE `modx_context` (
  `key` varchar(100) NOT NULL,
  `description` tinytext,
  `rank` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`key`),
  KEY `rank` (`rank`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_context` WRITE;
/*!40000 ALTER TABLE `modx_context` DISABLE KEYS */;

INSERT INTO `modx_context` (`key`, `description`, `rank`, `name`)
VALUES
	('web','The default front-end context for your web site.',0,NULL),
	('mgr','The default manager or administration context for content management activity.',0,NULL);

/*!40000 ALTER TABLE `modx_context` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_context_resource
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_context_resource`;

CREATE TABLE `modx_context_resource` (
  `context_key` varchar(255) NOT NULL,
  `resource` int(11) unsigned NOT NULL,
  PRIMARY KEY (`context_key`,`resource`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_context_setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_context_setting`;

CREATE TABLE `modx_context_setting` (
  `context_key` varchar(255) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` mediumtext,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`context_key`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_context_setting` WRITE;
/*!40000 ALTER TABLE `modx_context_setting` DISABLE KEYS */;

INSERT INTO `modx_context_setting` (`context_key`, `key`, `value`, `xtype`, `namespace`, `area`, `editedon`)
VALUES
	('mgr','allow_tags_in_post','1','combo-boolean','core','system','0000-00-00 00:00:00'),
	('mgr','modRequest.class','modManagerRequest','textfield','core','system','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `modx_context_setting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_dashboard
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_dashboard`;

CREATE TABLE `modx_dashboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `hide_trees` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `hide_trees` (`hide_trees`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_dashboard` WRITE;
/*!40000 ALTER TABLE `modx_dashboard` DISABLE KEYS */;

INSERT INTO `modx_dashboard` (`id`, `name`, `description`, `hide_trees`)
VALUES
	(1,'Default','',0);

/*!40000 ALTER TABLE `modx_dashboard` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_dashboard_widget
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_dashboard_widget`;

CREATE TABLE `modx_dashboard_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `type` varchar(100) NOT NULL,
  `content` mediumtext,
  `namespace` varchar(255) NOT NULL DEFAULT '',
  `lexicon` varchar(255) NOT NULL DEFAULT 'core:dashboards',
  `size` varchar(255) NOT NULL DEFAULT 'half',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `type` (`type`),
  KEY `namespace` (`namespace`),
  KEY `lexicon` (`lexicon`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_dashboard_widget` WRITE;
/*!40000 ALTER TABLE `modx_dashboard_widget` DISABLE KEYS */;

INSERT INTO `modx_dashboard_widget` (`id`, `name`, `description`, `type`, `content`, `namespace`, `lexicon`, `size`)
VALUES
	(1,'w_newsfeed','w_newsfeed_desc','file','[[++manager_path]]controllers/default/dashboard/widget.modx-news.php','core','core:dashboards','half'),
	(2,'w_securityfeed','w_securityfeed_desc','file','[[++manager_path]]controllers/default/dashboard/widget.modx-security.php','core','core:dashboards','half'),
	(3,'w_whosonline','w_whosonline_desc','file','[[++manager_path]]controllers/default/dashboard/widget.grid-online.php','core','core:dashboards','half'),
	(4,'w_recentlyeditedresources','w_recentlyeditedresources_desc','file','[[++manager_path]]controllers/default/dashboard/widget.grid-rer.php','core','core:dashboards','half'),
	(5,'w_configcheck','w_configcheck_desc','file','[[++manager_path]]controllers/default/dashboard/widget.configcheck.php','core','core:dashboards','full');

/*!40000 ALTER TABLE `modx_dashboard_widget` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_dashboard_widget_placement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_dashboard_widget_placement`;

CREATE TABLE `modx_dashboard_widget_placement` (
  `dashboard` int(10) unsigned NOT NULL DEFAULT '0',
  `widget` int(10) unsigned NOT NULL DEFAULT '0',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dashboard`,`widget`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_dashboard_widget_placement` WRITE;
/*!40000 ALTER TABLE `modx_dashboard_widget_placement` DISABLE KEYS */;

INSERT INTO `modx_dashboard_widget_placement` (`dashboard`, `widget`, `rank`)
VALUES
	(1,5,0),
	(1,1,1),
	(1,2,2),
	(1,3,3),
	(1,4,4);

/*!40000 ALTER TABLE `modx_dashboard_widget_placement` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_document_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_document_groups`;

CREATE TABLE `modx_document_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_group` (`document_group`),
  KEY `document` (`document`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_documentgroup_names
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_documentgroup_names`;

CREATE TABLE `modx_documentgroup_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `private_webgroup` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_element_property_sets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_element_property_sets`;

CREATE TABLE `modx_element_property_sets` (
  `element` int(10) unsigned NOT NULL DEFAULT '0',
  `element_class` varchar(100) NOT NULL DEFAULT '',
  `property_set` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`element`,`element_class`,`property_set`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_extension_packages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_extension_packages`;

CREATE TABLE `modx_extension_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `name` varchar(100) NOT NULL DEFAULT 'core',
  `path` text,
  `table_prefix` varchar(255) NOT NULL DEFAULT '',
  `service_class` varchar(255) NOT NULL DEFAULT '',
  `service_name` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `namespace` (`namespace`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_fc_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_fc_profiles`;

CREATE TABLE `modx_fc_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `rank` (`rank`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_fc_profiles` WRITE;
/*!40000 ALTER TABLE `modx_fc_profiles` DISABLE KEYS */;

INSERT INTO `modx_fc_profiles` (`id`, `name`, `description`, `active`, `rank`)
VALUES
	(1,'Content Editors','',1,0);

/*!40000 ALTER TABLE `modx_fc_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_fc_profiles_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_fc_profiles_usergroups`;

CREATE TABLE `modx_fc_profiles_usergroups` (
  `usergroup` int(11) NOT NULL DEFAULT '0',
  `profile` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usergroup`,`profile`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_fc_sets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_fc_sets`;

CREATE TABLE `modx_fc_sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile` int(11) NOT NULL DEFAULT '0',
  `action` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `template` int(11) NOT NULL DEFAULT '0',
  `constraint` varchar(255) NOT NULL DEFAULT '',
  `constraint_field` varchar(100) NOT NULL DEFAULT '',
  `constraint_class` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `profile` (`profile`),
  KEY `action` (`action`),
  KEY `active` (`active`),
  KEY `template` (`template`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_fc_sets` WRITE;
/*!40000 ALTER TABLE `modx_fc_sets` DISABLE KEYS */;

INSERT INTO `modx_fc_sets` (`id`, `profile`, `action`, `description`, `active`, `template`, `constraint`, `constraint_field`, `constraint_class`)
VALUES
	(1,1,'resource/create','',1,0,'','','modResource'),
	(3,1,'resource/update','',1,0,'','','modResource');

/*!40000 ALTER TABLE `modx_fc_sets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_lexicon_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_lexicon_entries`;

CREATE TABLE `modx_lexicon_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `topic` varchar(255) NOT NULL DEFAULT 'default',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `language` varchar(20) NOT NULL DEFAULT 'en',
  `createdon` datetime DEFAULT NULL,
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `topic` (`topic`),
  KEY `namespace` (`namespace`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_lexicon_entries` WRITE;
/*!40000 ALTER TABLE `modx_lexicon_entries` DISABLE KEYS */;

INSERT INTO `modx_lexicon_entries` (`id`, `name`, `value`, `topic`, `namespace`, `language`, `createdon`, `editedon`)
VALUES
	(9,'setting_primary_email','Primary Email','default','core','en','2015-02-26 13:15:31','0000-00-00 00:00:00'),
	(2,'setting_primary-email','Primary Email','setting','core','en','2015-02-26 12:26:54','0000-00-00 00:00:00'),
	(3,'setting_facebook-link','Facebook Link','default','core','en','2015-02-26 12:27:03','0000-00-00 00:00:00'),
	(4,'setting_twitter-link','Twitter Link','default','core','en','2015-02-26 12:27:13','0000-00-00 00:00:00'),
	(5,'setting_facebook-link','Facebook Link','setting','core','en','2015-02-26 12:27:21','0000-00-00 00:00:00'),
	(6,'setting_google_analytics','Google Analytics','default','core','en','2015-02-26 12:27:47','0000-00-00 00:00:00'),
	(7,'setting_google_analytics','Google Analytics','setting','core','en','2015-02-26 12:31:26','0000-00-00 00:00:00'),
	(8,'setting_twitter-link','Twitter Link','setting','core','en','2015-02-26 12:31:50','0000-00-00 00:00:00'),
	(10,'setting_primary_email','Primary Email','setting','core','en','2015-03-04 08:18:35','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `modx_lexicon_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_manager_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_manager_log`;

CREATE TABLE `modx_manager_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL DEFAULT '0',
  `occurred` datetime DEFAULT '0000-00-00 00:00:00',
  `action` varchar(100) NOT NULL DEFAULT '',
  `classKey` varchar(100) NOT NULL DEFAULT '',
  `item` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_manager_log` WRITE;
/*!40000 ALTER TABLE `modx_manager_log` DISABLE KEYS */;

INSERT INTO `modx_manager_log` (`id`, `user`, `occurred`, `action`, `classKey`, `item`)
VALUES
	(1,1,'2013-10-01 06:35:58','setting_update','modSystemSetting','friendly_urls'),
	(2,1,'2013-10-01 06:36:11','setting_update','modSystemSetting','use_alias_path'),
	(3,1,'2013-10-01 06:37:09','setting_update','modSystemSetting','automatic_alias'),
	(4,1,'2013-11-04 17:51:22','setting_update','modSystemSetting','feed_modx_news_enabled'),
	(5,1,'2013-11-04 17:51:27','setting_update','modSystemSetting','feed_modx_security_enabled'),
	(6,1,'2013-11-04 18:04:53','user_create','modUser','2'),
	(7,1,'2013-11-04 18:12:26','user_create','modUser','3'),
	(8,1,'2013-11-04 18:19:22','user_group_create','modUserGroup','2'),
	(9,1,'2013-11-04 18:23:49','profile_create','modFormCustomizationProfile','1'),
	(10,1,'2013-11-04 18:24:26','set_create','modFormCustomizationSet','1'),
	(11,1,'2013-11-04 18:27:42','set_update','modFormCustomizationSet','1'),
	(12,1,'2013-11-04 18:28:31','set_update','modFormCustomizationSet','1'),
	(13,1,'2013-11-04 18:29:40','profile_update','modFormCustomizationProfile','1'),
	(14,1,'2013-11-04 18:29:56','profile_update','modFormCustomizationProfile','1'),
	(15,1,'2013-11-04 18:30:30','profile_duplicate','modFormCustomizationSet','2'),
	(16,1,'2013-11-04 18:30:50','set_update','modFormCustomizationSet','2'),
	(17,1,'2013-11-04 18:38:39','set_update','modFormCustomizationSet','1'),
	(18,1,'2013-11-04 18:44:50','user_update','modUser','3'),
	(19,1,'2013-11-04 18:45:28','user_update','modUser','3'),
	(20,1,'2013-11-04 18:45:49','user_update','modUser','3'),
	(21,1,'2013-11-04 18:47:28','action_delete','modFormCustomizationSet','2'),
	(22,1,'2013-11-04 18:47:32','profile_duplicate','modFormCustomizationSet','3'),
	(23,1,'2013-11-04 18:47:45','set_update','modFormCustomizationSet','3'),
	(24,1,'2013-11-04 18:54:11','policy_update','modAccessPolicy','7'),
	(25,1,'2013-11-04 18:55:33','role_create','modUserGroupRole','3'),
	(26,1,'2013-11-04 18:55:53','policy_update','modAccessPolicy','7'),
	(27,1,'2013-11-04 18:57:30','user_update','modUser','3'),
	(28,1,'2013-11-06 18:18:49','role_create','modUserGroupRole','4'),
	(29,1,'2013-11-06 18:20:05','user_group_create','modUserGroup','3'),
	(30,1,'2013-11-06 18:20:17','policy_duplicate','modAccessPolicy','12'),
	(31,1,'2013-11-06 18:42:10','policy_update','modAccessPolicy','12'),
	(32,1,'2013-11-06 18:42:39','user_group_delete','modUserGroup','2'),
	(33,1,'2013-11-06 18:44:20','user_group_delete','modUserGroup','3'),
	(34,1,'2013-11-06 18:47:36','user_group_create','modUserGroup','4'),
	(35,1,'2013-11-06 19:55:01','user_group_update','modUserGroup','4'),
	(36,1,'2013-11-06 20:06:55','user_update','modUser','3'),
	(37,1,'2013-11-06 20:46:41','user_group_update','modUserGroup','4'),
	(38,1,'2013-11-06 20:48:27','policy_update','modAccessPolicy','12'),
	(39,1,'2013-11-06 20:53:02','user_update','modUser','3'),
	(40,3,'2013-11-06 21:13:09','directory_create','','/home/basemodx/public_html/assets/images'),
	(41,3,'2013-11-06 21:13:31','directory_create','','/home/basemodx/public_html/assets/pdf'),
	(42,1,'2013-11-06 21:32:25','source_create','sources.modMediaSource','2'),
	(43,1,'2013-11-06 21:33:11','source_update','sources.modMediaSource','2'),
	(44,1,'2013-11-06 21:34:39','source_update','sources.modMediaSource','2'),
	(45,1,'2013-11-06 21:34:53','source_update','sources.modMediaSource','2'),
	(46,1,'2013-11-06 21:35:26','source_update','sources.modAccessMediaSource','2'),
	(47,1,'2013-11-06 21:35:29','user_group_update','modUserGroup','4'),
	(48,1,'2013-11-06 21:36:05','access_source_delete','sources.modAccessMediaSource','2'),
	(49,1,'2013-11-06 21:36:25','source_create','sources.modAccessMediaSource','3'),
	(50,1,'2013-11-06 21:36:38','user_group_update','modUserGroup','4'),
	(51,1,'2013-11-06 21:39:04','source_update','sources.modMediaSource','2'),
	(52,1,'2013-11-06 21:39:41','source_update','sources.modMediaSource','2'),
	(53,1,'2013-11-06 21:40:13','source_update','sources.modMediaSource','2'),
	(54,1,'2013-11-06 21:40:33','source_update','sources.modMediaSource','2'),
	(55,1,'2013-11-06 21:42:00','source_update','sources.modMediaSource','1'),
	(56,1,'2013-11-07 17:00:41','category_create','modCategory','2'),
	(57,1,'2013-11-07 17:01:13','plugin_create','modPlugin','2'),
	(58,1,'2013-11-07 17:16:27','plugin_create','modPlugin','3'),
	(59,1,'2013-11-07 17:16:54','plugin_update','modPlugin','3'),
	(60,1,'2013-11-07 17:17:11','resource_update','modResource','1'),
	(61,1,'2013-11-07 17:37:50','category_create','modCategory','3'),
	(62,1,'2013-11-07 17:38:16','chunk_create','modChunk','1'),
	(63,1,'2013-11-07 17:38:32','chunk_duplicate','modChunk','2'),
	(64,1,'2013-11-07 17:38:44','chunk_update','modChunk','2'),
	(65,1,'2013-11-07 17:38:52','chunk_duplicate','modChunk','3'),
	(66,1,'2013-11-07 17:39:05','chunk_update','modChunk','3'),
	(67,1,'2013-11-07 17:39:12','chunk_duplicate','modChunk','4'),
	(68,1,'2013-11-07 17:39:22','chunk_update','modChunk','4'),
	(69,1,'2013-11-07 17:39:33','chunk_duplicate','modChunk','5'),
	(70,1,'2013-11-07 17:39:47','chunk_update','modChunk','5'),
	(71,1,'2013-11-07 17:40:31','template_create','modTemplate','2'),
	(72,1,'2013-11-07 17:40:49','resource_update','modResource','1'),
	(73,1,'2013-11-07 17:56:56','duplicate_resource','modDocument','2'),
	(74,1,'2013-11-07 17:57:46','resource_update','modResource','2'),
	(75,1,'2013-11-07 17:59:18','plugin_update','modPlugin','3'),
	(76,1,'2013-11-07 17:59:36','plugin_update','modPlugin','3'),
	(77,1,'2013-11-07 18:00:16','plugin_update','modPlugin','3'),
	(78,1,'2013-11-07 18:00:47','plugin_update','modPlugin','3'),
	(79,2,'2013-12-12 19:02:03','namespace_create','modNamespace','Help'),
	(80,2,'2013-12-12 19:02:33','namespace_create','modNamespace','contactsubmissions'),
	(81,2,'2013-12-12 19:05:52','menu_create','modMenu','contactsubmissions'),
	(82,2,'2013-12-12 19:06:35','action_create','modAction','78'),
	(83,2,'2013-12-12 19:07:05','menu_update','modMenu','contactsubmissions'),
	(84,2,'2013-12-12 19:07:20','action_create','modAction','79'),
	(85,2,'2013-12-12 19:08:35','menu_create','modMenu','Help'),
	(86,2,'2013-12-12 19:17:44','policy_update','modAccessPolicy','12'),
	(87,2,'2013-12-12 19:30:13','source_update','sources.modMediaSource','2'),
	(88,2,'2013-12-12 19:31:15','source_update','sources.modMediaSource','2'),
	(89,2,'2013-12-12 19:32:16','source_duplicate','sources.modMediaSource','3'),
	(90,2,'2013-12-12 19:32:36','source_update','sources.modMediaSource','3'),
	(91,2,'2013-12-12 19:35:28','setting_update','modSystemSetting','default_media_source'),
	(92,2,'2014-04-02 12:18:19','policy_import','modAccessPolicy','13'),
	(93,2,'2014-04-02 12:23:51','policy_update','modAccessPolicy','12'),
	(94,2,'2014-04-02 12:25:20','policy_update','modAccessPolicy','12'),
	(95,2,'2014-04-02 12:25:40','policy_update','modAccessPolicy','12'),
	(96,3,'2014-04-25 15:51:55','resource_update','modResource','1'),
	(97,2,'2014-04-28 08:39:08','file_upload','','/var/www/vhosts/modx-boilerplate.mcipreview.com/web/assets/media/pdfs/'),
	(98,2,'2014-04-28 08:43:10','policy_update','modAccessPolicy','12'),
	(99,2,'2015-02-10 04:06:16','user_create','modUser','4'),
	(100,2,'2015-02-10 04:06:34','user_update','modUser','4'),
	(101,2,'2015-02-10 04:06:35','user_update','modUser','4'),
	(102,4,'2015-02-10 06:02:17','template_create','modTemplate','3'),
	(103,4,'2015-02-10 06:03:21','chunk_update','modChunk','3'),
	(104,4,'2015-02-10 06:04:18','chunk_create','modChunk','6'),
	(105,4,'2015-02-10 06:04:34','chunk_update','modChunk','6'),
	(106,4,'2015-02-10 06:05:02','chunk_create','modChunk','7'),
	(107,4,'2015-02-10 06:05:34','chunk_create','modChunk','8'),
	(108,4,'2015-02-10 06:06:01','chunk_create','modChunk','9'),
	(109,4,'2015-02-10 06:07:20','resource_update','modResource','1'),
	(110,4,'2015-02-10 06:07:54','chunk_create','modChunk','10'),
	(111,4,'2015-02-10 06:09:01','setting_update','modSystemSetting','tiny.css_selectors'),
	(112,4,'2015-02-10 06:09:25','tv_create','modTemplateVar','1'),
	(113,4,'2015-02-10 06:09:42','tv_duplicate','modTemplateVar','2'),
	(114,4,'2015-02-10 06:10:26','resource_update','modResource','1'),
	(115,4,'2015-02-10 06:10:36','tv_update','modTemplateVar','2'),
	(116,4,'2015-02-10 06:10:48','resource_update','modResource','1'),
	(117,4,'2015-02-10 06:11:59','tv_create','modTemplateVar','3'),
	(118,4,'2015-02-10 06:12:08','category_create','modCategory','6'),
	(119,4,'2015-02-10 06:12:16','category_create','modCategory','7'),
	(120,4,'2015-02-10 06:12:24','category_create','modCategory','8'),
	(121,4,'2015-02-10 06:12:34','tv_update','modTemplateVar','2'),
	(122,4,'2015-02-10 06:12:41','tv_update','modTemplateVar','3'),
	(123,4,'2015-02-10 06:12:48','tv_update','modTemplateVar','1'),
	(124,4,'2015-02-10 06:13:28','tv_update','modTemplateVar','3'),
	(125,4,'2015-02-10 06:13:46','resource_update','modResource','1'),
	(126,4,'2015-02-10 06:19:30','resource_update','modResource','2'),
	(127,4,'2015-02-10 06:20:36','template_create','modTemplate','4'),
	(128,4,'2015-02-10 06:21:01','resource_update','modResource','2'),
	(129,4,'2015-02-10 06:21:20','resource_create','modDocument','3'),
	(130,4,'2015-02-10 06:21:39','resource_create','modDocument','4'),
	(131,4,'2015-02-10 06:21:47','resource_update','modResource','3'),
	(132,4,'2015-02-10 06:21:47','resource_update','modResource','3'),
	(133,4,'2015-02-10 06:22:03','resource_create','modDocument','5'),
	(134,4,'2015-02-10 06:22:18','resource_create','modDocument','6'),
	(135,4,'2015-02-10 06:22:46','resource_update','modResource','5'),
	(136,4,'2015-02-10 06:23:46','resource_create','modDocument','7'),
	(137,4,'2015-02-10 06:24:04','resource_create','modDocument','8'),
	(138,4,'2015-02-10 06:24:23','resource_create','modDocument','9'),
	(139,4,'2015-02-10 06:24:34','resource_create','modDocument','10'),
	(140,4,'2015-02-10 06:24:47','resource_update','modResource','7'),
	(141,4,'2015-02-10 06:25:32','resource_update','modResource','7'),
	(142,4,'2015-02-10 06:27:12','resource_update','modResource','8'),
	(143,4,'2015-02-10 06:28:04','resource_update','modResource','8'),
	(144,4,'2015-02-10 07:01:43','template_update','modTemplate','4'),
	(145,4,'2015-02-10 07:02:02','template_create','modTemplate','5'),
	(146,4,'2015-02-10 07:02:27','chunk_create','modChunk','11'),
	(147,4,'2015-02-10 07:03:49','resource_update','modResource','6'),
	(148,4,'2015-02-10 07:20:40','snippet_create','modSnippet','28'),
	(149,4,'2015-02-10 07:21:46','template_create','modTemplate','6'),
	(150,4,'2015-02-10 07:22:06','template_create','modTemplate','7'),
	(151,4,'2015-02-10 07:22:21','resource_update','modResource','2'),
	(152,4,'2015-02-10 07:22:30','resource_update','modResource','7'),
	(153,4,'2015-02-10 07:27:47','resource_update','modResource','7'),
	(154,4,'2015-02-10 07:28:35','resource_update','modResource','8'),
	(155,4,'2015-02-10 07:29:03','resource_update','modResource','9'),
	(156,4,'2015-02-10 07:29:26','resource_update','modResource','9'),
	(157,4,'2015-02-10 07:29:58','resource_update','modResource','10'),
	(158,4,'2015-02-10 07:31:39','resource_create','modDocument','11'),
	(159,4,'2015-02-10 07:32:05','resource_create','modDocument','12'),
	(160,4,'2015-02-10 07:32:41','resource_create','modDocument','13'),
	(161,4,'2015-02-10 07:32:51','resource_update','modResource','3'),
	(162,4,'2015-02-10 07:32:53','resource_update','modResource','3'),
	(163,4,'2015-02-10 07:38:40','resource_create','modDocument','14'),
	(164,4,'2015-02-10 07:38:45','resource_sort','modResource','14'),
	(165,4,'2015-02-10 07:38:47','resource_update','modResource','14'),
	(166,4,'2015-02-10 07:39:12','resource_update','modResource','14'),
	(167,4,'2015-02-10 07:40:01','resource_update','modResource','14'),
	(168,4,'2015-02-10 07:41:31','resource_update','modResource','5'),
	(169,4,'2015-02-10 07:42:11','content_type_save','modContentType','1'),
	(170,4,'2015-02-10 07:46:53','resource_update','modResource','14'),
	(171,4,'2015-02-10 07:47:11','resource_update','modResource','3'),
	(172,4,'2015-02-10 07:49:16','resource_update','modResource','2'),
	(173,4,'2015-02-10 07:50:24','resource_update','modResource','1'),
	(174,4,'2015-02-10 07:50:52','resource_update','modResource','4'),
	(175,4,'2015-02-10 16:31:48','chunk_create','modChunk','12'),
	(176,4,'2015-02-10 16:32:13','chunk_create','modChunk','13'),
	(177,4,'2015-02-10 16:33:08','chunk_create','modChunk','14'),
	(178,4,'2015-02-10 16:33:29','chunk_create','modChunk','15'),
	(179,4,'2015-02-10 16:35:25','tv_create','modTemplateVar','4'),
	(180,4,'2015-02-10 16:36:17','resource_update','modResource','6'),
	(181,4,'2015-02-10 16:36:23','resource_sort','modResource','6'),
	(182,4,'2015-02-10 16:36:24','resource_update','modResource','6'),
	(183,4,'2015-02-10 16:36:32','resource_sort','modResource','6'),
	(184,4,'2015-02-10 16:36:51','resource_sort','modResource','6'),
	(185,4,'2015-02-10 16:36:52','resource_update','modResource','6'),
	(186,4,'2015-02-10 16:36:58','resource_sort','modResource','6'),
	(187,4,'2015-02-10 17:24:00','chunk_create','modChunk','16'),
	(188,4,'2015-02-10 17:30:18','chunk_create','modChunk','17'),
	(189,4,'2015-02-10 17:30:58','category_create','modCategory','9'),
	(190,4,'2015-02-10 17:31:43','tv_create','modTemplateVar','5'),
	(191,4,'2015-02-10 17:32:22','tv_create','modTemplateVar','6'),
	(192,4,'2015-02-10 17:32:59','tv_update','modTemplateVar','4'),
	(193,4,'2015-02-10 17:36:31','tv_update','modTemplateVar','5'),
	(194,4,'2015-02-10 17:36:51','resource_update','modResource','7'),
	(195,4,'2015-02-10 17:49:53','chunk_create','modChunk','18'),
	(196,4,'2015-02-10 17:50:14','resource_create','modDocument','15'),
	(197,4,'2015-02-10 17:50:23','resource_update','modResource','15'),
	(198,4,'2015-02-10 17:51:12','tv_create','modTemplateVar','7'),
	(199,4,'2015-02-10 17:51:47','template_create','modTemplate','8'),
	(200,4,'2015-02-10 17:52:12','resource_update','modResource','15'),
	(201,4,'2015-02-10 17:52:25','resource_update','modResource','15'),
	(202,4,'2015-02-10 17:52:27','resource_update','modResource','15'),
	(203,4,'2015-02-10 17:55:05','duplicate_resource','modDocument','16'),
	(204,4,'2015-02-10 17:55:32','resource_update','modResource','16'),
	(205,4,'2015-02-10 17:55:52','resource_update','modResource','16'),
	(206,4,'2015-02-10 17:58:14','resource_update','modResource','16'),
	(207,4,'2015-02-10 17:58:23','resource_update','modResource','15'),
	(208,4,'2015-02-10 17:58:29','resource_update','modResource','16'),
	(209,4,'2015-02-10 17:58:46','duplicate_resource','modDocument','17'),
	(210,4,'2015-02-10 17:59:16','resource_update','modResource','17'),
	(211,4,'2015-02-10 17:59:20','duplicate_resource','modDocument','18'),
	(212,4,'2015-02-10 17:59:53','resource_update','modResource','18'),
	(213,4,'2015-02-10 18:01:39','duplicate_resource','modDocument','19'),
	(214,4,'2015-02-10 18:02:08','resource_update','modResource','19'),
	(215,4,'2015-02-10 18:02:19','duplicate_resource','modDocument','20'),
	(216,4,'2015-02-10 18:02:51','resource_update','modResource','20'),
	(217,4,'2015-02-10 18:03:18','duplicate_resource','modDocument','21'),
	(218,4,'2015-02-10 18:03:48','resource_update','modResource','21'),
	(219,4,'2015-02-10 18:04:45','resource_update','modResource','21'),
	(220,4,'2015-02-10 19:33:47','resource_update','modResource','1'),
	(221,4,'2015-02-10 19:34:51','resource_update','modResource','7'),
	(222,4,'2015-02-19 11:25:30','chunk_create','modChunk','19'),
	(223,4,'2015-02-19 11:26:05','resource_update','modResource','4'),
	(224,4,'2015-02-19 11:26:37','resource_update','modResource','18'),
	(225,4,'2015-02-19 11:32:46','chunk_create','modChunk','20'),
	(226,4,'2015-02-19 11:47:53','duplicate_resource','modDocument','22'),
	(227,4,'2015-02-19 11:48:30','resource_update','modResource','22'),
	(228,4,'2015-02-19 11:48:49','duplicate_resource','modDocument','23'),
	(229,4,'2015-02-19 11:49:11','resource_update','modResource','23'),
	(230,4,'2015-02-19 11:49:27','duplicate_resource','modDocument','24'),
	(231,4,'2015-02-19 11:50:02','resource_update','modResource','24'),
	(232,4,'2015-02-19 11:50:17','duplicate_resource','modDocument','25'),
	(233,4,'2015-02-19 11:50:44','resource_update','modResource','25'),
	(234,4,'2015-02-19 11:50:57','resource_update','modResource','24'),
	(235,4,'2015-02-19 11:51:08','resource_update','modResource','22'),
	(236,4,'2015-02-19 12:02:48','source_create','sources.modMediaSource','3'),
	(237,4,'2015-02-19 12:03:29','source_update','sources.modMediaSource','3'),
	(238,4,'2015-02-19 12:04:02','directory_create','','/Users/justin/Desktop/repos/grip-tite/web/images'),
	(239,4,'2015-02-19 12:04:29','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/images/'),
	(240,4,'2015-02-19 12:04:29','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/images/'),
	(241,4,'2015-02-19 12:04:29','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/images/'),
	(242,4,'2015-02-19 12:04:29','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/images/'),
	(243,4,'2015-02-19 12:04:29','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/images/'),
	(244,4,'2015-02-19 12:04:29','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/images/'),
	(245,4,'2015-02-19 12:04:29','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/images/'),
	(246,4,'2015-02-19 12:05:42','directory_create','','/Users/justin/Desktop/repos/grip-tite/web/client-files'),
	(247,4,'2015-02-19 12:06:10','source_update','sources.modMediaSource','3'),
	(248,4,'2015-02-19 12:06:40','tv_update','modTemplateVar','6'),
	(249,4,'2015-02-19 12:07:14','resource_update','modResource','11'),
	(250,4,'2015-02-19 12:07:24','tv_update','modTemplateVar','6'),
	(251,4,'2015-02-19 12:07:38','resource_update','modResource','3'),
	(252,4,'2015-02-19 12:07:54','resource_update','modResource','2'),
	(253,4,'2015-02-19 12:08:07','resource_update','modResource','14'),
	(254,4,'2015-02-19 12:10:21','tv_update','modTemplateVar','6'),
	(255,4,'2015-02-19 12:10:27','tv_update','modTemplateVar','6'),
	(256,4,'2015-02-19 12:10:41','resource_update','modResource','4'),
	(257,4,'2015-02-19 12:10:54','resource_update','modResource','6'),
	(258,4,'2015-02-19 12:11:07','resource_update','modResource','8'),
	(259,4,'2015-02-19 12:12:00','resource_update','modResource','9'),
	(260,4,'2015-02-19 12:13:02','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/'),
	(261,4,'2015-02-19 12:13:12','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/'),
	(262,4,'2015-02-19 12:13:28','resource_update','modResource','3'),
	(263,4,'2015-02-19 12:16:06','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(264,4,'2015-02-19 12:16:34','resource_update','modResource','14'),
	(265,4,'2015-02-19 12:17:10','source_update','sources.modMediaSource','3'),
	(266,4,'2015-02-19 12:18:16','resource_update','modResource','14'),
	(267,4,'2015-02-19 12:19:48','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(268,4,'2015-02-19 12:20:03','resource_update','modResource','14'),
	(269,4,'2015-02-19 12:22:58','resource_update','modResource','7'),
	(270,4,'2015-02-19 12:24:56','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(271,4,'2015-02-19 12:25:08','resource_update','modResource','7'),
	(272,4,'2015-02-19 12:25:43','resource_update','modResource','7'),
	(273,4,'2015-02-19 12:27:01','resource_update','modResource','8'),
	(274,4,'2015-02-19 12:27:34','resource_update','modResource','8'),
	(275,4,'2015-02-19 12:29:57','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(276,4,'2015-02-19 12:30:27','resource_update','modResource','8'),
	(277,4,'2015-02-19 12:30:56','resource_update','modResource','8'),
	(278,4,'2015-02-19 12:31:20','resource_update','modResource','8'),
	(279,4,'2015-02-19 12:32:43','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(280,4,'2015-02-19 12:33:11','resource_update','modResource','9'),
	(281,4,'2015-02-19 12:34:18','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(282,4,'2015-02-19 12:34:40','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(283,4,'2015-02-19 12:35:00','resource_update','modResource','10'),
	(284,4,'2015-02-19 12:41:26','resource_update','modResource','4'),
	(285,4,'2015-02-19 12:41:31','resource_update','modResource','1'),
	(286,4,'2015-02-19 12:41:41','resource_update','modResource','6'),
	(287,4,'2015-02-19 12:42:35','resource_update','modResource','5'),
	(288,4,'2015-02-19 12:44:40','resource_update','modResource','11'),
	(289,4,'2015-02-19 12:45:33','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(290,4,'2015-02-19 12:45:33','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/images/'),
	(291,4,'2015-02-19 12:46:54','resource_update','modResource','11'),
	(292,4,'2015-02-19 12:47:43','resource_update','modResource','12'),
	(293,4,'2015-02-19 12:48:09','resource_update','modResource','11'),
	(294,4,'2015-02-19 12:48:24','resource_update','modResource','13'),
	(295,4,'2015-02-19 12:49:16','resource_update','modResource','7'),
	(296,4,'2015-02-19 12:49:28','resource_update','modResource','2'),
	(297,4,'2015-02-19 12:50:11','resource_update','modResource','14'),
	(298,4,'2015-02-19 12:51:18','resource_update','modResource','1'),
	(299,4,'2015-02-19 12:51:29','resource_update','modResource','1'),
	(300,4,'2015-02-19 12:51:45','resource_update','modResource','1'),
	(301,4,'2015-02-19 12:52:23','resource_update','modResource','5'),
	(302,4,'2015-02-19 12:53:37','setting_update','modSystemSetting','site_name'),
	(303,4,'2015-02-19 12:55:53','tv_create','modTemplateVar','8'),
	(304,4,'2015-02-19 12:57:04','resource_update','modResource','1'),
	(305,4,'2015-02-19 13:02:08','resource_update','modResource','1'),
	(306,4,'2015-02-19 13:02:28','tv_update','modTemplateVar','6'),
	(307,4,'2015-02-19 13:04:52','file_upload','','/Users/justin/Desktop/repos/grip-tite/web/client-files/'),
	(308,4,'2015-02-19 13:05:08','resource_update','modResource','1'),
	(309,4,'2015-02-19 13:05:31','resource_update','modResource','1'),
	(310,4,'2015-02-25 10:21:13','resource_update','modResource','9'),
	(311,4,'2015-02-25 10:23:08','resource_update','modResource','9'),
	(312,4,'2015-02-25 10:24:01','resource_update','modResource','1'),
	(313,4,'2015-02-25 10:24:44','resource_update','modResource','1'),
	(314,4,'2015-02-26 06:50:55','resource_update','modResource','7'),
	(315,4,'2015-02-26 06:51:08','resource_update','modResource','8'),
	(316,4,'2015-02-26 06:51:19','resource_update','modResource','9'),
	(317,4,'2015-02-26 06:51:36','resource_update','modResource','10'),
	(318,4,'2015-02-26 12:26:40','setting_create','modSystemSetting','primary-email'),
	(319,4,'2015-02-26 12:26:55','setting_update','modSystemSetting','primary-email'),
	(320,4,'2015-02-26 12:27:04','setting_create','modSystemSetting','facebook-link'),
	(321,4,'2015-02-26 12:27:13','setting_create','modSystemSetting','twitter-link'),
	(322,4,'2015-02-26 12:27:21','setting_update','modSystemSetting','facebook-link'),
	(323,4,'2015-02-26 12:27:47','setting_create','modSystemSetting','google_analytics'),
	(324,4,'2015-02-26 12:28:07','setting_update','modSystemSetting','mail_smtp_hosts'),
	(325,4,'2015-02-26 12:28:09','setting_update','modSystemSetting','mail_smtp_auth'),
	(326,4,'2015-02-26 12:28:23','setting_update','modSystemSetting','mail_smtp_pass'),
	(327,4,'2015-02-26 12:28:28','setting_update','modSystemSetting','mail_smtp_port'),
	(328,4,'2015-02-26 12:28:36','setting_update','modSystemSetting','mail_smtp_prefix'),
	(329,4,'2015-02-26 12:28:49','setting_update','modSystemSetting','mail_smtp_user'),
	(330,4,'2015-02-26 12:28:52','setting_update','modSystemSetting','mail_use_smtp'),
	(331,4,'2015-02-26 12:31:26','setting_update','modSystemSetting','google_analytics'),
	(332,4,'2015-02-26 12:31:50','setting_update','modSystemSetting','twitter-link'),
	(333,4,'2015-02-26 12:39:08','chunk_create','modChunk','21'),
	(334,4,'2015-02-26 12:39:53','chunk_update','modChunk','21'),
	(335,4,'2015-02-26 12:48:29','setting_update','modSystemSetting','mail_smtp_pass'),
	(336,4,'2015-02-26 12:48:36','setting_update','modSystemSetting','mail_smtp_user'),
	(337,4,'2015-02-26 12:48:59','chunk_create','modChunk','22'),
	(338,4,'2015-02-26 12:50:06','chunk_update','modChunk','22'),
	(339,4,'2015-02-26 12:51:39','setting_update','modSystemSetting','mail_smtp_hosts'),
	(340,4,'2015-02-26 12:59:55','setting_update','modSystemSetting','mail_smtp_user'),
	(341,4,'2015-02-26 12:59:58','setting_update','modSystemSetting','mail_smtp_pass'),
	(342,4,'2015-02-26 13:00:24','chunk_duplicate','modChunk','23'),
	(343,4,'2015-02-26 13:02:11','chunk_update','modChunk','23'),
	(344,4,'2015-02-26 13:02:19','chunk_update','modChunk','23'),
	(345,4,'2015-02-26 13:02:32','chunk_update','modChunk','22'),
	(346,4,'2015-02-26 13:13:51','setting_update','modSystemSetting','mail_smtp_user'),
	(347,4,'2015-02-26 13:15:23','setting_delete','modSystemSetting','primary-email'),
	(348,4,'2015-02-26 13:15:31','setting_create','modSystemSetting','primary_email'),
	(349,4,'2015-02-26 13:34:45','setting_update','modSystemSetting','twitter-link'),
	(350,4,'2015-02-27 11:46:05','user_create','modUser','5'),
	(351,4,'2015-02-27 11:48:28','source_update','sources.modMediaSource','3'),
	(352,4,'2015-02-27 11:54:25','user_update','modUser','5'),
	(353,4,'2015-02-27 11:54:50','user_update','modUser','5'),
	(354,4,'2015-02-27 11:56:25','source_update','sources.modMediaSource','1'),
	(355,4,'2015-02-27 12:00:19','policy_import','modAccessPolicy','14'),
	(356,4,'2015-02-27 12:01:47','user_group_create','modUserGroup','5'),
	(357,4,'2015-02-27 12:02:50','source_create','sources.modMediaSource','4'),
	(358,4,'2015-02-27 12:03:53','source_update','sources.modMediaSource','4'),
	(359,4,'2015-02-27 12:04:26','source_create','sources.modAccessMediaSource','13'),
	(360,4,'2015-02-27 12:04:52','user_group_update','modUserGroup','5'),
	(361,4,'2015-02-27 12:05:33','user_update','modUser','5'),
	(362,4,'2015-02-27 12:06:52','user_group_update','modUserGroup','5'),
	(363,4,'2015-02-27 12:07:06','user_group_update','modUserGroup','5'),
	(364,4,'2015-02-27 12:09:01','source_create','sources.modMediaSource','5'),
	(365,4,'2015-02-27 12:10:21','source_create','sources.modAccessMediaSource','14'),
	(366,4,'2015-02-27 12:10:24','user_group_update','modUserGroup','5'),
	(367,4,'2015-02-27 12:11:29','source_create','sources.modMediaSource','6'),
	(368,4,'2015-02-27 12:12:42','source_update','sources.modMediaSource','6'),
	(369,4,'2015-02-27 12:13:28','source_update','sources.modMediaSource','1'),
	(370,4,'2015-02-27 12:15:41','tv_update','modTemplateVar','6'),
	(371,4,'2015-02-27 12:17:18','source_update','sources.modMediaSource','6'),
	(372,4,'2015-02-27 12:17:31','tv_update','modTemplateVar','6'),
	(373,4,'2015-02-27 12:17:54','tv_update','modTemplateVar','3'),
	(374,4,'2015-02-27 12:28:32','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(375,4,'2015-02-27 12:39:42','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(376,4,'2015-02-27 12:39:50','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(377,4,'2015-02-27 12:52:29','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(378,4,'2015-02-27 12:59:52','resource_update','modResource','1'),
	(379,4,'2015-02-27 13:00:52','user_create','modUser','6'),
	(380,4,'2015-02-27 13:02:19','source_update','sources.modMediaSource','6'),
	(381,5,'2015-02-27 13:04:03','resource_update','modResource','1'),
	(382,5,'2015-02-27 13:05:09','resource_update','modResource','1'),
	(383,5,'2015-02-27 13:05:22','resource_update','modResource','1'),
	(384,5,'2015-02-27 13:12:17','resource_update','modResource','1'),
	(385,6,'2015-02-27 13:23:20','resource_update','modResource','9'),
	(386,6,'2015-02-27 13:24:57','resource_update','modResource','9'),
	(387,6,'2015-02-27 13:25:51','resource_update','modResource','9'),
	(388,6,'2015-02-27 13:28:01','resource_update','modResource','9'),
	(389,6,'2015-02-27 13:29:24','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(390,6,'2015-02-27 13:29:24','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(391,6,'2015-02-27 13:30:56','resource_update','modResource','9'),
	(392,6,'2015-02-27 13:32:55','resource_update','modResource','9'),
	(393,5,'2015-02-27 13:35:08','resource_update','modResource','9'),
	(394,5,'2015-02-27 13:35:42','resource_update','modResource','9'),
	(395,5,'2015-02-27 13:36:06','resource_update','modResource','9'),
	(396,6,'2015-02-27 13:38:54','duplicate_resource','modDocument','26'),
	(397,6,'2015-02-27 13:40:43','resource_update','modResource','26'),
	(398,5,'2015-02-27 13:41:47','delete_resource','modDocument','26'),
	(399,6,'2015-02-27 13:45:00','duplicate_resource','modDocument','27'),
	(400,5,'2015-02-27 13:45:44','resource_update','modResource','27'),
	(401,6,'2015-02-27 13:46:33','resource_update','modResource','27'),
	(402,5,'2015-02-27 13:48:23','resource_create','modDocument','28'),
	(403,6,'2015-02-27 13:48:33','resource_update','modResource','7'),
	(404,6,'2015-02-27 13:50:02','resource_update','modResource','1'),
	(405,6,'2015-02-27 13:50:41','resource_update','modResource','1'),
	(406,6,'2015-02-27 13:51:11','resource_update','modResource','1'),
	(407,6,'2015-02-27 13:53:16','resource_update','modResource','27'),
	(408,6,'2015-02-27 13:53:51','resource_update','modResource','27'),
	(409,6,'2015-02-27 13:56:24','resource_update','modResource','27'),
	(410,6,'2015-02-27 13:56:32','resource_update','modResource','27'),
	(411,6,'2015-02-27 13:59:25','resource_update','modResource','7'),
	(412,6,'2015-02-27 13:59:42','resource_update','modResource','8'),
	(413,6,'2015-02-27 13:59:58','resource_update','modResource','9'),
	(414,6,'2015-02-27 14:00:20','resource_update','modResource','10'),
	(415,6,'2015-02-27 14:00:35','resource_update','modResource','27'),
	(416,4,'2015-02-27 14:53:33','setting_update','modSystemSetting','twitter-link'),
	(417,6,'2015-02-27 15:42:26','resource_update','modResource','14'),
	(418,6,'2015-02-27 16:05:46','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(419,6,'2015-02-27 16:12:41','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(420,6,'2015-02-27 16:12:42','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(421,6,'2015-02-27 16:13:39','resource_update','modResource','7'),
	(422,2,'2015-02-28 10:15:12','user_update','modUser','4'),
	(423,2,'2015-02-28 10:15:41','user_update','modUser','2'),
	(424,2,'2015-02-28 10:21:53','policy_update','modAccessPolicy','14'),
	(425,6,'2015-03-02 10:36:19','resource_update','modResource','2'),
	(426,6,'2015-03-02 11:25:14','resource_update','modResource','7'),
	(427,6,'2015-03-02 11:26:07','resource_update','modResource','8'),
	(428,6,'2015-03-02 11:26:51','resource_update','modResource','9'),
	(429,6,'2015-03-02 11:27:44','resource_update','modResource','27'),
	(430,6,'2015-03-02 11:42:52','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(431,6,'2015-03-02 11:43:03','resource_update','modResource','1'),
	(432,6,'2015-03-02 11:43:24','resource_update','modResource','1'),
	(433,6,'2015-03-02 13:14:06','resource_update','modResource','8'),
	(434,6,'2015-03-02 15:16:36','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(435,6,'2015-03-02 15:17:28','resource_update','modResource','14'),
	(436,6,'2015-03-02 15:18:03','resource_update','modResource','14'),
	(437,6,'2015-03-02 15:18:55','resource_update','modResource','14'),
	(438,6,'2015-03-02 15:19:32','resource_update','modResource','14'),
	(439,6,'2015-03-02 15:20:06','resource_update','modResource','14'),
	(440,6,'2015-03-02 15:36:03','resource_update','modResource','14'),
	(441,4,'2015-03-02 15:54:24','resource_update','modResource','7'),
	(442,4,'2015-03-02 15:54:51','resource_update','modResource','8'),
	(443,4,'2015-03-02 15:55:10','resource_update','modResource','9'),
	(444,4,'2015-03-02 15:55:29','resource_update','modResource','10'),
	(445,4,'2015-03-02 15:55:45','resource_update','modResource','27'),
	(446,6,'2015-03-03 09:53:52','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(447,6,'2015-03-03 09:54:08','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(448,6,'2015-03-03 09:54:25','resource_update','modResource','7'),
	(449,6,'2015-03-03 09:55:51','resource_update','modResource','8'),
	(450,6,'2015-03-03 09:56:47','resource_update','modResource','10'),
	(451,6,'2015-03-03 10:00:28','resource_update','modResource','10'),
	(452,6,'2015-03-03 10:21:52','resource_update','modResource','8'),
	(453,6,'2015-03-03 10:22:18','resource_update','modResource','8'),
	(454,6,'2015-03-03 13:58:02','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(455,6,'2015-03-03 13:58:07','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(456,6,'2015-03-03 13:59:16','resource_update','modResource','2'),
	(457,6,'2015-03-03 13:59:43','resource_update','modResource','2'),
	(458,6,'2015-03-03 14:00:13','resource_update','modResource','2'),
	(459,6,'2015-03-03 14:01:41','resource_update','modResource','2'),
	(460,6,'2015-03-03 14:02:06','resource_update','modResource','2'),
	(461,6,'2015-03-03 14:02:40','resource_update','modResource','2'),
	(462,6,'2015-03-03 14:04:53','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(463,6,'2015-03-03 14:05:24','resource_update','modResource','8'),
	(464,6,'2015-03-03 14:09:19','resource_update','modResource','7'),
	(465,6,'2015-03-03 14:09:46','resource_update','modResource','7'),
	(466,6,'2015-03-03 14:13:57','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/griptitefoundationrepair.com/web/client-files/images/'),
	(467,6,'2015-03-03 14:14:42','resource_update','modResource','27'),
	(468,6,'2015-03-03 14:15:12','resource_update','modResource','27'),
	(469,6,'2015-03-03 14:15:45','resource_update','modResource','27'),
	(470,6,'2015-03-03 14:16:41','resource_update','modResource','27'),
	(471,6,'2015-03-03 14:17:21','resource_update','modResource','27'),
	(472,6,'2015-03-03 14:17:52','resource_update','modResource','27'),
	(473,2,'2015-03-04 08:04:50','chunk_update','modChunk','23'),
	(474,2,'2015-03-04 08:08:41','chunk_update','modChunk','9'),
	(475,2,'2015-03-04 08:18:35','setting_update','modSystemSetting','primary_email'),
	(476,2,'2015-03-04 11:45:50','chunk_update','modChunk','23'),
	(477,2,'2015-03-04 11:47:22','chunk_update','modChunk','9'),
	(478,2,'2015-03-04 11:49:34','setting_update','modSystemSetting','primary_email'),
	(479,2,'2015-03-04 12:02:26','chunk_update','modChunk','9'),
	(480,2,'2015-03-04 12:03:33','chunk_update','modChunk','9');

/*!40000 ALTER TABLE `modx_manager_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_media_sources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_media_sources`;

CREATE TABLE `modx_media_sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `class_key` varchar(100) NOT NULL DEFAULT 'sources.modFileMediaSource',
  `properties` mediumtext,
  `is_stream` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `class_key` (`class_key`),
  KEY `is_stream` (`is_stream`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_media_sources` WRITE;
/*!40000 ALTER TABLE `modx_media_sources` DISABLE KEYS */;

INSERT INTO `modx_media_sources` (`id`, `name`, `description`, `class_key`, `properties`, `is_stream`)
VALUES
	(1,'Filesystem','','sources.modFileMediaSource','a:0:{}',1),
	(2,'Assets','SubAdmin Media Access','sources.modFileMediaSource','a:2:{s:8:\"basePath\";a:6:{s:4:\"name\";s:8:\"basePath\";s:4:\"desc\";s:23:\"prop_file.basePath_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:13:\"assets/media/\";s:7:\"lexicon\";s:11:\"core:source\";}s:7:\"baseUrl\";a:6:{s:4:\"name\";s:7:\"baseUrl\";s:4:\"desc\";s:22:\"prop_file.baseUrl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:13:\"assets/media/\";s:7:\"lexicon\";s:11:\"core:source\";}}',1),
	(3,'Client Files','','sources.modFileMediaSource','a:2:{s:8:\"basePath\";a:6:{s:4:\"name\";s:8:\"basePath\";s:4:\"desc\";s:23:\"prop_file.basePath_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:13:\"client-files/\";s:7:\"lexicon\";s:11:\"core:source\";}s:7:\"baseUrl\";a:6:{s:4:\"name\";s:7:\"baseUrl\";s:4:\"desc\";s:22:\"prop_file.baseUrl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:13:\"client-files/\";s:7:\"lexicon\";s:11:\"core:source\";}}',1),
	(4,'Client','','sources.modFileMediaSource','a:2:{s:8:\"basePath\";a:6:{s:4:\"name\";s:8:\"basePath\";s:4:\"desc\";s:23:\"prop_file.basePath_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:13:\"client-files/\";s:7:\"lexicon\";s:11:\"core:source\";}s:7:\"baseUrl\";a:6:{s:4:\"name\";s:7:\"baseUrl\";s:4:\"desc\";s:22:\"prop_file.baseUrl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:13:\"client-files/\";s:7:\"lexicon\";s:11:\"core:source\";}}',1),
	(5,'client-images','','sources.modFileMediaSource',NULL,1),
	(6,'client-test','','sources.modFileMediaSource','a:2:{s:8:\"basePath\";a:6:{s:4:\"name\";s:8:\"basePath\";s:4:\"desc\";s:23:\"prop_file.basePath_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:13:\"client-files/\";s:7:\"lexicon\";s:11:\"core:source\";}s:7:\"baseUrl\";a:6:{s:4:\"name\";s:7:\"baseUrl\";s:4:\"desc\";s:22:\"prop_file.baseUrl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:13:\"client-files/\";s:7:\"lexicon\";s:11:\"core:source\";}}',1);

/*!40000 ALTER TABLE `modx_media_sources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_media_sources_contexts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_media_sources_contexts`;

CREATE TABLE `modx_media_sources_contexts` (
  `source` int(11) NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  PRIMARY KEY (`source`,`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_media_sources_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_media_sources_elements`;

CREATE TABLE `modx_media_sources_elements` (
  `source` int(11) unsigned NOT NULL DEFAULT '0',
  `object_class` varchar(100) NOT NULL DEFAULT 'modTemplateVar',
  `object` int(11) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  PRIMARY KEY (`source`,`object`,`object_class`,`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_media_sources_elements` WRITE;
/*!40000 ALTER TABLE `modx_media_sources_elements` DISABLE KEYS */;

INSERT INTO `modx_media_sources_elements` (`source`, `object_class`, `object`, `context_key`)
VALUES
	(2,'modTemplateVar',1,'web'),
	(2,'modTemplateVar',2,'web'),
	(2,'modTemplateVar',4,'web'),
	(2,'modTemplateVar',5,'web'),
	(2,'modTemplateVar',7,'web'),
	(2,'modTemplateVar',8,'web'),
	(6,'modTemplateVar',3,'web'),
	(6,'modTemplateVar',6,'web');

/*!40000 ALTER TABLE `modx_media_sources_elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_member_groups`;

CREATE TABLE `modx_member_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_group` int(10) unsigned NOT NULL DEFAULT '0',
  `member` int(10) unsigned NOT NULL DEFAULT '0',
  `role` int(10) unsigned NOT NULL DEFAULT '1',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_member_groups` WRITE;
/*!40000 ALTER TABLE `modx_member_groups` DISABLE KEYS */;

INSERT INTO `modx_member_groups` (`id`, `user_group`, `member`, `role`, `rank`)
VALUES
	(18,1,4,2,0),
	(16,5,5,4,1),
	(17,5,6,2,0);

/*!40000 ALTER TABLE `modx_member_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_membergroup_names
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_membergroup_names`;

CREATE TABLE `modx_membergroup_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `parent` int(10) unsigned NOT NULL DEFAULT '0',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  `dashboard` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent` (`parent`),
  KEY `rank` (`rank`),
  KEY `dashboard` (`dashboard`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_membergroup_names` WRITE;
/*!40000 ALTER TABLE `modx_membergroup_names` DISABLE KEYS */;

INSERT INTO `modx_membergroup_names` (`id`, `name`, `description`, `parent`, `rank`, `dashboard`)
VALUES
	(1,'Administrator',NULL,0,0,1),
	(4,'SubAdmins','',0,0,1),
	(5,'Client','',0,0,1);

/*!40000 ALTER TABLE `modx_membergroup_names` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_menus`;

CREATE TABLE `modx_menus` (
  `text` varchar(255) NOT NULL DEFAULT '',
  `parent` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `menuindex` int(11) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `handler` text NOT NULL,
  `permissions` text NOT NULL,
  `namespace` varchar(100) NOT NULL DEFAULT 'core',
  PRIMARY KEY (`text`),
  KEY `parent` (`parent`),
  KEY `action` (`action`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_menus` WRITE;
/*!40000 ALTER TABLE `modx_menus` DISABLE KEYS */;

INSERT INTO `modx_menus` (`text`, `parent`, `action`, `description`, `icon`, `menuindex`, `params`, `handler`, `permissions`, `namespace`)
VALUES
	('dashboard','','0','','images/misc/logo_tbar.gif',0,'','MODx.loadPage(\"\"); return false;','home','core'),
	('dashboards','admin','system/dashboards','dashboards_desc','',2,'','','dashboards','core'),
	('site','topnav','','','',0,'','','menu_site','core'),
	('preview','site','','preview_desc','',4,'','MODx.preview(); return false;','','core'),
	('refresh_site','manage','','refresh_site_desc','',1,'','MODx.clearCache(); return false;','empty_cache','core'),
	('remove_locks','manage','','remove_locks_desc','',2,'','\nMODx.msg.confirm({\n    title: _(\'remove_locks\')\n    ,text: _(\'confirm_remove_locks\')\n    ,url: MODx.config.connectors_url\n    ,params: {\n        action: \'system/remove_locks\'\n    }\n    ,listeners: {\n        \'success\': {fn:function() {\n            var tree = Ext.getCmp(\"modx-resource-tree\");\n            if (tree && tree.rendered) {\n                tree.refresh();\n            }\n         },scope:this}\n    }\n});','remove_locks','core'),
	('search','site','54','search_desc','images/icons/context_view.gif',3,'','','search','core'),
	('installer','components','workspaces','installer_desc','',0,'','','packages','core'),
	('topnav','','','topnav_desc','',0,'','','','core'),
	('new_resource','site','resource/create','new_resource_desc','',0,'','','new_document','core'),
	('media','topnav','','media_desc','',1,'','','file_manager','core'),
	('file_browser','media','media/browser','file_browser_desc','',0,'','','file_manager','core'),
	('logout','user','','logout_desc','',2,'','MODx.logout(); return false;','logout','core'),
	('components','topnav','','','',2,'','','components','core'),
	('security','','0','','images/icons/lock.gif',3,'','','menu_security','core'),
	('user_management','security','56','user_management_desc','images/icons/user.gif',0,'','','view_user','core'),
	('user_group_management','security','57','user_group_management_desc','images/icons/mnu_users.gif',1,'','','access_permissions','core'),
	('resource_groups','site','security/resourcegroup','resource_groups_desc','',7,'','','access_permissions','core'),
	('form_customization','security','59','form_customization_desc','images/misc/logo_tbar.gif',3,'','','customize_forms','core'),
	('flush_access','manage','','flush_access_desc','',3,'','MODx.msg.confirm({\n    title: _(\'flush_access\')\n    ,text: _(\'flush_access_confirm\')\n    ,url: MODx.config.connector_url\n    ,params: {\n        action: \'security/access/flush\'\n    }\n    ,listeners: {\n        \'success\': {fn:function() { location.href = \'./\'; },scope:this}\n    }\n});','access_permissions','core'),
	('flush_sessions','manage','','flush_sessions_desc','',4,'','MODx.msg.confirm({\n    title: _(\'flush_sessions\')\n    ,text: _(\'flush_sessions_confirm\')\n    ,url: MODx.config.connector_url\n    ,params: {\n        action: \'security/flush\'\n    }\n    ,listeners: {\n        \'success\': {fn:function() { location.href = \'./\'; },scope:this}\n    }\n});','flush_sessions','core'),
	('tools','','0','','images/icons/menu_settings.gif',4,'','','menu_tools','core'),
	('import_resources','site','system/import','import_resources_desc','',6,'','','import_static','core'),
	('import_site','site','system/import/html','import_site_desc','',5,'','','import_static','core'),
	('propertysets','admin','element/propertyset','propertysets_desc','',6,'','','property_sets','core'),
	('sources','media','source','sources_desc','',1,'','','sources','core'),
	('reports','manage','','reports_desc','',5,'','','menu_reports','core'),
	('site_schedule','reports','resource/site_schedule','site_schedule_desc','',0,'','','view_document','core'),
	('view_logging','reports','system/logs','view_logging_desc','',1,'','','logs','core'),
	('eventlog_viewer','reports','system/event','eventlog_viewer_desc','',2,'','','view_eventlog','core'),
	('view_sysinfo','reports','system/info','view_sysinfo_desc','',3,'','','view_sysinfo','core'),
	('about','usernav','help','','<i class=\"icon-question-circle icon icon-large\"></i>',7,'','','help','core'),
	('system','','0','','images/misc/logo_tbar.gif',6,'','','menu_system','core'),
	('manage_workspaces','system','69','manage_workspaces_desc','images/icons/sysinfo.gif',0,'','','packages','core'),
	('system_settings','admin','system/settings','system_settings_desc','',0,'','','settings','core'),
	('lexicon_management','admin','workspaces/lexicon','lexicon_management_desc','',7,'','','lexicons','core'),
	('content_types','site','system/contenttype','content_types_desc','',8,'','','content_types','core'),
	('contexts','admin','context','contexts_desc','',3,'','','view_contexts','core'),
	('edit_menu','admin','system/action','edit_menu_desc','',4,'','','actions','core'),
	('namespaces','admin','workspaces/namespace','namespaces_desc','',8,'','','namespaces','core'),
	('user','usernav','','','<span id=\"user-avatar\">{$userImage}</span> <span id=\"user-username\">{$username}</span>',5,'','','menu_user','core'),
	('profile','user','security/profile','profile_desc','',0,'','','change_profile','core'),
	('messages','user','security/message','messages_desc','',1,'','','messages','core'),
	('support','','0','support_desc','images/icons/sysinfo.gif',8,'','','menu_support','core'),
	('forums','support','0','forums_desc','images/icons/sysinfo.gif',0,'','window.open(\"http://modx.com/forums\");','','core'),
	('wiki','support','0','wiki_desc','images/icons/sysinfo.gif',1,'','window.open(\"http://rtfm.modx.com/\");','','core'),
	('jira','support','0','jira_desc','images/icons/sysinfo.gif',2,'','window.open(\"http://bugs.modx.com/projects/revo/issues\");','','core'),
	('api_docs','support','0','api_docs_desc','images/icons/sysinfo.gif',3,'','window.open(\"http://api.modx.com/revolution/2.2/\");','','core'),
	('contactsubmissions','components','78','contactsubmissions.desc','',0,'','','','core'),
	('Help','','79','Help','',9,'','','','core'),
	('manage','topnav','','','',3,'','','menu_tools','core'),
	('users','manage','security/user','user_management_desc','',0,'','','view_user','core'),
	('refreshuris','refresh_site','','refreshuris_desc','',0,'','MODx.refreshURIs(); return false;','empty_cache','core'),
	('usernav','','','usernav_desc','',0,'','','','core'),
	('admin','usernav','','','<i class=\"icon-gear icon icon-large\"></i>',6,'','','settings','core'),
	('bespoke_manager','admin','security/forms','bespoke_manager_desc','',1,'','','customize_forms','core'),
	('acls','admin','security/permission','acls_desc','',5,'','','access_permissions','core'),
	('migx','components','index','','',0,'&configs=packagemanager||migxconfigs||setup','','','migx');

/*!40000 ALTER TABLE `modx_menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_migx_config_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_migx_config_elements`;

CREATE TABLE `modx_migx_config_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_id` int(10) NOT NULL DEFAULT '0',
  `element_id` int(10) NOT NULL DEFAULT '0',
  `rank` int(10) NOT NULL DEFAULT '0',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL,
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deletedon` datetime NOT NULL,
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `publishedon` datetime NOT NULL,
  `publishedby` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_migx_configs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_migx_configs`;

CREATE TABLE `modx_migx_configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `formtabs` text NOT NULL,
  `contextmenus` text NOT NULL,
  `actionbuttons` text NOT NULL,
  `columnbuttons` text NOT NULL,
  `filters` text NOT NULL,
  `extended` text NOT NULL,
  `columns` text NOT NULL,
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL,
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deletedon` datetime NOT NULL,
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `publishedon` datetime NOT NULL,
  `publishedby` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_migx_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_migx_elements`;

CREATE TABLE `modx_migx_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL,
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deletedon` datetime NOT NULL,
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `publishedon` datetime NOT NULL,
  `publishedby` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_migx_formtab_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_migx_formtab_fields`;

CREATE TABLE `modx_migx_formtab_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_id` int(10) NOT NULL DEFAULT '0',
  `formtab_id` int(10) NOT NULL DEFAULT '0',
  `field` varchar(255) NOT NULL DEFAULT '',
  `caption` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `pos` int(10) NOT NULL DEFAULT '0',
  `description_is_code` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `inputTV` varchar(255) NOT NULL DEFAULT '',
  `inputTVtype` varchar(255) NOT NULL DEFAULT '',
  `validation` text NOT NULL,
  `configs` varchar(255) NOT NULL DEFAULT '',
  `restrictive_condition` text NOT NULL,
  `display` varchar(255) NOT NULL DEFAULT '',
  `sourceFrom` varchar(255) NOT NULL DEFAULT '',
  `sources` varchar(255) NOT NULL DEFAULT '',
  `inputOptionValues` text NOT NULL,
  `default` text NOT NULL,
  `extended` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `config_id` (`config_id`),
  KEY `formtab_id` (`formtab_id`),
  KEY `field` (`field`),
  KEY `pos` (`pos`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_migx_formtabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_migx_formtabs`;

CREATE TABLE `modx_migx_formtabs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_id` int(10) NOT NULL DEFAULT '0',
  `caption` varchar(255) NOT NULL DEFAULT '',
  `pos` int(10) NOT NULL DEFAULT '0',
  `print_before_tabs` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `extended` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `config_id` (`config_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_namespaces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_namespaces`;

CREATE TABLE `modx_namespaces` (
  `name` varchar(40) NOT NULL DEFAULT '',
  `path` text,
  `assets_path` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_namespaces` WRITE;
/*!40000 ALTER TABLE `modx_namespaces` DISABLE KEYS */;

INSERT INTO `modx_namespaces` (`name`, `path`, `assets_path`)
VALUES
	('core','{core_path}','{assets_path}'),
	('breadcrumbs','{core_path}components/breadcrumbs/',''),
	('tinymce','{core_path}components/tinymce/',NULL),
	('wayfinder','{core_path}components/wayfinder/',''),
	('ultimateparent','{core_path}components/ultimateparent/',''),
	('Help','{core_path}components/help/','{assets_path}components/help/'),
	('contactsubmissions','{core_path}components/contactsubmissions/','{assets_path}/components/contactsubmissions/'),
	('codemirror','{core_path}components/codemirror/',''),
	('formit','{core_path}components/formit/',NULL),
	('migx','{core_path}components/migx/','{assets_path}components/migx/');

/*!40000 ALTER TABLE `modx_namespaces` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_property_set
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_property_set`;

CREATE TABLE `modx_property_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `category` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `properties` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_register_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_register_messages`;

CREATE TABLE `modx_register_messages` (
  `topic` int(10) unsigned NOT NULL,
  `id` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `valid` datetime NOT NULL,
  `accessed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `accesses` int(10) unsigned NOT NULL DEFAULT '0',
  `expires` int(20) NOT NULL DEFAULT '0',
  `payload` mediumtext NOT NULL,
  `kill` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`topic`,`id`),
  KEY `created` (`created`),
  KEY `valid` (`valid`),
  KEY `accessed` (`accessed`),
  KEY `accesses` (`accesses`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_register_queues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_register_queues`;

CREATE TABLE `modx_register_queues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `options` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_register_queues` WRITE;
/*!40000 ALTER TABLE `modx_register_queues` DISABLE KEYS */;

INSERT INTO `modx_register_queues` (`id`, `name`, `options`)
VALUES
	(1,'locks','a:1:{s:9:\"directory\";s:5:\"locks\";}'),
	(2,'resource_reload','a:1:{s:9:\"directory\";s:15:\"resource_reload\";}');

/*!40000 ALTER TABLE `modx_register_queues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_register_topics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_register_topics`;

CREATE TABLE `modx_register_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `queue` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `options` mediumtext,
  PRIMARY KEY (`id`),
  KEY `queue` (`queue`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_register_topics` WRITE;
/*!40000 ALTER TABLE `modx_register_topics` DISABLE KEYS */;

INSERT INTO `modx_register_topics` (`id`, `queue`, `name`, `created`, `updated`, `options`)
VALUES
	(1,1,'/resource/','2013-10-01 06:37:19',NULL,NULL),
	(2,2,'/resourcereload/','2013-11-07 17:40:45',NULL,NULL);

/*!40000 ALTER TABLE `modx_register_topics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_session`;

CREATE TABLE `modx_session` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `access` int(20) unsigned NOT NULL,
  `data` mediumtext,
  PRIMARY KEY (`id`),
  KEY `access` (`access`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_session` WRITE;
/*!40000 ALTER TABLE `modx_session` DISABLE KEYS */;

INSERT INTO `modx_session` (`id`, `access`, `data`)
VALUES
	('sappdrrbstl1vhfj6o3q1bipo0',1425134379,'modx.user.contextTokens|a:0:{}'),
	('v42e07fjvfv69fmi3qqlmccc37',1425135306,'modx.user.contextTokens|a:0:{}'),
	('cf6l4ehsidf9f9uetl5ml48hc5',1425135441,'modx.user.contextTokens|a:0:{}'),
	('rig7b8e3nb9ibfjtj0qumql274',1425136021,'modx.user.contextTokens|a:0:{}'),
	('43f5rnjcd4u276earh1gu38it6',1425229139,'modx.user.contextTokens|a:0:{}'),
	('ouucmgpqr3nht92b7bcmjnrgf3',1425229704,'modx.user.contextTokens|a:0:{}'),
	('778uhqrkqkka25mrv90ueijq45',1425229819,'modx.user.contextTokens|a:0:{}'),
	('6qncjaigg64974q87kj1id06o6',1425230284,'modx.user.contextTokens|a:0:{}'),
	('6t2hbq97bm1tk1f8qegll4vc57',1425230295,'modx.user.contextTokens|a:0:{}'),
	('801pmq1cl93hpotq0qm7vtkah5',1425230351,'modx.user.contextTokens|a:0:{}'),
	('evgh4u9l9f4tjq39t1ho0tsq31',1425231647,'modx.user.contextTokens|a:0:{}'),
	('7htjod5l5e33cn02gqvo4fl161',1425231936,'modx.user.contextTokens|a:0:{}'),
	('7cqrga5b0pjck5m3b6l0ngdct3',1425231937,'modx.user.contextTokens|a:0:{}'),
	('is26rsudrf69kjcpfbi7bpd2u5',1425066034,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('m1fjp1lrpt1ovqh6v2bnai6u25',1425232169,'modx.user.contextTokens|a:0:{}'),
	('i1d94rgsrjg6noa7b7o1sakij3',1425233864,'modx.user.contextTokens|a:0:{}'),
	('mt0k7to9mr6jddisu6112litu4',1425234007,'modx.user.contextTokens|a:0:{}'),
	('10n8q00vnpi3r13ju0rk6apsf5',1425235780,'modx.user.contextTokens|a:0:{}'),
	('gb2bcds1p8n4hefu85duokrhi2',1425235790,'modx.user.contextTokens|a:0:{}'),
	('6924muue9b2ct16ha64vkre1v5',1425236398,'modx.user.contextTokens|a:0:{}'),
	('rv23vb1ttq5qmsirosrttausb6',1425066651,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('geolfkclc5mo5vr9i8e1fnrem1',1425313801,'modx.user.contextTokens|a:0:{}'),
	('9kn86g6gea9ffdolfq88e7h1o3',1425314119,'modx.user.contextTokens|a:0:{}'),
	('1utdgetgnr8e1p9knskrfdm704',1425067570,'modx.user.contextTokens|a:0:{}'),
	('ek0ggl1uqqkbt6sjltoj3qbgd3',1425068014,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('br86tr8j5knjnn3igqfaipcem1',1425313448,'modx.user.contextTokens|a:0:{}'),
	('04gksobgvspemfhqbr3p8v7s91',1425069136,'modx.user.contextTokens|a:0:{}'),
	('e3d0nnljni2kjr81pu79r8fhl4',1425070326,'modx.user.contextTokens|a:0:{}'),
	('nmn3ghp02oubsl3g3kueq9qcp5',1425076155,'modx.user.contextTokens|a:0:{}'),
	('tiku1jdjicrgoa5r6bklvompb4',1425076155,'modx.user.contextTokens|a:0:{}'),
	('kb0g32gujma2fd76vb18ue8aa0',1425076156,'modx.user.contextTokens|a:0:{}'),
	('e9udm5mipoalbbqgu650006gg6',1425076910,'modx.user.contextTokens|a:0:{}'),
	('k6s60nejdnr0k9vpi5j1hia0d4',1425081159,'modx.user.contextTokens|a:0:{}'),
	('5sf6h9hdgf766o3crotog67b90',1425081307,'modx.user.contextTokens|a:0:{}'),
	('fo5oolhs85b1uda23kjldja114',1425081307,'modx.user.contextTokens|a:0:{}'),
	('0e4ml41tv36qhb5ebrlm1gg301',1425084319,'modx.user.contextTokens|a:0:{}'),
	('1s586gih2qviqecapml17671p3',1425084319,'modx.user.contextTokens|a:0:{}'),
	('qjqui8ha9iikl2k04qc82ehch5',1425085113,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('m312jbqkpjs7na92que78i7ea0',1425091366,'modx.user.contextTokens|a:0:{}'),
	('vt4q53v3tdkdrf2teeq0po9ai2',1425092058,'modx.user.contextTokens|a:0:{}'),
	('caeo8jquhnk7aiv72n4enbsbo6',1425092070,'modx.user.contextTokens|a:0:{}'),
	('1tljh4ipdfu8476kvrcs92vg41',1425092070,'modx.user.contextTokens|a:0:{}'),
	('g4jijtm4bugitq2cm493rvr5q1',1425094532,'modx.user.contextTokens|a:0:{}'),
	('rfjap39tbis3rb3s8eiemelv84',1425094914,'modx.user.contextTokens|a:0:{}'),
	('lmqk2p4dl9ut9feu3g6c5gipa6',1425094915,'modx.user.contextTokens|a:0:{}'),
	('dsa45togkkbv6ck25pbl3n1bt5',1425094991,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('bkbcga5p75seronmi5rmcp4sv5',1425096342,'modx.user.contextTokens|a:0:{}'),
	('g41lc5o8rp8hplvqu6kr5dkp01',1425096684,'modx.user.contextTokens|a:0:{}'),
	('8g4d81l51ob622qb6dep1mbv62',1425096685,'modx.user.contextTokens|a:0:{}'),
	('bo9tdrr18m61s2r606cniah4d1',1425096686,'modx.user.contextTokens|a:0:{}'),
	('1cubpev7cb8m7v04trmqq8p0o2',1425096687,'modx.user.contextTokens|a:0:{}'),
	('vius0nl60bjc1t4notums0qmp3',1425096718,'modx.user.contextTokens|a:0:{}'),
	('cs8cbe3o6tcmqtagi69u9sqpi6',1425096719,'modx.user.contextTokens|a:0:{}'),
	('cc4bdon41ndksm1pjgckgdnif1',1425096719,'modx.user.contextTokens|a:0:{}'),
	('lmt6erfnrng8moanjtc8bt6jb1',1425096720,'modx.user.contextTokens|a:0:{}'),
	('gjdkr233q8tqr42ns7p3ttgb73',1425098992,'modx.user.contextTokens|a:0:{}'),
	('3aohq5tac5k7f4nus6b0m92d15',1425099963,'modx.user.contextTokens|a:0:{}'),
	('nagciqnlhk5h35bvfib8igo646',1425100206,'modx.user.contextTokens|a:0:{}'),
	('o7fot8qj0v371flnnv10551kh6',1425100206,'modx.user.contextTokens|a:0:{}'),
	('j3e3tpukkn47nnqha7nmni48k2',1425101677,'modx.user.contextTokens|a:0:{}'),
	('agvkm862ussnvd87f6rv56id90',1425103852,'modx.user.contextTokens|a:0:{}'),
	('qg6u9bkgrodjgvenserkrvnl50',1425103936,'modx.user.contextTokens|a:0:{}'),
	('df675ecf4590c6slt6cnp5d296',1425103968,'modx.user.contextTokens|a:0:{}'),
	('du917mptbja4b66vm23l1dbb47',1425107124,'modx.user.contextTokens|a:0:{}'),
	('dk2jvrn1jrieq1mggmfmiss2e1',1425107124,'modx.user.contextTokens|a:0:{}'),
	('g20n5fdgsq7f1t1qedjh0d1qf3',1425109116,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('dc7oqp4t2ttndj94dg64omcov4',1425109349,'modx.user.contextTokens|a:0:{}'),
	('3bsiocpuf8if7og4b9ig39a5a1',1425110140,'modx.user.contextTokens|a:0:{}'),
	('t0fu5nufmrrd48sqqlfp1vvpb4',1425111544,'modx.user.contextTokens|a:0:{}'),
	('3ubn9sthvnbejbke8rsr2i9ei3',1425113733,'modx.user.contextTokens|a:0:{}'),
	('731injnjrbnjs83ig1vmufug84',1425114434,'modx.user.contextTokens|a:0:{}'),
	('kf3nmnuov9p2kksvm4n9l005d3',1425120043,'modx.user.contextTokens|a:0:{}'),
	('kd7pvqi7oh45rl7ltu28ptmt60',1425124260,'modx.user.contextTokens|a:0:{}'),
	('8fq69nshbbjnujav77qd9ct9d2',1425127112,'modx.user.contextTokens|a:0:{}'),
	('4k12hk37r135euhr5p6cfoqks6',1425127114,'modx.user.contextTokens|a:0:{}'),
	('h6tlf733ndpdbpop01i8v9dso6',1425127972,'modx.user.contextTokens|a:0:{}'),
	('m3c5su15bc508rhddaigv0gqi7',1425129445,'modx.user.contextTokens|a:0:{}'),
	('75n6dflog8nitqfm84uqg02h14',1425129641,'modx.user.contextTokens|a:0:{}'),
	('rl3suvnhlbjanh84u6qpg0mi82',1425129953,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('f3d44prb8mvh8lpofh7m8gh9v6',1425130136,'modx.user.contextTokens|a:0:{}'),
	('28nfbqkbudcb40k3sbnqttbm92',1425131480,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('nneahagj7nsnj6t4vcaus424h0',1425131701,'modx.user.contextTokens|a:0:{}'),
	('fn6hsmccmg9dqn0cb1m721r563',1425132416,'modx.user.contextTokens|a:0:{}'),
	('ntsqjecs70lr8gp9a8q91hm3c7',1425134236,'modx.user.contextTokens|a:0:{}'),
	('j6q9i1i0q9cohksdedvrmakse0',1425227698,'modx.user.contextTokens|a:0:{}'),
	('d9vimk74jb9oafk0osesl3orm5',1425160101,'modx.user.contextTokens|a:1:{s:3:\"mgr\";i:2;}modx.mgr.user.token|s:52:\"modx54d39f5c13a564.78288592_254f2376b488390.46799647\";modx.mgr.session.cookie.lifetime|i:604800;modx.mgr.user.config|a:0:{}newResourceTokens|a:1:{i:0;s:23:\"54f237a4d4c7a5.89945583\";}'),
	('5bprtgd1ck87k15s68hu80kpc3',1425160596,'modx.user.contextTokens|a:0:{}'),
	('3evua1ji6dgd3i69o3tm1q99c7',1425161682,''),
	('dlc24uqd36kp16ak4hka5fdjm3',1425161682,'modx.user.contextTokens|a:0:{}'),
	('f00sak4s4bhv73l6jp6gn8i211',1425163269,'modx.user.contextTokens|a:0:{}'),
	('nk50raqmapp5empnp2ggiipa83',1425165245,'modx.user.contextTokens|a:0:{}'),
	('jivmv5fjf6o9h5nrhh40mqv5d0',1425165246,'modx.user.contextTokens|a:0:{}'),
	('qt9d5f8549v0htpjtdpq8d3nh6',1425165247,'modx.user.contextTokens|a:0:{}'),
	('oqaudhqcqt5cg1p1ip1qtohal2',1425165247,'modx.user.contextTokens|a:0:{}'),
	('8l4uc4drusrtod0q9ojttmn7n2',1425165248,'modx.user.contextTokens|a:0:{}'),
	('ietl6hs0jqgovr29he0f3dhge0',1425165248,'modx.user.contextTokens|a:0:{}'),
	('l2a3adsv8utae6on5nnlndqgd6',1425165267,'modx.user.contextTokens|a:0:{}'),
	('56hnfd5rjqrlhirdo9a0rh7uk2',1425166266,'modx.user.contextTokens|a:0:{}'),
	('oft4nn57ktea8ad8afrd9csm63',1425166505,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ipg72epjtulenvvb0nrpj71tq0',1425167571,'modx.user.contextTokens|a:0:{}'),
	('4he4joepjkukbar1a7j597m3d0',1425167574,'modx.user.contextTokens|a:0:{}'),
	('3fck0ja1f0a54q14iad4pae6a7',1425167856,'modx.user.contextTokens|a:0:{}'),
	('qn2ckr0t6emgfbc9k6o431kq30',1425168312,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('k5ehnihqr7kgv4v3nrv6oo8r00',1425170443,'modx.user.contextTokens|a:0:{}'),
	('gmqem69fvvv5j8g0m44nsk5ek6',1425171310,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('9j2dndp5887f3t51stjaoe4634',1425171543,''),
	('o7qiq2pba3vnvmm9clp94c9oh3',1425171545,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('e08q14hfd3ks7bbvnv5jjmv3d1',1425171840,'modx.user.contextTokens|a:0:{}'),
	('h4q1fc0hkft3savm2e5rmmpre3',1425171983,'modx.user.contextTokens|a:0:{}'),
	('sq2qbuskgb0omkuekam3hf9f50',1425172236,'modx.user.contextTokens|a:0:{}'),
	('fr6v88qko3h0bqene57c9m0ae0',1425172236,'modx.user.contextTokens|a:0:{}'),
	('0fb1n5l14cbni9e8cjh3juava2',1425172238,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('fvg4fd59m9j78iemkm6p6m0c76',1425173063,'modx.user.contextTokens|a:0:{}'),
	('vv4dbnq96f99qvt54snq2hbcj7',1425175774,'modx.user.contextTokens|a:0:{}'),
	('b75vn2286b5pcv6p2j3pg3chk5',1425180544,'modx.user.contextTokens|a:0:{}'),
	('3jnrf6bkrjpf7g85rk0umnbka3',1425181195,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ikkpf8e8jr7kblf6r6bfesf6f5',1425181288,'modx.user.contextTokens|a:0:{}'),
	('jnijo3vbgv0n4bh6njk2uvn3e2',1425181330,'modx.user.contextTokens|a:0:{}'),
	('rerqqp3nahenpm10k065u68bm1',1425181337,'modx.user.contextTokens|a:0:{}'),
	('78beon740jnn691s9gt9uaenp2',1425181340,'modx.user.contextTokens|a:0:{}'),
	('h79bnuipf015ig5d83m229jsv5',1425181586,'modx.user.contextTokens|a:0:{}'),
	('1h79v53tu08b6o23ptcuvnha64',1425181596,'modx.user.contextTokens|a:0:{}'),
	('drt77jnlmj5ljdi8spma5lgpg5',1425181597,'modx.user.contextTokens|a:0:{}'),
	('me5is4rfv2q87criounbqub935',1425183392,'modx.user.contextTokens|a:0:{}'),
	('igqbh5rg38smoeub36odhvrud1',1425183765,'modx.user.contextTokens|a:0:{}'),
	('2vuo2avo8ht1vsd5i81eghnvg1',1425183975,'modx.user.contextTokens|a:0:{}'),
	('mrlccleoluau1iljjol5rlc880',1425183977,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('klg46te6it6fji22plfakfleh5',1425184819,''),
	('r46nktjjb18efr2ja1o4ne7916',1425184820,'modx.user.contextTokens|a:0:{}'),
	('dvbs0quhho2pd34vbrcjsgrhh3',1425185823,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ojnoh7caqralmrsed2c9hqk672',1425188354,'modx.user.contextTokens|a:0:{}'),
	('vcttv220ne01nu8ct8hejsvah3',1425189091,'modx.user.contextTokens|a:0:{}'),
	('114ihtqm644g0udsgbqbah8oh4',1425190602,'modx.user.contextTokens|a:0:{}'),
	('or200eo3qjc3shaqunvo90vqg5',1425190633,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ha6v6necchje0j7eud9dtlsai4',1425192313,'modx.user.contextTokens|a:0:{}'),
	('6idl5tr84eeou0u01ri1a12f31',1425195864,'modx.user.contextTokens|a:0:{}'),
	('u1mveshm3vsqur0uqar98opv21',1425196103,'modx.user.contextTokens|a:0:{}'),
	('cht513lg71d08a6fd6634lt3o6',1425199062,'modx.user.contextTokens|a:0:{}'),
	('jio7r79lv1do4odiokho73ur25',1425199063,'modx.user.contextTokens|a:0:{}'),
	('ek486e0el93fusohoigi978no3',1425199364,'modx.user.contextTokens|a:0:{}'),
	('7a97dcgr2djjvf3f8bqv824286',1425200185,'modx.user.contextTokens|a:0:{}'),
	('iprmi8svb69legbrq0tse19uo2',1425201072,'modx.user.contextTokens|a:0:{}'),
	('gl08dls5en3oobt9v75fb8i0r0',1425201074,'modx.user.contextTokens|a:0:{}'),
	('hort3derbciu29leckcsqcb225',1425201074,'modx.user.contextTokens|a:0:{}'),
	('9653vaap5auootqvq8erhcsdf6',1425201104,'modx.user.contextTokens|a:0:{}'),
	('us93n2tvl9nefsa323bf32f287',1425203879,'modx.user.contextTokens|a:0:{}'),
	('b3lbf9mdol4mu3av4lp34fr157',1425204466,'modx.user.contextTokens|a:0:{}'),
	('a2f78m419vbi11v21ph1lunpj5',1425205014,'modx.user.contextTokens|a:0:{}'),
	('kk834aih5i4lilvq1oilch2cu7',1425206728,'modx.user.contextTokens|a:0:{}'),
	('9voagp9r1328tqntjqrdmeaa90',1425210032,'modx.user.contextTokens|a:0:{}'),
	('9arc62aasqic3dbqqohi0o1ph6',1425210239,'modx.user.contextTokens|a:0:{}'),
	('ucfvt9t1nc13dkv192thr27s54',1425211474,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('b1ub41g31ka41452rnef9bp900',1425062380,'modx.user.contextTokens|a:0:{}'),
	('bbd3nf9925piupbi9d0k7s26r5',1425477699,'modx.user.contextTokens|a:1:{s:3:\"mgr\";i:2;}modx.mgr.user.token|s:52:\"modx54d39f5c13a564.78288592_254f71042d8e267.51273694\";modx.mgr.session.cookie.lifetime|i:0;modx.mgr.user.config|a:0:{}'),
	('a82tcmgo76fgckil31jfq92l03',1425478545,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('8m81vvhthn22ktpqsvc204iai4',1425480116,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('rls0qlsni90brkjtpomrvm5a11',1425480915,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('em7l6pd5o9l10e0ssm40g1bqm7',1425481612,'modx.user.contextTokens|a:0:{}'),
	('mbsi116nea1j0bfui48g59ekj5',1425481613,'modx.user.contextTokens|a:0:{}'),
	('3vgrg9fphisv39gr62ji38qu72',1425481616,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('vlcufqn9hdt7l9fj7u6tr0pf47',1425481618,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('1hjf5mcjnapqf9fm9plfikja64',1425481620,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('6105bh8655j8pf88eoe89pgbv4',1425481620,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('a0q995gebtjrhe8vdrv5k0vr20',1425481623,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ib9c53di4ok18tesvgddgvlav3',1425481623,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('571jclqajun3nckt5pnrplpj55',1425481626,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('olrtu3h5jb6vav9loupror62u7',1425481626,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('6ti2i6mqvt900qbdcj0g4fuv50',1425481627,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('3sm4vr2da15pjf4vdcqcacp6v2',1425481627,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('liglcasu5ehn41a4mf268f9525',1425481629,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('1infcaqvp3kaqmgogu7qk1hpn5',1425481630,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('2itu1ekjceo9a717i9k0hi4ml1',1425481630,'modx.user.contextTokens|a:0:{}'),
	('u0hbrudcukanjp55fkggb3mho5',1425481632,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('5j5fgd978i8jf2ecb04j2hlf83',1425483140,'modx.user.contextTokens|a:0:{}'),
	('ugkgk7sldo538rpn0jfflnrc37',1425484033,'modx.user.contextTokens|a:0:{}'),
	('46h1dtcagrv5ohddm70qljm0l1',1425484505,'modx.user.contextTokens|a:0:{}'),
	('8k9ja5d3jrmku18qcgr8f9opt6',1425485034,'modx.user.contextTokens|a:0:{}'),
	('p4l49qchb6jlc3md38iu9lltr2',1425485034,'modx.user.contextTokens|a:0:{}'),
	('o9bsldc63k10gi1afe5064gfr6',1425485089,''),
	('3bgeat10c3vdpb1sml6n3rrbs2',1425487994,'modx.user.contextTokens|a:0:{}'),
	('ogqkm8q06bbl9v80edhqbgasd7',1425488322,'modx.user.contextTokens|a:0:{}'),
	('41djefglg8q4luhemmkmva99e5',1425488323,'modx.user.contextTokens|a:0:{}'),
	('v3qr22lkvo63imj0ikp4esv3j5',1425488323,'modx.user.contextTokens|a:0:{}'),
	('md3i5qa9gd9687uk6q7c3ettr6',1425488617,'modx.user.contextTokens|a:0:{}'),
	('cqtnim76um62g8imo2nl97ohs7',1425488712,'modx.user.contextTokens|a:0:{}'),
	('mn8buihdq3v3apvua2sgt3ib11',1425488712,'modx.user.contextTokens|a:0:{}'),
	('hdum4dstb57rq2qep7jsfin4q0',1425488713,'modx.user.contextTokens|a:0:{}'),
	('qbsj9g57cjdou5a10vlfsc6i92',1425489191,'modx.user.contextTokens|a:0:{}'),
	('bhh68nsb2un3dkd8cipa1eeco6',1425489292,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ehqi0n66evdhd9qe4ilmmsdos2',1425143488,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('62dvk2851mdl0tir15t8acukg7',1425070628,'modx.user.contextTokens|a:0:{}'),
	('deqrof29jhlr66v0c6tg57d293',1425071012,'modx.user.contextTokens|a:0:{}'),
	('fu374ds4atihf11qn7q8lk4qv6',1425071527,'modx.user.contextTokens|a:0:{}'),
	('rkembrfca3rjlps3q49sble276',1425211564,'modx.user.contextTokens|a:0:{}'),
	('d9n7bvvac7enc6mbg3a0k7t033',1425211679,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('jbgqsth16n5qruh2p26dg95a36',1424915249,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('ln30ij6384rtpvl9nmamr4h2s5',1425489355,'modx.user.contextTokens|a:0:{}'),
	('85ckecgs2tn71mav41bjn7f1c3',1425490010,'modx.user.contextTokens|a:0:{}'),
	('16l6i7ah0328dtihthtptkp126',1425490830,'modx.user.contextTokens|a:1:{s:3:\"mgr\";i:2;}modx.mgr.user.token|s:52:\"modx54d39f5c13a564.78288592_254f7438d8e8e28.83123327\";modx.mgr.session.cookie.lifetime|i:0;modx.mgr.user.config|a:0:{}'),
	('orkobtdopk7herqdvfd7ud31k7',1424881432,'modx.user.contextTokens|a:1:{s:3:\"mgr\";i:4;}modx.mgr.user.token|s:52:\"modx54d39f5c13a564.78288592_454e759465f0ee4.15461454\";modx.mgr.session.cookie.lifetime|i:604800;modx.mgr.user.config|a:0:{}modx.user.4.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.4.attributes|a:2:{s:3:\"mgr\";a:4:{s:16:\"modAccessContext\";a:2:{s:3:\"mgr\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:1;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:1;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:1;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:1;s:9:\"countries\";b:1;s:6:\"create\";b:1;s:7:\"credits\";b:1;s:15:\"customize_forms\";b:1;s:10:\"dashboards\";b:1;s:8:\"database\";b:1;s:17:\"database_truncate\";b:1;s:15:\"delete_category\";b:1;s:12:\"delete_chunk\";b:1;s:14:\"delete_context\";b:1;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:1;s:13:\"delete_plugin\";b:1;s:18:\"delete_propertyset\";b:1;s:11:\"delete_role\";b:1;s:14:\"delete_snippet\";b:1;s:15:\"delete_template\";b:1;s:9:\"delete_tv\";b:1;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:1;s:10:\"edit_chunk\";b:1;s:12:\"edit_context\";b:1;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:1;s:16:\"edit_propertyset\";b:1;s:9:\"edit_role\";b:1;s:12:\"edit_snippet\";b:1;s:13:\"edit_template\";b:1;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:1;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:1;s:14:\"error_log_view\";b:1;s:13:\"export_static\";b:1;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:1;s:6:\"frames\";b:1;s:4:\"help\";b:1;s:4:\"home\";b:1;s:13:\"import_static\";b:1;s:9:\"languages\";b:1;s:8:\"lexicons\";b:1;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:1;s:5:\"menus\";b:1;s:12:\"menu_reports\";b:1;s:13:\"menu_security\";b:1;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:1;s:11:\"menu_system\";b:1;s:10:\"menu_tools\";b:1;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:1;s:12:\"new_category\";b:1;s:9:\"new_chunk\";b:1;s:11:\"new_context\";b:1;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:1;s:15:\"new_propertyset\";b:1;s:8:\"new_role\";b:1;s:11:\"new_snippet\";b:1;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:1;s:6:\"new_tv\";b:1;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:1;s:13:\"policy_delete\";b:1;s:11:\"policy_edit\";b:1;s:10:\"policy_new\";b:1;s:11:\"policy_save\";b:1;s:22:\"policy_template_delete\";b:1;s:20:\"policy_template_edit\";b:1;s:19:\"policy_template_new\";b:1;s:20:\"policy_template_save\";b:1;s:20:\"policy_template_view\";b:1;s:11:\"policy_view\";b:1;s:13:\"property_sets\";b:1;s:9:\"providers\";b:1;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:18:\"resource_duplicate\";b:1;s:20:\"resourcegroup_delete\";b:1;s:18:\"resourcegroup_edit\";b:1;s:17:\"resourcegroup_new\";b:1;s:27:\"resourcegroup_resource_edit\";b:1;s:27:\"resourcegroup_resource_list\";b:1;s:18:\"resourcegroup_save\";b:1;s:18:\"resourcegroup_view\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:1;s:10:\"save_chunk\";b:1;s:12:\"save_context\";b:1;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:1;s:16:\"save_propertyset\";b:1;s:9:\"save_role\";b:1;s:12:\"save_snippet\";b:1;s:13:\"save_template\";b:1;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:1;s:7:\"sources\";b:1;s:13:\"source_delete\";b:1;s:11:\"source_edit\";b:1;s:11:\"source_save\";b:1;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:1;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:1;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:1;s:14:\"usergroup_edit\";b:1;s:13:\"usergroup_new\";b:1;s:14:\"usergroup_save\";b:1;s:19:\"usergroup_user_edit\";b:1;s:19:\"usergroup_user_list\";b:1;s:14:\"usergroup_view\";b:1;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:1;s:13:\"view_document\";b:1;s:12:\"view_element\";b:1;s:13:\"view_eventlog\";b:1;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:1;s:16:\"view_propertyset\";b:1;s:9:\"view_role\";b:1;s:12:\"view_snippet\";b:1;s:12:\"view_sysinfo\";b:1;s:13:\"view_template\";b:1;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:1;s:10:\"workspaces\";b:1;}}}s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:1;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:1;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:1;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:1;s:9:\"countries\";b:1;s:6:\"create\";b:1;s:7:\"credits\";b:1;s:15:\"customize_forms\";b:1;s:10:\"dashboards\";b:1;s:8:\"database\";b:1;s:17:\"database_truncate\";b:1;s:15:\"delete_category\";b:1;s:12:\"delete_chunk\";b:1;s:14:\"delete_context\";b:1;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:1;s:13:\"delete_plugin\";b:1;s:18:\"delete_propertyset\";b:1;s:11:\"delete_role\";b:1;s:14:\"delete_snippet\";b:1;s:15:\"delete_template\";b:1;s:9:\"delete_tv\";b:1;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:1;s:10:\"edit_chunk\";b:1;s:12:\"edit_context\";b:1;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:1;s:16:\"edit_propertyset\";b:1;s:9:\"edit_role\";b:1;s:12:\"edit_snippet\";b:1;s:13:\"edit_template\";b:1;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:1;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:1;s:14:\"error_log_view\";b:1;s:13:\"export_static\";b:1;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:1;s:6:\"frames\";b:1;s:4:\"help\";b:1;s:4:\"home\";b:1;s:13:\"import_static\";b:1;s:9:\"languages\";b:1;s:8:\"lexicons\";b:1;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:1;s:5:\"menus\";b:1;s:12:\"menu_reports\";b:1;s:13:\"menu_security\";b:1;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:1;s:11:\"menu_system\";b:1;s:10:\"menu_tools\";b:1;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:1;s:12:\"new_category\";b:1;s:9:\"new_chunk\";b:1;s:11:\"new_context\";b:1;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:1;s:15:\"new_propertyset\";b:1;s:8:\"new_role\";b:1;s:11:\"new_snippet\";b:1;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:1;s:6:\"new_tv\";b:1;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:1;s:13:\"policy_delete\";b:1;s:11:\"policy_edit\";b:1;s:10:\"policy_new\";b:1;s:11:\"policy_save\";b:1;s:22:\"policy_template_delete\";b:1;s:20:\"policy_template_edit\";b:1;s:19:\"policy_template_new\";b:1;s:20:\"policy_template_save\";b:1;s:20:\"policy_template_view\";b:1;s:11:\"policy_view\";b:1;s:13:\"property_sets\";b:1;s:9:\"providers\";b:1;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:18:\"resource_duplicate\";b:1;s:20:\"resourcegroup_delete\";b:1;s:18:\"resourcegroup_edit\";b:1;s:17:\"resourcegroup_new\";b:1;s:27:\"resourcegroup_resource_edit\";b:1;s:27:\"resourcegroup_resource_list\";b:1;s:18:\"resourcegroup_save\";b:1;s:18:\"resourcegroup_view\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:1;s:10:\"save_chunk\";b:1;s:12:\"save_context\";b:1;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:1;s:16:\"save_propertyset\";b:1;s:9:\"save_role\";b:1;s:12:\"save_snippet\";b:1;s:13:\"save_template\";b:1;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:1;s:7:\"sources\";b:1;s:13:\"source_delete\";b:1;s:11:\"source_edit\";b:1;s:11:\"source_save\";b:1;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:1;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:1;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:1;s:14:\"usergroup_edit\";b:1;s:13:\"usergroup_new\";b:1;s:14:\"usergroup_save\";b:1;s:19:\"usergroup_user_edit\";b:1;s:19:\"usergroup_user_list\";b:1;s:14:\"usergroup_view\";b:1;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:1;s:13:\"view_document\";b:1;s:12:\"view_element\";b:1;s:13:\"view_eventlog\";b:1;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:1;s:16:\"view_propertyset\";b:1;s:9:\"view_role\";b:1;s:12:\"view_snippet\";b:1;s:12:\"view_sysinfo\";b:1;s:13:\"view_template\";b:1;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:1;s:10:\"workspaces\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:1:{i:1;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}}}s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:2:{s:3:\"mgr\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:1;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:1;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:1;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:1;s:9:\"countries\";b:1;s:6:\"create\";b:1;s:7:\"credits\";b:1;s:15:\"customize_forms\";b:1;s:10:\"dashboards\";b:1;s:8:\"database\";b:1;s:17:\"database_truncate\";b:1;s:15:\"delete_category\";b:1;s:12:\"delete_chunk\";b:1;s:14:\"delete_context\";b:1;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:1;s:13:\"delete_plugin\";b:1;s:18:\"delete_propertyset\";b:1;s:11:\"delete_role\";b:1;s:14:\"delete_snippet\";b:1;s:15:\"delete_template\";b:1;s:9:\"delete_tv\";b:1;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:1;s:10:\"edit_chunk\";b:1;s:12:\"edit_context\";b:1;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:1;s:16:\"edit_propertyset\";b:1;s:9:\"edit_role\";b:1;s:12:\"edit_snippet\";b:1;s:13:\"edit_template\";b:1;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:1;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:1;s:14:\"error_log_view\";b:1;s:13:\"export_static\";b:1;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:1;s:6:\"frames\";b:1;s:4:\"help\";b:1;s:4:\"home\";b:1;s:13:\"import_static\";b:1;s:9:\"languages\";b:1;s:8:\"lexicons\";b:1;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:1;s:5:\"menus\";b:1;s:12:\"menu_reports\";b:1;s:13:\"menu_security\";b:1;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:1;s:11:\"menu_system\";b:1;s:10:\"menu_tools\";b:1;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:1;s:12:\"new_category\";b:1;s:9:\"new_chunk\";b:1;s:11:\"new_context\";b:1;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:1;s:15:\"new_propertyset\";b:1;s:8:\"new_role\";b:1;s:11:\"new_snippet\";b:1;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:1;s:6:\"new_tv\";b:1;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:1;s:13:\"policy_delete\";b:1;s:11:\"policy_edit\";b:1;s:10:\"policy_new\";b:1;s:11:\"policy_save\";b:1;s:22:\"policy_template_delete\";b:1;s:20:\"policy_template_edit\";b:1;s:19:\"policy_template_new\";b:1;s:20:\"policy_template_save\";b:1;s:20:\"policy_template_view\";b:1;s:11:\"policy_view\";b:1;s:13:\"property_sets\";b:1;s:9:\"providers\";b:1;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:18:\"resource_duplicate\";b:1;s:20:\"resourcegroup_delete\";b:1;s:18:\"resourcegroup_edit\";b:1;s:17:\"resourcegroup_new\";b:1;s:27:\"resourcegroup_resource_edit\";b:1;s:27:\"resourcegroup_resource_list\";b:1;s:18:\"resourcegroup_save\";b:1;s:18:\"resourcegroup_view\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:1;s:10:\"save_chunk\";b:1;s:12:\"save_context\";b:1;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:1;s:16:\"save_propertyset\";b:1;s:9:\"save_role\";b:1;s:12:\"save_snippet\";b:1;s:13:\"save_template\";b:1;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:1;s:7:\"sources\";b:1;s:13:\"source_delete\";b:1;s:11:\"source_edit\";b:1;s:11:\"source_save\";b:1;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:1;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:1;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:1;s:14:\"usergroup_edit\";b:1;s:13:\"usergroup_new\";b:1;s:14:\"usergroup_save\";b:1;s:19:\"usergroup_user_edit\";b:1;s:19:\"usergroup_user_list\";b:1;s:14:\"usergroup_view\";b:1;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:1;s:13:\"view_document\";b:1;s:12:\"view_element\";b:1;s:13:\"view_eventlog\";b:1;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:1;s:16:\"view_propertyset\";b:1;s:9:\"view_role\";b:1;s:12:\"view_snippet\";b:1;s:12:\"view_sysinfo\";b:1;s:13:\"view_template\";b:1;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:1;s:10:\"workspaces\";b:1;}}}s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:1;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:1;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:1;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:1;s:9:\"countries\";b:1;s:6:\"create\";b:1;s:7:\"credits\";b:1;s:15:\"customize_forms\";b:1;s:10:\"dashboards\";b:1;s:8:\"database\";b:1;s:17:\"database_truncate\";b:1;s:15:\"delete_category\";b:1;s:12:\"delete_chunk\";b:1;s:14:\"delete_context\";b:1;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:1;s:13:\"delete_plugin\";b:1;s:18:\"delete_propertyset\";b:1;s:11:\"delete_role\";b:1;s:14:\"delete_snippet\";b:1;s:15:\"delete_template\";b:1;s:9:\"delete_tv\";b:1;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:1;s:10:\"edit_chunk\";b:1;s:12:\"edit_context\";b:1;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:1;s:16:\"edit_propertyset\";b:1;s:9:\"edit_role\";b:1;s:12:\"edit_snippet\";b:1;s:13:\"edit_template\";b:1;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:1;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:1;s:14:\"error_log_view\";b:1;s:13:\"export_static\";b:1;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:1;s:6:\"frames\";b:1;s:4:\"help\";b:1;s:4:\"home\";b:1;s:13:\"import_static\";b:1;s:9:\"languages\";b:1;s:8:\"lexicons\";b:1;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:1;s:5:\"menus\";b:1;s:12:\"menu_reports\";b:1;s:13:\"menu_security\";b:1;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:1;s:11:\"menu_system\";b:1;s:10:\"menu_tools\";b:1;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:1;s:12:\"new_category\";b:1;s:9:\"new_chunk\";b:1;s:11:\"new_context\";b:1;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:1;s:15:\"new_propertyset\";b:1;s:8:\"new_role\";b:1;s:11:\"new_snippet\";b:1;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:1;s:6:\"new_tv\";b:1;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:1;s:13:\"policy_delete\";b:1;s:11:\"policy_edit\";b:1;s:10:\"policy_new\";b:1;s:11:\"policy_save\";b:1;s:22:\"policy_template_delete\";b:1;s:20:\"policy_template_edit\";b:1;s:19:\"policy_template_new\";b:1;s:20:\"policy_template_save\";b:1;s:20:\"policy_template_view\";b:1;s:11:\"policy_view\";b:1;s:13:\"property_sets\";b:1;s:9:\"providers\";b:1;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:18:\"resource_duplicate\";b:1;s:20:\"resourcegroup_delete\";b:1;s:18:\"resourcegroup_edit\";b:1;s:17:\"resourcegroup_new\";b:1;s:27:\"resourcegroup_resource_edit\";b:1;s:27:\"resourcegroup_resource_list\";b:1;s:18:\"resourcegroup_save\";b:1;s:18:\"resourcegroup_view\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:1;s:10:\"save_chunk\";b:1;s:12:\"save_context\";b:1;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:1;s:16:\"save_propertyset\";b:1;s:9:\"save_role\";b:1;s:12:\"save_snippet\";b:1;s:13:\"save_template\";b:1;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:1;s:7:\"sources\";b:1;s:13:\"source_delete\";b:1;s:11:\"source_edit\";b:1;s:11:\"source_save\";b:1;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:1;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:1;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:1;s:14:\"usergroup_edit\";b:1;s:13:\"usergroup_new\";b:1;s:14:\"usergroup_save\";b:1;s:19:\"usergroup_user_edit\";b:1;s:19:\"usergroup_user_list\";b:1;s:14:\"usergroup_view\";b:1;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:1;s:13:\"view_document\";b:1;s:12:\"view_element\";b:1;s:13:\"view_eventlog\";b:1;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:1;s:16:\"view_propertyset\";b:1;s:9:\"view_role\";b:1;s:12:\"view_snippet\";b:1;s:12:\"view_sysinfo\";b:1;s:13:\"view_template\";b:1;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:1;s:10:\"workspaces\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:1:{i:1;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}}}}newResourceTokens|a:2:{i:0;s:23:\"54edf5edcee729.45759839\";i:1;s:23:\"54edf7185d75d4.32403406\";}'),
	('btfsfic37npfuv9t4i8038mtv3',1425071877,'modx.user.contextTokens|a:0:{}'),
	('5inj6qroimm4mabpr66kv91sr6',1425072048,'modx.user.contextTokens|a:0:{}'),
	('ht4ph5s6mekgpaomknp72la222',1425313364,'modx.user.contextTokens|a:0:{}'),
	('k1d6ce0rjlom6avi8044f9nq80',1425339586,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('3dlsl9o42h6jj1kr4dfi19bni3',1425074033,'modx.user.contextTokens|a:0:{}'),
	('83ei6pap2pm0dqlslaukglqeg3',1425074034,'modx.user.contextTokens|a:0:{}'),
	('reug7cgtl98eulkroitsr2pqo3',1425312118,'modx.user.contextTokens|a:0:{}'),
	('5174nfkhriar90hv0iqnms0rt6',1425074983,'modx.user.contextTokens|a:0:{}'),
	('6r6tno8rem5judindi8n67es82',1425083526,'modx.user.contextTokens|a:0:{}'),
	('pl9h8735vfra963qce5rhdtdg4',1425333596,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:1:{s:3:\"mgr\";i:4;}modx.mgr.user.token|s:52:\"modx54d39f5c13a564.78288592_454ef07214c04e3.90497983\";modx.mgr.session.cookie.lifetime|i:0;modx.mgr.user.config|a:0:{}modx.user.4.resourceGroups|a:1:{s:3:\"web\";a:0:{}}newResourceTokens|a:6:{i:0;s:23:\"54f4dbedebd0d2.98210389\";i:1;s:23:\"54f4dbf6e2b827.09010850\";i:2;s:23:\"54f4dc1353a387.77572349\";i:3;s:23:\"54f4dc2d2cc807.36064118\";i:4;s:23:\"54f4dc415d06f0.57787764\";i:5;s:23:\"54f4dc535e62a0.98407857\";}modx.user.4.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:2:{s:3:\"mgr\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:1;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:1;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:1;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:1;s:9:\"countries\";b:1;s:6:\"create\";b:1;s:7:\"credits\";b:1;s:15:\"customize_forms\";b:1;s:10:\"dashboards\";b:1;s:8:\"database\";b:1;s:17:\"database_truncate\";b:1;s:15:\"delete_category\";b:1;s:12:\"delete_chunk\";b:1;s:14:\"delete_context\";b:1;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:1;s:13:\"delete_plugin\";b:1;s:18:\"delete_propertyset\";b:1;s:11:\"delete_role\";b:1;s:14:\"delete_snippet\";b:1;s:15:\"delete_template\";b:1;s:9:\"delete_tv\";b:1;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:1;s:10:\"edit_chunk\";b:1;s:12:\"edit_context\";b:1;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:1;s:16:\"edit_propertyset\";b:1;s:9:\"edit_role\";b:1;s:12:\"edit_snippet\";b:1;s:13:\"edit_template\";b:1;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:1;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:1;s:14:\"error_log_view\";b:1;s:13:\"export_static\";b:1;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:1;s:6:\"frames\";b:1;s:4:\"help\";b:1;s:4:\"home\";b:1;s:13:\"import_static\";b:1;s:9:\"languages\";b:1;s:8:\"lexicons\";b:1;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:1;s:5:\"menus\";b:1;s:12:\"menu_reports\";b:1;s:13:\"menu_security\";b:1;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:1;s:11:\"menu_system\";b:1;s:10:\"menu_tools\";b:1;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:1;s:12:\"new_category\";b:1;s:9:\"new_chunk\";b:1;s:11:\"new_context\";b:1;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:1;s:15:\"new_propertyset\";b:1;s:8:\"new_role\";b:1;s:11:\"new_snippet\";b:1;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:1;s:6:\"new_tv\";b:1;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:1;s:13:\"policy_delete\";b:1;s:11:\"policy_edit\";b:1;s:10:\"policy_new\";b:1;s:11:\"policy_save\";b:1;s:22:\"policy_template_delete\";b:1;s:20:\"policy_template_edit\";b:1;s:19:\"policy_template_new\";b:1;s:20:\"policy_template_save\";b:1;s:20:\"policy_template_view\";b:1;s:11:\"policy_view\";b:1;s:13:\"property_sets\";b:1;s:9:\"providers\";b:1;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:18:\"resource_duplicate\";b:1;s:20:\"resourcegroup_delete\";b:1;s:18:\"resourcegroup_edit\";b:1;s:17:\"resourcegroup_new\";b:1;s:27:\"resourcegroup_resource_edit\";b:1;s:27:\"resourcegroup_resource_list\";b:1;s:18:\"resourcegroup_save\";b:1;s:18:\"resourcegroup_view\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:1;s:10:\"save_chunk\";b:1;s:12:\"save_context\";b:1;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:1;s:16:\"save_propertyset\";b:1;s:9:\"save_role\";b:1;s:12:\"save_snippet\";b:1;s:13:\"save_template\";b:1;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:1;s:7:\"sources\";b:1;s:13:\"source_delete\";b:1;s:11:\"source_edit\";b:1;s:11:\"source_save\";b:1;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:1;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:1;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:1;s:14:\"usergroup_edit\";b:1;s:13:\"usergroup_new\";b:1;s:14:\"usergroup_save\";b:1;s:19:\"usergroup_user_edit\";b:1;s:19:\"usergroup_user_list\";b:1;s:14:\"usergroup_view\";b:1;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:1;s:13:\"view_document\";b:1;s:12:\"view_element\";b:1;s:13:\"view_eventlog\";b:1;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:1;s:16:\"view_propertyset\";b:1;s:9:\"view_role\";b:1;s:12:\"view_snippet\";b:1;s:12:\"view_sysinfo\";b:1;s:13:\"view_template\";b:1;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:1;s:10:\"workspaces\";b:1;}}}s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:1;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:1;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:1;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:1;s:9:\"countries\";b:1;s:6:\"create\";b:1;s:7:\"credits\";b:1;s:15:\"customize_forms\";b:1;s:10:\"dashboards\";b:1;s:8:\"database\";b:1;s:17:\"database_truncate\";b:1;s:15:\"delete_category\";b:1;s:12:\"delete_chunk\";b:1;s:14:\"delete_context\";b:1;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:1;s:13:\"delete_plugin\";b:1;s:18:\"delete_propertyset\";b:1;s:11:\"delete_role\";b:1;s:14:\"delete_snippet\";b:1;s:15:\"delete_template\";b:1;s:9:\"delete_tv\";b:1;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:1;s:10:\"edit_chunk\";b:1;s:12:\"edit_context\";b:1;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:1;s:16:\"edit_propertyset\";b:1;s:9:\"edit_role\";b:1;s:12:\"edit_snippet\";b:1;s:13:\"edit_template\";b:1;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:1;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:1;s:14:\"error_log_view\";b:1;s:13:\"export_static\";b:1;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:1;s:6:\"frames\";b:1;s:4:\"help\";b:1;s:4:\"home\";b:1;s:13:\"import_static\";b:1;s:9:\"languages\";b:1;s:8:\"lexicons\";b:1;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:1;s:5:\"menus\";b:1;s:12:\"menu_reports\";b:1;s:13:\"menu_security\";b:1;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:1;s:11:\"menu_system\";b:1;s:10:\"menu_tools\";b:1;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:1;s:12:\"new_category\";b:1;s:9:\"new_chunk\";b:1;s:11:\"new_context\";b:1;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:1;s:15:\"new_propertyset\";b:1;s:8:\"new_role\";b:1;s:11:\"new_snippet\";b:1;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:1;s:6:\"new_tv\";b:1;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:1;s:13:\"policy_delete\";b:1;s:11:\"policy_edit\";b:1;s:10:\"policy_new\";b:1;s:11:\"policy_save\";b:1;s:22:\"policy_template_delete\";b:1;s:20:\"policy_template_edit\";b:1;s:19:\"policy_template_new\";b:1;s:20:\"policy_template_save\";b:1;s:20:\"policy_template_view\";b:1;s:11:\"policy_view\";b:1;s:13:\"property_sets\";b:1;s:9:\"providers\";b:1;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:18:\"resource_duplicate\";b:1;s:20:\"resourcegroup_delete\";b:1;s:18:\"resourcegroup_edit\";b:1;s:17:\"resourcegroup_new\";b:1;s:27:\"resourcegroup_resource_edit\";b:1;s:27:\"resourcegroup_resource_list\";b:1;s:18:\"resourcegroup_save\";b:1;s:18:\"resourcegroup_view\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:1;s:10:\"save_chunk\";b:1;s:12:\"save_context\";b:1;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:1;s:16:\"save_propertyset\";b:1;s:9:\"save_role\";b:1;s:12:\"save_snippet\";b:1;s:13:\"save_template\";b:1;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:1;s:7:\"sources\";b:1;s:13:\"source_delete\";b:1;s:11:\"source_edit\";b:1;s:11:\"source_save\";b:1;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:1;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:1;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:1;s:14:\"usergroup_edit\";b:1;s:13:\"usergroup_new\";b:1;s:14:\"usergroup_save\";b:1;s:19:\"usergroup_user_edit\";b:1;s:19:\"usergroup_user_list\";b:1;s:14:\"usergroup_view\";b:1;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:1;s:13:\"view_document\";b:1;s:12:\"view_element\";b:1;s:13:\"view_eventlog\";b:1;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:1;s:16:\"view_propertyset\";b:1;s:9:\"view_role\";b:1;s:12:\"view_snippet\";b:1;s:12:\"view_sysinfo\";b:1;s:13:\"view_template\";b:1;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:1;s:10:\"workspaces\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:2:{i:1;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}i:6;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}}}}'),
	('1f2h7ajlkea38o93kiku98lit5',1425323435,'modx.user.contextTokens|a:0:{}'),
	('q6js1dim3uhqj2onnsu6qgnio3',1425323447,'modx.user.contextTokens|a:0:{}'),
	('3dlci56o15meviqt29eldam5s1',1425323448,'modx.user.contextTokens|a:0:{}'),
	('p2vt4hh45fsgd6ff6jbt53kfk4',1425323449,'modx.user.contextTokens|a:0:{}'),
	('ripgcdl1pph02g4dnhqq65beo4',1425323450,'modx.user.contextTokens|a:0:{}'),
	('8vbbkn1lkoon0qqv8c5igqim51',1425323434,'modx.user.contextTokens|a:0:{}'),
	('l1bf8skuob3tf7tsp63vmqle45',1425323400,'modx.user.contextTokens|a:0:{}'),
	('16gj7u1gk37q1dfecef7bkgrh6',1425323399,'modx.user.contextTokens|a:0:{}'),
	('2eloqvl2r0qin81g6ucanrnpt3',1424725155,'modx.user.contextTokens|a:0:{}'),
	('2pagvmigst37bij4d139hb0ub5',1424793041,'modx.user.contextTokens|a:0:{}'),
	('q6m73mngdcn4ri4m760ekjotb6',1424811125,'modx.user.contextTokens|a:0:{}'),
	('f2ruhvuj7pfs8j4htij9efbbv0',1424876917,'modx.user.contextTokens|a:0:{}'),
	('i7p9rs61v6g34ornhbofsec8k0',1424895978,'modx.user.contextTokens|a:0:{}'),
	('e5b0qqs5a02o6o8ij91ef1nfp6',1425333836,'modx.user.contextTokens|a:0:{}'),
	('n1do0ktr1s9i6hbse9j6p059a6',1425333837,'modx.user.contextTokens|a:0:{}'),
	('pnntgga39ob1bpkeor72rcv110',1425333858,'modx.user.contextTokens|a:0:{}'),
	('4n9h1cdc7el5k4od9d9vlgts42',1425333859,'modx.user.contextTokens|a:0:{}'),
	('7uucv7utqio1744jg0jlu9ki22',1425333993,'modx.user.contextTokens|a:0:{}'),
	('k788tmbluf8ufttvar5icgbcl5',1425333995,'modx.user.contextTokens|a:0:{}'),
	('hfv7u725jbo27krv80bcuq50t7',1425333996,'modx.user.contextTokens|a:0:{}'),
	('odm8pthklkhdpkno43dv4jj0t6',1425333997,'modx.user.contextTokens|a:0:{}'),
	('h7bsuu68rf4pkd4iqtsjeumvg6',1425334254,'modx.user.contextTokens|a:0:{}'),
	('p2v5ilvp5jirl2f7otf2v8t0k7',1425334255,'modx.user.contextTokens|a:0:{}'),
	('f6i93pp9q3let3gbboa6j7ug13',1425334542,'modx.user.contextTokens|a:0:{}'),
	('a1idlsgdpk7nnkiievvsqrpt00',1425334543,'modx.user.contextTokens|a:0:{}'),
	('6iv8fkslati9n9nc3ukafa1af6',1425334610,'modx.user.contextTokens|a:0:{}'),
	('j37v7mkgk3la9kirb3dpj6ot31',1425334611,'modx.user.contextTokens|a:0:{}'),
	('gmn2vju66je45bmqt2o030ii33',1425334698,'modx.user.contextTokens|a:0:{}'),
	('5oh8qmmrai57c85ic3072toeu5',1425334698,'modx.user.contextTokens|a:0:{}'),
	('1c080sdr1t5kh2h5jqf8u28qt7',1425334759,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('8c7aq6ig8nscp2ud1bfi8s8cu0',1425334816,'modx.user.contextTokens|a:0:{}'),
	('s2vcivepa724capjk1fgnc5a16',1425334816,'modx.user.contextTokens|a:0:{}'),
	('dlpdvct7uqqhgup8eqkadu6s43',1425334875,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('f01bdek6612gt7301rlccu4k94',1425334847,'modx.user.contextTokens|a:0:{}'),
	('hm8vs7aj8ck3g31c67eh00h7s2',1425335483,'modx.user.contextTokens|a:0:{}'),
	('3iii8rsr7014ltcvm7u5p64m53',1425335484,'modx.user.contextTokens|a:0:{}'),
	('jni57rvltbu10iiij40k7omok1',1425335580,'modx.user.contextTokens|a:0:{}'),
	('4hj2rommnqbl0chag4ceggisk6',1425335580,'modx.user.contextTokens|a:0:{}'),
	('pm15h4o369l9gsvi0q4pvgdvn6',1425335796,'modx.user.contextTokens|a:0:{}'),
	('06tql3q2dikk88onmh1u66d0k6',1425335796,'modx.user.contextTokens|a:0:{}'),
	('qhbq7t1s3eim88l8bu29ihhj44',1425336251,'modx.user.contextTokens|a:0:{}'),
	('nplt5afslseaj3b3uffu98ff83',1425336251,'modx.user.contextTokens|a:0:{}'),
	('ftsf3jnj6np698bs8jqpid1lm6',1425336302,'modx.user.contextTokens|a:0:{}'),
	('0md7ii9o0996r48q6omic8sr62',1425336486,'modx.user.contextTokens|a:0:{}'),
	('as2uo5uc1u9i934ug523u69f87',1425336486,'modx.user.contextTokens|a:0:{}'),
	('visq2adm9juc9l2fsjs7kmfqo6',1425336605,'modx.user.contextTokens|a:0:{}'),
	('gd61idd77hvgc7os9fsbu3qmf5',1425336605,'modx.user.contextTokens|a:0:{}'),
	('pk0plm0l4uv2jempg0eep45ia4',1425336632,'modx.user.contextTokens|a:0:{}'),
	('59s9g39obj6odchr04enhnbku1',1425336632,'modx.user.contextTokens|a:0:{}'),
	('cmsg3uaaodqn4n0ntm40ejqvh2',1425337315,'modx.user.contextTokens|a:0:{}'),
	('akj7d99ch6de30va3bn14sj7s6',1425337316,'modx.user.contextTokens|a:0:{}'),
	('gvj84rtcprp49b8b1o9ju82j92',1425337506,'modx.user.contextTokens|a:0:{}'),
	('4o3039olgj6mqelpk0iv70lf96',1425337506,'modx.user.contextTokens|a:0:{}'),
	('1lg04es1o7hv2fac7msf43n9k1',1425337811,'modx.user.contextTokens|a:0:{}'),
	('l5ljh3b9i424fmb9ch2dr2j3f6',1425337812,'modx.user.contextTokens|a:0:{}'),
	('rb9c6u9a2u1dopgriddhoj5tr3',1425337917,'modx.user.contextTokens|a:0:{}'),
	('0puq4k0cegc6cje0e67r5so0d4',1425338565,'modx.user.contextTokens|a:0:{}'),
	('3elk4jq9rid1ga5d0m5i3ovnv5',1425338566,'modx.user.contextTokens|a:0:{}'),
	('6uhccdn1884p1hl1qvr6usl6q6',1425338978,'modx.user.contextTokens|a:0:{}'),
	('bo274stsj8t0rjet7fcemikum6',1425338979,'modx.user.contextTokens|a:0:{}'),
	('id1le41bea54i7m1anu15rhi12',1425338980,'modx.user.contextTokens|a:0:{}'),
	('t628ed1atrhj03sssn6olg27d2',1425338981,'modx.user.contextTokens|a:0:{}'),
	('1ouau0lnr05lpnt7v06d18p4o7',1425339017,'modx.user.contextTokens|a:0:{}'),
	('2kl4gocqfq5orkniolipno6405',1425339018,'modx.user.contextTokens|a:0:{}'),
	('i1qs18jqiglola8i0e9r017b17',1425339453,'modx.user.contextTokens|a:0:{}'),
	('us85mud01e5oarrfp7dc4pt4m1',1425339454,'modx.user.contextTokens|a:0:{}'),
	('agjjnpbrksrvbmcioma3f3gqn4',1425339625,'modx.user.contextTokens|a:0:{}'),
	('kvhts4fcqortq0o19hhl6705j3',1425340008,'modx.user.contextTokens|a:0:{}'),
	('r6l4c0r7q1ffmveops7a18cdt6',1425340008,'modx.user.contextTokens|a:0:{}'),
	('e54biii7gi9ls3qgj8eanddq80',1425340179,'modx.user.contextTokens|a:0:{}'),
	('6t7q9gratdkhq4hckukjclu024',1425340187,'modx.user.contextTokens|a:0:{}'),
	('e8qd7nqug5aro7v2nggi6d3h64',1425340193,'modx.user.contextTokens|a:0:{}'),
	('8i8efq4lt16psedna4brronte5',1425340195,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('si2ct6olu9qqgj3tfi02kurog6',1425340196,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('bi71vpdl5affujcd8hv7kloti6',1425340196,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('78tlhnj3a8rfivoruc2uv16v01',1425340197,'modx.user.contextTokens|a:0:{}'),
	('pgn3tafkk3jgm5hmga0cgkdkf0',1425340197,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('pai8cg8e84ld918oivhpmgaoj0',1425340197,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('sl21guph2s15dn28ka3rut3p26',1425340197,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('8v35e526kcb68dgd853fqfcsl4',1424955081,'modx.user.contextTokens|a:1:{s:3:\"mgr\";i:4;}modx.mgr.user.token|s:52:\"modx54d39f5c13a564.78288592_454ef167e121e22.18547517\";modx.mgr.session.cookie.lifetime|i:604800;modx.mgr.user.config|a:0:{}modx.user.4.resourceGroups|a:1:{s:3:\"mgr\";a:0:{}}modx.user.4.attributes|a:1:{s:3:\"mgr\";a:4:{s:16:\"modAccessContext\";a:2:{s:3:\"mgr\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:1;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:1;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:1;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:1;s:9:\"countries\";b:1;s:6:\"create\";b:1;s:7:\"credits\";b:1;s:15:\"customize_forms\";b:1;s:10:\"dashboards\";b:1;s:8:\"database\";b:1;s:17:\"database_truncate\";b:1;s:15:\"delete_category\";b:1;s:12:\"delete_chunk\";b:1;s:14:\"delete_context\";b:1;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:1;s:13:\"delete_plugin\";b:1;s:18:\"delete_propertyset\";b:1;s:11:\"delete_role\";b:1;s:14:\"delete_snippet\";b:1;s:15:\"delete_template\";b:1;s:9:\"delete_tv\";b:1;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:1;s:10:\"edit_chunk\";b:1;s:12:\"edit_context\";b:1;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:1;s:16:\"edit_propertyset\";b:1;s:9:\"edit_role\";b:1;s:12:\"edit_snippet\";b:1;s:13:\"edit_template\";b:1;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:1;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:1;s:14:\"error_log_view\";b:1;s:13:\"export_static\";b:1;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:1;s:6:\"frames\";b:1;s:4:\"help\";b:1;s:4:\"home\";b:1;s:13:\"import_static\";b:1;s:9:\"languages\";b:1;s:8:\"lexicons\";b:1;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:1;s:5:\"menus\";b:1;s:12:\"menu_reports\";b:1;s:13:\"menu_security\";b:1;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:1;s:11:\"menu_system\";b:1;s:10:\"menu_tools\";b:1;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:1;s:12:\"new_category\";b:1;s:9:\"new_chunk\";b:1;s:11:\"new_context\";b:1;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:1;s:15:\"new_propertyset\";b:1;s:8:\"new_role\";b:1;s:11:\"new_snippet\";b:1;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:1;s:6:\"new_tv\";b:1;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:1;s:13:\"policy_delete\";b:1;s:11:\"policy_edit\";b:1;s:10:\"policy_new\";b:1;s:11:\"policy_save\";b:1;s:22:\"policy_template_delete\";b:1;s:20:\"policy_template_edit\";b:1;s:19:\"policy_template_new\";b:1;s:20:\"policy_template_save\";b:1;s:20:\"policy_template_view\";b:1;s:11:\"policy_view\";b:1;s:13:\"property_sets\";b:1;s:9:\"providers\";b:1;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:18:\"resource_duplicate\";b:1;s:20:\"resourcegroup_delete\";b:1;s:18:\"resourcegroup_edit\";b:1;s:17:\"resourcegroup_new\";b:1;s:27:\"resourcegroup_resource_edit\";b:1;s:27:\"resourcegroup_resource_list\";b:1;s:18:\"resourcegroup_save\";b:1;s:18:\"resourcegroup_view\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:1;s:10:\"save_chunk\";b:1;s:12:\"save_context\";b:1;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:1;s:16:\"save_propertyset\";b:1;s:9:\"save_role\";b:1;s:12:\"save_snippet\";b:1;s:13:\"save_template\";b:1;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:1;s:7:\"sources\";b:1;s:13:\"source_delete\";b:1;s:11:\"source_edit\";b:1;s:11:\"source_save\";b:1;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:1;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:1;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:1;s:14:\"usergroup_edit\";b:1;s:13:\"usergroup_new\";b:1;s:14:\"usergroup_save\";b:1;s:19:\"usergroup_user_edit\";b:1;s:19:\"usergroup_user_list\";b:1;s:14:\"usergroup_view\";b:1;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:1;s:13:\"view_document\";b:1;s:12:\"view_element\";b:1;s:13:\"view_eventlog\";b:1;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:1;s:16:\"view_propertyset\";b:1;s:9:\"view_role\";b:1;s:12:\"view_snippet\";b:1;s:12:\"view_sysinfo\";b:1;s:13:\"view_template\";b:1;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:1;s:10:\"workspaces\";b:1;}}}s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:1;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:1;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:1;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:1;s:9:\"countries\";b:1;s:6:\"create\";b:1;s:7:\"credits\";b:1;s:15:\"customize_forms\";b:1;s:10:\"dashboards\";b:1;s:8:\"database\";b:1;s:17:\"database_truncate\";b:1;s:15:\"delete_category\";b:1;s:12:\"delete_chunk\";b:1;s:14:\"delete_context\";b:1;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:1;s:13:\"delete_plugin\";b:1;s:18:\"delete_propertyset\";b:1;s:11:\"delete_role\";b:1;s:14:\"delete_snippet\";b:1;s:15:\"delete_template\";b:1;s:9:\"delete_tv\";b:1;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:1;s:10:\"edit_chunk\";b:1;s:12:\"edit_context\";b:1;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:1;s:16:\"edit_propertyset\";b:1;s:9:\"edit_role\";b:1;s:12:\"edit_snippet\";b:1;s:13:\"edit_template\";b:1;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:1;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:1;s:14:\"error_log_view\";b:1;s:13:\"export_static\";b:1;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:1;s:6:\"frames\";b:1;s:4:\"help\";b:1;s:4:\"home\";b:1;s:13:\"import_static\";b:1;s:9:\"languages\";b:1;s:8:\"lexicons\";b:1;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:1;s:5:\"menus\";b:1;s:12:\"menu_reports\";b:1;s:13:\"menu_security\";b:1;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:1;s:11:\"menu_system\";b:1;s:10:\"menu_tools\";b:1;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:1;s:12:\"new_category\";b:1;s:9:\"new_chunk\";b:1;s:11:\"new_context\";b:1;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:1;s:15:\"new_propertyset\";b:1;s:8:\"new_role\";b:1;s:11:\"new_snippet\";b:1;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:1;s:6:\"new_tv\";b:1;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:1;s:13:\"policy_delete\";b:1;s:11:\"policy_edit\";b:1;s:10:\"policy_new\";b:1;s:11:\"policy_save\";b:1;s:22:\"policy_template_delete\";b:1;s:20:\"policy_template_edit\";b:1;s:19:\"policy_template_new\";b:1;s:20:\"policy_template_save\";b:1;s:20:\"policy_template_view\";b:1;s:11:\"policy_view\";b:1;s:13:\"property_sets\";b:1;s:9:\"providers\";b:1;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:18:\"resource_duplicate\";b:1;s:20:\"resourcegroup_delete\";b:1;s:18:\"resourcegroup_edit\";b:1;s:17:\"resourcegroup_new\";b:1;s:27:\"resourcegroup_resource_edit\";b:1;s:27:\"resourcegroup_resource_list\";b:1;s:18:\"resourcegroup_save\";b:1;s:18:\"resourcegroup_view\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:1;s:10:\"save_chunk\";b:1;s:12:\"save_context\";b:1;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:1;s:16:\"save_propertyset\";b:1;s:9:\"save_role\";b:1;s:12:\"save_snippet\";b:1;s:13:\"save_template\";b:1;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:1;s:7:\"sources\";b:1;s:13:\"source_delete\";b:1;s:11:\"source_edit\";b:1;s:11:\"source_save\";b:1;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:1;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:1;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:1;s:14:\"usergroup_edit\";b:1;s:13:\"usergroup_new\";b:1;s:14:\"usergroup_save\";b:1;s:19:\"usergroup_user_edit\";b:1;s:19:\"usergroup_user_list\";b:1;s:14:\"usergroup_view\";b:1;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:1;s:13:\"view_document\";b:1;s:12:\"view_element\";b:1;s:13:\"view_eventlog\";b:1;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:1;s:16:\"view_propertyset\";b:1;s:9:\"view_role\";b:1;s:12:\"view_snippet\";b:1;s:12:\"view_sysinfo\";b:1;s:13:\"view_template\";b:1;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:1;s:10:\"workspaces\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:1:{i:1;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"1\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}}}}newResourceTokens|a:5:{i:0;s:23:\"54ef1683e20e37.36513518\";i:1;s:23:\"54ef168d8d5d01.10581755\";i:2;s:23:\"54ef16b285fbc6.39119691\";i:3;s:23:\"54ef16be75ad08.74928268\";i:4;s:23:\"54ef16c9cbad38.83538781\";}'),
	('a4u9cm6iaapq2sm6brjehl5d67',1425137815,'modx.user.contextTokens|a:0:{}'),
	('8tcohjf9q8065rdreprfpm9391',1425138982,'modx.user.contextTokens|a:0:{}'),
	('v6s0rg3n5ikidmf14m9l5md335',1425138983,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('u563pgrldcpanf2jaolm0r8vn6',1425139889,'modx.user.contextTokens|a:0:{}'),
	('k9tjp3vjji1fcl3h602orn8e27',1425140267,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('bb7vcr7nvb19igg3q8e5n27261',1425140450,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('splkh7g268qck5rijtfg4f9fl5',1425144510,'modx.user.contextTokens|a:0:{}'),
	('09tbds0sd5278trgctfsjf0m52',1425149113,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('1dp28d6u8ij3f7oe4onbgorqa5',1425149355,'modx.user.contextTokens|a:0:{}'),
	('lp056kh17t6jcp0obeevkh0iu2',1425149672,'modx.user.contextTokens|a:0:{}'),
	('4k29dkf6d0g9le4be9uq9t0246',1425150912,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('u4ff71qs24nhvb2015fv6s2jg5',1425155559,''),
	('fd8lo18ncbrn91nrt2qhisfr52',1425155563,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('pl3c6k9f06rq4qoam3pkc08q81',1425156890,'modx.user.contextTokens|a:0:{}'),
	('3lc0sf2nh5o8ojl120jftksv67',1425157090,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('tnhh4tr6kpqdfajqenm68ikdg7',1425157810,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('1mge0h7ghfof33g5vaedtnfvi6',1425157886,'modx.user.contextTokens|a:0:{}'),
	('1g8r3d0ud68t8mqd7lvhpmeg97',1425158816,'modx.user.contextTokens|a:0:{}'),
	('d68kc5l0fvh1sl0eau96g45747',1425212496,'modx.user.contextTokens|a:0:{}'),
	('pen30aoj9fd22ovhvp52t81dq0',1425212624,'modx.user.contextTokens|a:0:{}'),
	('g827dkgnq757kq8oce2j5a8714',1425213040,'modx.user.contextTokens|a:0:{}'),
	('tipi1i70a68pg4qfq4mp9nef16',1425213075,'modx.user.contextTokens|a:0:{}'),
	('h32bhfeqkc9da79305a48glhj2',1425213782,'modx.user.contextTokens|a:0:{}'),
	('pn9ejfclqjrjjo5r2fg1q4rcr0',1425214323,'modx.user.contextTokens|a:0:{}'),
	('ap780ifc6uqad5t7ecc0hd4b14',1425214730,'modx.user.contextTokens|a:0:{}'),
	('e32ms6bp009dlnua36nckamu73',1425215234,'modx.user.contextTokens|a:0:{}'),
	('3miphijsic7j18urdh2sqn9lo2',1425215352,'modx.user.contextTokens|a:0:{}'),
	('v8q42iic8emooe8bpfkdni2604',1425215352,'modx.user.contextTokens|a:0:{}'),
	('vlt251pp9buc86o0d1li7m6196',1425215547,'modx.user.contextTokens|a:0:{}'),
	('367tjnkvhq4cni96nkgarhs7h7',1425215719,'modx.user.contextTokens|a:0:{}'),
	('8fqav2ckv65ght6km4411i0nd7',1425215748,'modx.user.contextTokens|a:0:{}'),
	('a5i3lj4vmjd4fk9e1m38vl7ik7',1425215969,'modx.user.contextTokens|a:0:{}'),
	('943m99p4jot8te9oq0c5cvir24',1425216080,'modx.user.contextTokens|a:0:{}'),
	('b2nrujjml25b256l77ha824mu7',1425217788,'modx.user.contextTokens|a:0:{}'),
	('9ugvgs3tl3jjlsagf3tvdr05v4',1425217788,'modx.user.contextTokens|a:0:{}'),
	('3re3ms2v1l8rh6l1051k2rrae6',1425218027,'modx.user.contextTokens|a:0:{}'),
	('2nrtaddhgavgthg3r3qlf10ne4',1425218589,'modx.user.contextTokens|a:0:{}'),
	('29ovadsf947fm92hm0o3kvj8n7',1425219323,'modx.user.contextTokens|a:0:{}'),
	('b69ihci041b9dfj2hmls0q6rs6',1425219715,'modx.user.contextTokens|a:0:{}'),
	('pj7he2eopstm0eg6tjfid4ch82',1425219899,'modx.user.contextTokens|a:0:{}'),
	('1doshq5tb1j88gdg0bgjeput96',1425220526,'modx.user.contextTokens|a:0:{}'),
	('jtkkbfom7tue1ebb4bvgtvdk33',1425220543,'modx.user.contextTokens|a:0:{}'),
	('pktg8jv331nmqvg5o7sriband6',1425220562,'modx.user.contextTokens|a:0:{}'),
	('frp4q05ksoumvasjf87qo6ha44',1425220838,'modx.user.contextTokens|a:0:{}'),
	('ujm7q0mp748idcjeuf7hn585q2',1425221170,'modx.user.contextTokens|a:0:{}'),
	('bm0d0s1a1n7l373uijnqhjq1d4',1425221216,'modx.user.contextTokens|a:0:{}'),
	('abqpb8e6io643em542dj6l2g27',1425222269,'modx.user.contextTokens|a:0:{}'),
	('5u855be2qsbrpm1su2ibhnqnk1',1425222927,'modx.user.contextTokens|a:0:{}'),
	('h4mij7f6lki7d62sobk2jro9g3',1425224565,'modx.user.contextTokens|a:0:{}'),
	('cij85bshgsmv0jdkf8hfhk9ju2',1425224643,'modx.user.contextTokens|a:0:{}'),
	('nepvsdjnmnmsfbkk8ff3j6ame4',1425224841,'modx.user.contextTokens|a:0:{}'),
	('1t3a4u6l42op13jicblrbg6u80',1425225205,'modx.user.contextTokens|a:0:{}'),
	('vvuihcoe36aktgsevfcrf3mqa6',1425226341,'modx.user.contextTokens|a:0:{}'),
	('lbdgb5gaem8cgb3inpurdjbcp1',1425227697,'modx.user.contextTokens|a:0:{}'),
	('tv44kccjuqts24nr96vsde5r77',1424975464,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('0ideetbov8c2s9k292n59uipk5',1424979733,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('vmt7l1h2ef6mel6t4ttongpve5',1424976163,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('0t7g3db7o08fi3el4e53ihdrq3',1424976210,'modx.user.contextTokens|a:0:{}'),
	('4utt3d03pst036793mk9sthe62',1424991008,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('l4e7a022c4jhlkhvsr7e93fnp1',1424976507,'modx.user.contextTokens|a:0:{}'),
	('j63jjn79ct4b9ejdo8lfmv07l5',1424977378,'modx.user.contextTokens|a:0:{}'),
	('jbibl06hgujdb5li7d1tvllfg3',1424977404,'modx.user.contextTokens|a:0:{}'),
	('sa5pbb9pt88hspb7223anlmkp0',1424978025,'modx.user.contextTokens|a:0:{}'),
	('ak2ldt5nknpn1qj33db63go5d2',1424979127,'modx.user.contextTokens|a:0:{}'),
	('a7icdf3b9fiulknu26ih6uah65',1424980096,'modx.user.contextTokens|a:0:{}'),
	('41ia6o28nfhkl1br8huc3e8mc6',1424980860,'modx.user.contextTokens|a:0:{}'),
	('k23levsauot6qdta91fd978lg1',1424981193,'modx.user.contextTokens|a:0:{}'),
	('cug1ruicv0u9scsh0vj8aaa451',1424986332,'modx.user.contextTokens|a:0:{}'),
	('apg0pkpqpea15j9gbg6dac24n5',1424986825,'modx.user.contextTokens|a:0:{}'),
	('9h1r9jcp5ngt7v16c5favf2vf2',1424987474,'modx.user.contextTokens|a:0:{}'),
	('8tb4sjj36a4gftuqes7fi1q941',1424988947,'modx.user.contextTokens|a:0:{}'),
	('aksjt7hifei709guiv8503hkt5',1424992337,'modx.user.contextTokens|a:0:{}'),
	('ktcicut8h58nl85rmjs84jumc5',1424992977,'modx.user.contextTokens|a:0:{}'),
	('43u6vh9g82bd6ad2kpvj5l8ap5',1424993254,'modx.user.contextTokens|a:0:{}'),
	('71gonk2mnko11c3m2lr5uagcb0',1424993254,'modx.user.contextTokens|a:0:{}'),
	('nklp47rknbabub8a83pn0q8i42',1424993254,'modx.user.contextTokens|a:0:{}'),
	('2pcptiem49r74hn8ds1jo95mn7',1424993735,'modx.user.contextTokens|a:0:{}'),
	('18682u7584nnkja754a09cbk36',1424994494,'modx.user.contextTokens|a:0:{}'),
	('buu5l5ed0v6db1a47n3eni0m86',1424994494,'modx.user.contextTokens|a:0:{}'),
	('c2nf935ur4un3n89113i2al0t2',1424997794,'modx.user.contextTokens|a:0:{}'),
	('hbjrv3ou5bpakjbjlt8k22aad4',1424997795,'modx.user.contextTokens|a:0:{}'),
	('mr3fqmg29s70qaij74bn7ibtp4',1424998119,'modx.user.contextTokens|a:0:{}'),
	('nk283ve8hssit4smp5pomn6j36',1425075954,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('ioaonv9kaf96ai8agg0m81uan1',1425001800,'modx.user.contextTokens|a:0:{}'),
	('mun4ebniob96dlf0mukrvnn912',1425002007,'modx.user.contextTokens|a:0:{}'),
	('j4vgmfcnh902bpltv4hb23ddo0',1425002399,''),
	('uek8467df736dml1k0ecvra2j3',1425002400,'modx.user.contextTokens|a:0:{}'),
	('hiecc9opf6esd49cdrani4vs94',1425003776,'modx.user.contextTokens|a:0:{}'),
	('m6sb3k4tl15cs8f6j182cntlb5',1425003958,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('i3nl4j50c3cbmq307um0s7pcs6',1425004275,'modx.user.contextTokens|a:0:{}'),
	('hodl322nh8ij2q55l0dao3h9p7',1425006308,'modx.user.contextTokens|a:0:{}'),
	('qro2j1vpvt5abgj4agi8sjsdv5',1425006566,'modx.user.contextTokens|a:0:{}'),
	('3h2h5hevt81k53k5h3fj4fivh6',1425111398,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('1pgouq1h99up1sdnt109gvu0f6',1425006917,'modx.user.contextTokens|a:0:{}'),
	('urv103f693rkv9usfr9nng5i81',1425011932,'modx.user.contextTokens|a:0:{}'),
	('0trcsuarq5sjd3cruiqulqvbr7',1425014465,'modx.user.contextTokens|a:0:{}'),
	('69i6bst6ljtll5po7h072csb07',1425021294,'modx.user.contextTokens|a:0:{}'),
	('0g4pkha3acmib6hrac7g2r2dl1',1425021329,'modx.user.contextTokens|a:0:{}'),
	('po3riahps7ium6ehurfb6i57m7',1425021688,'modx.user.contextTokens|a:0:{}'),
	('0u45fd9ml9v0d01190tukql9k0',1425021910,'modx.user.contextTokens|a:0:{}'),
	('1sj1okm3ck3jqnrg0koresc102',1425023376,'modx.user.contextTokens|a:0:{}'),
	('j822r9u7j36k64urh2te5t1hf3',1425023376,'modx.user.contextTokens|a:0:{}'),
	('vqj60hn18numng14lp8b421227',1425024473,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('vsea58p502lg2rqa1psvctf6d3',1425024498,'modx.user.contextTokens|a:0:{}'),
	('p8d147v8634mbei8c8galu2773',1425024499,'modx.user.contextTokens|a:0:{}'),
	('r0caa9gnj68e1q2eq19cak9db3',1425024499,'modx.user.contextTokens|a:0:{}'),
	('udtmvutckm67mksr5h8e9knen4',1425024505,'modx.user.contextTokens|a:0:{}'),
	('kqnjarfva9bb2gfdsorqr5oap1',1425024528,'modx.user.contextTokens|a:0:{}'),
	('g2a3odi7dn80jct8hhj1h9mbn0',1425024566,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('9j3c7bf1u7vk22je7oj1m5n5k2',1425024597,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('jft7114na9hmb6nsa6kdo0a254',1425024630,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ceb302qb8kod8k31muckjuj1k6',1425024664,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('hn5eb5uaemu3kka5b89s7lobp0',1425024700,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('2n6lifj9rcf1jmu3k8js0ued34',1425024732,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('b0u1n4ufcdshsrv2u09bbj8rp1',1425025901,'modx.user.contextTokens|a:0:{}'),
	('qdv9s7bn6pjd45ui3uctvmqfm2',1425026336,'modx.user.contextTokens|a:0:{}'),
	('g021pj5r2d5jcu0qqge7lj1di7',1425029189,'modx.user.contextTokens|a:0:{}'),
	('3rong5778dd6baosoackjii4m0',1425029652,'modx.user.contextTokens|a:0:{}'),
	('hbudsd1p7tig486jjnm4nmjtk0',1425029661,'modx.user.contextTokens|a:0:{}'),
	('6pgu9ggfd3i3tt95au13kkn787',1425033921,'modx.user.contextTokens|a:0:{}'),
	('ri8574rvdk5gsjs454m047ebu0',1425034336,'modx.user.contextTokens|a:0:{}'),
	('d24i5stj8u1esil5u9879m0ra5',1425035915,'modx.user.contextTokens|a:0:{}'),
	('hrjc2r2hntqh7rs6qrajjekn81',1425037539,'modx.user.contextTokens|a:0:{}'),
	('h6vuvc4bhbtatgmrdrdjmikb02',1425037539,'modx.user.contextTokens|a:0:{}'),
	('qdkd4fi390nqfar4q4mncvsis3',1425040382,'modx.user.contextTokens|a:0:{}'),
	('id8hl6507ohb1gni5lhrs8mn67',1425040524,'modx.user.contextTokens|a:0:{}'),
	('2pslmsjaplv1841tghc8jv0i57',1425042559,'modx.user.contextTokens|a:0:{}'),
	('0svg5gn0qi2ht84b52an2gu1s4',1425042559,'modx.user.contextTokens|a:0:{}'),
	('83q0teevsnbdv67i9natl7cel4',1425043188,'modx.user.contextTokens|a:0:{}'),
	('jldfc1jlcue44v1oqoioabl383',1425043917,'modx.user.contextTokens|a:0:{}'),
	('75gegmjdd1p3mj10mhpkofktg2',1425045060,'modx.user.contextTokens|a:0:{}'),
	('ug44pr85mqtqvf2qu2knn499c4',1425045061,'modx.user.contextTokens|a:0:{}'),
	('v4i7epn17t21fia9ea7o94fhc1',1425045174,'modx.user.contextTokens|a:0:{}'),
	('968j70p25ukg2oo0nqv69j5rf2',1425046267,'modx.user.contextTokens|a:0:{}'),
	('i4j9m6011n0qi8lcpj3j6eq160',1425046396,'modx.user.contextTokens|a:0:{}'),
	('00l8onodnfa7hq8ilkt6gl4i84',1425059752,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('tt6k7rvenjudlhfuarsmdad990',1425048298,'modx.user.contextTokens|a:0:{}'),
	('o2at9m54qm3uech1r25nstsks4',1425048311,'modx.user.contextTokens|a:0:{}'),
	('ts91o1bjqu9r1mk6pgl55hebe5',1425048317,'modx.user.contextTokens|a:0:{}'),
	('5ff8lud6amg1c2tnpva1n53vs6',1425049357,'modx.user.contextTokens|a:0:{}'),
	('fg1fe3qiqve5gad96ljt2uiao3',1425049388,'modx.user.contextTokens|a:0:{}'),
	('i928dd16h68i6tkkee4sv71h66',1425417321,'modx.user.contextTokens|a:1:{s:3:\"mgr\";i:6;}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.mgr.user.token|s:52:\"modx54d39f5c13a564.78288592_654f0c027a7e8c1.48598424\";modx.mgr.session.cookie.lifetime|i:604800;modx.mgr.user.config|a:0:{}modx.user.6.resourceGroups|a:1:{s:3:\"web\";a:0:{}}newResourceTokens|a:69:{i:0;s:23:\"54f0c0a85460c8.03012952\";i:1;s:23:\"54f0c247c253e6.72507025\";i:2;s:23:\"54f0c35361b3e2.83559099\";i:3;s:23:\"54f0c3a613ba67.63865818\";i:4;s:23:\"54f0c780cef6a7.28744840\";i:5;s:23:\"54f0c7a1579364.88506884\";i:6;s:23:\"54f0c7d54b00c7.67372847\";i:7;s:23:\"54f0c81b75f866.07795882\";i:8;s:23:\"54f0c853830291.48444247\";i:9;s:23:\"54f0c863d3e745.25843818\";i:10;s:23:\"54f0c886695023.95669452\";i:11;s:23:\"54f0c8b1811c49.41279689\";i:12;s:23:\"54f0c8c7c93930.52175349\";i:13;s:23:\"54f0c91e1208e6.20705552\";i:14;s:23:\"54f0c98f655267.20971116\";i:15;s:23:\"54f0c99c73bb69.92690046\";i:16;s:23:\"54f0c9fcebc594.61475025\";i:17;s:23:\"54f0ca4ba7b952.16178619\";i:18;s:23:\"54f0cad2a1bc66.39260709\";i:19;s:23:\"54f0cc0af0a208.51794034\";i:20;s:23:\"54f0cc20081677.13528768\";i:21;s:23:\"54f0cc2f614690.38798193\";i:22;s:23:\"54f0cc7f0e0864.94123434\";i:23;s:23:\"54f0cca088d905.85903779\";i:24;s:23:\"54f0ccb11bed24.14023879\";i:25;s:23:\"54f0ccc1d338e7.05601609\";i:26;s:23:\"54f0ccd76deb51.08080851\";i:27;s:23:\"54f0d0b8f3ff65.10134454\";i:28;s:23:\"54f0e48fac5269.88663061\";i:29;s:23:\"54f0e4a4d38004.78483472\";i:30;s:23:\"54f0e4cd411983.26982259\";i:31;s:23:\"54f0e97f6aa634.69216637\";i:32;s:23:\"54f491637a5417.98903854\";i:33;s:23:\"54f493d8978588.13917002\";i:34;s:23:\"54f49be2347de5.99942435\";i:35;s:23:\"54f49bee6f3072.92650984\";i:36;s:23:\"54f49c09bfbda9.88805889\";i:37;s:23:\"54f49c2820be50.93934900\";i:38;s:23:\"54f49cd4897769.66432823\";i:39;s:23:\"54f49d0e3bacb3.83410064\";i:40;s:23:\"54f49d33dc7252.84321626\";i:41;s:23:\"54f49d4a3a9464.72954279\";i:42;s:23:\"54f49d5e70b911.13422134\";i:43;s:23:\"54f49e003752b9.85283961\";i:44;s:23:\"54f4a0ab754695.29686569\";i:45;s:23:\"54f4b570ef48b8.16052650\";i:46;s:23:\"54f4b57656a285.27920132\";i:47;s:23:\"54f4b57f02a790.23786386\";i:48;s:23:\"54f4b666b123d0.45365230\";i:49;s:23:\"54f4d2db76b979.45676281\";i:50;s:23:\"54f4d43bb3ff98.77721452\";i:51;s:23:\"54f4d7b28d4024.20790970\";i:52;s:23:\"54f5d8f833fe20.09942073\";i:53;s:23:\"54f5d9764d0db2.29201530\";i:54;s:23:\"54f5d9a6a77159.36378463\";i:55;s:23:\"54f5daa0265036.76092353\";i:56;s:23:\"54f5daa874a8e9.62570222\";i:57;s:23:\"54f5df738dbc48.45336090\";i:58;s:23:\"54f5e0590b3a85.22555487\";i:59;s:23:\"54f5e065b9f1a7.73376966\";i:60;s:23:\"54f611cf30aa74.71895465\";i:61;s:23:\"54f61395b8be22.27701473\";i:62;s:23:\"54f6139c5a3403.04468904\";i:63;s:23:\"54f613a3cbf390.58310362\";i:64;s:23:\"54f614e073c493.20812970\";i:65;s:23:\"54f6155dbc3ce5.26219828\";i:66;s:23:\"54f61dd34791e7.63285673\";i:67;s:23:\"54f6245451d0d2.63238741\";i:68;s:23:\"54f62469096a46.40005622\";}modx.user.6.attributes|a:2:{s:3:\"mgr\";a:4:{s:16:\"modAccessContext\";a:2:{s:3:\"mgr\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:0;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:0;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:0;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:0;s:9:\"countries\";b:0;s:6:\"create\";b:1;s:7:\"credits\";b:0;s:15:\"customize_forms\";b:0;s:10:\"dashboards\";b:0;s:8:\"database\";b:0;s:17:\"database_truncate\";b:0;s:15:\"delete_category\";b:0;s:12:\"delete_chunk\";b:0;s:14:\"delete_context\";b:0;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:0;s:13:\"delete_plugin\";b:0;s:18:\"delete_propertyset\";b:0;s:11:\"delete_role\";b:0;s:14:\"delete_snippet\";b:0;s:15:\"delete_template\";b:0;s:9:\"delete_tv\";b:0;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:0;s:10:\"edit_chunk\";b:0;s:12:\"edit_context\";b:0;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:0;s:16:\"edit_propertyset\";b:0;s:9:\"edit_role\";b:0;s:12:\"edit_snippet\";b:0;s:13:\"edit_template\";b:0;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:0;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:0;s:14:\"error_log_view\";b:0;s:13:\"export_static\";b:0;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:0;s:6:\"frames\";b:1;s:4:\"help\";b:0;s:4:\"home\";b:1;s:13:\"import_static\";b:0;s:9:\"languages\";b:0;s:8:\"lexicons\";b:0;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:0;s:5:\"menus\";b:0;s:12:\"menu_reports\";b:0;s:13:\"menu_security\";b:0;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:0;s:11:\"menu_system\";b:0;s:10:\"menu_tools\";b:0;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:0;s:12:\"new_category\";b:0;s:9:\"new_chunk\";b:0;s:11:\"new_context\";b:0;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:0;s:15:\"new_propertyset\";b:0;s:8:\"new_role\";b:0;s:11:\"new_snippet\";b:0;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:0;s:6:\"new_tv\";b:0;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:0;s:13:\"policy_delete\";b:0;s:11:\"policy_edit\";b:0;s:10:\"policy_new\";b:0;s:11:\"policy_save\";b:0;s:22:\"policy_template_delete\";b:0;s:20:\"policy_template_edit\";b:0;s:19:\"policy_template_new\";b:0;s:20:\"policy_template_save\";b:0;s:20:\"policy_template_view\";b:0;s:11:\"policy_view\";b:0;s:13:\"property_sets\";b:0;s:9:\"providers\";b:0;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:20:\"resourcegroup_delete\";b:0;s:18:\"resourcegroup_edit\";b:0;s:17:\"resourcegroup_new\";b:0;s:27:\"resourcegroup_resource_edit\";b:0;s:27:\"resourcegroup_resource_list\";b:0;s:18:\"resourcegroup_save\";b:0;s:18:\"resourcegroup_view\";b:0;s:18:\"resource_duplicate\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:0;s:10:\"save_chunk\";b:0;s:12:\"save_context\";b:0;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:0;s:16:\"save_propertyset\";b:0;s:9:\"save_role\";b:0;s:12:\"save_snippet\";b:0;s:13:\"save_template\";b:0;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:0;s:7:\"sources\";b:1;s:13:\"source_delete\";b:0;s:11:\"source_edit\";b:0;s:11:\"source_save\";b:0;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:0;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:0;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:0;s:14:\"usergroup_edit\";b:0;s:13:\"usergroup_new\";b:0;s:14:\"usergroup_save\";b:0;s:19:\"usergroup_user_edit\";b:0;s:19:\"usergroup_user_list\";b:0;s:14:\"usergroup_view\";b:0;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:0;s:13:\"view_document\";b:1;s:12:\"view_element\";b:0;s:13:\"view_eventlog\";b:0;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:0;s:16:\"view_propertyset\";b:0;s:9:\"view_role\";b:0;s:12:\"view_snippet\";b:0;s:12:\"view_sysinfo\";b:0;s:13:\"view_template\";b:0;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:0;s:10:\"workspaces\";b:0;}}}s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:0;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:0;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:0;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:0;s:9:\"countries\";b:0;s:6:\"create\";b:1;s:7:\"credits\";b:0;s:15:\"customize_forms\";b:0;s:10:\"dashboards\";b:0;s:8:\"database\";b:0;s:17:\"database_truncate\";b:0;s:15:\"delete_category\";b:0;s:12:\"delete_chunk\";b:0;s:14:\"delete_context\";b:0;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:0;s:13:\"delete_plugin\";b:0;s:18:\"delete_propertyset\";b:0;s:11:\"delete_role\";b:0;s:14:\"delete_snippet\";b:0;s:15:\"delete_template\";b:0;s:9:\"delete_tv\";b:0;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:0;s:10:\"edit_chunk\";b:0;s:12:\"edit_context\";b:0;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:0;s:16:\"edit_propertyset\";b:0;s:9:\"edit_role\";b:0;s:12:\"edit_snippet\";b:0;s:13:\"edit_template\";b:0;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:0;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:0;s:14:\"error_log_view\";b:0;s:13:\"export_static\";b:0;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:0;s:6:\"frames\";b:1;s:4:\"help\";b:0;s:4:\"home\";b:1;s:13:\"import_static\";b:0;s:9:\"languages\";b:0;s:8:\"lexicons\";b:0;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:0;s:5:\"menus\";b:0;s:12:\"menu_reports\";b:0;s:13:\"menu_security\";b:0;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:0;s:11:\"menu_system\";b:0;s:10:\"menu_tools\";b:0;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:0;s:12:\"new_category\";b:0;s:9:\"new_chunk\";b:0;s:11:\"new_context\";b:0;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:0;s:15:\"new_propertyset\";b:0;s:8:\"new_role\";b:0;s:11:\"new_snippet\";b:0;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:0;s:6:\"new_tv\";b:0;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:0;s:13:\"policy_delete\";b:0;s:11:\"policy_edit\";b:0;s:10:\"policy_new\";b:0;s:11:\"policy_save\";b:0;s:22:\"policy_template_delete\";b:0;s:20:\"policy_template_edit\";b:0;s:19:\"policy_template_new\";b:0;s:20:\"policy_template_save\";b:0;s:20:\"policy_template_view\";b:0;s:11:\"policy_view\";b:0;s:13:\"property_sets\";b:0;s:9:\"providers\";b:0;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:20:\"resourcegroup_delete\";b:0;s:18:\"resourcegroup_edit\";b:0;s:17:\"resourcegroup_new\";b:0;s:27:\"resourcegroup_resource_edit\";b:0;s:27:\"resourcegroup_resource_list\";b:0;s:18:\"resourcegroup_save\";b:0;s:18:\"resourcegroup_view\";b:0;s:18:\"resource_duplicate\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:0;s:10:\"save_chunk\";b:0;s:12:\"save_context\";b:0;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:0;s:16:\"save_propertyset\";b:0;s:9:\"save_role\";b:0;s:12:\"save_snippet\";b:0;s:13:\"save_template\";b:0;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:0;s:7:\"sources\";b:1;s:13:\"source_delete\";b:0;s:11:\"source_edit\";b:0;s:11:\"source_save\";b:0;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:0;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:0;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:0;s:14:\"usergroup_edit\";b:0;s:13:\"usergroup_new\";b:0;s:14:\"usergroup_save\";b:0;s:19:\"usergroup_user_edit\";b:0;s:19:\"usergroup_user_list\";b:0;s:14:\"usergroup_view\";b:0;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:0;s:13:\"view_document\";b:1;s:12:\"view_element\";b:0;s:13:\"view_eventlog\";b:0;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:0;s:16:\"view_propertyset\";b:0;s:9:\"view_role\";b:0;s:12:\"view_snippet\";b:0;s:12:\"view_sysinfo\";b:0;s:13:\"view_template\";b:0;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:0;s:10:\"workspaces\";b:0;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:4:{i:1;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}i:4;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}i:5;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}i:6;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}}}s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:2:{s:3:\"mgr\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:0;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:0;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:0;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:0;s:9:\"countries\";b:0;s:6:\"create\";b:1;s:7:\"credits\";b:0;s:15:\"customize_forms\";b:0;s:10:\"dashboards\";b:0;s:8:\"database\";b:0;s:17:\"database_truncate\";b:0;s:15:\"delete_category\";b:0;s:12:\"delete_chunk\";b:0;s:14:\"delete_context\";b:0;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:0;s:13:\"delete_plugin\";b:0;s:18:\"delete_propertyset\";b:0;s:11:\"delete_role\";b:0;s:14:\"delete_snippet\";b:0;s:15:\"delete_template\";b:0;s:9:\"delete_tv\";b:0;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:0;s:10:\"edit_chunk\";b:0;s:12:\"edit_context\";b:0;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:0;s:16:\"edit_propertyset\";b:0;s:9:\"edit_role\";b:0;s:12:\"edit_snippet\";b:0;s:13:\"edit_template\";b:0;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:0;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:0;s:14:\"error_log_view\";b:0;s:13:\"export_static\";b:0;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:0;s:6:\"frames\";b:1;s:4:\"help\";b:0;s:4:\"home\";b:1;s:13:\"import_static\";b:0;s:9:\"languages\";b:0;s:8:\"lexicons\";b:0;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:0;s:5:\"menus\";b:0;s:12:\"menu_reports\";b:0;s:13:\"menu_security\";b:0;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:0;s:11:\"menu_system\";b:0;s:10:\"menu_tools\";b:0;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:0;s:12:\"new_category\";b:0;s:9:\"new_chunk\";b:0;s:11:\"new_context\";b:0;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:0;s:15:\"new_propertyset\";b:0;s:8:\"new_role\";b:0;s:11:\"new_snippet\";b:0;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:0;s:6:\"new_tv\";b:0;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:0;s:13:\"policy_delete\";b:0;s:11:\"policy_edit\";b:0;s:10:\"policy_new\";b:0;s:11:\"policy_save\";b:0;s:22:\"policy_template_delete\";b:0;s:20:\"policy_template_edit\";b:0;s:19:\"policy_template_new\";b:0;s:20:\"policy_template_save\";b:0;s:20:\"policy_template_view\";b:0;s:11:\"policy_view\";b:0;s:13:\"property_sets\";b:0;s:9:\"providers\";b:0;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:20:\"resourcegroup_delete\";b:0;s:18:\"resourcegroup_edit\";b:0;s:17:\"resourcegroup_new\";b:0;s:27:\"resourcegroup_resource_edit\";b:0;s:27:\"resourcegroup_resource_list\";b:0;s:18:\"resourcegroup_save\";b:0;s:18:\"resourcegroup_view\";b:0;s:18:\"resource_duplicate\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:0;s:10:\"save_chunk\";b:0;s:12:\"save_context\";b:0;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:0;s:16:\"save_propertyset\";b:0;s:9:\"save_role\";b:0;s:12:\"save_snippet\";b:0;s:13:\"save_template\";b:0;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:0;s:7:\"sources\";b:1;s:13:\"source_delete\";b:0;s:11:\"source_edit\";b:0;s:11:\"source_save\";b:0;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:0;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:0;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:0;s:14:\"usergroup_edit\";b:0;s:13:\"usergroup_new\";b:0;s:14:\"usergroup_save\";b:0;s:19:\"usergroup_user_edit\";b:0;s:19:\"usergroup_user_list\";b:0;s:14:\"usergroup_view\";b:0;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:0;s:13:\"view_document\";b:1;s:12:\"view_element\";b:0;s:13:\"view_eventlog\";b:0;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:0;s:16:\"view_propertyset\";b:0;s:9:\"view_role\";b:0;s:12:\"view_snippet\";b:0;s:12:\"view_sysinfo\";b:0;s:13:\"view_template\";b:0;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:0;s:10:\"workspaces\";b:0;}}}s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:172:{s:5:\"about\";b:0;s:18:\"access_permissions\";b:1;s:7:\"actions\";b:0;s:15:\"change_password\";b:1;s:14:\"change_profile\";b:1;s:8:\"charsets\";b:0;s:9:\"class_map\";b:1;s:10:\"components\";b:1;s:13:\"content_types\";b:0;s:9:\"countries\";b:0;s:6:\"create\";b:1;s:7:\"credits\";b:0;s:15:\"customize_forms\";b:0;s:10:\"dashboards\";b:0;s:8:\"database\";b:0;s:17:\"database_truncate\";b:0;s:15:\"delete_category\";b:0;s:12:\"delete_chunk\";b:0;s:14:\"delete_context\";b:0;s:15:\"delete_document\";b:1;s:15:\"delete_eventlog\";b:0;s:13:\"delete_plugin\";b:0;s:18:\"delete_propertyset\";b:0;s:11:\"delete_role\";b:0;s:14:\"delete_snippet\";b:0;s:15:\"delete_template\";b:0;s:9:\"delete_tv\";b:0;s:11:\"delete_user\";b:1;s:15:\"directory_chmod\";b:1;s:16:\"directory_create\";b:1;s:14:\"directory_list\";b:1;s:16:\"directory_remove\";b:1;s:16:\"directory_update\";b:1;s:13:\"edit_category\";b:0;s:10:\"edit_chunk\";b:0;s:12:\"edit_context\";b:0;s:13:\"edit_document\";b:1;s:11:\"edit_locked\";b:1;s:11:\"edit_plugin\";b:0;s:16:\"edit_propertyset\";b:0;s:9:\"edit_role\";b:0;s:12:\"edit_snippet\";b:0;s:13:\"edit_template\";b:0;s:7:\"edit_tv\";b:1;s:9:\"edit_user\";b:1;s:12:\"element_tree\";b:0;s:11:\"empty_cache\";b:1;s:15:\"error_log_erase\";b:0;s:14:\"error_log_view\";b:0;s:13:\"export_static\";b:0;s:11:\"file_create\";b:1;s:9:\"file_list\";b:1;s:12:\"file_manager\";b:1;s:11:\"file_remove\";b:1;s:9:\"file_tree\";b:1;s:11:\"file_update\";b:1;s:11:\"file_upload\";b:1;s:9:\"file_view\";b:1;s:14:\"flush_sessions\";b:0;s:6:\"frames\";b:1;s:4:\"help\";b:0;s:4:\"home\";b:1;s:13:\"import_static\";b:0;s:9:\"languages\";b:0;s:8:\"lexicons\";b:0;s:4:\"list\";b:1;s:4:\"load\";b:1;s:6:\"logout\";b:1;s:4:\"logs\";b:0;s:5:\"menus\";b:0;s:12:\"menu_reports\";b:0;s:13:\"menu_security\";b:0;s:9:\"menu_site\";b:1;s:12:\"menu_support\";b:0;s:11:\"menu_system\";b:0;s:10:\"menu_tools\";b:0;s:9:\"menu_user\";b:1;s:8:\"messages\";b:1;s:10:\"namespaces\";b:0;s:12:\"new_category\";b:0;s:9:\"new_chunk\";b:0;s:11:\"new_context\";b:0;s:12:\"new_document\";b:1;s:20:\"new_document_in_root\";b:1;s:10:\"new_plugin\";b:0;s:15:\"new_propertyset\";b:0;s:8:\"new_role\";b:0;s:11:\"new_snippet\";b:0;s:19:\"new_static_resource\";b:1;s:11:\"new_symlink\";b:1;s:12:\"new_template\";b:0;s:6:\"new_tv\";b:0;s:8:\"new_user\";b:1;s:11:\"new_weblink\";b:1;s:8:\"packages\";b:0;s:13:\"policy_delete\";b:0;s:11:\"policy_edit\";b:0;s:10:\"policy_new\";b:0;s:11:\"policy_save\";b:0;s:22:\"policy_template_delete\";b:0;s:20:\"policy_template_edit\";b:0;s:19:\"policy_template_new\";b:0;s:20:\"policy_template_save\";b:0;s:20:\"policy_template_view\";b:0;s:11:\"policy_view\";b:0;s:13:\"property_sets\";b:0;s:9:\"providers\";b:0;s:16:\"publish_document\";b:1;s:13:\"purge_deleted\";b:1;s:6:\"remove\";b:1;s:12:\"remove_locks\";b:1;s:20:\"resourcegroup_delete\";b:0;s:18:\"resourcegroup_edit\";b:0;s:17:\"resourcegroup_new\";b:0;s:27:\"resourcegroup_resource_edit\";b:0;s:27:\"resourcegroup_resource_list\";b:0;s:18:\"resourcegroup_save\";b:0;s:18:\"resourcegroup_view\";b:0;s:18:\"resource_duplicate\";b:1;s:21:\"resource_quick_create\";b:1;s:21:\"resource_quick_update\";b:1;s:13:\"resource_tree\";b:1;s:4:\"save\";b:1;s:13:\"save_category\";b:0;s:10:\"save_chunk\";b:0;s:12:\"save_context\";b:0;s:13:\"save_document\";b:1;s:11:\"save_plugin\";b:0;s:16:\"save_propertyset\";b:0;s:9:\"save_role\";b:0;s:12:\"save_snippet\";b:0;s:13:\"save_template\";b:0;s:7:\"save_tv\";b:1;s:9:\"save_user\";b:1;s:6:\"search\";b:1;s:8:\"settings\";b:0;s:7:\"sources\";b:1;s:13:\"source_delete\";b:0;s:11:\"source_edit\";b:0;s:11:\"source_save\";b:0;s:11:\"source_view\";b:1;s:11:\"steal_locks\";b:1;s:21:\"tree_show_element_ids\";b:0;s:22:\"tree_show_resource_ids\";b:1;s:17:\"undelete_document\";b:1;s:25:\"unlock_element_properties\";b:0;s:18:\"unpublish_document\";b:1;s:16:\"usergroup_delete\";b:0;s:14:\"usergroup_edit\";b:0;s:13:\"usergroup_new\";b:0;s:14:\"usergroup_save\";b:0;s:19:\"usergroup_user_edit\";b:0;s:19:\"usergroup_user_list\";b:0;s:14:\"usergroup_view\";b:0;s:4:\"view\";b:1;s:13:\"view_category\";b:1;s:10:\"view_chunk\";b:1;s:12:\"view_context\";b:0;s:13:\"view_document\";b:1;s:12:\"view_element\";b:0;s:13:\"view_eventlog\";b:0;s:12:\"view_offline\";b:1;s:11:\"view_plugin\";b:0;s:16:\"view_propertyset\";b:0;s:9:\"view_role\";b:0;s:12:\"view_snippet\";b:0;s:12:\"view_sysinfo\";b:0;s:13:\"view_template\";b:0;s:7:\"view_tv\";b:1;s:16:\"view_unpublished\";b:1;s:9:\"view_user\";b:0;s:10:\"workspaces\";b:0;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:4:{i:1;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}i:4;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}i:5;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}i:6;a:1:{i:0;a:3:{s:9:\"principal\";s:1:\"5\";s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:7:{s:6:\"create\";b:1;s:4:\"copy\";b:1;s:4:\"load\";b:1;s:4:\"list\";b:1;s:4:\"save\";b:1;s:6:\"remove\";b:1;s:4:\"view\";b:1;}}}}}}'),
	('p0s772uecj0loto2i8vl3q60n4',1425049689,'modx.user.contextTokens|a:0:{}'),
	('75vpj1dkm9i361lbfoc027kom5',1425050357,'modx.user.contextTokens|a:0:{}'),
	('9g5edjhtrvq280v3onja75bm53',1425051388,'modx.user.contextTokens|a:0:{}'),
	('j2ri7kbgvrl5i7n75t44rc4u02',1425053310,'modx.user.contextTokens|a:0:{}'),
	('tu3dnlu2ltqe6ltsmjsc2vf7c1',1425053536,'modx.user.contextTokens|a:0:{}'),
	('u71vuorbm3h52qqbnpi7j5thq1',1425053917,'modx.user.contextTokens|a:0:{}'),
	('fpmnfjn5alsvhol9tbtonr36v6',1425054021,'modx.user.contextTokens|a:0:{}'),
	('h7ugb32obqos9pbc3r4l7l4j70',1425054403,'modx.user.contextTokens|a:0:{}'),
	('12avuuuve6k6cpn2ls2ke2amt6',1425056363,'modx.user.contextTokens|a:0:{}'),
	('6fjk5177v5nf32asbupfbilnp4',1425236884,'modx.user.contextTokens|a:0:{}'),
	('o6iecbnbg42ebgm3mivkjd4270',1425236884,'modx.user.contextTokens|a:0:{}'),
	('4s0o7usiman86rrpsbvpmuib31',1425237308,'modx.user.contextTokens|a:0:{}'),
	('hl970crbklchei7hjh81siv2o6',1425238904,'modx.user.contextTokens|a:0:{}'),
	('gtamoe8l7t71u227nsbdvt14e0',1425240282,'modx.user.contextTokens|a:0:{}'),
	('81krlpla5asqk3pqlkvgsg5he3',1425240652,'modx.user.contextTokens|a:0:{}'),
	('8orsuqso3l9hsi87utpglvu1k0',1425241711,'modx.user.contextTokens|a:0:{}'),
	('pda3dg0a5g7s6kluvkgoqti6v0',1425242370,'modx.user.contextTokens|a:0:{}'),
	('7p3en3cl9vesk42uf0tdlhjh57',1425243887,'modx.user.contextTokens|a:0:{}'),
	('pd0bbdl24oc604gr5b7vt6avu4',1425244096,'modx.user.contextTokens|a:0:{}'),
	('f0p7madt1122tvkhc5h51t7gm7',1425244097,'modx.user.contextTokens|a:0:{}'),
	('a4hkf6svu6usup79mjo0eblhb6',1425244197,'modx.user.contextTokens|a:0:{}'),
	('1i1m6erdaqmmc8h29ma0q980i3',1425246941,'modx.user.contextTokens|a:0:{}'),
	('fm6r8d8qp1dnv07l0ksr2b8im5',1425247619,'modx.user.contextTokens|a:0:{}'),
	('uuv3r2rvs7263il13k4qgokms0',1425247772,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('le3ep3m87vb3aqtgpqc2qfv720',1425247900,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('f088ijlrig61p8ah18e9obop30',1425248169,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('pto7v4vtn7p352hvojt73gmi47',1425248261,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('vuvft3j851s6d7fj3e2hd5ki45',1425248290,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('8ks0a8q1jo5ui78ejtanf4icc6',1425248643,'modx.user.contextTokens|a:0:{}'),
	('6gk8pg62srdduecp3s6d14gc23',1425248831,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('8utok52f01a5qstmj4qkvduli0',1425249611,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('jrteon9i089fq3o6i7a0mrut62',1425249919,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('kos5geiq25a324hrohumtordo2',1425249985,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('n1c085e6leou95v53n6211mdl7',1425249987,'modx.user.contextTokens|a:0:{}'),
	('j2vr190f4ca9bnhi9po1lb8lg7',1425249993,'modx.user.contextTokens|a:0:{}'),
	('tstg31rubqepn7d198j69gi657',1425250023,'modx.user.contextTokens|a:0:{}'),
	('vmcdeskmdskpdjt72c6ljcnt34',1425250074,'modx.user.contextTokens|a:0:{}'),
	('pda2e5scfgaqlo1h5q9csiskm6',1425250107,'modx.user.contextTokens|a:0:{}'),
	('o29m1jr8ifkdm89slmd01skhv4',1425250133,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('rd4kr4l6rd1moejj5bnm7ck2h7',1425250134,'modx.user.contextTokens|a:0:{}'),
	('vfm73d4jdf6uht9cjl9ejhcfq1',1425250335,'modx.user.contextTokens|a:0:{}'),
	('4gls21rum3b41gnms2h3j6f1m4',1425250820,'modx.user.contextTokens|a:0:{}'),
	('2qdr7gofoh37u2t9flqjqnm8s3',1425250968,'modx.user.contextTokens|a:0:{}'),
	('2q40c89tkj69iuoabp4rh4b8n3',1425250969,'modx.user.contextTokens|a:0:{}'),
	('71d0lbf2g4k4d27cph7s0s9a94',1425250985,'modx.user.contextTokens|a:0:{}'),
	('tsr7e17scho0opbtvuoi7m72d7',1425250986,'modx.user.contextTokens|a:0:{}'),
	('iqnu22f13hilsqpc94086lqnp4',1425252354,'modx.user.contextTokens|a:0:{}'),
	('4gkv2mdjjkqhtfbck1l76d89b5',1425252355,'modx.user.contextTokens|a:0:{}'),
	('ki1fjlm60352hllaa284g9j1j6',1425255964,'modx.user.contextTokens|a:0:{}'),
	('4cq7o4atv9hip6o65a51lhkhc6',1425257047,''),
	('v3kju1ghab8185023g5edlk9a1',1425257047,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('70b413k5kj2c6q58cttqtl1i00',1425257136,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('vhf6ji60oo0fshk7jukgmc4v33',1425258023,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('d89968iicjeu8r9plaqm1g8nt4',1425258057,'modx.user.contextTokens|a:0:{}'),
	('nvaqa58285ljp8fjsi7g7so214',1425258058,'modx.user.contextTokens|a:0:{}'),
	('adb4h65mi2p7eu56qv5rbplns1',1425258650,'modx.user.contextTokens|a:0:{}'),
	('3fnd6h573q84ti51h8cdh3d4n0',1425258650,'modx.user.contextTokens|a:0:{}'),
	('tv4lqckhto91nu7to190ljq8t4',1425259641,'modx.user.contextTokens|a:0:{}'),
	('e83aqasl14am4ags9bsb37l446',1425260927,'modx.user.contextTokens|a:0:{}'),
	('9fnkuo7ufmcuc4b4r8tpuaogt6',1425263322,'modx.user.contextTokens|a:0:{}'),
	('oi7m32u0hl6meci67lt5h3q2a3',1425263717,'modx.user.contextTokens|a:0:{}'),
	('98rn9d7fmf6lmarcm2ru001e25',1425264614,'modx.user.contextTokens|a:0:{}'),
	('0goa62mutjp7f35a78n9lr2of3',1425266219,'modx.user.contextTokens|a:0:{}'),
	('ssakd5pm7rp0a6rkeum043e7v0',1425268066,'modx.user.contextTokens|a:0:{}'),
	('q2flduhphnsgcggqlv1ddkrtd7',1425271378,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('g05nl7e0lnst2gecsl89963ko6',1425271856,'modx.user.contextTokens|a:0:{}'),
	('3uo2fl1l1ggdtnlvf3hj198c00',1425272112,'modx.user.contextTokens|a:0:{}'),
	('p4v2i4e7n195e3semph153bh90',1425272754,'modx.user.contextTokens|a:0:{}'),
	('al3c40l12a73he4prbnjbkqj35',1425272763,'modx.user.contextTokens|a:0:{}'),
	('hcvmr61jdgjjddpbbo1n7r3kj2',1425272764,'modx.user.contextTokens|a:0:{}'),
	('0hqil9ktj7oq8bgv5qufkr9jv6',1425273002,'modx.user.contextTokens|a:0:{}'),
	('ar1lgno18tn9hq2sofo0a2utf3',1425274962,'modx.user.contextTokens|a:0:{}'),
	('f8gbbgutj9gum6llgh84bebfd2',1425275703,'modx.user.contextTokens|a:0:{}'),
	('ps9ntng9l0u9q4uo22rr5cmvq0',1425275703,'modx.user.contextTokens|a:0:{}'),
	('6hi4j3b3devipemrt1um8ag1p6',1425277778,'modx.user.contextTokens|a:0:{}'),
	('o35idnaptihev559sn9kgi49l0',1425278875,'modx.user.contextTokens|a:0:{}'),
	('5mg5d8531utcbd0qnot18v8121',1425279060,'modx.user.contextTokens|a:0:{}'),
	('1iilljb7t8coopoc73afs7ctv5',1425279060,'modx.user.contextTokens|a:0:{}'),
	('vafcs26lorl6jnb42eib5hnmk4',1425284414,'modx.user.contextTokens|a:0:{}'),
	('o2rujejlavse5khcp639rkl8k2',1425284414,'modx.user.contextTokens|a:0:{}'),
	('rqmpketftqv23efhr0j3a050o3',1425288061,'modx.user.contextTokens|a:0:{}'),
	('q6eb4k9rjfnr7sl7bilnt5p063',1425288452,'modx.user.contextTokens|a:0:{}'),
	('hra3ed89co0dgmecp9a4brt0q4',1425289662,'modx.user.contextTokens|a:0:{}'),
	('7hk8hlssvr5qadi24j2jkbc905',1425289663,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('kkuh0r0klvp7d6pb399m4c11e5',1425289893,'modx.user.contextTokens|a:0:{}'),
	('grn95h2u20dc7p51aqt0nu7f41',1425290707,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('c4869ccoqp7j46fh9fsul5nmf7',1425292370,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('2jjgamnhj5ik9ad1p6g68q9an5',1425294017,'modx.user.contextTokens|a:0:{}'),
	('721eh7epmk01nlghhn4b0lhib0',1425295463,'modx.user.contextTokens|a:0:{}'),
	('2du1a9kr2jjvorb93ahs2q4jf4',1425295670,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('07f2k82hepdk7aajm9mpg4s562',1425295955,'modx.user.contextTokens|a:0:{}'),
	('m6qeiaosbivajvtoe92eso4nv4',1425295967,'modx.user.contextTokens|a:0:{}'),
	('elnse69mbut2bbbbtuidogiaj6',1425296440,'modx.user.contextTokens|a:0:{}'),
	('3dvc88sv7hr81i7i217hjqbes3',1425296518,'modx.user.contextTokens|a:0:{}'),
	('g35qheij7npdogptpd264ml8t5',1425296537,'modx.user.contextTokens|a:0:{}'),
	('2psnnj63a2sgjgbn9q6f5og5g0',1425297520,'modx.user.contextTokens|a:0:{}'),
	('mrk9mqgi4igqcgpha81h641097',1425297579,'modx.user.contextTokens|a:0:{}'),
	('f5mfkdfion63ocm2eli2m4me52',1425297604,'modx.user.contextTokens|a:0:{}'),
	('07ja48dsraguqs3h8ekrs8rd16',1425298067,'modx.user.contextTokens|a:0:{}'),
	('9k90nhcno9m49ju65q5u4ohmf6',1425298068,'modx.user.contextTokens|a:0:{}'),
	('pt27oubedt17486423065solq2',1425298069,'modx.user.contextTokens|a:0:{}'),
	('bb9grju7jjeeb0lgk5fp5j8j03',1425298425,'modx.user.contextTokens|a:0:{}'),
	('chla77lgh29m8b1std6rsb1tq3',1425298532,'modx.user.contextTokens|a:0:{}'),
	('4h87mshb2312vofqt7jgho2si7',1425298559,'modx.user.contextTokens|a:0:{}'),
	('q838k2j3pq6f24t4lp827gni47',1425298626,'modx.user.contextTokens|a:0:{}'),
	('kmoe37p2pod7htel33foc56k95',1425299198,'modx.user.contextTokens|a:0:{}'),
	('trfds2nv4d37f19tveau6rvkq4',1425299286,'modx.user.contextTokens|a:0:{}'),
	('j5t921gkkgdvcrkk46h00ul4f2',1425299448,'modx.user.contextTokens|a:0:{}'),
	('vgedftu3n4llhai4lb5kj4o5i0',1425299449,'modx.user.contextTokens|a:0:{}'),
	('dlqq5qrjhhqjtiif3gtg7nv766',1425299450,'modx.user.contextTokens|a:0:{}'),
	('b9re6oad5j6uuk50uepo6qiag3',1425299451,'modx.user.contextTokens|a:0:{}'),
	('21jv8abma57ucmm6tan0kb4l51',1425299714,'modx.user.contextTokens|a:0:{}'),
	('5adsm01mfh8rost593blt79fm1',1425299724,'modx.user.contextTokens|a:0:{}'),
	('jrdrgck3dtv6jlqvhog7umcuk7',1425299975,'modx.user.contextTokens|a:0:{}'),
	('t65glur3lrjps5gbl2jh0bv407',1425299981,'modx.user.contextTokens|a:0:{}'),
	('j4kqjr42onufa8tofffv312585',1425300369,'modx.user.contextTokens|a:0:{}'),
	('bc3i3d24b1053hdtjrcfmoa7l7',1425300627,'modx.user.contextTokens|a:0:{}'),
	('1tbr42pslu4atj3uu1c0jh0fb3',1425300627,'modx.user.contextTokens|a:0:{}'),
	('ad3g0sq2nrqb1fikftupcd5v65',1425300628,'modx.user.contextTokens|a:0:{}'),
	('3ch4k04claerid0iqcrksnaql6',1425302211,'modx.user.contextTokens|a:0:{}'),
	('7fea8ugkmm6brau0cusa9ma7b5',1425303108,'modx.user.contextTokens|a:0:{}'),
	('9201n7c7gt0mvqmk1ihh5qbte0',1425303111,'modx.user.contextTokens|a:0:{}'),
	('itbffo4cni7f19o6g40t3997j3',1425303476,'modx.user.contextTokens|a:0:{}'),
	('53mnvgnb80ht91k2e98hluhop5',1425303484,'modx.user.contextTokens|a:0:{}'),
	('hmhbd4fenoq5482s7ni5q46fb6',1425303592,'modx.user.contextTokens|a:0:{}'),
	('lf2j9o8ioabufvhjplnb6jqa53',1425303599,'modx.user.contextTokens|a:0:{}'),
	('co9s2h84l2ev84ipc3kn50t7d5',1425303604,'modx.user.contextTokens|a:0:{}'),
	('dg5epqhpv8377bsfs1mr3dsqn4',1425303607,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('78o4qmkuhmqnmjgd5frkng7k61',1425303610,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('nhdcsu2ojp3qj87kfjjcgge7t1',1425303614,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('27qievl65923mlcfuu5doas3a4',1425303637,'modx.user.contextTokens|a:0:{}'),
	('1bkh10j6qnocm3d3bo04frhro4',1425303687,'modx.user.contextTokens|a:0:{}'),
	('8vvg52f3jul8bkouhn0m1vgpt3',1425303691,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('sqsv1hu5a277pip83ud7u3c997',1425303694,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('3v9npml0gok0firijsblvm3so7',1425303702,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('3ikvpuptr5a8eea53c2sichj95',1425303714,'modx.user.contextTokens|a:0:{}'),
	('1l60smmlvhhdes0tgfevdkbpo7',1425303740,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('o4nr9r8luned1k9b72tjb0ap17',1425303758,'modx.user.contextTokens|a:0:{}'),
	('9vp1922mtupu3egt15mufqnco1',1425303776,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('72rmueihdhjkh6olilb9t0fq37',1425303780,'modx.user.contextTokens|a:0:{}'),
	('aov9jgdnq4j14t69ftmf7qodj2',1425303789,'modx.user.contextTokens|a:0:{}'),
	('6s9ec02m6nbhihic2noqvdt750',1425304866,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('n80mlqjqios9bpjdhorhioa437',1425307072,'modx.user.contextTokens|a:0:{}'),
	('9qrfpql7a7jaglvuc2k17pmla3',1425307073,'modx.user.contextTokens|a:0:{}'),
	('p4amer6cff141i38h2kjl1pvl3',1425307075,'modx.user.contextTokens|a:0:{}'),
	('1a7lqajkj6fjkhr2tic4b811j1',1425307076,'modx.user.contextTokens|a:0:{}'),
	('rcgbgp3pjt2hnqmdthnhke0920',1425307445,'modx.user.contextTokens|a:0:{}'),
	('prkkpha877lgtf1l9dp8begn37',1425307645,'modx.user.contextTokens|a:0:{}'),
	('l4h6b75e11tfiivl983fa689h2',1425307651,'modx.user.contextTokens|a:0:{}'),
	('p93n4bf0jtm8kr32fb61gie2d2',1425307652,'modx.user.contextTokens|a:0:{}'),
	('tbj5icm9jpd6s8optm90v7kh46',1425307653,'modx.user.contextTokens|a:0:{}'),
	('pap9kcc4ducq7mdpfsi1mvg1b0',1425308015,'modx.user.contextTokens|a:0:{}'),
	('o8dg6ijib3ap8nkf94qh8bmgj7',1425308016,'modx.user.contextTokens|a:0:{}'),
	('3i1s9ptu69asm6oicui0ec1h97',1425308017,'modx.user.contextTokens|a:0:{}'),
	('hms6qu2eutludsnvini406uh07',1425308018,'modx.user.contextTokens|a:0:{}'),
	('nksoguh79v67bf4s0ciis2bf04',1425308359,'modx.user.contextTokens|a:0:{}'),
	('s3nus8vusdscfp25qtvp2a90e7',1425308636,'modx.user.contextTokens|a:0:{}'),
	('ekknnoe9a433v36bfbm15bdvc0',1425312074,'modx.user.contextTokens|a:0:{}'),
	('46f7q2i0hb0m0vkqiu4gv8gm95',1425317128,'modx.user.contextTokens|a:0:{}'),
	('ins0m5t81emfgfj5fpul49mae2',1425317190,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('o4lt21j2elf2lrhnp2gihdacq4',1425317610,'modx.user.contextTokens|a:0:{}'),
	('tk4l929jj21os6bvbol6311rb1',1425317611,'modx.user.contextTokens|a:0:{}'),
	('04og4gver3nt8poe7g4hss6mg5',1425317612,'modx.user.contextTokens|a:0:{}'),
	('1sk56187h55l4ljenl7bc7voe2',1425317613,'modx.user.contextTokens|a:0:{}'),
	('cr3brhk7spgemvsnnr5qdnles0',1425317613,'modx.user.contextTokens|a:0:{}'),
	('lfu6r0eabcfkarqtoqekcdduv2',1425317614,'modx.user.contextTokens|a:0:{}'),
	('m4d11siif8pt3lk6gtbjc8a2c1',1425317614,'modx.user.contextTokens|a:0:{}'),
	('rflnjfd7u4dqkltrrnolilqbc7',1425317645,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('clthfkmnmfgujcfqle6u2cquh5',1425317661,'modx.user.contextTokens|a:0:{}'),
	('hq9f571cn0rec438klgraetq97',1425317662,'modx.user.contextTokens|a:0:{}'),
	('n5qrj3lg2f8uve88o52ijpe0o3',1425317662,'modx.user.contextTokens|a:0:{}'),
	('jqdq8fvjlg9be74r1fvsvuvmi4',1425317662,'modx.user.contextTokens|a:0:{}'),
	('4u9lhnqhh8c4jrna7566icae90',1425317662,'modx.user.contextTokens|a:0:{}'),
	('rv9vd5op2gn0kkkpou9vqoios3',1425317662,'modx.user.contextTokens|a:0:{}'),
	('o94as96ti5ndg4ijv21vvs21r7',1425317662,'modx.user.contextTokens|a:0:{}'),
	('0sa083papviooet8rclv48nr06',1425317662,'modx.user.contextTokens|a:0:{}'),
	('f25ipbmrfj0732024e14rpu1m1',1425317663,'modx.user.contextTokens|a:0:{}'),
	('8lhq34s75j4h6jc3qqt3m2ldo5',1425317663,'modx.user.contextTokens|a:0:{}'),
	('tc9okjav9t3c9a3mdnphtnoue5',1425317663,'modx.user.contextTokens|a:0:{}'),
	('hiemdqu4noh7gtgvf4mqh6v124',1425317663,'modx.user.contextTokens|a:0:{}'),
	('4n6i57okfrgcc6i12g26uol7g0',1425317668,'modx.user.contextTokens|a:0:{}'),
	('t7mija3dr6ud00cf3fcl6peer1',1425317668,'modx.user.contextTokens|a:0:{}'),
	('3ptqn8dq3dlck6neqirjt9jci5',1425317668,'modx.user.contextTokens|a:0:{}'),
	('nmt91tisgt834mfmfc0j95qes1',1425317668,'modx.user.contextTokens|a:0:{}'),
	('v1g7n5qnd5phkq4cg18rlf9fc3',1425317669,'modx.user.contextTokens|a:0:{}'),
	('s2icv5hc85r59ajdctm453tqm5',1425317669,'modx.user.contextTokens|a:0:{}'),
	('v44l8ou2bbimtr333dohv4qjo1',1425317670,'modx.user.contextTokens|a:0:{}'),
	('pismfulve41jaa6vvn0264bh41',1425317670,'modx.user.contextTokens|a:0:{}'),
	('jhgu5ure4792h2katt305ij381',1425317670,'modx.user.contextTokens|a:0:{}'),
	('jqfarg9ttrh4h4vgc96jtr1gl7',1425317670,'modx.user.contextTokens|a:0:{}'),
	('qkb9j205on6rol8dk0908bf5h6',1425317670,'modx.user.contextTokens|a:0:{}'),
	('5edf3p0cdi6bjgore9i79h6ik6',1425317671,'modx.user.contextTokens|a:0:{}'),
	('f7gn3q0gl8s0f5uvcq12bvtlk2',1425317683,'modx.user.contextTokens|a:0:{}'),
	('00372j1r8lha3c8g9qab1pgsu0',1425317684,'modx.user.contextTokens|a:0:{}'),
	('ssb2eptun7pimhdkj0u9m1rd54',1425317722,'modx.user.contextTokens|a:0:{}'),
	('j4mttvdoptl35thf6grp32en12',1425317723,'modx.user.contextTokens|a:0:{}'),
	('ljg5u7sh0f0ucg588cs8imlm93',1425317727,'modx.user.contextTokens|a:0:{}'),
	('78r2k5ioi67sannt10ll0hj172',1425317765,'modx.user.contextTokens|a:0:{}'),
	('9b0m7suasnau588jv3pets8d40',1425317768,'modx.user.contextTokens|a:0:{}'),
	('cherj65951l8nhu3i2jlnfjek1',1425317768,'modx.user.contextTokens|a:0:{}'),
	('0kpd5o6hqjeq31rk3fhir42up6',1425317796,'modx.user.contextTokens|a:0:{}'),
	('gqncpnnqjsbgjgar8h30ml3cg4',1425317797,'modx.user.contextTokens|a:0:{}'),
	('s10p4dv2fabu12b9jb0qn1kdj4',1425317810,'modx.user.contextTokens|a:0:{}'),
	('v0juhnoc57hqr9pjjvmunvs8f1',1425317811,'modx.user.contextTokens|a:0:{}'),
	('pra5tb2u2frbr5n5h2ai4kkh45',1425317827,'modx.user.contextTokens|a:0:{}'),
	('glvpfispcecsqhjfjfhnmb4u75',1425317828,'modx.user.contextTokens|a:0:{}'),
	('r2b6uj30873nbbmu7alki4b9g6',1425317899,'modx.user.contextTokens|a:0:{}'),
	('rfo6vr7r4sk7issqossi8k3p42',1425317900,'modx.user.contextTokens|a:0:{}'),
	('gj1m4ut5p8t86vems3n2ks9m37',1425317906,'modx.user.contextTokens|a:0:{}'),
	('6e5jucgqclqiqm09i8f82gipj5',1425317906,'modx.user.contextTokens|a:0:{}'),
	('448mumvfrbvarv8dl7sfkjg7n0',1425317996,'modx.user.contextTokens|a:0:{}'),
	('r4bbk7rde4l6tgshh76o1l2bd7',1425318039,'modx.user.contextTokens|a:0:{}'),
	('gp912sedpo47ppllo7oejhgk37',1425318063,'modx.user.contextTokens|a:0:{}'),
	('gke72pm32ir9s0o1t1vm74hrr6',1425318064,'modx.user.contextTokens|a:0:{}'),
	('12j309i1ju71hbdqppk8b72vo7',1425318070,'modx.user.contextTokens|a:0:{}'),
	('5orbecr6amshspq1lqssej1gg7',1425318071,'modx.user.contextTokens|a:0:{}'),
	('bpq0a5v7mmtm0q5dhejpq94r20',1425318174,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('fidmlgbhmvmg8u4hfs1d1dqje4',1425318154,'modx.user.contextTokens|a:0:{}'),
	('3ulae3l0q4hk2035kgdto6ima3',1425318285,'modx.user.contextTokens|a:0:{}'),
	('u9nig56vtvf4ihe7aj0ajlu4v0',1425318285,'modx.user.contextTokens|a:0:{}'),
	('fh2p4ngcda78v0o6dq0huebib1',1425318355,'modx.user.contextTokens|a:0:{}'),
	('gdiai55langp15hn2ik67594g6',1425318356,'modx.user.contextTokens|a:0:{}'),
	('sj4e7p9ncsdv7n1ct1db1p5it7',1425318484,'modx.user.contextTokens|a:0:{}'),
	('hptkb3o84rs6mmng5r96dqv4v0',1425318485,'modx.user.contextTokens|a:0:{}'),
	('rcm3519ffg3n0sg974mg2gto25',1425318497,'modx.user.contextTokens|a:0:{}'),
	('asr1hapnhclt66n13ej5b1unb2',1425318548,'modx.user.contextTokens|a:0:{}'),
	('4ngds9ir9tgfq61mbpj24evph7',1425318548,'modx.user.contextTokens|a:0:{}'),
	('5ghvbsvv1e01dhvti2oh2ii113',1425318568,'modx.user.contextTokens|a:0:{}'),
	('k6m4el8brd66mcv7otgeflgab7',1425318569,'modx.user.contextTokens|a:0:{}'),
	('v4mcksi86118p9q4u6cqe6o9r3',1425318579,'modx.user.contextTokens|a:0:{}'),
	('sl9pqrsqj7grndonci4hprnve3',1425318579,'modx.user.contextTokens|a:0:{}'),
	('08rp9i7jfglg6iced0si086911',1425318789,'modx.user.contextTokens|a:0:{}'),
	('fet8bgsrrn82rjv349q0fm7tm6',1425318789,'modx.user.contextTokens|a:0:{}'),
	('7iqle360nnd5lbc45krdgv8bt2',1425318890,'modx.user.contextTokens|a:0:{}'),
	('hif16cl9573fka14bsbacj4jq0',1425318890,'modx.user.contextTokens|a:0:{}'),
	('gnck46or6qiu3opt7lrepr75q1',1425318922,'modx.user.contextTokens|a:0:{}'),
	('2oak5lcsehc4acno0vh9o013i6',1425318923,'modx.user.contextTokens|a:0:{}'),
	('mhci0kecaqqs9eobt43g4ssbe4',1425319038,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('e505obf5nvt92oj73k8cfemr86',1425319028,'modx.user.contextTokens|a:0:{}'),
	('u36p4jr7d06infgc11dmj3ik45',1425319029,'modx.user.contextTokens|a:0:{}'),
	('g7c7fhc2veaqofa4mcto3alb53',1425319037,'modx.user.contextTokens|a:0:{}'),
	('3kl57p2qrbfbalokt3cn677dt1',1425319038,'modx.user.contextTokens|a:0:{}'),
	('1l20qgi5dsr1dfenmuiqnc2k67',1425319066,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('lrc03svumd1umg5p4cjc68i1s3',1425319160,'modx.user.contextTokens|a:0:{}'),
	('25iieik2l59f4dcqatlcrv7hq7',1425319160,'modx.user.contextTokens|a:0:{}'),
	('4g503nv6an8pr47fqg08jqpct2',1425319203,'modx.user.contextTokens|a:0:{}'),
	('h68se05qkifrpk3pvesp2nehm7',1425319203,'modx.user.contextTokens|a:0:{}'),
	('pp4mtlbvnd88mks3aeism4h0u3',1425319204,'modx.user.contextTokens|a:0:{}'),
	('g8qchogs21ocnce368hp33p5u4',1425319205,'modx.user.contextTokens|a:0:{}'),
	('5puklil8lakdvflv54n0tunh12',1425319443,'modx.user.contextTokens|a:0:{}'),
	('79v1otkkp1ih20djo9jnut1nk6',1425319444,'modx.user.contextTokens|a:0:{}'),
	('b0jfui7tntum9jit5vc6hb5q93',1425319516,'modx.user.contextTokens|a:0:{}'),
	('cijg90llqtdiikdpfjs4qe0u91',1425319517,'modx.user.contextTokens|a:0:{}'),
	('ouqeb298ol43es5gmh7ana6u95',1425319517,'modx.user.contextTokens|a:0:{}'),
	('9l4dm9ab9naovjbhic6t171rf5',1425319518,'modx.user.contextTokens|a:0:{}'),
	('6fj3h0upmr19nfci1sojai7bf2',1425319519,'modx.user.contextTokens|a:0:{}'),
	('mpgdcft99egjcmd18onb5bo790',1425319571,'modx.user.contextTokens|a:0:{}'),
	('d278a9jei412ri96e1f6nharg0',1425319572,'modx.user.contextTokens|a:0:{}'),
	('mamj18hocu81nrgisis075t8g2',1425319656,'modx.user.contextTokens|a:0:{}'),
	('e3023842hvn2k11b3bkbfg8gh2',1425319657,'modx.user.contextTokens|a:0:{}'),
	('126g4921r1idavfao4b26a48q1',1425319824,'modx.user.contextTokens|a:0:{}'),
	('0p9u6kn3k2ulep0nu5gc1pm9m4',1425319824,'modx.user.contextTokens|a:0:{}'),
	('f25smujleij02f10lgcbuj4q54',1425319875,'modx.user.contextTokens|a:0:{}'),
	('ooeghmtera8q0f5b7vt9v4h6t2',1425319876,'modx.user.contextTokens|a:0:{}'),
	('nsr61pad7c675f1erlr05i23p1',1425320180,'modx.user.contextTokens|a:0:{}'),
	('m0kq05qek5gvj3vkcqi63u37c1',1425320181,'modx.user.contextTokens|a:0:{}'),
	('0h1ivahin7ospg38lve4bq9ca6',1425320215,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('eo85ojimpfjdoqg5keh75f9qq3',1425320285,'modx.user.contextTokens|a:0:{}'),
	('0oghbrgoacrbq8a201q72psp36',1425320285,'modx.user.contextTokens|a:0:{}'),
	('2rgrur0r9a63mpt71hjstdbcq0',1425320344,'modx.user.contextTokens|a:0:{}'),
	('9lddrltvrqrae1su1ekc69vbo2',1425320344,'modx.user.contextTokens|a:0:{}'),
	('dv21fh732a94dltboslosbug53',1425320426,'modx.user.contextTokens|a:0:{}'),
	('a6vde4rf2nbtjtqg7s2pm4qfd4',1425320778,'modx.user.contextTokens|a:0:{}'),
	('63f3ciggd96lnhkkt702c3mrp6',1425320779,'modx.user.contextTokens|a:0:{}'),
	('g90uoaipmhmn9moankf599j2f2',1425320811,'modx.user.contextTokens|a:0:{}'),
	('p69j2r1v60vkdq24mpoq0i7bp3',1425320811,'modx.user.contextTokens|a:0:{}'),
	('od5opiagstvenjg255tl5vplm6',1425320818,'modx.user.contextTokens|a:0:{}'),
	('j80b3d2g73vvtj40k5vc12he51',1425320818,'modx.user.contextTokens|a:0:{}'),
	('9g6dj4bghau2gpbfs8ppuuk3j7',1425320954,'modx.user.contextTokens|a:0:{}'),
	('pgu6vtn0lo8pk46146e5i1lep5',1425320955,'modx.user.contextTokens|a:0:{}'),
	('uhup5t24efttbrst33b6urnop4',1425321098,'modx.user.contextTokens|a:0:{}'),
	('r2br1acj7bfsb5tif3bou7s9l4',1425321099,'modx.user.contextTokens|a:0:{}'),
	('so6jf5d9krb9qr71kn58i70jp3',1425321167,'modx.user.contextTokens|a:0:{}'),
	('p3q7l92t75tr9lepkd5m90i4n2',1425321168,'modx.user.contextTokens|a:0:{}'),
	('ha6v4plee16opica39oi520dd4',1425321364,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('8pubhjlo953d32fdbp51fanp14',1425321386,'modx.user.contextTokens|a:0:{}'),
	('gtf1klcll9a87pgqcl1k4eidd6',1425321386,'modx.user.contextTokens|a:0:{}'),
	('g90m6qbto0jplrbrhnm5mderi1',1425321410,'modx.user.contextTokens|a:0:{}'),
	('kae1ltarc0ravlghscnvrtpuj1',1425321410,'modx.user.contextTokens|a:0:{}'),
	('59eqokb0j9pumd11osmtpi4if6',1425321412,'modx.user.contextTokens|a:0:{}'),
	('rqtltgvrf9q3ldb6cr6oggdap3',1425321413,'modx.user.contextTokens|a:0:{}'),
	('01bqau15d7qfssu9mtsq8pu4t3',1425321430,'modx.user.contextTokens|a:0:{}'),
	('6mqf61da34qb7o9m0nfeboa5s7',1425321430,'modx.user.contextTokens|a:0:{}'),
	('58b25f35l2rt8jvpiafdb96vr5',1425321639,'modx.user.contextTokens|a:0:{}'),
	('1e07sljs0lu2kh1sfs2ptd9oo0',1425321639,'modx.user.contextTokens|a:0:{}'),
	('b656rt6sa4ogkvd12ki998cnt2',1425322043,'modx.user.contextTokens|a:0:{}'),
	('rjhoi23u9ha6mm3ptaadfar840',1425322044,'modx.user.contextTokens|a:0:{}'),
	('pfflk0ueg02skje2loqap6qvd5',1425322130,'modx.user.contextTokens|a:0:{}'),
	('7t7d0f7do43clb2nr7u3kq6rb5',1425322131,'modx.user.contextTokens|a:0:{}'),
	('819n3mdro2h6e1c29tvv5nqjv3',1425322445,'modx.user.contextTokens|a:0:{}'),
	('2bpo6ika5e5h4lo1jsdv0hk912',1425322445,'modx.user.contextTokens|a:0:{}'),
	('d4j887roltlpf2aj0m8gb58v91',1425322518,'modx.user.contextTokens|a:0:{}'),
	('v8me91ksi35qjhpk6dqc45usn3',1425322519,'modx.user.contextTokens|a:0:{}'),
	('t731fcrnmg88270vi1c0krnen2',1425322585,'modx.user.contextTokens|a:0:{}'),
	('2lpv5fpvkqggpj5ld4qrt0cpg6',1425322586,'modx.user.contextTokens|a:0:{}'),
	('9d4k2jajv6aurhfcqc4fo3anh2',1425322696,'modx.user.contextTokens|a:0:{}'),
	('tetn8ecgq68hsh857posblkjv2',1425322747,'modx.user.contextTokens|a:0:{}'),
	('a57h5satr20dkkocdfvpbi6d32',1425322748,'modx.user.contextTokens|a:0:{}'),
	('gp477a1vgpvimcviijclgjmbm7',1425322752,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('q60jgflutou27jv92p67min7e1',1425322757,'modx.user.contextTokens|a:0:{}'),
	('eccov6q5jbljod4s0i5ba9qr43',1425322815,'modx.user.contextTokens|a:0:{}'),
	('8h896hs7ed6oq29gvk2ic0e345',1425322815,'modx.user.contextTokens|a:0:{}'),
	('6q46hrsbdlr6prr828kdthm503',1425322833,'modx.user.contextTokens|a:0:{}'),
	('t0qgkk7bsvmrpjlgj0n8s08tb3',1425322833,'modx.user.contextTokens|a:0:{}'),
	('eugd7acdj0htmf0j69ublk8122',1425323212,'modx.user.contextTokens|a:0:{}'),
	('nd9563t42frbifnso3a01piho7',1425323452,'modx.user.contextTokens|a:0:{}'),
	('036h3edgdsrlq2gbm4pn361ns2',1425323453,'modx.user.contextTokens|a:0:{}'),
	('q139bm4icqnkveck68534jd6b3',1425323455,'modx.user.contextTokens|a:0:{}'),
	('l4nrnd881vlljm7606qp5plrq2',1425323468,'modx.user.contextTokens|a:0:{}'),
	('bm9mudnmf68ns11d3tj894vhm7',1425323469,'modx.user.contextTokens|a:0:{}'),
	('6hijqmbbbr0kuki51rf17558i1',1425323781,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('f22ra8es9pm9guce80h4t3b5t0',1425324115,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('9lg1qf4u1s76q28vmsuae476d2',1425324051,'modx.user.contextTokens|a:0:{}'),
	('9cl8bps3k7lrn51ujd30paamn0',1425324051,'modx.user.contextTokens|a:0:{}'),
	('ovbus2ai9r8egsd2jdbidgoi42',1425324094,'modx.user.contextTokens|a:0:{}'),
	('gtm5peimtjubc0c5ggpttqsjr7',1425324095,'modx.user.contextTokens|a:0:{}'),
	('i4ku2bh4iojf6aokrgooki6kg2',1425324312,'modx.user.contextTokens|a:0:{}'),
	('bidiap9qrh92dc7v9s4m7lhru6',1425324560,'modx.user.contextTokens|a:0:{}'),
	('scq65uqr1ds1d1ptsb114afr81',1425324560,'modx.user.contextTokens|a:0:{}'),
	('1qsjcv4gj6rt8f0dud441sn6h1',1425324601,'modx.user.contextTokens|a:0:{}'),
	('uric4rovcldcm03t92n1050ac7',1425324602,'modx.user.contextTokens|a:0:{}'),
	('pgdjmomdaktc0dtkhktcdmedd7',1425324785,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('3manven53d83shmlhti2i9e350',1425324798,'modx.user.contextTokens|a:0:{}'),
	('d8v68ukr3rqrojh3vchanup997',1425324798,'modx.user.contextTokens|a:0:{}'),
	('jgv1unhlrjonp9b2b28d6so5u5',1425324963,'modx.user.contextTokens|a:0:{}'),
	('nai7enj7rqbgeko3rql3utlgu5',1425324964,'modx.user.contextTokens|a:0:{}'),
	('v9evrl7pm58o9r75holmd9jjg0',1425324985,'modx.user.contextTokens|a:0:{}'),
	('1ri1megn1q1e4oo7a6854fips2',1425324986,'modx.user.contextTokens|a:0:{}'),
	('b0f600b9qcmn7lemakm6qtn4t3',1425325062,'modx.user.contextTokens|a:0:{}'),
	('h2ff620dot6p0nh5li4rftbdf4',1425325062,'modx.user.contextTokens|a:0:{}'),
	('i72op7qps87udtr657n8kmaua4',1425325248,'modx.user.contextTokens|a:0:{}'),
	('kc489uhm7h3hctultvspvv9go5',1425325248,'modx.user.contextTokens|a:0:{}'),
	('t4lc9sn31i45bjge5n29noj375',1425325565,'modx.user.contextTokens|a:0:{}'),
	('9edo1trsh9p9hj2o82158hr7l2',1425325566,'modx.user.contextTokens|a:0:{}'),
	('qv2ecs4dul17o4qmnovc45dag0',1425325582,'modx.user.contextTokens|a:0:{}'),
	('ij1jv77sn78pkuhg9gjttlcv57',1425325582,'modx.user.contextTokens|a:0:{}'),
	('aji955bklflf44fbdlg9h3ent1',1425325601,'modx.user.contextTokens|a:0:{}'),
	('q9iqur3gnmbbis848ebj7th3g3',1425325602,'modx.user.contextTokens|a:0:{}'),
	('kilfufn6rae06r9ej1puvedsi7',1425325724,'modx.user.contextTokens|a:0:{}'),
	('bvnqrkllvu3jqsfk17radfuer6',1425325725,'modx.user.contextTokens|a:0:{}'),
	('mj5lu0idnjibk6g2bmsguvso53',1425325959,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('c5a3s148r32bf45th4bs89fti5',1425325945,'modx.user.contextTokens|a:0:{}'),
	('j1ffb1vq1eg4bcfddl71esvi64',1425325945,'modx.user.contextTokens|a:0:{}'),
	('0gf9qg3got8jsra4jg41o332n7',1425326028,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('8hbbfq7cf2g3t2ochghvhtfc50',1425326004,'modx.user.contextTokens|a:0:{}'),
	('t9g0ne2pnl6n5hbs8o1h1m3vh2',1425326004,'modx.user.contextTokens|a:0:{}'),
	('kt6bndgh0ct021s1rjh2lma3b5',1425326048,'modx.user.contextTokens|a:0:{}'),
	('aps8e8c831jfsor0j3ljagg603',1425326049,'modx.user.contextTokens|a:0:{}'),
	('l3rrfudv6hfn22tdrbbctpse72',1425326060,'modx.user.contextTokens|a:0:{}'),
	('0ri3qeaguduun9a58bt4jb34t2',1425326060,'modx.user.contextTokens|a:0:{}'),
	('5jn83j81k3voiomg5vu9kpcu47',1425326346,'modx.user.contextTokens|a:0:{}'),
	('0p7cpupin8de9uhdg7rupd1t06',1425326347,'modx.user.contextTokens|a:0:{}'),
	('o7jrk7420iest3tris82gpujv3',1425326490,'modx.user.contextTokens|a:0:{}'),
	('fsi4on0kh7lhfudv3eqso2fao0',1425326531,'modx.user.contextTokens|a:0:{}'),
	('vshsjtlb4036jun8nlmgq7dgs2',1425326576,'modx.user.contextTokens|a:0:{}'),
	('aohk1197ffvmblkfvf5t4m4r50',1425326577,'modx.user.contextTokens|a:0:{}'),
	('mfhe8uqr98bhfd8slabo1hegg4',1425326605,'modx.user.contextTokens|a:0:{}'),
	('7ts3u1k9in6vdv911p18i442d7',1425326605,'modx.user.contextTokens|a:0:{}'),
	('8eoouqv80u0v5tk6j12uc6kl60',1425326700,'modx.user.contextTokens|a:0:{}'),
	('eenk8obqat2635lpfah633t013',1425326701,'modx.user.contextTokens|a:0:{}'),
	('rme0lts84klcscc4llefgp3nm6',1425326901,'modx.user.contextTokens|a:0:{}'),
	('702hhsl4ru072j6uc3dct3tke3',1425326901,'modx.user.contextTokens|a:0:{}'),
	('qh7a7t2grq7f45bi587j1jk211',1425326982,'modx.user.contextTokens|a:0:{}'),
	('2ia6r1rlhqail33fboienv2l23',1425326982,'modx.user.contextTokens|a:0:{}'),
	('epdtv42rrvtd097rjdkm7ubo74',1425326985,'modx.user.contextTokens|a:0:{}'),
	('lrisanlfju1cgrpjgne6ihnhs5',1425326985,'modx.user.contextTokens|a:0:{}'),
	('o970ql2oj7quk2r9rh4o3qusb3',1425327035,'modx.user.contextTokens|a:0:{}'),
	('ih1lgatrhscg38ampm998ncbd6',1425327035,'modx.user.contextTokens|a:0:{}'),
	('b96ceto7orsuojrb169l23obh1',1425327329,'modx.user.contextTokens|a:0:{}'),
	('lrrd28e34fghl2p2v8hutkmp77',1425327330,'modx.user.contextTokens|a:0:{}'),
	('lmun2ncp7ii6stmmp6gn3gu3t7',1425327612,'modx.user.contextTokens|a:0:{}'),
	('pa9o7leisac696tad3uft2tc64',1425327613,'modx.user.contextTokens|a:0:{}'),
	('empmd52ronl4ifq76flbqvaar6',1425327710,'modx.user.contextTokens|a:0:{}'),
	('6bdadplntnnj7eobtv70hrnjq6',1425327710,'modx.user.contextTokens|a:0:{}'),
	('fft82basgscs0fra41u6ls3np4',1425328064,'modx.user.contextTokens|a:0:{}'),
	('4d57u29q40kqtaus1jdqvhu831',1425328065,'modx.user.contextTokens|a:0:{}'),
	('57s35tv6jfk1j0g9j63aj9klq3',1425328297,'modx.user.contextTokens|a:0:{}'),
	('getrhme1ckegmimhis8rs6kmr5',1425328297,'modx.user.contextTokens|a:0:{}'),
	('9opudksjg3lctro3g2vi0eh622',1425328457,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('1i02414bo6a0uv5htophnv25k0',1425328626,'modx.user.contextTokens|a:0:{}'),
	('et17u1giatapjudt34n8tsbe45',1425328627,'modx.user.contextTokens|a:0:{}'),
	('79r83ap934j9193pih64h95fe0',1425328627,'modx.user.contextTokens|a:0:{}'),
	('u1gq2tc3jo0qjbfebomafuffo3',1425328628,'modx.user.contextTokens|a:0:{}'),
	('nlsfha5que4qjs6malehcs3qv2',1425328628,'modx.user.contextTokens|a:0:{}'),
	('bftn8ujj7r555imhfdil9blmt5',1425328629,'modx.user.contextTokens|a:0:{}'),
	('a0tpk7a88ven0kt3h80jc951g1',1425328640,'modx.user.contextTokens|a:0:{}'),
	('c30hh7iiv5p146sivdq0061k51',1425328645,'modx.user.contextTokens|a:0:{}'),
	('32jrdlfhinvup8i8gptmn36fp1',1425328661,'modx.user.contextTokens|a:0:{}'),
	('ukaqrs6eef0s7kgtabbn6qt2a6',1425328670,'modx.user.contextTokens|a:0:{}'),
	('20niqk0orn7e00uhpls968noa6',1425328672,'modx.user.contextTokens|a:0:{}'),
	('r7mpj95qs28trvi793r2q2uov2',1425328673,'modx.user.contextTokens|a:0:{}'),
	('l76248629d6m39ibr6m76auds6',1425328692,'modx.user.contextTokens|a:0:{}'),
	('ganjf0pjl8q6ido1ksmha41v80',1425328693,'modx.user.contextTokens|a:0:{}'),
	('cdjecl4h74k9tc370nc6mqtii7',1425328729,'modx.user.contextTokens|a:0:{}'),
	('2e9corkdbb7mlp07bprjdj9ii5',1425328737,'modx.user.contextTokens|a:0:{}'),
	('5fnj8d8sp8rots5tvv2bvm10q3',1425328814,'modx.user.contextTokens|a:0:{}'),
	('8mhtuctiqso0djquo0gk61e1k6',1425328883,'modx.user.contextTokens|a:0:{}'),
	('n5c2lplnd6o67765qk1tgsn796',1425328986,'modx.user.contextTokens|a:0:{}'),
	('63i86nodiftbr2e94b0k003l75',1425328986,'modx.user.contextTokens|a:0:{}'),
	('h8su5aeim9s881pr13484qopu7',1425329124,'modx.user.contextTokens|a:0:{}'),
	('vl7oqhi129o2oj69fg8liq5bd6',1425329133,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('uphkcsi7k1egh6nu0nbtg9tgp2',1425329217,'modx.user.contextTokens|a:0:{}'),
	('pu3h16sl6drc34kee2bj0922q0',1425329218,'modx.user.contextTokens|a:0:{}'),
	('c3pgvkpmuug10dldtl7u4liev6',1425329361,'modx.user.contextTokens|a:0:{}'),
	('9qkta7iv1t69vud08fqvi08re2',1425329361,'modx.user.contextTokens|a:0:{}'),
	('m73f3esd57rodm44ffql5rloq1',1425329362,'modx.user.contextTokens|a:0:{}'),
	('e3vob8h15k59eo5n8sfdunram7',1425329363,'modx.user.contextTokens|a:0:{}'),
	('dchi2v5qkfj0rma511vskr8d66',1425329419,'modx.user.contextTokens|a:0:{}'),
	('6d34e9pda3uae8re7jfeqbaps3',1425329620,'modx.user.contextTokens|a:0:{}'),
	('dcfegb4qdpeseq1df3g1nlitn3',1425329620,'modx.user.contextTokens|a:0:{}'),
	('b0lvlib6qs77mbmr34crprk2v7',1425329641,'modx.user.contextTokens|a:0:{}'),
	('p7sbjncdnsu9i9264errc6ssb7',1425329642,'modx.user.contextTokens|a:0:{}'),
	('30l35kdgq7bue0eise6io2ee41',1425329672,'modx.user.contextTokens|a:0:{}'),
	('j7g29rqmvo3grgmink8q817f24',1425329672,'modx.user.contextTokens|a:0:{}'),
	('nlvtmf9n7ve8d6pg7g0ddm4hu7',1425329691,'modx.user.contextTokens|a:0:{}'),
	('s7oe6abt5b5rvi32jpb9phhm53',1425329692,'modx.user.contextTokens|a:0:{}'),
	('cp17qfpb1t8la1oqm4si9g1i82',1425329956,'modx.user.contextTokens|a:0:{}'),
	('jhn7bi79sb1kevvflk0fnrabs0',1425329956,'modx.user.contextTokens|a:0:{}'),
	('a74e79nbb4jg5vtffctjh20br7',1425330306,'modx.user.contextTokens|a:0:{}'),
	('u1ng6n91t2bhlaistppf49uls7',1425330307,'modx.user.contextTokens|a:0:{}'),
	('6ot9c0u974pvpi1o61rcvqtl70',1425330431,'modx.user.contextTokens|a:0:{}'),
	('s5v8h9aofnlv3h22c6mcboh225',1425330431,'modx.user.contextTokens|a:0:{}'),
	('hmrvfb3p60f3daje57upnh3n62',1425330555,'modx.user.contextTokens|a:0:{}'),
	('hdl1kio340vavio1vb0ksmlk24',1425330560,'modx.user.contextTokens|a:0:{}'),
	('0oune66e24bmmm216ebguq4n97',1425330560,'modx.user.contextTokens|a:0:{}'),
	('3rg8fetivt1cpj8ko2p0vjioc4',1425330561,'modx.user.contextTokens|a:0:{}'),
	('opth6kqu1p5854mgvoa52gkta1',1425330857,'modx.user.contextTokens|a:0:{}'),
	('1ggtn88cm3lvltdpt98ucp7ku5',1425330981,'modx.user.contextTokens|a:0:{}'),
	('cn5gf900e0ddh1u9b66428kvd6',1425330981,'modx.user.contextTokens|a:0:{}'),
	('oomc2hb7d9du5595bgk68n4ut2',1425331292,'modx.user.contextTokens|a:0:{}'),
	('m4el54e8igthsjqgce883am1v6',1425331308,'modx.user.contextTokens|a:0:{}'),
	('47sfl2sbloja8vgpjs5nh9po02',1425331308,'modx.user.contextTokens|a:0:{}'),
	('gf66l3cp72j25s8iqut8fqi395',1425331336,'modx.user.contextTokens|a:0:{}'),
	('6ktp97uaibml2ctd5j4qq401k7',1425331635,'modx.user.contextTokens|a:0:{}'),
	('bkd4u0cc9ck85r4bo7jq69j5i5',1425331784,'modx.user.contextTokens|a:0:{}'),
	('n4r2s5i0cl5erurklrg1v7crd5',1425331784,'modx.user.contextTokens|a:0:{}'),
	('jcn3lvbgbnsttibdrpb2bmd0i3',1425331999,'modx.user.contextTokens|a:0:{}'),
	('t6df06aiebf7bbaikdv4qmcks3',1425332000,'modx.user.contextTokens|a:0:{}'),
	('gu47oa8qbiupi86n4jeqah80a4',1425332175,'modx.user.contextTokens|a:0:{}'),
	('fo4625qhc036kuqjmlp1feeib6',1425332175,'modx.user.contextTokens|a:0:{}'),
	('gg0kpcunt9nh0877ma63vu1k96',1425332263,'modx.user.contextTokens|a:0:{}'),
	('70eej7s0n0cbe1ntjqrlrtuu54',1425332264,'modx.user.contextTokens|a:0:{}'),
	('ijnd57bli4j76mm5kfv8a93b65',1425332286,'modx.user.contextTokens|a:0:{}'),
	('p6sdvn3so4oneqgeh7v16h4cd6',1425332286,'modx.user.contextTokens|a:0:{}'),
	('hqlben80be6rf1bo18ukpstnb5',1425332387,'modx.user.contextTokens|a:0:{}'),
	('ar6d4p74ea0m1dblk14b9pn2i4',1425332388,'modx.user.contextTokens|a:0:{}'),
	('0o2gpb1d0qt2blk6nssvn7m0a3',1425332566,'modx.user.contextTokens|a:0:{}'),
	('ihsvd65akl798cdkpffg1njre2',1425332567,'modx.user.contextTokens|a:0:{}'),
	('deql8qtkrbp9jt4mjcb9nass00',1425332754,'modx.user.contextTokens|a:0:{}'),
	('of0qs6astmhm2pb8i2knog0g27',1425332755,'modx.user.contextTokens|a:0:{}'),
	('849bsnbkssuh5rj251q9p7t8r3',1425333150,'modx.user.contextTokens|a:0:{}'),
	('3dpnfl3r313eoim5n1md5m9863',1425333151,'modx.user.contextTokens|a:0:{}'),
	('dm2g1v9ouihk3f274551td34l4',1425333182,'modx.user.contextTokens|a:0:{}'),
	('5n4qtco7ctgh35bfagit9osvi1',1425333183,'modx.user.contextTokens|a:0:{}'),
	('l06dpkhikh86hq7pmtng17k390',1425333714,'modx.user.contextTokens|a:0:{}'),
	('kg2bfd00mkkio6oq9b9j6m1040',1425333714,'modx.user.contextTokens|a:0:{}'),
	('ie2ag7ta9bhdkcgj4es539rsk0',1425333714,'modx.user.contextTokens|a:0:{}'),
	('pg3cp96g7h3d7u473jsb27vim5',1425333383,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('31ihjm2jca3taapp26j7o0rej4',1425333384,'modx.user.contextTokens|a:0:{}'),
	('es6ldfrgu35qvtsamra07m6r50',1425333473,'modx.user.contextTokens|a:0:{}'),
	('6730rtm504fqnhtmnmvuan7hp6',1425340197,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('3is8rk1gouefnogckauioikic4',1425340197,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('dpkno7r1dljb6rnum5qn53c7r2',1425340390,'modx.user.contextTokens|a:0:{}'),
	('iocu7jseb5h6295e60aric3i72',1425340391,'modx.user.contextTokens|a:0:{}'),
	('7tq908bbvkj8md1enol0aonme4',1425340551,'modx.user.contextTokens|a:0:{}'),
	('mmo9if21nm7oju09l6cnn7a0o5',1425340551,'modx.user.contextTokens|a:0:{}'),
	('7nh7hdq33k4m0sbc5iju22c8k7',1425340932,'modx.user.contextTokens|a:0:{}'),
	('fniq36hut53nepqj5q09pjfv12',1425340944,'modx.user.contextTokens|a:0:{}'),
	('8fuec45eb76m3ns4cfdcd3f9k3',1425340944,'modx.user.contextTokens|a:0:{}'),
	('tl5s059o54hagk65q2ng5op0r5',1425341029,'modx.user.contextTokens|a:0:{}'),
	('vtk606na55l02mbh4mdsi7hif1',1425341029,'modx.user.contextTokens|a:0:{}'),
	('08o67jqkqio8d1ll2tn9vfuc14',1425341420,'modx.user.contextTokens|a:0:{}'),
	('uo5t46qppm76iuj17de978l857',1425341583,'modx.user.contextTokens|a:0:{}'),
	('bm1i7vcuq4vus5rdbt1v96q353',1425341583,'modx.user.contextTokens|a:0:{}'),
	('vh9gqfses8apbhcrhocmdce0o1',1425342036,'modx.user.contextTokens|a:0:{}'),
	('27e41vfoo2hevr2sh03id0p314',1425342036,'modx.user.contextTokens|a:0:{}'),
	('j0kl3n5571v79a0e13odiijup4',1425342220,'modx.user.contextTokens|a:0:{}'),
	('t45aavpcaiu28mi03juqm5emn7',1425342228,'modx.user.contextTokens|a:0:{}'),
	('8qguhldef7d5v4oreiulg9upj0',1425342228,'modx.user.contextTokens|a:0:{}'),
	('g226b1cbhajn3d7loa5f4hagh7',1425342265,'modx.user.contextTokens|a:0:{}'),
	('ejao2rtc8srafh7eql1qfdfga3',1425342265,'modx.user.contextTokens|a:0:{}'),
	('avpq6vmqs6c8dtr49q9qjlpru5',1425342428,'modx.user.contextTokens|a:0:{}'),
	('2193v122vh6ofteqae7edrq486',1425342428,'modx.user.contextTokens|a:0:{}'),
	('2f7gc10gnltrcjp0e60e9il414',1425342820,'modx.user.contextTokens|a:0:{}'),
	('q0e1lj5vc7t7o2k9gmd6s61vv1',1425342833,'modx.user.contextTokens|a:0:{}'),
	('6n8s1937pueg8kl9caj8ot5ei0',1425342834,'modx.user.contextTokens|a:0:{}'),
	('beg98kq80eu9td6debndgj2sj7',1425342911,'modx.user.contextTokens|a:0:{}'),
	('8kdopkcm7eh66slha5ehf13e54',1425342912,'modx.user.contextTokens|a:0:{}'),
	('kurv1b8srgrvsgslbb3e3hd3h4',1425343632,'modx.user.contextTokens|a:0:{}'),
	('pcgn3pqmm0lnvtbgre25ea0ua6',1425343632,'modx.user.contextTokens|a:0:{}'),
	('u6ako840prsl4kt48b14g2sc07',1425343772,'modx.user.contextTokens|a:0:{}'),
	('r7rkgrqfhjditcnvtpo3mn67b4',1425343772,'modx.user.contextTokens|a:0:{}'),
	('1ogpbi1sp1h6j5gjmk63asi0d5',1425343799,'modx.user.contextTokens|a:0:{}'),
	('0olpb88ns24546pc09pd8ssou2',1425343799,'modx.user.contextTokens|a:0:{}'),
	('ngb03399modv0djgptq5ojtbt3',1425343902,'modx.user.contextTokens|a:0:{}'),
	('k34cnkkvgokfoqgstr1qa81t83',1425343902,'modx.user.contextTokens|a:0:{}'),
	('ldqeqivr3inb36oruqsfsj4221',1425344124,'modx.user.contextTokens|a:0:{}'),
	('2l9911r37o1gijsuu6t39eg3t0',1425344411,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('5qdkmdfsgv23eael0guucgcrt0',1425344928,'modx.user.contextTokens|a:0:{}'),
	('fpop61i8o238upmn3dkidi45d1',1425345046,'modx.user.contextTokens|a:0:{}'),
	('9rf0brgbv5qoj1lh0kflsm4vh3',1425345047,'modx.user.contextTokens|a:0:{}'),
	('kk1odk4ndh8v0hd2kn1ho4qd71',1425346338,'modx.user.contextTokens|a:0:{}'),
	('34tnaq71f8mbvta6b7l8fa4t67',1425346339,'modx.user.contextTokens|a:0:{}'),
	('3r81nd88v6mddn1p4mbeu2aro6',1425346375,'modx.user.contextTokens|a:0:{}'),
	('ge2fuuau7hh6fafu66v46r7180',1425346376,'modx.user.contextTokens|a:0:{}'),
	('qb7cmcpdkth2gl4e71j73hc227',1425346386,'modx.user.contextTokens|a:0:{}'),
	('2nlnadvtv9fai54g58nhfg03j1',1425346386,'modx.user.contextTokens|a:0:{}'),
	('n23hk8olj5a1s5r1r3aoohnvf0',1425346447,'modx.user.contextTokens|a:0:{}'),
	('2ds0afc04n1enmq1gc7c58l7q2',1425346447,'modx.user.contextTokens|a:0:{}'),
	('ea2v64o63tj69d1t92vb2f3rt0',1425346474,'modx.user.contextTokens|a:0:{}'),
	('i2vcnl32jjhit5h2rc2fr7cia1',1425346475,'modx.user.contextTokens|a:0:{}'),
	('pettnshhuaa5mqvdm51oulbkf5',1425347153,'modx.user.contextTokens|a:0:{}'),
	('2gtc0sbj46a0bs8go2kgqs7q21',1425347154,'modx.user.contextTokens|a:0:{}'),
	('7h46hsn2rjl4o6li2hao8k9f93',1425347722,'modx.user.contextTokens|a:0:{}'),
	('927kn0stcodbrep5emge5m2lu5',1425347723,'modx.user.contextTokens|a:0:{}'),
	('6cd11g0ivgm0pic5gvv3iuusn6',1425348345,'modx.user.contextTokens|a:0:{}'),
	('6apfadn6gnukqdg98vt0vbbbb7',1425348345,'modx.user.contextTokens|a:0:{}'),
	('4nhohmeoc8k9qth8b7ddekl644',1425348390,'modx.user.contextTokens|a:0:{}'),
	('0qij174m7q27busojk523857m1',1425348682,'modx.user.contextTokens|a:0:{}'),
	('obju32t9l53qd7b7sbahhkh5m1',1425348683,'modx.user.contextTokens|a:0:{}'),
	('9jpgmkvt6mos6396fp4cm8j2t3',1425348804,'modx.user.contextTokens|a:0:{}'),
	('7rv4fpnhuh69qnmckodjojhp96',1425348805,'modx.user.contextTokens|a:0:{}'),
	('pl4mmlhkpojb466663j262u5i5',1425348831,'modx.user.contextTokens|a:0:{}'),
	('714g5sbgh7jbes42fhmt88erc0',1425348831,'modx.user.contextTokens|a:0:{}'),
	('fee3o2sdmi7asrej7h1maom9u7',1425348838,'modx.user.contextTokens|a:0:{}'),
	('fh17ttr2ub2c38ks3tb8en12q0',1425348952,'modx.user.contextTokens|a:0:{}'),
	('j6gic3j009afno60spa8ievte2',1425348952,'modx.user.contextTokens|a:0:{}'),
	('pg0vt018e9bqdd79rijuohflb6',1425349011,'modx.user.contextTokens|a:0:{}'),
	('66n3s9l6ru5ua3uv4sioa0q2i5',1425349012,'modx.user.contextTokens|a:0:{}'),
	('grl60rahtiuqv8348i1r23vof0',1425349485,'modx.user.contextTokens|a:0:{}'),
	('a0hjleun2nfkrn7ckdj53fd173',1425349485,'modx.user.contextTokens|a:0:{}'),
	('4g4gs6mghcs3jqh9kit1gt5da0',1425349770,'modx.user.contextTokens|a:0:{}'),
	('ljsi86ebceb0ighgk7tn3cpnn5',1425349771,'modx.user.contextTokens|a:0:{}'),
	('sm1mddg41u9pfrm0fbnaovhdn5',1425350000,'modx.user.contextTokens|a:0:{}'),
	('9idj9vuhvu9gqejpeconlaiji1',1425350000,'modx.user.contextTokens|a:0:{}'),
	('4a88eoe49mob4ajtlc5k24obe2',1425350097,'modx.user.contextTokens|a:0:{}'),
	('np37kc9ar4vbi5karafbffad52',1425350097,'modx.user.contextTokens|a:0:{}'),
	('e5ss00tbm3lg7ls6hanaemap72',1425350308,'modx.user.contextTokens|a:0:{}'),
	('as78dtpun2rf9kvi63mj3v3fb2',1425350309,'modx.user.contextTokens|a:0:{}'),
	('oi4bvo51bjtfkds2usonch0233',1425350355,'modx.user.contextTokens|a:0:{}'),
	('p5dskrc3kj4pe00gas6fggqhn3',1425350356,'modx.user.contextTokens|a:0:{}'),
	('4gsla0b0g9l4d8eeasql7hcrf7',1425350666,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('d0kvbkgdvqvmnj254isl48fm81',1425351013,'modx.user.contextTokens|a:0:{}'),
	('eoo06v6hv1rmfj2n39bnfeade4',1425351013,'modx.user.contextTokens|a:0:{}'),
	('jtuj578o6ucgdrqna2avk2hm41',1425351047,'modx.user.contextTokens|a:0:{}'),
	('i70uro7s164n1kcnbmpm0nbkh4',1425351048,'modx.user.contextTokens|a:0:{}'),
	('5m8cchst47k45g4k337e3m7ed7',1425351566,'modx.user.contextTokens|a:0:{}'),
	('8otp8k46epsm7hd0tt9p0mhlj0',1425351566,'modx.user.contextTokens|a:0:{}'),
	('umdtml0odblmu2ta9iggofekg5',1425351613,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('4tnb1p5h54ull7pcq93g0e9gj7',1425351649,'modx.user.contextTokens|a:0:{}'),
	('52kigjc241s11ka8nvlp9f7l86',1425351649,'modx.user.contextTokens|a:0:{}'),
	('ctvkdqsqj7lndldfih1lkgavh0',1425351682,'modx.user.contextTokens|a:0:{}'),
	('kg6nqpobrfs6l0g7uebtthunc0',1425351860,'modx.user.contextTokens|a:0:{}'),
	('cplm9s84cbvielplokmgc18261',1425351860,'modx.user.contextTokens|a:0:{}'),
	('39akdlgrnpg2vtfbvjtcot9q94',1425352036,'modx.user.contextTokens|a:0:{}'),
	('eb9dv2j7vg0fu36rudtbp4t000',1425352190,'modx.user.contextTokens|a:0:{}'),
	('e4fcvjvurm88rfrelmijqru4v2',1425352191,'modx.user.contextTokens|a:0:{}'),
	('pbrfg3t28mg6tbsk6qigatq956',1425352287,'modx.user.contextTokens|a:0:{}'),
	('rj9kpm7m02amsdg8n739r465u3',1425352288,'modx.user.contextTokens|a:0:{}'),
	('06ivrnegdjjfes4ih1j5eqjqs3',1425352316,'modx.user.contextTokens|a:0:{}'),
	('0sc2i6gdotf9p5lh9756nu0dv7',1425352451,'modx.user.contextTokens|a:0:{}'),
	('jmn3jivtkouj66eu5a44brf932',1425352453,'modx.user.contextTokens|a:0:{}'),
	('7tq2dasklmiidt8hi3d24tlqn0',1425352681,'modx.user.contextTokens|a:0:{}'),
	('4u0s9p9ce4atugmk295o7bsih2',1425352681,'modx.user.contextTokens|a:0:{}'),
	('3j7hq9bi0nvkm2a6a6pdg3ou83',1425352862,'modx.user.contextTokens|a:0:{}'),
	('eiubqcfqeu2uu797uashkenru0',1425352862,'modx.user.contextTokens|a:0:{}'),
	('3i34g8ppuo3kptb7isthgkbj52',1425352993,'modx.user.contextTokens|a:0:{}'),
	('ptll9001g8n84vb8gfd4gp6ki6',1425353485,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('7b1posl5o3fderrkcn1och93c1',1425353744,'modx.user.contextTokens|a:0:{}'),
	('0iq03d7ms4po508gld343m6j74',1425353745,'modx.user.contextTokens|a:0:{}'),
	('5ejjepebos0iq6end26s8m1la6',1425353811,'modx.user.contextTokens|a:0:{}'),
	('m2ilf624bikqff1aag0ea9k787',1425353811,'modx.user.contextTokens|a:0:{}'),
	('seapga7sqaivdhp74dkb930mn4',1425353954,'modx.user.contextTokens|a:0:{}'),
	('bkd0umvik1c3qmlpa7q78v5v40',1425353955,'modx.user.contextTokens|a:0:{}'),
	('n1o6vi7d7h4sb3u3dnnki4bm43',1425353975,'modx.user.contextTokens|a:0:{}'),
	('sec6vdlfe5fpvqeb914ve8m362',1425353976,'modx.user.contextTokens|a:0:{}'),
	('up2gjpdi21icqnkkqc7bgl92h3',1425354519,'modx.user.contextTokens|a:0:{}'),
	('prk3n795cdcbskal7lo2ecd2o4',1425354613,'modx.user.contextTokens|a:0:{}'),
	('794fqbrd09rda3lephhkh129f0',1425354614,'modx.user.contextTokens|a:0:{}'),
	('s7fk6paf2mt5rugefu6d3gq766',1425355483,'modx.user.contextTokens|a:0:{}'),
	('6glrln4hm182m5sl5edorhppc3',1425356024,'modx.user.contextTokens|a:0:{}'),
	('7m0p0dv2beqvtc3atuels66un4',1425356249,'modx.user.contextTokens|a:0:{}'),
	('mdr6vls1c6ri08cn2sceuinai1',1425356249,'modx.user.contextTokens|a:0:{}'),
	('0a321lnpig1b3p44qqkage4sh3',1425356296,'modx.user.contextTokens|a:0:{}'),
	('b5au0g8qk25gpvlr7eerjj1kn5',1425356436,'modx.user.contextTokens|a:0:{}'),
	('aka8sr2b8ef6aa589k4quphiq6',1425356437,'modx.user.contextTokens|a:0:{}'),
	('1a2ofmmkb5smne1arj0kcd22o4',1425356453,'modx.user.contextTokens|a:0:{}'),
	('8k7kqds7pfe9ihaeudordv3sg2',1425356821,'modx.user.contextTokens|a:0:{}'),
	('9an131hm4kc4rhkje3igkfjnh7',1425356821,'modx.user.contextTokens|a:0:{}'),
	('6tviu8pio1c17oqi9qfss3vki7',1425357542,'modx.user.contextTokens|a:0:{}'),
	('hkpa7llmoihud275jvgoe6khg2',1425358619,'modx.user.contextTokens|a:0:{}'),
	('1894f5c3mll6jsmkoif1u7qq21',1425358743,'modx.user.contextTokens|a:0:{}'),
	('ittdpniqqm5t7vrtpmrb52k5f6',1425358770,'modx.user.contextTokens|a:0:{}'),
	('u98c56hdfs854mt7n03gf895k6',1425358771,'modx.user.contextTokens|a:0:{}'),
	('5sud5g4fp5uj93tvsu74cg60r1',1425359058,'modx.user.contextTokens|a:0:{}'),
	('0feuho3d07hsutsm4vqg55jg31',1425359137,'modx.user.contextTokens|a:0:{}'),
	('jadh1l3n37e11fi5qg9rpkbcr3',1425359330,'modx.user.contextTokens|a:0:{}'),
	('euq69r00o0ufbjqap88drs3ng0',1425359331,'modx.user.contextTokens|a:0:{}'),
	('480a4gouill0mlodjlqegkmmo3',1425359394,'modx.user.contextTokens|a:0:{}'),
	('ndvsu6t8qali88hafroojjrit1',1425359395,'modx.user.contextTokens|a:0:{}'),
	('qtg55smbbo375q5lseufu46ti7',1425359396,'modx.user.contextTokens|a:0:{}'),
	('sgk8hq00edlf56ippbii1e4tp1',1425359397,'modx.user.contextTokens|a:0:{}'),
	('vpmr5vfh5s4tunkeerme2797k4',1425359416,'modx.user.contextTokens|a:0:{}'),
	('9uqtva9v00egt5g9r21mg9hjj2',1425359417,'modx.user.contextTokens|a:0:{}'),
	('ucqh1ccbulp8vvt05os2lmak03',1425360224,''),
	('7ne4i9smdur5l4qtt9v1rp0qc5',1425360225,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ki5fpor8hjn4um5mbf086415d4',1425360226,'modx.user.contextTokens|a:0:{}'),
	('vijhlae2forvq3o8mqe3ii6kc0',1425360227,'modx.user.contextTokens|a:0:{}'),
	('fjo3rfetpqqvp7tbaof3ltlv90',1425360254,'modx.user.contextTokens|a:0:{}'),
	('j6mth5qdkg2raum422s66c00r7',1425360255,'modx.user.contextTokens|a:0:{}'),
	('vroil4gt1cpgkuevnnbetadlp1',1425360288,'modx.user.contextTokens|a:0:{}'),
	('o1r49vu6ol7e7vebtnk91eed47',1425360289,'modx.user.contextTokens|a:0:{}'),
	('a64t6s4hrn6f72nnjufkm4o2v3',1425360497,'modx.user.contextTokens|a:0:{}'),
	('vcifnj13i8cc6bp9df91ni80a7',1425360501,'modx.user.contextTokens|a:0:{}'),
	('n8br0efltm9dmgfr8m21gd9u27',1425361213,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('9vatgg1gvtt7396ffu2jdm0o57',1425361289,'modx.user.contextTokens|a:0:{}'),
	('osqadg527jp9c83gl998h10cb4',1425361290,'modx.user.contextTokens|a:0:{}'),
	('btodpjs1drfsffe9note9pmes7',1425362093,'modx.user.contextTokens|a:0:{}'),
	('emfqmnii4dorrv9f5sqdkk60o6',1425362280,'modx.user.contextTokens|a:0:{}'),
	('8pb1epl9jb7pkdsu3n57aj5720',1425362281,'modx.user.contextTokens|a:0:{}'),
	('01chodndgivp4hbudenu9jbs23',1425362907,'modx.user.contextTokens|a:0:{}'),
	('e4rspavq0jn2ar83ih5nkht8d5',1425363757,'modx.user.contextTokens|a:0:{}'),
	('qp9bdvr4tq61sf0b6non7k9c81',1425363758,'modx.user.contextTokens|a:0:{}'),
	('skj804v8abj4qa2hkt464tasn1',1425364626,'modx.user.contextTokens|a:0:{}'),
	('c8jqrap3m6tr9hlnmqgt6c1dq5',1425364639,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('bdnt081ps8haq8dksp54st0ct6',1425364663,'modx.user.contextTokens|a:0:{}'),
	('vop749pvvahj71kbg2ajmjd6e1',1425364683,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('l95recemdqd3n69ifrpaa64ua7',1425364726,'modx.user.contextTokens|a:0:{}'),
	('8o7q7chj5cfgaiqsbngqvm9j56',1425365087,'modx.user.contextTokens|a:0:{}'),
	('n7ne621jihm9gov5ev14tvpfq1',1425365087,'modx.user.contextTokens|a:0:{}'),
	('uaij29u2jli8cnjjivfb8tjt55',1425365128,'modx.user.contextTokens|a:0:{}'),
	('0us70q6nhickkciro2ck8md3e0',1425365355,'modx.user.contextTokens|a:0:{}'),
	('mffcmiipkqkcas701n80viefn6',1425365356,'modx.user.contextTokens|a:0:{}'),
	('ctbaaeaat7pijv726p7n06n7r5',1425367754,'modx.user.contextTokens|a:0:{}'),
	('81egqnhf26vu0kbu5k4e22q247',1425367756,'modx.user.contextTokens|a:0:{}'),
	('i0tpmd6roscrrpr8i3petm5c65',1425370465,'modx.user.contextTokens|a:0:{}'),
	('23embrlvhstv0ne27f1i53dsq5',1425375713,'modx.user.contextTokens|a:0:{}'),
	('nt5tlh7rskrt2kdbd4rdi15bq1',1425376302,'modx.user.contextTokens|a:0:{}'),
	('gdba5lkiioo9uq9ghcebp006h7',1425376303,'modx.user.contextTokens|a:0:{}'),
	('qb8qg9cs3uhjnm0fkltvu22m67',1425376396,'modx.user.contextTokens|a:0:{}'),
	('5goqmmo4qv5f35ilvgujaikn75',1425376396,'modx.user.contextTokens|a:0:{}'),
	('l9usd5dmmnqe70prs9712bn1j3',1425378545,'modx.user.contextTokens|a:0:{}'),
	('hqmocppojv8p1026htld5du2o7',1425378791,'modx.user.contextTokens|a:0:{}'),
	('2ogh5jb2lutds4mm3st5u3idl4',1425379197,'modx.user.contextTokens|a:0:{}'),
	('1gg39sim2vfmcvt4tkhf33rhr3',1425379197,'modx.user.contextTokens|a:0:{}'),
	('i6b8inpga82pucbkd0mq4qj4r6',1425379224,'modx.user.contextTokens|a:0:{}'),
	('64kbh4aqe131ne29mai4s03r01',1425379251,'modx.user.contextTokens|a:0:{}'),
	('4d5vcgmp0hhpdf154bfbot3083',1425379252,'modx.user.contextTokens|a:0:{}'),
	('4g2jcabi8qmog8egn8m2i7igv0',1425380486,'modx.user.contextTokens|a:0:{}'),
	('j5eaeg9dqblod5biqi56nvddb7',1425380487,'modx.user.contextTokens|a:0:{}'),
	('i17jihdr3c0npr9oldfc4kvuk2',1425380890,'modx.user.contextTokens|a:0:{}'),
	('q7i0tn0ug5csbjg8ruh9iu7sg1',1425380891,'modx.user.contextTokens|a:0:{}'),
	('mcoqol6rhgh6se69p491l6qe82',1425381097,'modx.user.contextTokens|a:0:{}'),
	('d64gigaqpca4n5hc5q2prajd75',1425381097,'modx.user.contextTokens|a:0:{}'),
	('vdqn87a1op09td7dkt5trkgrn2',1425381326,'modx.user.contextTokens|a:0:{}'),
	('i6pqo36bec05edu2qtgb850id7',1425382310,'modx.user.contextTokens|a:0:{}'),
	('bf23bf6g2hc918iba1hifo84p2',1425382310,'modx.user.contextTokens|a:0:{}'),
	('lth8pt5s7crj1ujg8cntgc2qu2',1425382702,'modx.user.contextTokens|a:0:{}'),
	('c8a77575je00e3bc0s9e0mlda6',1425382774,'modx.user.contextTokens|a:0:{}'),
	('c4p4f955fgpnmqkoiks22lmcr3',1425383794,'modx.user.contextTokens|a:0:{}'),
	('jb9q2bhthg1uj2ev889qenroa3',1425383795,'modx.user.contextTokens|a:0:{}'),
	('6s518glrvckdlcsa9jlcgsem33',1425383817,'modx.user.contextTokens|a:0:{}'),
	('lr1eicu4jvff4fqjbcbcb88ev5',1425383991,'modx.user.contextTokens|a:0:{}'),
	('4bssalq0t9ll6oc5c3eqrqn5c4',1425384417,'modx.user.contextTokens|a:0:{}'),
	('p2om1g63mgfd99flr4t5cd8bc4',1425384730,'modx.user.contextTokens|a:0:{}'),
	('80lq5eiu2f3d6l986tft11mps0',1425384731,'modx.user.contextTokens|a:0:{}'),
	('aapkbr5384rbol69mutb41hfh3',1425385353,'modx.user.contextTokens|a:0:{}'),
	('9egl2jf3eamtqnifk3uvro41o2',1425385564,'modx.user.contextTokens|a:0:{}'),
	('1qtf3rf1o3nrvq9a5qvvnd07i2',1425385565,'modx.user.contextTokens|a:0:{}'),
	('uhci7opri12geun8u1guhj6791',1425385638,'modx.user.contextTokens|a:0:{}'),
	('l63motdfeck335qbod6ldl00k6',1425385639,'modx.user.contextTokens|a:0:{}'),
	('8jcvq6b70qm60qosmkq4enrjn4',1425386698,'modx.user.contextTokens|a:0:{}'),
	('jalf9cfrmt4i65lan3dih89n13',1425386699,'modx.user.contextTokens|a:0:{}'),
	('38b2o3pc7pkl485oru2ai7o2k6',1425387164,'modx.user.contextTokens|a:0:{}'),
	('p671ini0gh9e51u4tl3jcv8ms3',1425387165,'modx.user.contextTokens|a:0:{}'),
	('sikcvk1fnjtut9uts92n8249d2',1425387383,'modx.user.contextTokens|a:0:{}'),
	('5ldpkabmili56vdnlvf9g3tgc1',1425387384,'modx.user.contextTokens|a:0:{}'),
	('7b6l7sfi0qggl2iup08o27hbr6',1425387617,'modx.user.contextTokens|a:0:{}'),
	('nnjdt92o9igk5hea8c96ia7i32',1425387617,'modx.user.contextTokens|a:0:{}'),
	('ql3raof503pv86c9s5rhoint53',1425387619,'modx.user.contextTokens|a:0:{}'),
	('ql4h18h5vbfftjonjjt2nmibe3',1425388376,'modx.user.contextTokens|a:0:{}'),
	('dud8mrlkaqto180s93sh4d4kb4',1425388377,'modx.user.contextTokens|a:0:{}'),
	('o4vgnhljn4ca6v46qnb1213sr7',1425388531,'modx.user.contextTokens|a:0:{}'),
	('kq4nlkgqudc600b96t24pnbmq7',1425388531,'modx.user.contextTokens|a:0:{}'),
	('91sdmjq7jjlsmu1fbbtfarmo51',1425389118,'modx.user.contextTokens|a:0:{}'),
	('s8658o7aboo1of72hs6659nd87',1425390041,'modx.user.contextTokens|a:0:{}'),
	('qam6gt8mfagn0qgv13o4q9opg5',1425390042,'modx.user.contextTokens|a:0:{}'),
	('rjfq98rjnf1oifd7dmvfi368g7',1425392927,'modx.user.contextTokens|a:0:{}'),
	('s0abl5dr1ndqjj76gbvu3uffq5',1425392928,'modx.user.contextTokens|a:0:{}'),
	('23bdr45trthndqq2j4vmdg3170',1425393997,'modx.user.contextTokens|a:0:{}'),
	('g7cdljqrbvgcvk0vli2rhmo1p5',1425394014,'modx.user.contextTokens|a:0:{}'),
	('g866kfgqeih5if453lir9ius15',1425394015,'modx.user.contextTokens|a:0:{}'),
	('75s8f2hvqevlpb9rhmcfj1fsa3',1425394391,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('io5kl3nol9jda0jvavmdfdtee0',1425394918,'modx.user.contextTokens|a:0:{}'),
	('3fin19nnqfpn4mf89775mfu7l3',1425394919,'modx.user.contextTokens|a:0:{}'),
	('73clo9a8m38nehae3su6lhmu70',1425394972,'modx.user.contextTokens|a:0:{}'),
	('19nft4e47dv9crjvt63mvpsqa1',1425394973,'modx.user.contextTokens|a:0:{}'),
	('fq13jqm7f0f8q49gpme9r1km32',1425395325,'modx.user.contextTokens|a:0:{}'),
	('ob0ef3edp452e0pveosou1vb25',1425395325,'modx.user.contextTokens|a:0:{}'),
	('6pt0cimvkvmpit3m9kt2hdri64',1425395516,'modx.user.contextTokens|a:0:{}'),
	('ka5qohmp6bedolup3gmh400mb1',1425395847,'modx.user.contextTokens|a:0:{}'),
	('vv7ukk79aker8lsqm7uod4v9p4',1425395848,'modx.user.contextTokens|a:0:{}'),
	('todq7i044t2l8ca5e3tf5t9gq1',1425396249,'modx.user.contextTokens|a:0:{}'),
	('fbaclmvgvmm2mdvli8cga57tg6',1425396879,'modx.user.contextTokens|a:0:{}'),
	('3ts86smoko74t33p733ff1nah1',1425396879,'modx.user.contextTokens|a:0:{}'),
	('eobsss1v03qsibekeirf1oect3',1425397072,'modx.user.contextTokens|a:0:{}'),
	('n2bi3b3rbf0n6jp0p4o4kkf917',1425397176,'modx.user.contextTokens|a:0:{}'),
	('9aeuijmg21oo2rl8nlnmtpnpb7',1425397609,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('t765lv7hjlbmqe14jbds6u7lk2',1425397652,'modx.user.contextTokens|a:0:{}'),
	('a2uipkgsa50lmnn9membapes77',1425397730,'modx.user.contextTokens|a:0:{}'),
	('qeiioq1rlmcfipvf5ge9eiv5p6',1425397732,'modx.user.contextTokens|a:0:{}'),
	('apcg12hnu17qrsr946a82bs6a3',1425397789,'modx.user.contextTokens|a:0:{}'),
	('m6iad99sbougc4bku771ecsud4',1425398202,'modx.user.contextTokens|a:0:{}'),
	('2ak6jkmgbeflhng478jt968nm6',1425398205,'modx.user.contextTokens|a:0:{}'),
	('pkmugdmfb8rteeuiq0jii24h52',1425398218,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('c1i05ncgfbkfrreicdffbrf8n0',1425398357,'modx.user.contextTokens|a:0:{}'),
	('v69f0hdkmkvuev4namospcbg36',1425398379,'modx.user.contextTokens|a:0:{}'),
	('sug26fq2u1j7o3s48in8qcfau6',1425398379,'modx.user.contextTokens|a:0:{}'),
	('210n57hkcqvj1c4epc1adm1af0',1425398669,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('7plbt7m3ehqqjlbculjnoblpt5',1425398670,'modx.user.contextTokens|a:0:{}'),
	('e92p8q495ge44vjqf5cjohbfg7',1425399132,'modx.user.contextTokens|a:0:{}'),
	('8h7csei6cmjrcippc4cp92u6k0',1425399132,'modx.user.contextTokens|a:0:{}'),
	('av9490r4e30l5v458g57oaose3',1425399341,'modx.user.contextTokens|a:0:{}'),
	('p1d6in1k5c6p015e42j3lgad37',1425399341,'modx.user.contextTokens|a:0:{}'),
	('9nbbf3bdil6bmns1k77gsjsib2',1425399755,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('vkcb8hkks8vtc14p9fiifvseq7',1425399756,'modx.user.contextTokens|a:0:{}'),
	('uqk0nik228jso2tj6lqg6771d7',1425399757,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('q4qe9tj44gdrkuci68muncmog6',1425399758,'modx.user.contextTokens|a:0:{}'),
	('rqucmjibo478n1b7tjs8e7pb66',1425399759,'modx.user.contextTokens|a:0:{}'),
	('rbcg9v6flq81a2e571dfr5be55',1425399764,'modx.user.contextTokens|a:0:{}'),
	('6mrl95kc2amq5mn3q0oa9ml3l6',1425399810,'modx.user.contextTokens|a:0:{}'),
	('k5fgg63vrpj15mch643kjaqcc3',1425399811,'modx.user.contextTokens|a:0:{}'),
	('vfunvnrdt6sdvc8cj5gdem4il2',1425399980,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('a3mhugpd5f7clfihhbm6ft3cc4',1425400566,'modx.user.contextTokens|a:0:{}'),
	('3lct75nbibp75h49gn0r016c85',1425400567,'modx.user.contextTokens|a:0:{}'),
	('l4ho3bkgrn5b3hoplbj8jp4ju1',1425400615,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('13juert2191979vuv0eequ4f26',1425400620,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('gi49mfi0r16e050savovpbe031',1425400640,'modx.user.contextTokens|a:0:{}'),
	('mmhejd241rpup7q5g3749jin02',1425400913,'modx.user.contextTokens|a:0:{}'),
	('jmb333s01v4aihd5ekopkr7hr1',1425400916,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ccas7qeqg0hptlrdak1a7mj916',1425400919,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('momjl8kmgv761nashggeo3lrl6',1425400924,'modx.user.contextTokens|a:0:{}'),
	('jiihtjjkr50k4gmhs5pb5ef070',1425400925,'modx.user.contextTokens|a:0:{}'),
	('f74omqf5v971maedt95p72mr16',1425400930,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('9thur2nev0n4mkl2enit9hir53',1425400936,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('8jar8r8jt593l2fd50k8t309e1',1425400945,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('iu1tq8is7h09l9l97bca4k3n75',1425400945,'modx.user.contextTokens|a:0:{}'),
	('4gvh646ogplb3fi9vfpfr1c745',1425401837,'modx.user.contextTokens|a:0:{}'),
	('d2hndj0q1fj97fvmnareg8tga1',1425401838,'modx.user.contextTokens|a:0:{}'),
	('d7blv97icf0svf7g3uu16ikne1',1425403209,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ecf897tjbi9rppk11d8vl010f2',1425403325,'modx.user.contextTokens|a:0:{}'),
	('3t1uoilfa8tmbdjb1p1kvks0o2',1425403325,'modx.user.contextTokens|a:0:{}'),
	('vt6sc0ove28s5mag9ie4ossd30',1425403325,'modx.user.contextTokens|a:0:{}'),
	('h3iomrr0k9d0oqaia9g7vua097',1425403996,'modx.user.contextTokens|a:0:{}'),
	('ndms3tl0sucqpnhqb09ajq7v72',1425404303,'modx.user.contextTokens|a:0:{}'),
	('9queqhanhku6mvkds2q5mliq47',1425404304,'modx.user.contextTokens|a:0:{}'),
	('15cmbase1frcbhmf8vijt02ur2',1425404841,'modx.user.contextTokens|a:0:{}'),
	('t7vnlgd2u1gclk5t8mod0odee0',1425404842,'modx.user.contextTokens|a:0:{}'),
	('uo5dhfqdvs2cbtq6bbn2tb4ei3',1425405100,'modx.user.contextTokens|a:0:{}'),
	('hn4c4vodjc5p0j4asilrkrtav0',1425405214,'modx.user.contextTokens|a:0:{}'),
	('vq4v7gbjsgkerdkkkrfkeqpih6',1425405214,'modx.user.contextTokens|a:0:{}'),
	('809av358hc559v1t0jrnhumal2',1425405778,'modx.user.contextTokens|a:0:{}'),
	('70pt9jfnnuu39ld1sdaq5fnrp1',1425405816,'modx.user.contextTokens|a:0:{}'),
	('teb7a821169953idesi3dqkku4',1425405817,'modx.user.contextTokens|a:0:{}'),
	('2uelm9osuf2at71jdckp1cmnd5',1425405844,'modx.user.contextTokens|a:0:{}'),
	('rrvpbaleractkt2qqu0fgv9ef4',1425405859,'modx.user.contextTokens|a:0:{}'),
	('e0colqqktj92pbmsered0hi1l2',1425406186,'modx.user.contextTokens|a:0:{}'),
	('v07d4sghkit12srpek6hi5hps0',1425406410,'modx.user.contextTokens|a:0:{}'),
	('ai8mrntsmhs7a18skpjku1uoe3',1425406412,'modx.user.contextTokens|a:0:{}'),
	('bcmeudojq3le6impp0ekrejmb2',1425406929,'modx.user.contextTokens|a:0:{}'),
	('caph2hcq54443tcqh57khr90n0',1425407669,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('c42iroh1rv67saasfujekthv03',1425407770,'modx.user.contextTokens|a:0:{}'),
	('ljvbjpvqfeth7ci33m9gmufog5',1425407772,'modx.user.contextTokens|a:0:{}'),
	('hu85mnjcsjc85cfqrfsps32n67',1425407811,'modx.user.contextTokens|a:0:{}'),
	('3lt0dri4ogo43gcj4bpafu7757',1425407811,'modx.user.contextTokens|a:0:{}'),
	('qcdj36eqagf80fi68cclrpsbk4',1425408041,'modx.user.contextTokens|a:0:{}'),
	('an6k9a6vmokenkc8fvhcbsdp15',1425408042,'modx.user.contextTokens|a:0:{}'),
	('t9t3ffmndbe2teah8m32ect1c0',1425408051,'modx.user.contextTokens|a:0:{}'),
	('p95jeqbh7nfs51sfa0mtrssh71',1425408326,'modx.user.contextTokens|a:0:{}'),
	('8toh702db80iivvvehm4bgo9b6',1425408326,'modx.user.contextTokens|a:0:{}'),
	('7ov3s9hp0jik9k9rf6ielpvjk1',1425408819,'modx.user.contextTokens|a:0:{}'),
	('bsp84dksueerpr47t1fpsejq11',1425408820,'modx.user.contextTokens|a:0:{}'),
	('2iu5jh0f1q0rviasa4bnto2nf5',1425409376,'modx.user.contextTokens|a:0:{}'),
	('mukenv0sv8bj1qk4vltnsqdk04',1425409376,'modx.user.contextTokens|a:0:{}'),
	('5chn08eh8ll0muqjtjkoqdb1e4',1425410206,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('evc10nkhpv3g7dq05lkffh0jk0',1425410358,'modx.user.contextTokens|a:0:{}'),
	('n6vi7obg4jle9ular9gv2mi6i1',1425410449,'modx.user.contextTokens|a:0:{}'),
	('h9993663q6metm35ml1onsm8j2',1425410489,'modx.user.contextTokens|a:0:{}'),
	('5u5n8lh7k3p053riut6ht0ak34',1425410489,'modx.user.contextTokens|a:0:{}'),
	('qo7v334ao8clm7urm4icpen5n5',1425410495,'modx.user.contextTokens|a:0:{}'),
	('5735gonfk515mf28l6bmmm8r53',1425410495,'modx.user.contextTokens|a:0:{}'),
	('dr3j7guluj0e5qmcf737j4gso3',1425411370,'modx.user.contextTokens|a:0:{}'),
	('9rp8nh118mf1af3e2nfuhu8aj4',1425411371,'modx.user.contextTokens|a:0:{}'),
	('5digalght8slagvchm6fgnk2f2',1425411879,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('admal1vukhe0ttmmovffck67s7',1425412659,'modx.user.contextTokens|a:0:{}'),
	('ta6c2ddma0tga4nirclpe8av63',1425412660,'modx.user.contextTokens|a:0:{}'),
	('j1g9ahva0g7v0hd0uv63m2dng2',1425413208,'modx.user.contextTokens|a:0:{}'),
	('aav1vqg7qb7fsru0o8193hf5i1',1425413213,'modx.user.contextTokens|a:0:{}'),
	('musdg5nuhede7gr8afajsd6rn1',1425414123,'modx.user.contextTokens|a:0:{}'),
	('j7b0onmlikiqfv47scin9l9qa3',1425415697,'modx.user.contextTokens|a:0:{}'),
	('k1dfcq2161bqk0eqtjnjd4gam7',1425415698,'modx.user.contextTokens|a:0:{}'),
	('pc0va5msg3sqk8rb4njovt97e4',1425415721,'modx.user.contextTokens|a:0:{}'),
	('03ck0mtsm14vrfhcihjk037nd2',1425416120,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('q1159rbmchocj134o240s0kur0',1425416679,'modx.user.contextTokens|a:0:{}'),
	('9ga1360jvibglj5pabs8m28mq7',1425416953,'modx.user.contextTokens|a:0:{}'),
	('ulsjquu64f8l6cnc9e7c3elqo4',1425416954,'modx.user.contextTokens|a:0:{}'),
	('rae5bcpcvn0mjnk664s6c9b682',1425417383,'modx.user.contextTokens|a:0:{}'),
	('0h6g1tagql7nuhgjokd8k0api6',1425417791,'modx.user.contextTokens|a:0:{}'),
	('41q1h10r8vfsehkp3p3spbg340',1425417791,'modx.user.contextTokens|a:0:{}'),
	('e4oo0bugnjt6elal9dlaa6jdf3',1425418720,'modx.user.contextTokens|a:0:{}'),
	('iaatmi01fet9p5frhmv2edd133',1425419050,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('p1dtkl99anlejciv5qb8c4h526',1425419612,'modx.user.contextTokens|a:0:{}'),
	('jd0e08talb2vc9g51apqpnial2',1425419632,'modx.user.contextTokens|a:0:{}'),
	('tnvqon1pci5b6ig5oc47d2iad1',1425419632,'modx.user.contextTokens|a:0:{}'),
	('881ooasbai5n084o3j5537bpj3',1425419633,'modx.user.contextTokens|a:0:{}'),
	('l7apojmd9gmgdr3glnhdl7cfa1',1425419633,'modx.user.contextTokens|a:0:{}'),
	('pmeosh0nahjqnflmsqqp8pk0n3',1425419634,'modx.user.contextTokens|a:0:{}'),
	('eht1ska8qc3td73d28ek71rgs2',1425419634,'modx.user.contextTokens|a:0:{}'),
	('9gg7p7083vuljl1e47s58dn683',1425419675,'modx.user.contextTokens|a:0:{}'),
	('ln23jkhm1jp2rreftd47idc1n5',1425419792,'modx.user.contextTokens|a:0:{}'),
	('ftjk0b5u4lll5jdclluqhab4a1',1425419801,'modx.user.contextTokens|a:0:{}'),
	('dr717eee2o7krfkgrrqvfvoho2',1425419818,'modx.user.contextTokens|a:0:{}'),
	('eqvsc605asp3av6obbbqs8n727',1425419872,'modx.user.contextTokens|a:0:{}'),
	('tig59b534imam3nrbtdqthf4q4',1425420498,'modx.user.contextTokens|a:0:{}'),
	('u06udnq211kpej4dk55u0tbo74',1425420499,'modx.user.contextTokens|a:0:{}'),
	('4i4k5393ckfseoj37gcp7r2gb5',1425420649,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('0ci5u6udo5lkkdc30gsuatvkr0',1425420651,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('pg8jvrgu7jrsuofo9gkhf9tns4',1425421000,'modx.user.contextTokens|a:0:{}'),
	('c8gi5pagmk2vaku20lnvtu3qr7',1425421026,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('gqhb5rib66fvu042jeohrk9ju0',1425421093,'modx.user.contextTokens|a:0:{}'),
	('7mjhtqv8of9cr5ks9am4hsqcp4',1425421241,'modx.user.contextTokens|a:0:{}'),
	('vocclpf23bb6ogisl9tqhrcio3',1425421242,'modx.user.contextTokens|a:0:{}'),
	('kcqqfcmn64to14ne9m8ugvvau5',1425421299,'modx.user.contextTokens|a:0:{}'),
	('6oh5cm17o0sj9oimbt3bh4rc46',1425421643,'modx.user.contextTokens|a:0:{}'),
	('otl57hu67t420lfsnfvgus9pq4',1425421734,'modx.user.contextTokens|a:0:{}'),
	('tsun7beugfr5nkvkjop4tm3mp1',1425421735,'modx.user.contextTokens|a:0:{}'),
	('vh4psbs3vnfldjdkrnfstv00s2',1425421955,'modx.user.contextTokens|a:0:{}'),
	('7fjjpfm4a1jhcau5l44hq8omc6',1425422022,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('sh6our8tp3fvb42u7mms52ra61',1425422081,'modx.user.contextTokens|a:0:{}'),
	('5p2jeqqokme2liifdbspb449q3',1425422082,'modx.user.contextTokens|a:0:{}'),
	('sjajjslrq0779a9ov9b1um6dm4',1425422082,'modx.user.contextTokens|a:0:{}'),
	('3g87n7cra5gljsk5nafvqge861',1425422249,'modx.user.contextTokens|a:0:{}'),
	('l6o1586s3he6f2517dcr4qht53',1425422594,'modx.user.contextTokens|a:0:{}'),
	('jht9nb7soedg2i50e3p8cnlha7',1425422594,'modx.user.contextTokens|a:0:{}'),
	('u8qdud92geidb9d4mpune8r730',1425422658,'modx.user.contextTokens|a:0:{}'),
	('969dmf085colpulcce51liub10',1425422660,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('u26pi02ccpq1cbo8lpeo0p4b15',1425422661,'modx.user.contextTokens|a:0:{}'),
	('12b4updh0j2off44mp2a5g1s71',1425423570,'modx.user.contextTokens|a:0:{}'),
	('qgnoieju2busm8pnid88eki130',1425423739,'modx.user.contextTokens|a:0:{}'),
	('ot9fnsvastv8vqfgbil352e0o6',1425424080,'modx.user.contextTokens|a:0:{}'),
	('eh8s2ic7b85kik5k81gv2drba0',1425424468,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('94i5ge8k984bl4js61sjlh18o3',1425424383,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('s80kh0fkbq7l5h2v6vebkvipp5',1425424542,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('fcndmi7novo8kfm3ccle3fs1f3',1425424667,'modx.user.contextTokens|a:0:{}'),
	('mrpaimemkesqk9bcpj2071kam0',1425425048,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('kmck48304923ppuck87v0g4k13',1425425089,'modx.user.contextTokens|a:0:{}'),
	('e14j25a630ndfvptbgeed1ibo6',1425425089,'modx.user.contextTokens|a:0:{}'),
	('frqtreu6se0h661oqrqgdnt0b1',1425425680,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('e031s1f8vf76vqkrqihekfii92',1425425892,'modx.user.contextTokens|a:0:{}'),
	('s9b4nbqlijibtr8lfakka20kf7',1425425893,'modx.user.contextTokens|a:0:{}'),
	('7l8to4kdp2hs91brpln8i2reh0',1425426189,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('7od4dhvie2ukh0cjmf28kbg232',1425426909,'modx.user.contextTokens|a:0:{}'),
	('bqruqho7klt7sg9eiqqjptqs71',1425427343,'modx.user.contextTokens|a:0:{}'),
	('3q20adra0293oj5ksrr4s50ad4',1425427344,'modx.user.contextTokens|a:0:{}'),
	('q177om4hn7u91djf6jaqnhkdo2',1425428436,'modx.user.contextTokens|a:0:{}'),
	('tefcal4g774ke13unlnipjnur6',1425428599,'modx.user.contextTokens|a:0:{}'),
	('9630hhnth3b6cc1dqr5qs7vau6',1425428895,'modx.user.contextTokens|a:0:{}'),
	('61f3opvaucbbkgcm1g2ukufc60',1425428895,'modx.user.contextTokens|a:0:{}'),
	('0mah0627j6ccc2qit4hp2bt2a2',1425429466,'modx.user.contextTokens|a:0:{}'),
	('sel84tk23420qb1ea067866s32',1425430069,'modx.user.contextTokens|a:0:{}'),
	('i8ofl9ub3ungmut8knbe6cg4h6',1425430634,''),
	('mnqli5dljkq43t6n2jkg83ad52',1425430635,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('tfpppq7gn9rl1sp3n17j05rri4',1425430665,'modx.user.contextTokens|a:0:{}'),
	('lopq6tb7ei2mr76881ppvqevl2',1425430744,'modx.user.contextTokens|a:0:{}'),
	('9rr3lf5b3pekcffb4mdb5692v1',1425430745,'modx.user.contextTokens|a:0:{}'),
	('5aa2060kv7kmgoq43plflo9s57',1425430745,'modx.user.contextTokens|a:0:{}'),
	('h7cf70c35pr9g2uu9bspk82a56',1425430746,'modx.user.contextTokens|a:0:{}'),
	('73p3itjveqluft80n64debig80',1425430746,'modx.user.contextTokens|a:0:{}'),
	('q04ciu511dgrjnq8s2gutmini4',1425430982,'modx.user.contextTokens|a:0:{}'),
	('s6as1sqqhjdr2s566q2datt4l3',1425431532,'modx.user.contextTokens|a:0:{}'),
	('m6in6spcl96oubu4ei3v2qpi85',1425431533,'modx.user.contextTokens|a:0:{}'),
	('40bkcn8tvrv23hked1ifmshjd7',1425431655,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('7smeib06sqt4a7vskvv5e9pkq0',1425431799,'modx.user.contextTokens|a:0:{}'),
	('aq0dl6ifcpno0t2f8bi41olhc6',1425431800,'modx.user.contextTokens|a:0:{}'),
	('ad8l7ac55u1qb6hegmatp1f3t2',1425431829,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('v1u44bemsb78oro37k205t8100',1425431998,'modx.user.contextTokens|a:0:{}'),
	('ebgjdsruk9pp75krt3aq9c0ho3',1425431999,'modx.user.contextTokens|a:0:{}'),
	('qbj9rsdt0tudmot3f8mvl24ns0',1425432191,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('nddl72phhb1ggmji4h79sv66g7',1425432264,'modx.user.contextTokens|a:0:{}'),
	('mdkvf83q84l17u0jbv16mris24',1425432481,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('80rbr4o6ah59jbju32cjp3m9p6',1425432573,'modx.user.contextTokens|a:0:{}'),
	('l1tcci09tuq474s6bef4lg2dk4',1425432697,'modx.user.contextTokens|a:0:{}'),
	('fh5kgc6c72ev88cp3st01m45k6',1425432700,'modx.user.contextTokens|a:0:{}'),
	('hk8l059adbo4r3nj9sn8a66185',1425432719,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('oqg8a7neeh821tddmoifrlmu55',1425433686,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('k1j617bla55skt6jubvllqqvt5',1425434884,'modx.user.contextTokens|a:0:{}'),
	('q7un2o82uef3r3il1pt8i5njr1',1425434885,'modx.user.contextTokens|a:0:{}'),
	('hc2d9opghlpa2p4g22mk0o06k3',1425435729,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('eeoqrcs8hodgeuf7tnkm41ufc1',1425435743,'modx.user.contextTokens|a:0:{}'),
	('2nf77ictcr4afudbhaotu57hd1',1425436354,'modx.user.contextTokens|a:0:{}'),
	('gfmj89493pbtnectq24nllea35',1425436451,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ag48dkve1k1qs07ap9kng6aek1',1425437150,'modx.user.contextTokens|a:0:{}'),
	('tkfttg2qdci4u8koj75inv1bg7',1425437507,'modx.user.contextTokens|a:0:{}'),
	('hbvcr90vfk58lnkakjlovnvf97',1425437510,'modx.user.contextTokens|a:0:{}'),
	('ke4bcsi9b8kgp4mb5cmglgfha3',1425437512,'modx.user.contextTokens|a:0:{}'),
	('oqpno8dtiqr8o3fs07mj2pea45',1425437929,'modx.user.contextTokens|a:0:{}'),
	('43m652tef04msgop4ffp5l6em4',1425437930,'modx.user.contextTokens|a:0:{}'),
	('61d0qh0lv4hmedieukrkdkhck0',1425437932,'modx.user.contextTokens|a:0:{}'),
	('3lgdpagf7v87p5qs7s6ef0ud76',1425438318,'modx.user.contextTokens|a:0:{}'),
	('qrkjtsuhfipq284msmqv212l35',1425438327,'modx.user.contextTokens|a:0:{}'),
	('cirbdd7f21md3aghdajdcgk5d3',1425438328,'modx.user.contextTokens|a:0:{}'),
	('0d1ltpfc1klp0a1nuf52soqva1',1425438545,'modx.user.contextTokens|a:0:{}'),
	('ko4f05qmrlt6rj0q09vggpjkb1',1425438546,'modx.user.contextTokens|a:0:{}'),
	('a62meu0td3bj9239h75ocv19a4',1425438971,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('p2dbtp1ia41ajakrhiaafgbl21',1425439263,'modx.user.contextTokens|a:0:{}'),
	('r3oj6nsutqocvevdv2p3o0g184',1425439295,'modx.user.contextTokens|a:0:{}'),
	('3kv2dtanedj5laqfl6cfil2k81',1425439411,'modx.user.contextTokens|a:0:{}'),
	('ceto4638obev89d5emodrmu8j2',1425439787,'modx.user.contextTokens|a:0:{}'),
	('sa4csq1qtukv8oceelkkduou61',1425440522,'modx.user.contextTokens|a:0:{}'),
	('5ekpuvgrlt05cdsc3g26a0q510',1425440522,'modx.user.contextTokens|a:0:{}'),
	('r1kc471p2minc18ep3mi5nmgi1',1425440536,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('3ejgd4lb0i1r5uhjaebr8qtpo0',1425440767,'modx.user.contextTokens|a:0:{}'),
	('2lq88viptaan30mro1e9s911f1',1425442105,'modx.user.contextTokens|a:0:{}'),
	('chtotunhq1gauinuoetu9gjb75',1425442119,'modx.user.contextTokens|a:0:{}'),
	('1q9kbtrpeicgu2fsen3in2rdq3',1425442279,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('l35g1043ugocbke5k60f1r4493',1425442995,'modx.user.contextTokens|a:0:{}'),
	('bi6l4eifgd7fc0bmoafia0qju5',1425442996,'modx.user.contextTokens|a:0:{}'),
	('fokunilqsk56p7o6o3vt764in1',1425444392,'modx.user.contextTokens|a:0:{}'),
	('ggov5mhmvuo4k197ub0tmpdgm2',1425444447,'modx.user.contextTokens|a:0:{}'),
	('lk8e3c36mohnusv1h5hri8pdo2',1425444485,'modx.user.contextTokens|a:0:{}'),
	('ge9rqrihnncv8k334u3odfjdl0',1425444486,'modx.user.contextTokens|a:0:{}'),
	('hd5fej42pk4mn0uultsmgrb062',1425445581,'modx.user.contextTokens|a:0:{}'),
	('24auooioo23f41diutog304oi5',1425445676,'modx.user.contextTokens|a:0:{}'),
	('99mnjqdq1qi5583p4a3m96mq31',1425445698,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('dv8bhaljjplpmtgo33334mulo7',1425445802,'modx.user.contextTokens|a:0:{}'),
	('0itk1qrotqtgik12b9ick687r6',1425446098,'modx.user.contextTokens|a:0:{}'),
	('du0iane5j2av7patqgbldscdj6',1425446682,'modx.user.contextTokens|a:0:{}'),
	('kq2e95vnvjtdrkeb7sep1ao3i0',1425447522,'modx.user.contextTokens|a:0:{}'),
	('h1gliquqhi2dgehhd4sua7odi3',1425449173,'modx.user.contextTokens|a:0:{}'),
	('quglfeu32926pvual2hkd41u94',1425449531,'modx.user.contextTokens|a:0:{}'),
	('l2n2ffer32pv4ijcd10g48fn70',1425449532,'modx.user.contextTokens|a:0:{}'),
	('1vggvirb2s7jhjeq1sjk1ef7m4',1425449532,'modx.user.contextTokens|a:0:{}'),
	('44jcv8gaj3ankqk302224fhm43',1425449706,'modx.user.contextTokens|a:0:{}'),
	('88b61ake5r2r3geq736te3u4k1',1425449707,'modx.user.contextTokens|a:0:{}'),
	('aivhdl4o4vtqijj2c029sv5ce7',1425451351,'modx.user.contextTokens|a:0:{}'),
	('eo7aphv7v9spv08gmcuu3f0ic1',1425451353,'modx.user.contextTokens|a:0:{}'),
	('9i3v4rbv5ahroba1h4a7ul8u12',1425451578,'modx.user.contextTokens|a:0:{}'),
	('o47aed6hui0hi4feor2gh7ogv7',1425451578,'modx.user.contextTokens|a:0:{}'),
	('qh5oleo26lkjs0f58jd57hqbh6',1425453716,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('ml0i2md0vkkdrsnkqvj30ohm12',1425456295,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('9i2f2t2vmi7m8uhju3ckhqhn37',1425460827,'modx.user.contextTokens|a:0:{}'),
	('9rns7vsd7m6baodbgbfjlvil42',1425461115,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ivcaanvucaldl5mkrhkq4clvh0',1425467626,'modx.user.contextTokens|a:0:{}'),
	('ai74c22em3ejd19lvdch5708l4',1425467626,'modx.user.contextTokens|a:0:{}'),
	('hnj123fuvhcsqo9edtu3k5fej4',1425467739,'modx.user.contextTokens|a:0:{}'),
	('rbn3ahcqguf9u1h5vgn2o97me2',1425468552,'modx.user.contextTokens|a:0:{}'),
	('9if5qdd146597d25glkieoulh2',1425468683,'modx.user.contextTokens|a:0:{}'),
	('aha0p489a2kncuhhvj5h1h8hl7',1425469055,'modx.user.contextTokens|a:0:{}'),
	('epgf8vge99u9ceko1hv7tea1m2',1425469391,'modx.user.contextTokens|a:0:{}'),
	('gtsvrcfoqha2p3tnreahncc2d1',1425469391,'modx.user.contextTokens|a:0:{}'),
	('2stfncev9pp87nanket1d4m0a3',1425470070,'modx.user.contextTokens|a:0:{}'),
	('ns110s7p30vvd5k4jsi8qtjtm4',1425470360,'modx.user.contextTokens|a:0:{}'),
	('8uffc510i8a707cafa2tda0n46',1425470643,'modx.user.contextTokens|a:0:{}'),
	('fcus7q6kb1h6futpggrsdaarp1',1425470760,'modx.user.contextTokens|a:0:{}'),
	('9ckne0vtc7p90fohc0diar45p6',1425471122,'modx.user.contextTokens|a:0:{}'),
	('7oqv08bk4hgbq5cdv940ctc2e7',1425471530,'modx.user.contextTokens|a:0:{}'),
	('hkt7bc0aioglaog1uuo2ioset3',1425471531,'modx.user.contextTokens|a:0:{}'),
	('f4e3dhno8chlf9lmbvbttiq6m1',1425471558,'modx.user.contextTokens|a:0:{}'),
	('uavarirosjmd9r8n2q3agraj43',1425471644,'modx.user.contextTokens|a:0:{}'),
	('35ogo6hfsh0390atorsi6bi2i1',1425471645,'modx.user.contextTokens|a:0:{}'),
	('ijtvto3b4gstfj547ftdcdsed7',1425471646,'modx.user.contextTokens|a:0:{}'),
	('p40kn8j47ath0t80h92gupmm45',1425471647,'modx.user.contextTokens|a:0:{}'),
	('p17f3dtl5vasep43nb2lsshro0',1425471662,'modx.user.contextTokens|a:0:{}'),
	('hbqvh7000a7rs1t9j9qoh7onb7',1425471670,'modx.user.contextTokens|a:0:{}'),
	('30b8d2mtbuinbvs04eoh3c1r53',1425471708,'modx.user.contextTokens|a:0:{}'),
	('6qtpoddqsrindi6k1jqpqabmj2',1425471708,'modx.user.contextTokens|a:0:{}'),
	('r4batu47og3p71qa7e0ojvabn3',1425471891,'modx.user.contextTokens|a:0:{}'),
	('89dmkqrn8pc8cvt6h1l6ala5s6',1425471900,'modx.user.contextTokens|a:0:{}'),
	('m13i0kr7phvvg91bnt88qat2h2',1425472724,'modx.user.contextTokens|a:0:{}'),
	('c6hhrk75p890m78krh3d5ql1f7',1425472729,'modx.user.contextTokens|a:0:{}'),
	('8u1m8q96k7roe8p6ubospkm4b7',1425472733,'modx.user.contextTokens|a:0:{}'),
	('2p6tcojo6titup4p1cbf1us506',1425472736,'modx.user.contextTokens|a:0:{}'),
	('6enop2k92ouh4gamq2g0ijdds4',1425472740,'modx.user.contextTokens|a:0:{}'),
	('53lj59k8pfa73ekpf4lsdoesu1',1425472744,'modx.user.contextTokens|a:0:{}'),
	('r93d0ootamv9rvivcar8mkg0d0',1425472748,'modx.user.contextTokens|a:0:{}'),
	('rcb081gt973pnhrohinuu7do30',1425472752,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('ib941is7ajlesptvcnjdi7lts3',1425472755,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('oau0un9j2917dattl921t3nua5',1425472759,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('29q0h327u685ujgdn49dll9pi4',1425472763,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('b9pdeso4e28nsirmpa83q1fi15',1425472766,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('d52us7kjeot9ccm4b0b2ovh6k4',1425472770,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('cqltim8bhcrh4dl5i862l1pcl3',1425472774,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('9i3h5fk0eu7hj7ttiion1uv075',1425472779,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('dg984asamcv60o1nc1vppkp1k3',1425474719,'modx.user.contextTokens|a:0:{}'),
	('jness53lvkvgtjf3l4d1csgrj0',1425474749,'modx.user.contextTokens|a:0:{}'),
	('dut3b7e4j812n0k42ok4bu8vl7',1425474750,'modx.user.contextTokens|a:0:{}'),
	('0r1rqrni30p4c0pn5sjua7kaf6',1425475529,'modx.user.contextTokens|a:0:{}'),
	('pucbekdqvvkv563rfh96gl4064',1425475529,'modx.user.contextTokens|a:0:{}'),
	('7cmg8816qmqcod1esr69ccdpe3',1425475807,'modx.user.contextTokens|a:0:{}'),
	('gi4t9bbv2uvbjbm3lv1cih86n3',1425476181,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('5vdcdnsgkkj5421k4h3uk3aap3',1425477527,'modx.user.contextTokens|a:0:{}'),
	('7n445ufnu92m4b1sad9tfcqjc5',1425477528,'modx.user.contextTokens|a:0:{}'),
	('tq28cbt16lfontegqf32mfv017',1425477528,'modx.user.contextTokens|a:0:{}'),
	('f6simepffo3n9k83bm6qa4k325',1425477532,'modx.user.contextTokens|a:0:{}'),
	('m74ejmc8pi7abgjqp6malmbsf3',1425477538,'modx.user.contextTokens|a:0:{}'),
	('cib1rni2rf0dpbk3ntsutdpvp2',1425491002,'modx.user.contextTokens|a:0:{}'),
	('u3pudvvosu2urcnr34b06rlae1',1425491763,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('l1ocdp7275ieelf8ookp0ij2v1',1425491859,'modx.user.contextTokens|a:0:{}');

/*!40000 ALTER TABLE `modx_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_content`;

CREATE TABLE `modx_site_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT 'document',
  `contentType` varchar(50) NOT NULL DEFAULT 'text/html',
  `pagetitle` varchar(255) NOT NULL DEFAULT '',
  `longtitle` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) DEFAULT '',
  `link_attributes` varchar(255) NOT NULL DEFAULT '',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pub_date` int(20) NOT NULL DEFAULT '0',
  `unpub_date` int(20) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `isfolder` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `introtext` text,
  `content` mediumtext,
  `richtext` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` int(10) NOT NULL DEFAULT '0',
  `menuindex` int(10) NOT NULL DEFAULT '0',
  `searchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `cacheable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0',
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` int(20) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deletedon` int(20) NOT NULL DEFAULT '0',
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `publishedon` int(20) NOT NULL DEFAULT '0',
  `publishedby` int(10) NOT NULL DEFAULT '0',
  `menutitle` varchar(255) NOT NULL DEFAULT '',
  `donthit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `privateweb` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `privatemgr` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `content_dispo` tinyint(1) NOT NULL DEFAULT '0',
  `hidemenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_key` varchar(100) NOT NULL DEFAULT 'modDocument',
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  `content_type` int(11) unsigned NOT NULL DEFAULT '1',
  `uri` text,
  `uri_override` tinyint(1) NOT NULL DEFAULT '0',
  `hide_children_in_tree` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_tree` tinyint(1) NOT NULL DEFAULT '1',
  `properties` mediumtext,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`),
  KEY `published` (`published`),
  KEY `pub_date` (`pub_date`),
  KEY `unpub_date` (`unpub_date`),
  KEY `parent` (`parent`),
  KEY `isfolder` (`isfolder`),
  KEY `template` (`template`),
  KEY `menuindex` (`menuindex`),
  KEY `searchable` (`searchable`),
  KEY `cacheable` (`cacheable`),
  KEY `hidemenu` (`hidemenu`),
  KEY `class_key` (`class_key`),
  KEY `context_key` (`context_key`),
  KEY `uri` (`uri`(333)),
  KEY `uri_override` (`uri_override`),
  KEY `hide_children_in_tree` (`hide_children_in_tree`),
  KEY `show_in_tree` (`show_in_tree`),
  KEY `cache_refresh_idx` (`parent`,`menuindex`,`id`),
  FULLTEXT KEY `content_ft_idx` (`pagetitle`,`longtitle`,`description`,`introtext`,`content`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_content` WRITE;
/*!40000 ALTER TABLE `modx_site_content` DISABLE KEYS */;

INSERT INTO `modx_site_content` (`id`, `type`, `contentType`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `class_key`, `context_key`, `content_type`, `uri`, `uri_override`, `hide_children_in_tree`, `show_in_tree`, `properties`)
VALUES
	(1,'document','text/html','Home',' Bowing basement walls, bowed walls, cracked foundation settling foundation, settlement. leaning chimney, central Iowa, Des Moines Iowa basement repair. Waterproofing, water in basement','Grip-Tite foundation reapir (Iowa) offers the complete scope of foundation repair and settlement products that will fix or repair any foundation settlement issue. Grip-Tite manufactures products that go along with the following: Foundation Repair','index','',1,0,0,0,0,'','<p>Get Grip-Tite. Get it Done Right. You only need to remember those words. When it comes to any type of foundation repair â€“ be it residential or commercial â€“ Grip-Tite Foundation Repair has the expertise, the industry-leading products, and the thoroughness and care youâ€™d expect from an Iowa-based company using products made right here in Iowa.</p>',1,3,0,1,1,1,1380601770,6,1425318204,0,0,0,0,0,'',0,0,0,0,0,'modDocument','web',1,'index',0,0,1,NULL),
	(2,'document','text/html','Foundation Repair',' Foundation repair des moines, central Iowa, foundation settling, sinking basement, sticking doors, sticking windows, drywall cracking, settling house, settling slab, settling basement, sinking slab, stair step cracks, brick cracks, settlement,','Grip-Tite of central Iowa offers the complete scope of foundation repair and settlement products that will fix or repair any foundation settlement issue. Grip-Tite manufactures products that go along with the following: Foundation Repair.','foundation-repair','',1,0,0,0,1,'','<h1>Foundation Repair</h1>\r\n<p><strong>Signs of Foundation Failure</strong></p>\r\n<ul>\r\n<li>Windows and doors are sticking and hard to open</li>\r\n<li>Multiple nail pops areÂ appearing in ceilings and walls</li>\r\n<li>Sticking Doors</li>\r\n<li>There are large gaps in window and door frames</li>\r\n<li>Window and/or door trim are developing spaces</li>\r\n<li>Floors are starting to settle and become uneven</li>\r\n<li>Bowed or leaning basement walls</li>\r\n<li>Interior plaster walls are cracking</li>\r\n<li>Chimneys are tilting or leaning</li>\r\n<li>Foundations are sinking</li>\r\n<li>Cracks in your basement walls</li>\r\n</ul>\r\n<p><strong style=\"line-height: 1.5em;\">Proven Grip-TiteÂ® Solutions</strong></p>\r\n<p><strong><img style=\"margin: 15px; float: right;\" src=\"client-files/images/reisetter%20corner.JPG\" alt=\"\" width=\"250\" height=\"333\" /></strong></p>\r\n<ul>\r\n<li><a href=\"http://www.griptite.com/applications/foundation-settlement.aspx\" target=\"blank\">Grip-TiteÂ® Push Pier System</a></li>\r\n<li><a href=\"http://www.griptite.com/applications/new-construction.aspx\" target=\"blank\">Grip-TiteÂ® Helical Pier System</a></li>\r\n</ul>\r\n<p>All Grip-Tite Dealers are thoroughly trained and certified in the installation of the Grip-TiteÂ® Foundation System and are supported by the company\'s over 90 years of earth anchoring experience.</p>',1,6,1,1,1,1,1383843416,6,1425412960,0,0,0,0,0,'',0,0,0,0,0,'modDocument','web',1,'foundation-repair/',0,0,1,NULL),
	(3,'document','text/html','Commercial Foundations','caissons, geo piers, spread footings, deep foundations, settlement prevention, over excavation alternative, Des Moines Foundation contractor','Grip-Tite Foundation Repair in central Iowa can provide helical piles for commercial foundations. Helical Piles can be used instead of over excavations, caissons, spread footings.','commercial-foundations','',1,0,0,0,1,'','<h1>Commercial Foundations</h1>\r\n<p>Â </p>\r\n<p>Grip-Tite Foundation Repair offers a variety of high-capacity foundation stabilization products for use in the following:</p>\r\n<p>Please Refer to ourÂ Grip-TiteÂ <a href=\"http://www.griptite.com/products/products.aspx\" target=\"blank\">Products</a>Â page for product specifications.</p>\r\n<ul>\r\n<li>Piering or underpinning of commercial and residential structures</li>\r\n<li>Deep foundation alternatives for commercial and residential construction</li>\r\n<li>Concrete slab stabilization and lifting</li>\r\n<li>Tiebacks and wall anchors</li>\r\n<li>Boathouse and sea wall repair</li>\r\n</ul>\r\n<p>Helical Piles can be used to avoid over excavation.</p>\r\n<p>Â </p>',1,6,2,1,1,4,1423545680,4,1424344408,0,0,0,1423545660,4,'',0,0,0,0,0,'modDocument','web',1,'commercial-foundations/',0,0,1,NULL),
	(4,'document','text/html','Testimonials',' Bowing basement walls, bowed walls, cracked foundation settling foundation, settlement. leaning chimney, central Iowa, Des Moines Iowa basement repair. Waterproofing, water in basement','Grip-Tite foundation reapir (Iowa) offers the complete scope of foundation repair and settlement products that will fix or repair any foundation settlement issue. Grip-Tite manufactures products that go along with the following: Foundation Repair','testimonials','',1,0,0,0,1,'','',1,8,3,1,1,4,1423545699,4,1424346086,0,0,0,1423545660,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/',0,0,1,NULL),
	(5,'document','text/html','Service Area',' Bowing basement walls, bowed walls, cracked foundation settling foundation, settlement. leaning chimney, central Iowa, Des Moines Iowa basement repair. Waterproofing, water in basement','Grip-Tite Foundation Repair covers all aspects of Basement repair. Bowing basement walls, cracked foundations, settlement, sinking slabs in Central Iowa.','service-area','',0,0,0,0,0,'','',1,4,4,1,1,4,1423545723,4,1424346743,0,0,0,0,0,'',0,0,0,0,1,'modDocument','web',1,'service-area',0,0,1,NULL),
	(6,'document','text/html','Contact us',' Bowing basement walls, bowed walls, cracked foundation settling foundation, settlement. leaning chimney, central Iowa, Des Moines Iowa basement repair. Waterproofing, water in basement','Grip-Tite Foundation Repair covers all aspects of Basement repair. Bowing basement walls, cracked foundations, settlement, sinking slabs in Central Iowa.','contact','',1,0,0,0,0,'','<h1>Contact Us</h1>\r\n<p>P: Â (515) 462-1313<br />T: Â (800) 474-7848<br />F: Â (515) 462-3465</p>\r\n<p>Email: GTFR@griptite.com</p>\r\n<p>115 W Jefferson St<br />Winterset, IA 50273-0111</p>\r\n<p><span style=\"line-height: 1.5em;\">Contacting us today is the first step to solving your foundation problems!</span></p>\r\n<p>Â </p>',1,5,6,1,1,4,1423545738,4,1424346101,0,0,0,1423545720,4,'',0,0,0,0,0,'modDocument','web',1,'contact',0,0,1,NULL),
	(7,'document','text/html','Bowing Walls',' Bowed walls, Cracked basement walls, Leaning basement walls, bulging block wall, leaning wall, bowed basement wall, cracks in basement, blocked basement wall, basement wall failure, des moines, iowa, clive, waukee, altoona, indianola, winterset','The Grip-TiteÂ® Wall Anchor System central Iowa utilizes proven engineering methods to secure and stabilize deteriorating basement walls.','bowing-walls','',1,0,0,2,0,'','<h1>Bowing Walls</h1>\r\n<p><img style=\"float: right; margin: 15px;\" src=\"client-files/images/wall%20ancors%201.jpg\" alt=\"\" width=\"350\" height=\"263\" />A home is usually the biggest investment a person or family makes - not to mention the value of the possessions contained within it. A cave-in can cause irreparable damage to the house and contents making it important to secure, stabilize and correct deteriorating basement walls -- before it\'s too late.<br />Bowed, cracked or leaning basement walls are a sign that something is going very wrong with the structural integrity of your property. This issue happens when hydrostatic pressure from too much water building up against the walls, or the soil that is around your property swells and exerts pressure that forces the foundation wall inward.</p>\r\n<p>In the past homeowners had few choices for repairing foundation walls, except to excavate around the wall or build a new basement wall. This solution is expensive, time consuming, and destructive to landscaping. Not only that , the problem can recur if the wall is not properly rebuilt or if steps are not taken to correct the causes of the initial deterioration.</p>\r\n<p><strong>Grip-TiteÂ® Wall Anchor System Is the Solution!</strong><br /><a href=\"http://www.griptite.com/applications/bowed-walls.aspx\" target=\"blank\">The Grip-TiteÂ® Wall Anchor System</a>Â utilizes proven engineering methods to secure and stabilize deteriorating basement walls. What\'s more, the system works on any kind of basement wall - concrete block, clay block, poured concrete, timber, or stone.</p>\r\n<p>The system consists of an interior wall plate, an exterior soil anchor, commonly referred to as a \"deadman anchor\" and a connecting steel rod to stabilize foundation walls by counteracting pressure exerted against the wall.</p>\r\n<p><img src=\"client-files/images/WallAnchorFinal.jpg\" alt=\"\" width=\"864\" height=\"679\" /></p>\r\n<p>The system is a property owner\'s alternative to completely removing and rebuilding basement walls that have become cracked, leaning or bowed as a result of pressures exceeding the allowable design capacity of the wall.</p>\r\n<p><a href=\"http://www.griptite.com/applications/bowed-walls.aspx\" target=\"blank\">The Grip-TiteÂ® Wall Anchors</a>Â are spaced in different locations along a wall and rod extenders can be used to avoid decks, flowerbeds and other landscaping.<br />Â <br />Installing The Grip-TiteÂ® Wall Anchor System usually takes less than one day, and the technicians will leave your home and lawn as it was before the installation. Anchors are placed in specific locations along the wall, and rod extenders can be added to avoid all decking, cement work, flower beds, and other landscaping. And because the system works with the wall and ground, structural changes to your house are unnecessary.</p>\r\n<p>Whether the cracking or bowing is caused by hydrostatic pressure, expansive soil or another problem, don\'t risk the investment in your home by failing to address the issue.</p>\r\n<p><strong>Benefits:</strong><br />Â </p>\r\n<ul>\r\n<li>Minimal excavation or disturbance to property, lawn or landscaping.</li>\r\n<li>Installed in a day or less.</li>\r\n<li>No wait â€“ can be installed year-round.</li>\r\n<li>Easily installed in areas with decks, landscaping or flowerbeds.</li>\r\n<li>Cost-effective â€“ less cost than re-building foundation wall(s).</li>\r\n</ul>',1,7,0,1,1,4,1423545826,6,1425413386,0,0,0,1423545780,4,'',0,0,0,0,0,'modDocument','web',1,'foundation-repair/bowing-walls',0,0,1,NULL),
	(8,'document','text/html','Foundation Settlement',' Foundation repair, foundation settling, sinking basement, sticking doors, sticking windows, drywall cracking, settling house, settling slab, settling basement, sinking slab, stair step cracks, brick cracks, settlement, des moines, Iowa, central iowa','Grip-Tite central Iowa offers the complete scope of foundation repair and settlement products that will fix or repair any foundation settlement issue. Grip-Tite manufactures products that go along with the following: Foundation Repair, Foundation Failure.','foundation-settlement','',1,0,0,2,0,'','<h1>Foundation Settlement</h1>\r\n<p>Why let the problems continue to cause your home and property further damage; damage that will not only cost you more money in repairs but dramatically lowers the value of your property.</p>\r\n<p>OurÂ <a href=\"http://www.griptite.com/\" target=\"blank\">Grip-TiteÂ®</a>Â products offer the complete scope of foundation repair and settlement products that will fix or repair any foundation settlement issue.</p>\r\n<p><strong><img style=\"float: right; margin: 15px;\" src=\"client-files/images/Foundation%20Settlement%20Signs.jpg\" alt=\"\" width=\"250\" height=\"193\" />Signs of Foundation Failure</strong></p>\r\n<ul>\r\n<li>Windows and doors are sticking and hard to open</li>\r\n<li>Multiple nail pops are appearing in ceilings and walls</li>\r\n<li>Sticking Doors Settlement</li>\r\n<li>There are large gaps in window and door frames</li>\r\n<li>Window and/or door trim are developing spaces</li>\r\n<li>Floors are starting to settle and become uneven</li>\r\n</ul>\r\n<ul>\r\n<li>Bowed or leaning basement walls</li>\r\n<li>Cracks in your foundation walls</li>\r\n<li>Interior plaster walls are cracking</li>\r\n<li>Chimneys are tilting or leaning</li>\r\n<li>Foundations are sinking</li>\r\n<li>Cracks in your basement walls</li>\r\n</ul>\r\n<p><strong>Grip-Tite Push Piers are the answer!</strong></p>\r\n<p><img src=\"client-files/images/PIERART-FNL.jpg\" alt=\"Grip-Tite Foundation Repair push pier system\" width=\"864\" height=\"864\" /></p>\r\n<p>The Grip-Tite Foundation Push Pier System is used to fix foundation settlement. The Push Pier System uses steel tubes that utilize the weight of the structure to hydraulically drive the piers to bedrock or other competent load bearing strata. The piers are spaced a maximum of 6 feet apart and are used to level or stabilized an existing foundation. Once the tubes are driven to bedrock or the competent load-bearing strata, the load of the structure is fully transferred to the pier through an underpinning bracket that is positioned under the footing. The structure is now stabilized to prevent any further settlement.</p>\r\n<p><strong>Advantages</strong></p>\r\n<ul>\r\n<li><span style=\"line-height: 1.5em;\">End-bearing pier that does not rely on friction for capacity</span></li>\r\n<li><span style=\"line-height: 1.5em;\">Each pier load tested during installation</span></li>\r\n<li><span style=\"line-height: 1.5em;\">Ability to reach greater depths using a starter tube with friction collar</span></li>\r\n<li><span style=\"line-height: 1.5em;\">High-strength steel pier sections and sleeved couplings have high resistance to bending</span></li>\r\n<li><span style=\"line-height: 1.5em;\">Long life span, resistance to corrosion (design life in moderate soils more than 100 years)</span></li>\r\n<li><span style=\"line-height: 1.5em;\">Available with hot-dipped galvanized coating for added corrosion resistance</span></li>\r\n</ul>',1,7,1,1,1,4,1423545844,6,1425413124,0,0,0,1423545840,4,'',0,0,0,0,0,'modDocument','web',1,'foundation-repair/foundation-settlement',0,0,1,NULL),
	(9,'document','text/html','Sinking Slabs',' applications sinking slab concrete slab sinking cracked up sinking slab','Grip-Tite Foundation Repair of central Iowa offers slabjacking to solve sinking, cracked, and uneven concrete floors.','sinking-slabs','',1,0,0,2,0,'','<h4><span style=\"font-size: 2em; line-height: 1.5em; font-weight: normal;\">Sinking Slabs</span></h4>\r\n<p>Â </p>\r\n<p><strong><img style=\"margin: 10px; float: right;\" src=\"client-files/images/before_after_2.jpg\" alt=\"\" width=\"250\" height=\"133\" />Slab jacking</strong>, also called mud jacking, can usually be done for less than half the cost of pouring new concrete. But the savings may be even greater once you factor in eliminating the costs of demolition, removal and landscaping. Most projects can be completed quickly and economically with minimal disruption. OurÂ foundation repairÂ professionals can raise a patio in the morning and the owner can have a party on it later that night!</p>\r\n<p>Mud jacking is a cost-effective alternative to replacingÂ sinking concrete. At Grip-Tite Foundation Repair, we use this repair method atÂ Central IowaÂ homes to fix: sidewalks, streets, ramps, patios, driveways, floors, parking lots, and garage floors--almost anything that\'s concrete. It is highly effective in commercial and residential situations.</p>\r\n<p><img style=\"float: right; margin: 10px;\" src=\"client-files/images/before_after_1.jpg\" alt=\"\" width=\"250\" height=\"133\" /></p>\r\n<p>Mud jacking is quick and easy. Since the old concrete is not removed, very little mess or inconvenience is involved. Surrounding sod is not damaged or removed, so there is no need to re-seed and wait for grass to grow, and the newly-raised slabs can be put back into service as soon as the job is finished. You don\'t have to wait for the grout to cure!</p>\r\n<p>Also available are the Grip-Tite Slab Piers, a permanent fix for basement floors or slab on grade homes. Visit our parent-company website for more information on how slab piers might be the solution for you. <a href=\"http://www.griptite.com/products/slab-piers-system.aspx\" target=\"_blank\">More Information</a></p>\r\n<p>Â </p>',1,7,2,1,1,4,1423545863,4,1425333310,0,0,0,1423545840,4,'',0,0,0,0,0,'modDocument','web',1,'foundation-repair/sinking-slabs',0,0,1,NULL),
	(10,'document','text/html','Sagging Floors','Sagging floors, bowing floor joists, uneven floors, nail pops, developing uneven floor spaces','By installing a series of Grip-Tite Of Central Iowa stabilizers that are adjustable, the structure can be lifted and supported evenly, reducing any damage to the structure.','sagging-floors','',1,0,0,2,0,'','<h1>Sagging Floors</h1>\r\n<p><strong style=\"font-size: 1.5em; line-height: 1.5em;\">Look for the Signs!</strong></p>\r\n<ul>\r\n<li>Doors that stick or are hard to open<img style=\"float: right; margin: 10px;\" src=\"client-files/images/sagging-floors-1.jpg\" alt=\"sagging floors\" width=\"350\" height=\"299\" /></li>\r\n<li>Floors sloping or not level.</li>\r\n<li>Interior plaster walls cracking</li>\r\n<li>Nail pops in ceilings and walls</li>\r\n<li>Window and door trim developing spaces</li>\r\n<li>Uneven floors</li>\r\n<li>Sinking or bowing floor joists</li>\r\n</ul>\r\n<h2>The Grip-Tite<sup>Â®</sup>Â Stabilizer Solution For Crawl Spaces</h2>\r\n<p>This solution can solve a common problem of sagging floors. Wood beams that carry the load of your home can sag causing drywall cracks, sloping floors, doors or windows that won\'t open properly and other structural issues.</p>\r\n<p>This is typically an issue when the current support system is spaced too far apart to be effective. By installing a series of stabilizers that are adjustable, the structure can be lifted and supported evenly, reducing any damage to the structure.</p>\r\n<p>Your Grip-Tite certified dealer can help you determine the best approach to supporting your structure.</p>',1,7,3,1,1,4,1423545874,6,1425398428,0,0,0,1423545840,4,'',0,0,0,0,0,'modDocument','web',1,'foundation-repair/sagging-floors',0,0,1,NULL),
	(11,'document','text/html','Helical Piers/Piles','caissons, geo piers, spread footings, deep foundations, settlement prevention, over excavation alternative, Des Moines Foundation contractor','Grip-Tite Foundation Repair in central Iowa can provide helical piles for commercial foundations. Helical Piles can be used instead of over excavations, caissons, spread footings.','helical-piers/piles','',1,0,0,3,0,'','<h1>Helical Piers/Piles</h1>\r\n<p>Â </p>\r\n<p><img style=\"float: left; margin: 10px;\" src=\"client-files/images/Helix%20Pier%20-%20Top%20caps%20with%202%20x%204s.bmp\" alt=\"Helical pier top caps\" width=\"432\" height=\"288\" />Grip-Tite Foundation Repair of Central Iowa installs Helical Piles.</p>\r\n<p>Helical piers (aka helical piles) are either square or round shafts with one or more helix bearing plates welded to the shaft. Helical piers are hydraulically â€œscrewedâ€ into load bearing soils. Grip-Titeâ€™s helical piers have a true helix, meaning, the second and/or third helical plate follows the same path as the first when â€œscrewedâ€ into the soil. This minimizes the disruption of soil.</p>\r\n<p>Helical piers can are used to repair an existing structure and also for new construction in place of micropiles, caissons, geopiers or other deep foundation systems.Â  They can also be used instead of an over- excavation. The helical pier can be both time and cost effective.</p>\r\n<p><img src=\"client-files/images/HelixIllustrationFinal2.jpg\" alt=\"Helical pier installation\" width=\"800\" height=\"600\" /></p>\r\n<p>When helical piers are used to repair an existing structure, they are â€œscrewedâ€ into the soil until a pre-determined torque is reached. A bracket is then placed on the steel shaft and positioned under the footing of the structure, transferring the load of the structure to the helical piers. The structure can then be stabilized or lifted back level.</p>\r\n<p>Helical Piers have been in use for almost 200 years, and more recently have become more popular with engineers and contractors and used often in place of more costly alternatives.Â </p>',1,7,0,1,1,4,1423549899,4,1424346489,0,0,0,1423549860,4,'',0,0,0,0,0,'modDocument','web',1,'commercial-foundations/helical-piers/piles',0,0,1,NULL),
	(12,'document','text/html','Helical Tiebacks','Helical Tiebacks iowa, soil nails Iowa, Helical Piles Iowa','Helical Tiebacks are used in Iowa commercial constructions, tiebacks are used to fix a leaning wall, avoid a new wall from moving, hold soil back.','helical-tiebacks','',1,0,0,3,0,'','<h1>Helical Tiebacks</h1>\r\n<p>Grip-Tite Foundation Repair of Central Iowa offers a 1.5 inch Square Shaft Helical and 1.75 Square Shaft Helical suitable for anchor and tie-back applications. TheÂ <a href=\"http://www.griptite.com/products/helical-anchors.aspx\" target=\"blank\">Grip-TiteÂ® Square Shaft Helical Anchor</a>Â lead sections and extensions are available with helix blade configurations to meet project-specific requirements.</p>\r\n<h2>Â Advantages</h2>\r\n<ul>\r\n<li>Predictable capacity</li>\r\n<li>No excavation required on the high-grade side of foundation or retaining walls.</li>\r\n<li>Installs with either handheld or small equipment in areas of limited or tight access</li>\r\n<li>Load test can be conducted immediately after installation</li>\r\n<li>Generates no spoils</li>\r\n<li>Clean installation â€“ no messy grout</li>\r\n<li>All-weather installation</li>\r\n</ul>\r\n<h2>Features</h2>\r\n<ul>\r\n<li>Lead sections available in 2, 5 and 7-foot lengths</li>\r\n<li>Extensions available in 3, 5 and 7-foot lengths</li>\r\n<li>Socket and bolt connections</li>\r\n<li>Helical leads and extensions available with helix blade configurations to meet project-specific requirements.</li>\r\n<li>Helix blade diameters of 8, 10, 12 and 14 inches.</li>\r\n<li>Available with hot-dipped galvanized coating for added corrosion resistance.</li>\r\n</ul>',1,7,1,1,1,4,1423549925,4,1424346463,0,0,0,1423549920,4,'',0,0,0,0,0,'modDocument','web',1,'commercial-foundations/helical-tiebacks',0,0,1,NULL),
	(13,'document','text/html','Push Pier/Pile','','','push-pier/pile','',1,0,0,3,0,'','<h1>Push Pier/Pile</h1>\r\n<p>Grip-Tite Foundation Repair offers theÂ <a href=\"http://www.griptite.com/products/push-pier-system.aspx\" target=\"blank\">The Grip-Tite Foundation Push Pier System</a>Â is used to fix foundation settlement. The Push Pier System uses steel tubes that utilize the weight of the structure to hydraulically drive the piers to bedrock or other competent load bearing strata. The piers are spaced a maximum of 6 feet apart and are used to level or stabilized an existing foundation. Once the tubes are driven to bedrock or the competent load-bearing strata, the load of the structure is fully transferred to the pier through an underpinning bracket that is positioned under the footing. The structure is now stabilized to prevent any further settlement.</p>\r\n<h2><strong>Â <span class=\"sub-header\">Signs of Foundation Failure</span></strong></h2>\r\n<ul>\r\n<li>Windows and doors are sticking and hard to open</li>\r\n<li>Multiple nail pops are\r\n<p><img style=\"float: right; margin: 10px;\" src=\"http://www.griptite.com/webres/Image/founation-settlement-2.jpg\" alt=\"Foundation Crack\" width=\"200\" height=\"150\" />appearing in ceilings and walls</p>\r\n</li>\r\n<li>\r\n<p>Sticking Doors Settlement There are large gaps in window and door frames</p>\r\n</li>\r\n<li>Window and/or door trim are developing spaces</li>\r\n<li>Floors are starting to settle and become uneven</li>\r\n<li><a href=\"http://www.griptite.com/applications/bowed-walls.aspx\" target=\"blank\">Bowed or leaning basement walls</a></li>\r\n<li>Interior plaster walls are cracking</li>\r\n<li>Chimneys are tilting or leaning</li>\r\n<li>Foundations are sinking</li>\r\n<li><a href=\"http://www.griptite.com/foundation-repair.aspx\" target=\"blank\">Cracks in your basement walls</a></li>\r\n</ul>',1,7,2,1,1,4,1423549961,4,1424346504,0,0,0,1423549920,4,'',0,0,0,0,0,'modDocument','web',1,'commercial-foundations/push-pier/pile',0,0,1,NULL),
	(14,'document','text/html','About Us',' Bowing basement walls, bowed walls, cracked foundation settling foundation, settlement. leaning chimney, central Iowa, Des Moines Iowa basement repair. Waterproofing, water in basement','Grip-Tite Foundation Repair covers all aspects of Basement repair. Bowing basement walls, cracked foundations, settlement, sinking slabs in Central Iowa.','about-us','',1,0,0,0,0,'','<p><br /><img style=\"float: left; margin: 15px 10px;\" src=\"client-files/images/gtfr%20fleet%20pic.jpg\" alt=\"\" width=\"400\" height=\"300\" />Grip-Tite Foundation Repair, a division of Grip-Tite Manufacturing Co., LLC conveniently located in Winterset, Iowa, provides foundation wall restoration, foundation stabilization and re-leveling, new construction solutions, and interior and exterior water control systems using genuine, guaranteed Grip-Tite products installed by trained and certified Grip-Tite personnel. Grip-Tite Foundation Repair brings the expertise and integrity of over 90 years of anchoring know how to residential and commercial customers in Central Iowa by offering a wide variety of foundation repair solutions utilizing the same Grip-Tite products that have been synonymous with a safe, secure basement in Iowa and the nation for more than 35 years.</p>\r\n<p><strong style=\"line-height: 1.5em;\">Grip-TiteÂ® Manufacturing Co., L.L.C.Â </strong><span style=\"line-height: 1.5em;\">of Winterset, Iowa has been manufacturing earth anchoring products for over 88 years! In the 1920\'s and 30\'s, Grip-TiteÂ® anchors were used to secure highway guard cables, then used extensively for guying oil well derricks and oil pipelines.</span></p>\r\n<p>Following WWII, Grip-TiteÂ® developed anchors for the rapidly growing rural electrification network of overhead lines. Since that time, Grip-TiteÂ® has been supplying earth anchoring products for overhead electric and telephone lines â€“ serving electric and telephone utilities coast to coast and outside the United States. Today, Grip-TiteÂ® continues as one of the leading earth anchor manufacturers for the electric and telephone industries.</p>\r\n<p>Grip-TiteÂ® recognized the tremendous need for a product to simply and effectively secure and stabilize cracked and bowing basement walls without replacement of the walls! After an extensive development effort and using a modified anchor Grip-TiteÂ® originally produced for the electric systems of the Island of Madagascar in the Indian Ocean, the Grip-TiteÂ® Wall Anchor System became a reality. Hundreds of thousands of the Grip-TiteÂ® Wall Anchor Systems have been installed and continue to give confidence and peace of mind to the homeowners involved.</p>\r\n<p>What is the best solution for your foundation settlement, bowed walls, sagging floors, sinking slabs, or other foundation repair needs?</p>\r\n<p><strong><img style=\"float: right; margin: 10px;\" src=\"client-files/images/DSC_1296.jpg\" alt=\"\" width=\"300\" height=\"447\" />Grip-TiteÂ® Foundation Systems!</strong><br />We can say this because we have been delivering on a promise of celebrated quality and service in the foundation repair and earth anchoring industry for more than 88 years. Itâ€™s the little things that matter that you will benefit from our unmatched experience and the exceeding attention to detail that our dealers put into every foundation repair job.</p>\r\n<p>Why are more and more homeowners choosing us for their foundation repair needs?Â  Because it is not easy finding the right foundation contractor, but you will find that Grip-TiteÂ® Authorized Dealers adhere to some very basic rules of honesty, integrity, responsibility and respect.</p>\r\n<p>If you are looking for the highest quality foundation repair products with the most cost-effective solutions then you have found it.</p>',1,4,5,1,1,4,1423550320,6,1425332163,0,0,0,1423550280,4,'',0,0,0,0,0,'modDocument','web',1,'about-us',0,0,1,NULL),
	(15,'document','text/html','D. Bowman','','','d-bowman','',1,0,0,4,0,'','<p>Wonderful, from salesman to installers all were great!! Thanks so much you folks are first class through and through!!</p>',1,1,0,1,1,4,1423587014,4,1423587503,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/d-bowman',0,0,1,NULL),
	(16,'document','text/html','R. Montgomery','','','r-montgomery','',1,0,0,4,0,'','<p>Excellent! The installation was outstanding!</p>',1,1,1,1,1,4,1423587305,4,1423587509,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/r-montgomery',0,0,1,NULL),
	(17,'document','text/html','B. Phillips','','','b-phillips','',1,0,0,4,0,'','<p>Doug was a superb worker. Knows his work and does a fine job. A very courteous young man. He could work here anytime we\'d need work done.</p>',1,1,2,1,1,4,1423587526,4,1423587556,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/b-phillips',0,0,1,NULL),
	(18,'document','text/html','A. Wolfe','','','a-wolfe','',1,0,0,4,0,'','<p>The installation crew worked so hard and fast, I was impressed!</p>',1,1,3,1,1,4,1423587560,4,1424341596,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/a-wolfe',0,0,1,NULL),
	(19,'document','text/html','S. Pappus','','','s-pappus','',1,0,0,4,0,'','<p>We are pleased, good job well done. No more water will be a blessing!</p>',1,1,4,1,1,4,1423587699,4,1423587728,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/s-pappus',0,0,1,NULL),
	(20,'document','text/html','R. Langel','','','r-langel','',1,0,0,4,0,'','<p>It was great working with you. I will refer others to you. Thanks.</p>',1,1,5,1,1,4,1423587739,4,1423587771,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/r-langel',0,0,1,NULL),
	(21,'document','text/html','J. Kooker','','','j-kooker','',1,0,0,4,0,'','<p><span style=\"line-height: 1.5em;\">Their work was fantastic. They stayed right on the job and did an excellent job. Will be glad to be a spokesperson for your business.</span></p>',1,1,6,1,1,4,1423587798,4,1423587885,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/j-kooker',0,0,1,NULL),
	(22,'document','text/html','V. Smith','','','v-smith','',1,0,0,4,0,'','<p>Not knowing what to expect, Scott and the guys were great! They raked and cleaned the dirt afterwards. Thank you!!</p>',1,1,7,1,1,4,1424342873,4,1424343068,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/v-smith',0,0,1,NULL),
	(23,'document','text/html','B. Paulding','','','b-paulding','',1,0,0,4,0,'','<p>We are very thankful to have our foundation secured. All 5â€™s!</p>',1,1,8,1,1,4,1424342929,4,1424342951,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/b-paulding',0,0,1,NULL),
	(24,'document','text/html','S. & S. Mayer','','','s-and-s-mayer','',1,0,0,4,0,'','<p><span style=\"line-height: 1.5em;\">We are SO pleased to have our foundation and walls fixed that the job was done in a week\'s time! We will certainly recommend Grip-Tite to others. Best wishes.</span></p>',1,1,9,1,1,4,1424342967,4,1424343057,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/s-and-s-mayer',0,0,1,NULL),
	(25,'document','text/html','S. & R. Ryals','','','s-and-r-ryals','',1,0,0,4,0,'','<p>We will definitely refer you to others! Thank you!</p>',1,1,10,1,1,4,1424343017,4,1424343044,0,0,0,1423587000,4,'',0,0,0,0,0,'modDocument','web',1,'testimonials/s-and-r-ryals',0,0,1,NULL),
	(27,'document','text/html','Waterproofing','Sagging floors, bowing floor joists, uneven floors, nail pops, developing uneven floor spaces, wet basement, wet floor','By installing Grip-Tite\'s interior drain tile system, you can rest easy knowing you won\'t have to be anxious about a wet basement.','waterproofing','',1,0,0,2,0,'','<h1>Basement Waterproofing</h1>\r\n<p>When it comes toÂ <span>basement waterproofing</span>Â for yourÂ <span>central Iowa</span>Â home,Â <span>Grip-Tite Foundation Repair</span>Â has you covered. We cannot stress enough how important it is to keep your basement dry, safe, and healthy. With as much rain that we get in this area of the country, it is important to make sure that the basement of your home doesn\'t let any moisture inside where it doesn\'t belong.</p>\r\n<h2>Warning Signs of Wet Basement</h2>\r\n<p>If you notice any of the following warning signs of water in your basement, be sure to get in touch with us immediately for a free consultation.</p>\r\n<ul>\r\n<li>Noticeable water stains on walls or floors</li>\r\n<li>Water seeping in through a clogged floor drain</li>\r\n<li>Moisture lying in puddles on the floor</li>\r\n<li>Water coming in over the top of the foundation and dripping down the walls</li>\r\n<li>Seepage getting in through the cove joint (where the wall and floor meet)</li>\r\n<li><span style=\"line-height: 1.5em;\">Water damage on boxes and other belongings you have stored in the basement</span></li>\r\n</ul>\r\n<p><img style=\"margin: 15px; vertical-align: bottom;\" src=\"client-files/images/wet-basement.jpg\" alt=\"\" width=\"450\" height=\"232\" /></p>',1,7,4,1,1,6,1425066300,6,1425413872,0,0,0,1425066960,6,'',0,0,0,0,0,'modDocument','web',1,'foundation-repair/waterproofing',0,0,1,NULL),
	(28,'document','text/html','Test page','','','test-page','',0,0,0,0,0,'','',1,4,7,1,1,5,1425066503,0,0,0,0,0,0,0,'',0,0,0,0,0,'modDocument','web',1,'test-page',0,0,1,NULL);

/*!40000 ALTER TABLE `modx_site_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_htmlsnippets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_htmlsnippets`;

CREATE TABLE `modx_site_htmlsnippets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `snippet` mediumtext,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `properties` text,
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_htmlsnippets` WRITE;
/*!40000 ALTER TABLE `modx_site_htmlsnippets` DISABLE KEYS */;

INSERT INTO `modx_site_htmlsnippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `static`, `static_file`)
VALUES
	(1,1,0,'doc_head','',0,3,0,'<!DOCTYPE html>\n<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->\n<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->\n<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->\n    <head>\n        <meta charset=\"utf-8\">\n        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n        <title>[[*pagetitle]] | [[++site_name]]</title>\n        <meta name=\"description\" content=\"[[*description]]\">\n        <meta name=\"keywords\" content=\"[[*longtitle]]\">\n        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0\">\n\n        <!-- THIS IS NEEDED FOR FURL -->\n        <base href=\"[[++site_url]]\" />\n        \n        <meta name=\"description\" content=\"\">\n        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0\">\n\n        <!-- Facebook OpenGraph Data -->\n        <meta property=\"og:title\" content=\"[[++site_name]]\" />\n        <meta property=\"og:type\" content=\"website\" />\n        <!-- If it\'s an article:\n        <meta property=\"og:type\" content=\"article\" />\n        <meta property=\"article:published_time\" content = \"\" />\n        -->\n        <meta property=\"og:description\" content=\"[[*seo_description]]\" />        \n        <meta property=\"og:url\" content=\"[[++site_url]]\"/>\n        <meta property=\"og:image\" content=\"[[++social_image]]\" />\n        <meta property=\"og:site_name\" content=\"[[++site_name]]\" />\n\n        <!-- Canonical URL for CMS SEO -->\n        <link rel=\"canonical\" href=\"[[++site_url]]\" />\n        \n        <link rel=\"stylesheet\" href=\"/resources/css/production.css\">\n        <link rel=\"stylesheet\" href=\"/resources/css/base.css\">    \n        <!--[if lte IE 8]>\n        <link rel=\"stylesheet\" href=\"/resources/css/fallback.css\">\n        <link rel=\"stylesheet\" href=\"/resources/css/ie8.css\">\n        <![endif]-->\n        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>\n        <script src=\"/resources/js/vendor/modernizr.js\"></script>\n        <!-- FAVICON -->\n        <link rel=\"icon\" href=\"/resources/img/favicon.png\" type=\"image/png\">\n        <link href=\'http://fonts.googleapis.com/css?family=Montserrat:400,700|Raleway:800,400\' rel=\'stylesheet\' type=\'text/css\'>\n        <link rel=\"stylesheet\" href=\"https://s3.amazonaws.com/icomoon.io/29117/GripTite/style.css\">\n\n        <!-- GOOGLE ANALYTICS -->\n        <script>\n          (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n          })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n          ga(\'create\', \'[[++google_analytics]]\', \'auto\');\n          ga(\'send\', \'pageview\');\n\n        </script>        \n    </head>',0,'a:0:{}',1,'assets/chunks/layout/doc_head.html'),
	(2,1,0,'doc_footer','',0,3,0,'<script src=\"resources/js/production.min.js\">\n        </script>\n\n    </body>\n</html>',0,'a:0:{}',1,'assets/chunks/layout/doc_footer.html'),
	(3,1,0,'header','',0,3,0,'[[$quote-modal]]\n                        [[$tophat]]\n                        [[$navigation]]',0,'a:0:{}',1,'assets/chunks/layout/header.html'),
	(4,1,0,'footer','',0,3,0,'<footer class=\"mainfooter\">\n            <div class=\"row-fluid wrap\">\n                <div class=\"span3\">\n                    <img src=\"resources/img/footerLogo.png\" alt=\"\">\n                    <ul class=\"social\">\n                        [[$social-links]]\n                    </ul>\n                </div>\n                <div class=\"span3 offset1\">\n                    <h4>Foundation Repair</h4>\n                    [[Wayfinder? \n                        &level=`1`\n                        &startId=`2`\n                        &innerTpl=`nav-innner`\n                        &outerTpl=`nav-outer`\n                        &sortBy=`menuindex`\n\n                        ]]\n                </div>\n                <div class=\"span3\">\n                    <h4>Commercial Foundations</h4>\n                    [[Wayfinder? \n        &level=`1`\n        &startId=`3`\n        &innerTpl=`nav-innner`\n        &outerTpl=`nav-outer`\n        &sortBy=`menuindex`\n\n        ]]\n                </div>\n                <div class=\"span2\">\n                    <h4><a href=\"/testimonials\">Testimonials</a></h4>\n                    <h4><a href=\"/about-us\">About Us</a></h4>\n                    <!-- <h4><a href=\"/service-area\">Service Area</a></h4> -->\n                    <h4><a href=\"/contact\">Contact</a></h4>\n                    <h4><a href=\"#quote\" data-target=\"modal\" class=\"quote\">Free Quote</a></h4>\n                </div>\n            </div>\n            <div class=\"row-fluid wrap\">\n            <hr>\n                \n                <p>Grip-tite Foundation Repair | 115 W. Jefferson St. Winterset, Iowa 50273 | 1-800-474-7848<br>\n                    Â©2015 Grip-Tite Foundation Repair - All Rights Reserved</p>\n            </div>\n        </footer>',0,'a:0:{}',1,'assets/chunks/layout/footer.html'),
	(21,1,0,'social-links','',0,3,0,'<li><a target=\"_blank\" href=\"[[++facebook-link]]\" class=\"ico-facebook\"></a></li>\n<li><a target=\"_blank\" href=\"[[++twitter-link]]\" class=\"ico-twitter\"></a></li>',0,'a:0:{}',1,'assets/chunks/layout/social-links.html'),
	(5,1,0,'navigation','',0,3,0,'<nav class=\"mainnav\">\n    <div class=\"row-fluid wrap\">\n        <div class=\"logo\"><a href=\"/\"><img src=\"resources/img/logo.jpg\" alt=\"\"></a></div>\n        \n            [[Wayfinder? \n                &level=`1`\n                &startId=`0`\n                &innerTpl=`nav-innner`\n                &outerTpl=`nav-outer`\n                &sortBy=`menuindex`\n                &outerClass=`visible-desktop`\n                &rowClass=`visible-desktop` \n\n\n                ]]\n    </div>\n    <div class=\"hidden-desktop\">\n            <a href=\"#menu\" class=\"hidden-desktop\" id=\"toggle\"><span></span></a>\n        \n        <div id=\"menu\">\n            \n          [[Wayfinder? \n                &level=`1`\n                &startId=`0`\n                &innerTpl=`nav-innner`\n                &outerTpl=`nav-outer`\n                &sortBy=`menuindex`\n\n                ]]\n        </div>\n    </div>\n\n    </div>\n</nav>',0,'a:0:{}',1,'assets/chunks/layout/navigation.html'),
	(6,1,0,'nav-inner','',0,3,0,'<li><a href=\"[[~[[+id]]]]\" title=\"[[+menutitle]]\">[[+menutitle]]</a></li>',0,'a:0:{}',1,'assets/chunks/layout/nav-inner.html'),
	(7,1,0,'nav-outer','',0,3,0,'<ul>\n[[+wf.wrapper]]\n</ul>',0,'a:0:{}',1,'assets/chunks/layout/ nav-outer.html'),
	(8,1,0,'quote-bar','',0,3,0,'<section class=\"quoteBar\">\n                            <div class=\"row-fluid wrap\">\n                                <div class=\"span8\">\n                                    <h1>Fast and Reliable Service</h1>\n                                    <p>Contacting us today is the first step to solving your foundation problems!</p>\n                                </div>\n                                <div class=\"span4\"><a href=\"#quote\" data-toggle=\"modal\" class=\"button\">Get a quote</a></div>\n                            </div>\n                        </section>',0,'a:0:{}',1,'assets/chunks/layout/quote-bar.html'),
	(9,1,0,'quote-modal','',0,3,0,'\n[[!FormIt?\n&hooks=`spam, email`\n&emailFrom=`[[++primary_email]]` \n&emailTpl=`snt_from_quote`\n&submitVar=`quote`\n&emailTo=`justin.lobaito@weloideas.com, [[++primary_email]]`\n&emailSubject=`Contact Form Submission`\n&validate=`quote-phone:required,  quote-first_name:required, quote-last_name:required, quote-city:required, quote-address:required, quote-email:required, quote-email:email, quote-comments:required:stripTags, nospam:blank`\n&successMessage=`<div class=\"alert alert-success\">\n<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n<h3>Thank you for getting in contact with us.</h3>\n<p>A [[++site_name]] representative will get in contact with you shortly</p>\n</div>`\n]]\n\n\n<div class=\"modal hide fade\" id=\"quote\">\n  <div class=\"header\">\n    <h2>Request a free quote</h2>\n    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n</div>\n\n<div class=\"modal-body\">\n    [[!+fi.validation_error_message:!empty=`\n\n\n    <div class=\"alert alert-error\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n      <div>\n          <strong><h3>Please review the following errors:</h3></strong>\n          [[!+fi.error.quote-first_name:!empty=`<p>First Name is required</p>`]]\n          [[!+fi.error.quote-last_name:!empty=`<p>Last Name is required</p>`]]\n          [[!+fi.error.quote-email:!empty=`<p>Email Address is required</p>`]]\n          [[!+fi.error.quote-phone:!empty=`<p>Phone Number is required</p>`]]\n          [[!+fi.error.quote-address:!empty=`<p>Address is required</p>`]]\n          [[!+fi.error.quote-city:!empty=`<p>City is required</p>`]]\n          [[!+fi.error.quote-comments:!empty=`<p>Additional Comments is required</p>`]]\n\n      </div>\n  </div>\n\n\n  `]]\n  <form action=\"\" class=\"main-form\">\n    <div class=\"row-fluid\">\n        <div class=\"span6\"><label for=\"first_name\">First Name</label>\n            <input type=\"text\" value=\"[[!+fi.quote-first_name]]\" id=\"first_name\" name=\"first_name\"  placeholder=\"Jonathan\">\n        </div>\n        <div class=\"span6\"><label for=\"last_name\">Last Name</label>\n            <input type=\"text\" value=\"[[!+fi.quote-last_name]]\" id=\"last_name\" name=\"last_name\"  placeholder=\"Doe\">\n\n        </div>\n    </div>\n    <div class=\"row-fluid\">\n        <div class=\"span6\">\n            <label for=\"email\">Email</label>\n            <input type=\"text\" value=\"[[!+fi.quote-email]]\" id=\"email\" name=\"email\"   placeholder=\"jon@doe.com\">\n        </div>\n        <div class=\"span6\">\n            <label for=\"phone\">Phone</label>\n            <input type=\"tel\" value=\"[[!+fi.quote-phone]]\" id=\"phone\" name=\"phone\"   placeholder=\"999-999-9999\">\n        </div>\n    </div>\n    <div class=\"row-fluid\">\n        <label for=\"address\">Street Address</label>\n        <input type=\"text\" value=\"[[!+fi.quote-address]]\" id=\"address\" name=\"address\"  >\n\n    </div>\n    <div class=\"row-fluid\">\n        <label for=\"city\">City<sup>*</sup></label>\n        <input value=\"[[!+fi.quote-city]]\" type=\"text\" id=\"city\" name=\"city\"  >\n\n    </div>\n    <input value=\"[[!+fi.nospam]]\" type=\"hidden\" id=\"nospam\" name=\"nospam\" class=\"nospam\">\n\n    <label for=\"comments\">Additional Comments</label>\n    <textarea value=\"[[!+fi.quote-comments]]\" id=\"comments\" name=\"comments\"></textarea>\n    <input type=\"submit\" class=\"cbp-mc-submit\" name=\"quote\" value=\"Request Quote\">\n</form>\n</div>\n</div>\n\n\n',0,'a:0:{}',1,'assets/chunks/layout/quote-modal.html'),
	(10,1,0,'tophat','',0,3,0,'<div class=\"tophat\">\n                            <div class=\"row-fluid wrap\">\n                                <ul class=\"social hidden-phone\">\n                                    [[$social-links]]\n                                </ul>\n                                <ul class=\"tophatNav\">\n                                    <li class=\"hidden-phone\"><a href=\"\" class=\"ico-clock\">Mon - Fri: 8:00 - 4:30</a></li>\n                                    <li><a href=\"\" class=\"ico-phone\">(515)462-1313</a></li>\n                                    <li class=\"hidden-phone\"><a href=\"mailto:gtfr@griptite.com\" class=\"ico-envelope\">gtfr@griptite.com</a></li>\n                                    <li class=\"quote\"><a href=\"#quote\" data-toggle=\"modal\" class=\"quote ico-\">Free Quote</a></li>\n                                </ul>\n                            </div>\n                        </div>',0,'a:0:{}',1,'assets/chunks/layout/tophat.html'),
	(11,1,0,'contact_form','',0,3,0,' [[!FormIt?\n       &hooks=`spam, email`\n       &emailFrom=`[[++primary_email]]` \n       &emailTpl=`snt_from`\n       &submitVar=`contactSubmit`\n       &emailTo=`justin.lobaito@weloideas.com, [[++primary_email]]`\n       &emailSubject=`Contact Form Submission - GTFR`\n       &validate=`phone:required, state:required, first_name:required, last_name:required, city:required, address:required, email:required, email:email, comments:required:stripTags, nospam:blank`\n      &successMessage=`<div class=\"alert alert-success\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n      <h3>Thank you for getting in contact with us.</h3>\n      <p>A [[++site_name]] representative will get in contact with you shortly</p>\n    </div>`\n    ]]\n\n   [[!+fi.validation_error_message:!empty=`\n\n\n<div class=\"alert alert-error\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n		<div>\n		      <strong><h3>Please review the following errors:</h3></strong>\n		            [[!+fi.error.first_name:!empty=`<p>First Name is required</p>`]]\n		            [[!+fi.error.last_name:!empty=`<p>Last Name is required</p>`]]\n		            [[!+fi.error.email:!empty=`<p>Email Address is required</p>`]]\n		            [[!+fi.error.phone:!empty=`<p>Phone Number is required</p>`]]\n		            [[!+fi.error.address:!empty=`<p>Address is required</p>`]]\n		            [[!+fi.error.city:!empty=`<p>City is required</p>`]]\n		            [[!+fi.error.state:!empty=`<p>State is required</p>`]]\n					[[!+fi.error.comments:!empty=`<p>Additional Comments is required</p>`]]\n\n		</div>\n</div>\n\n\n`]]\n    [[!+fi.successMessage]]\n\n\n                   <form class=\"main-form row-fluid\" action=\"[[~[[*id]]]]\" method=\"post\"  >\n                        \n                            <div class=\"row-fluid\">\n                                <div class=\"span6\">\n                                    <label for=\"first_name\">First Name<sup>*</sup></label>\n                                    <input type=\"text\" value=\"[[!+fi.first_name]]\" id=\"first_name\" name=\"first_name\"  placeholder=\"Jonathan\">\n                                </div>\n                                <div class=\"span6\">\n                                    <label for=\"last_name\">Last Name<sup>*</sup></label>\n                                    <input type=\"text\" value=\"[[!+fi.last_name]]\" id=\"last_name\" name=\"last_name\"  placeholder=\"Doe\">\n                                </div>\n                            </div>\n                            <label for=\"email\">Email Address<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.email]]\" id=\"email\" name=\"email\"   placeholder=\"jon@doe.com\">\n                            <label for=\"phone\">Phone<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.phone]]\" id=\"phone\" name=\"phone\"   placeholder=\"999-999-9999\">\n                            <label for=\"address\">Address<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.address]]\" id=\"address\" name=\"address\"  >\n                            <div class=\"row-fluid\">\n                                <div class=\"span4\">\n                                    <label for=\"city\">City<sup>*</sup></label>\n                                    <input value=\"[[!+fi.city]]\" type=\"text\" id=\"city\" name=\"city\"  >\n                                </div>\n                                \n                                <div class=\"span4\">\n                            <label for=\"state\">State<sup>*</sup></label>\n                                    <select value=\"[[!+fi.state]]\" id=\"state\" name=\"state\">\n                                        <option value=\"AL\">Alabama</option>\n                                        <option value=\"AK\">Alaska</option>\n                                        <option value=\"AZ\">Arizona</option>\n                                        <option value=\"AR\">Arkansas</option>\n                                        <option value=\"CA\">California</option>\n                                        <option value=\"CO\">Colorado</option>\n                                        <option value=\"CT\">Connecticut</option>\n                                        <option value=\"DE\">Delaware</option>\n                                        <option value=\"DC\">District of Columbia</option>\n                                        <option value=\"FL\">Florida</option>\n                                        <option value=\"GA\">Georgia</option>\n                                        <option value=\"HI\">Hawaii</option>\n                                        <option value=\"ID\">Idaho</option>\n                                        <option value=\"IL\">Illinois</option>\n                                        <option value=\"IN\">Indiana</option>\n                                        <option value=\"IA\">Iowa</option>\n                                        <option value=\"KS\">Kansas</option>\n                                        <option value=\"KY\">Kentucky</option>\n                                        <option value=\"LA\">Louisiana</option>\n                                        <option value=\"ME\">Maine</option>\n                                        <option value=\"MD\">Maryland</option>\n                                        <option value=\"MA\">Massachusetts</option>\n                                        <option value=\"MI\">Michigan</option>\n                                        <option value=\"MN\">Minnesota</option>\n                                        <option value=\"MS\">Mississippi</option>\n                                        <option value=\"MO\">Missouri</option>\n                                        <option value=\"MT\">Montana</option>\n                                        <option value=\"NE\">Nebraska</option>\n                                        <option value=\"NV\">Nevada</option>\n                                        <option value=\"NH\">New Hampshire</option>\n                                        <option value=\"NJ\">New Jersey</option>\n                                        <option value=\"NM\">New Mexico</option>\n                                        <option value=\"NY\">New York</option>\n                                        <option value=\"NC\">North Carolina</option>\n                                        <option value=\"ND\">North Dakota</option>\n                                        <option value=\"OH\">Ohio</option>\n                                        <option value=\"OK\">Oklahoma</option>\n                                        <option value=\"OR\">Oregon</option>\n                                        <option value=\"PA\">Pennsylvania</option>\n                                        <option value=\"RI\">Rhode Island</option>\n                                        <option value=\"SC\">South Carolina</option>\n                                        <option value=\"SD\">South Dakota</option>\n                                        <option value=\"TN\">Tennessee</option>\n                                        <option value=\"TX\">Texas</option>\n                                        <option value=\"UT\">Utah</option>\n                                        <option value=\"VT\">Vermont</option>\n                                        <option value=\"VA\">Virginia</option>\n                                        <option value=\"WA\">Washington</option>\n                                        <option value=\"WV\">West Virginia</option>\n                                        <option value=\"WI\">Wisconsin</option>\n                                        <option value=\"WY\">Wyoming</option>\n                                      </select>\n                                </div>\n                                <div class=\"span4\">\n                                    <label for=\"zip\">Zip</label>\n                            <input value=\"[[!+fi.zip]]\" type=\"text\" id=\"zip\" name=\"zip\"  >\n<input value=\"[[!+fi.nospam]]\" type=\"text\" id=\"nospam\" name=\"nospam\" class=\"nospam\">\n                                </div>\n                            </div>\n                            <label for=\"comments\">Additional Comments<sup>*</sup></label>\n                            <textarea value=\"[[!+fi.comments]]\" id=\"comments\" name=\"comments\"></textarea>\n                        <input class=\"cbp-mc-submit\" type=\"submit\" name=\"contactSubmit\" value=\"Send our way\" />\n                    </form>\n                  ',0,'a:0:{}',1,'assets/chunks/contact_form.html'),
	(12,1,0,'nav-inner-home','',0,0,0,'<li><a href=\"#[[+pagetitle:lcase:replace=` ==-`]]\" data-toggle=\"tab\" title=\"[[+pagetitle]]\">[[+pagetitle]]</a></li>',0,'a:0:{}',1,'assets/chunks/layout/nav-inner-home.html'),
	(13,1,0,'nav-inner-home-first','',0,0,0,'<li class=\"active\"><a href=\"#[[+pagetitle:lcase:replace=` ==-`]]\" class=\"active\" data-toggle=\"tab\" title=\"[[+pagetitle]]\">[[+pagetitle]]</a></li>',0,'a:0:{}',1,'assets/chunks/layout/nav-inner-home-first.html'),
	(14,1,0,'home-service','',0,0,0,'<div id=\"[[+pagetitle:lcase:replace=` ==-`]]\" class=\"tab-pane fade-in \">\n                                <h2>[[+pagetitle]]</h2>\n                                [[+home-content]]\n                            </div>',0,'a:0:{}',1,'assets/chunks/layout/home-service.html'),
	(15,1,0,'home-service-first','',0,0,0,'<div id=\"[[+pagetitle:lcase:replace=` ==-`]]\" class=\"tab-pane fade-in active\">\n                                <h2>[[+pagetitle]]</h2>\n                                [[+home-content]]\n                            </div>',0,'a:0:{}',1,'assets/chunks/layout/home-service-first.html'),
	(16,1,0,'nav-outer-list','',0,3,0,'<ul class=\"list\">\n[[+wf.wrapper]]\n</ul>',0,'a:0:{}',1,'assets/chunks/layout/ nav-outer-list.html'),
	(17,1,0,'intro-interior','',0,3,0,'<section class=\"hero\" style=\"background-image:url(\'[[*header-image]]\')\">\n                            <div class=\"row-fluid wrap\">\n                                <div class=\"span7\">\n                                    <h1>[[*pagetitle]]</h1>\n                                    [[*intro-content]]\n                                    \n                                </div>\n                            </div>\n                        </section>',0,'a:0:{}',1,'assets/chunks/layout/intro-interior.html'),
	(18,1,0,'testimonial','',0,3,0,'<li>\n	<p class=\"quote\">[[+content]]</p>\n                        <p class=\"byline\"><span>~ [[+pagetitle]]</span>, [[+location]]</p>\n</li>',0,'a:0:{}',1,'assets/chunks/layout/testimonial.html'),
	(19,1,0,'testimonial-odd','',0,3,0,'<li class=\"odd\">\n	<div class=\"row-fluid wrap\">\n		<p class=\"quote\">[[+content:striptags]]</p>\n	    <p class=\"byline\"><span>~ [[+pagetitle]]</span>, [[+location]]</p>\n	</div>\n</li>',0,'a:0:{}',1,'assets/chunks/layout/testimonial-odd.html'),
	(20,1,0,'testimonial-interior','',0,3,0,'<li>\n	<div class=\"row-fluid wrap\">\n		<p class=\"quote\">[[+content:striptags]]</p>\n	    <p class=\"byline\"><span>~ [[+pagetitle]]</span>, [[+location]]</p>\n	</div>\n</li>',0,'a:0:{}',1,'assets/chunks/layout/testimonial-interior.html'),
	(22,2,0,'snt_from','',0,0,0,'[[+first_name]] [[+last_name]] left you the message below from [[++site_url]]: <br />\n<br />\nPhone Number: [[+phone]]<br />\nAddress: [[+address]]<br />\nCity: [[+city]]<br />\nState: [[+state]]<br />\nZip: [[+zip]]<br />\n<br />\n\n\n<strong>[[+comments]]</strong><br /><br />\n\nYou can respond to [[+first_name]] [[+last_name]] by email at <a href=\"mailto:[[+email]]\">[[+email]]</a> or by phone at [[+phone]]',0,'a:0:{}',0,''),
	(23,2,0,'snt_from_quote','',0,0,0,'[[+first_name]] [[+last_name]] Is requseting a free quote from [[++site_url]]: <br />\n<br />\nPhone Number: [[+phone]]<br />\nAddress: [[+address]]<br />\nCity: [[+city]]<br />\n<br />\n\n\n<strong>[[+comments]]</strong><br /><br />\n\nYou can respond to [[+first_name]] [[+last_name]] by email at <a href=\"mailto:[[+email]]\">[[+email]]</a> or by phone at [[+phone]]',0,'a:0:{}',0,'');

/*!40000 ALTER TABLE `modx_site_htmlsnippets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_plugin_events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_plugin_events`;

CREATE TABLE `modx_site_plugin_events` (
  `pluginid` int(10) NOT NULL DEFAULT '0',
  `event` varchar(255) NOT NULL DEFAULT '',
  `priority` int(10) NOT NULL DEFAULT '0',
  `propertyset` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pluginid`,`event`),
  KEY `priority` (`priority`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_plugin_events` WRITE;
/*!40000 ALTER TABLE `modx_site_plugin_events` DISABLE KEYS */;

INSERT INTO `modx_site_plugin_events` (`pluginid`, `event`, `priority`, `propertyset`)
VALUES
	(1,'OnRichTextBrowserInit',0,0),
	(1,'OnRichTextEditorRegister',0,0),
	(1,'OnRichTextEditorInit',0,0),
	(2,'OnDocFormSave',0,0),
	(3,'OnWebPageInit',0,0),
	(3,'OnParseDocument',0,0),
	(4,'OnChunkFormPrerender',0,0),
	(4,'OnPluginFormPrerender',0,0),
	(4,'OnSnipFormPrerender',0,0),
	(4,'OnTempFormPrerender',0,0),
	(4,'OnFileEditFormPrerender',0,0),
	(4,'OnDocFormPrerender',0,0),
	(4,'OnRichTextEditorRegister',0,0),
	(5,'OnDocFormPrerender',0,0),
	(5,'OnTVInputPropertiesList',0,0),
	(5,'OnTVInputRenderList',0,0),
	(6,'OnDocFormPrerender',0,0);

/*!40000 ALTER TABLE `modx_site_plugin_events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_plugins`;

CREATE TABLE `modx_site_plugins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `plugincode` mediumtext NOT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `properties` text,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `moduleguid` varchar(32) NOT NULL DEFAULT '',
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `disabled` (`disabled`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_plugins` WRITE;
/*!40000 ALTER TABLE `modx_site_plugins` DISABLE KEYS */;

INSERT INTO `modx_site_plugins` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `plugincode`, `locked`, `properties`, `disabled`, `moduleguid`, `static`, `static_file`)
VALUES
	(1,0,0,'TinyMCE','TinyMCE 4.3.3-pl plugin for MODx Revolution',0,0,0,'/**\n * TinyMCE RichText Editor Plugin\n *\n * Events: OnRichTextEditorInit, OnRichTextEditorRegister,\n * OnBeforeManagerPageInit, OnRichTextBrowserInit\n *\n * @author Jeff Whitfield <jeff@collabpad.com>\n * @author Shaun McCormick <shaun@collabpad.com>\n *\n * @var modX $modx\n * @var array $scriptProperties\n *\n * @package tinymce\n * @subpackage build\n */\nif ($modx->event->name == \'OnRichTextEditorRegister\') {\n    $modx->event->output(\'TinyMCE\');\n    return;\n}\nrequire_once $modx->getOption(\'tiny.core_path\',null,$modx->getOption(\'core_path\').\'components/tinymce/\').\'tinymce.class.php\';\n$tiny = new TinyMCE($modx,$scriptProperties);\n\n$useEditor = $tiny->context->getOption(\'use_editor\',false);\n$whichEditor = $tiny->context->getOption(\'which_editor\',\'\');\n\n/* Handle event */\nswitch ($modx->event->name) {\n    case \'OnRichTextEditorInit\':\n        if ($useEditor && $whichEditor == \'TinyMCE\') {\n            unset($scriptProperties[\'chunk\']);\n            if (isset($forfrontend) || $modx->context->get(\'key\') != \'mgr\') {\n                $def = $tiny->context->getOption(\'cultureKey\',$tiny->context->getOption(\'manager_language\',\'en\'));\n                $tiny->properties[\'language\'] = $modx->getOption(\'fe_editor_lang\',array(),$def);\n                $tiny->properties[\'frontend\'] = true;\n                unset($def);\n            }\n            /* commenting these out as it causes problems with richtext tvs */\n            //if (isset($scriptProperties[\'resource\']) && !$resource->get(\'richtext\')) return;\n            //if (!isset($scriptProperties[\'resource\']) && !$modx->getOption(\'richtext_default\',null,false)) return;\n            $tiny->setProperties($scriptProperties);\n            $html = $tiny->initialize();\n            $modx->event->output($html);\n            unset($html);\n        }\n        break;\n    case \'OnRichTextBrowserInit\':\n        if ($useEditor && $whichEditor == \'TinyMCE\') {\n            $inRevo20 = (boolean)version_compare($modx->version[\'full_version\'],\'2.1.0-rc1\',\'<\');\n            $modx->getVersionData();\n            $source = $tiny->context->getOption(\'default_media_source\',null,1);\n            \n            $modx->controller->addHtml(\'<script type=\"text/javascript\">var inRevo20 = \'.($inRevo20 ? 1 : 0).\';MODx.source = \"\'.$source.\'\";</script>\');\n            \n            $modx->controller->addJavascript($tiny->config[\'assetsUrl\'].\'jscripts/tiny_mce/tiny_mce_popup.js\');\n            if (file_exists($tiny->config[\'assetsPath\'].\'jscripts/tiny_mce/langs/\'.$tiny->properties[\'language\'].\'.js\')) {\n                $modx->controller->addJavascript($tiny->config[\'assetsUrl\'].\'jscripts/tiny_mce/langs/\'.$tiny->properties[\'language\'].\'.js\');\n            } else {\n                $modx->controller->addJavascript($tiny->config[\'assetsUrl\'].\'jscripts/tiny_mce/langs/en.js\');\n            }\n            $modx->controller->addJavascript($tiny->config[\'assetsUrl\'].\'tiny.browser.js\');\n            $modx->event->output(\'Tiny.browserCallback\');\n        }\n        return \'\';\n        break;\n\n   default: break;\n}\nreturn;',0,'a:39:{s:22:\"accessibility_warnings\";a:7:{s:4:\"name\";s:22:\"accessibility_warnings\";s:4:\"desc\";s:315:\"If this option is set to true some accessibility warnings will be presented to the user if they miss specifying that information. This option is set to true by default, since we should all try to make this world a better place for disabled people. But if you are annoyed with the warnings, set this option to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:23:\"apply_source_formatting\";a:7:{s:4:\"name\";s:23:\"apply_source_formatting\";s:4:\"desc\";s:229:\"This option enables you to tell TinyMCE to apply some source formatting to the output HTML code. With source formatting, the output HTML code is indented and formatted. Without source formatting, the output HTML is more compact. \";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"button_tile_map\";a:7:{s:4:\"name\";s:15:\"button_tile_map\";s:4:\"desc\";s:338:\"If this option is set to true TinyMCE will use tiled images instead of individual images for most of the editor controls. This produces faster loading time since only one GIF image needs to be loaded instead of a GIF for each individual button. This option is set to false by default since it doesn\'t work with some DOCTYPE declarations. \";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"cleanup\";a:7:{s:4:\"name\";s:7:\"cleanup\";s:4:\"desc\";s:331:\"This option enables or disables the built-in clean up functionality. TinyMCE is equipped with powerful clean up functionality that enables you to specify what elements and attributes are allowed and how HTML contents should be generated. This option is set to true by default, but if you want to disable it you may set it to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:18:\"cleanup_on_startup\";a:7:{s:4:\"name\";s:18:\"cleanup_on_startup\";s:4:\"desc\";s:135:\"If you set this option to true, TinyMCE will perform a HTML cleanup call when the editor loads. This option is set to false by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:22:\"convert_fonts_to_spans\";a:7:{s:4:\"name\";s:22:\"convert_fonts_to_spans\";s:4:\"desc\";s:348:\"If you set this option to true, TinyMCE will convert all font elements to span elements and generate span elements instead of font elements. This option should be used in order to get more W3C compatible code, since font elements are deprecated. How sizes get converted can be controlled by the font_size_classes and font_size_style_values options.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:23:\"convert_newlines_to_brs\";a:7:{s:4:\"name\";s:23:\"convert_newlines_to_brs\";s:4:\"desc\";s:128:\"If you set this option to true, newline characters codes get converted into br elements. This option is set to false by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"convert_urls\";a:7:{s:4:\"name\";s:12:\"convert_urls\";s:4:\"desc\";s:495:\"This option enables you to control whether TinyMCE is to be clever and restore URLs to their original values. URLs are automatically converted (messed up) by default because the built-in browser logic works this way. There is no way to get the real URL unless you store it away. If you set this option to false it will try to keep these URLs intact. This option is set to true by default, which means URLs will be forced to be either absolute or relative depending on the state of relative_urls.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"dialog_type\";a:7:{s:4:\"name\";s:11:\"dialog_type\";s:4:\"desc\";s:246:\"This option enables you to specify how dialogs/popups should be opened. Possible values are \"window\" and \"modal\", where the window option opens a normal window and the dialog option opens a modal dialog. This option is set to \"window\" by default.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{i:0;s:6:\"window\";s:4:\"text\";s:6:\"Window\";}i:1;a:2:{i:0;s:5:\"modal\";s:4:\"text\";s:5:\"Modal\";}}s:5:\"value\";s:6:\"window\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:14:\"directionality\";a:7:{s:4:\"name\";s:14:\"directionality\";s:4:\"desc\";s:261:\"This option specifies the default writing direction. Some languages (Like Hebrew, Arabic, Urdu...) write from right to left instead of left to right. The default value of this option is \"ltr\" but if you want to use from right to left mode specify \"rtl\" instead.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:5:\"value\";s:3:\"ltr\";s:4:\"text\";s:13:\"Left to Right\";}i:1;a:2:{s:5:\"value\";s:3:\"rtl\";s:4:\"text\";s:13:\"Right to Left\";}}s:5:\"value\";s:3:\"ltr\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:14:\"element_format\";a:7:{s:4:\"name\";s:14:\"element_format\";s:4:\"desc\";s:210:\"This option enables control if elements should be in html or xhtml mode. xhtml is the default state for this option. This means that for example &lt;br /&gt; will be &lt;br&gt; if you set this option to \"html\".\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:5:\"value\";s:5:\"xhtml\";s:4:\"text\";s:5:\"XHTML\";}i:1;a:2:{s:5:\"value\";s:4:\"html\";s:4:\"text\";s:4:\"HTML\";}}s:5:\"value\";s:5:\"xhtml\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"entity_encoding\";a:7:{s:4:\"name\";s:15:\"entity_encoding\";s:4:\"desc\";s:70:\"This option controls how entities/characters get processed by TinyMCE.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:4:{i:0;a:2:{s:5:\"value\";s:0:\"\";s:4:\"text\";s:4:\"None\";}i:1;a:2:{s:5:\"value\";s:5:\"named\";s:4:\"text\";s:5:\"Named\";}i:2;a:2:{s:5:\"value\";s:7:\"numeric\";s:4:\"text\";s:7:\"Numeric\";}i:3;a:2:{s:5:\"value\";s:3:\"raw\";s:4:\"text\";s:3:\"Raw\";}}s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:16:\"force_p_newlines\";a:7:{s:4:\"name\";s:16:\"force_p_newlines\";s:4:\"desc\";s:147:\"This option enables you to disable/enable the creation of paragraphs on return/enter in Mozilla/Firefox. The default value of this option is true. \";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:22:\"force_hex_style_colors\";a:7:{s:4:\"name\";s:22:\"force_hex_style_colors\";s:4:\"desc\";s:277:\"This option enables you to control TinyMCE to force the color format to use hexadecimal instead of rgb strings. It converts for example \"color: rgb(255, 255, 0)\" to \"#FFFF00\". This option is set to true by default since otherwice MSIE and Firefox would differ in this behavior.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"height\";a:7:{s:4:\"name\";s:6:\"height\";s:4:\"desc\";s:38:\"Sets the height of the TinyMCE editor.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:5:\"400px\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"indentation\";a:7:{s:4:\"name\";s:11:\"indentation\";s:4:\"desc\";s:139:\"This option allows specification of the indentation level for indent/outdent buttons in the UI. This defaults to 30px but can be any value.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:4:\"30px\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:16:\"invalid_elements\";a:7:{s:4:\"name\";s:16:\"invalid_elements\";s:4:\"desc\";s:163:\"This option should contain a comma separated list of element names to exclude from the content. Elements in this list will removed when TinyMCE executes a cleanup.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"nowrap\";a:7:{s:4:\"name\";s:6:\"nowrap\";s:4:\"desc\";s:212:\"This nowrap option enables you to control how whitespace is to be wordwrapped within the editor. This option is set to false by default, but if you enable it by setting it to true editor contents will never wrap.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"object_resizing\";a:7:{s:4:\"name\";s:15:\"object_resizing\";s:4:\"desc\";s:148:\"This option gives you the ability to turn on/off the inline resizing controls of tables and images in Firefox/Mozilla. These are enabled by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"path_options\";a:7:{s:4:\"name\";s:12:\"path_options\";s:4:\"desc\";s:119:\"Sets a group of options. Note: This will override the relative_urls, document_base_url and remove_script_host settings.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:3:{i:0;a:2:{s:5:\"value\";s:11:\"docrelative\";s:4:\"text\";s:17:\"Document Relative\";}i:1;a:2:{s:5:\"value\";s:12:\"rootrelative\";s:4:\"text\";s:13:\"Root Relative\";}i:2;a:2:{s:5:\"value\";s:11:\"fullpathurl\";s:4:\"text\";s:13:\"Full Path URL\";}}s:5:\"value\";s:11:\"docrelative\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:28:\"plugin_insertdate_dateFormat\";a:7:{s:4:\"name\";s:28:\"plugin_insertdate_dateFormat\";s:4:\"desc\";s:53:\"Formatting of dates when using the InsertDate plugin.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:8:\"%Y-%m-%d\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:28:\"plugin_insertdate_timeFormat\";a:7:{s:4:\"name\";s:28:\"plugin_insertdate_timeFormat\";s:4:\"desc\";s:53:\"Formatting of times when using the InsertDate plugin.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:8:\"%H:%M:%S\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"preformatted\";a:7:{s:4:\"name\";s:12:\"preformatted\";s:4:\"desc\";s:231:\"If you enable this feature, whitespace such as tabs and spaces will be preserved. Much like the behavior of a &lt;pre&gt; element. This can be handy when integrating TinyMCE with webmail clients. This option is disabled by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"relative_urls\";a:7:{s:4:\"name\";s:13:\"relative_urls\";s:4:\"desc\";s:231:\"If this option is set to true, all URLs returned from the file manager will be relative from the specified document_base_url. If it is set to false all URLs will be converted to absolute URLs. This option is set to true by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:17:\"remove_linebreaks\";a:7:{s:4:\"name\";s:17:\"remove_linebreaks\";s:4:\"desc\";s:531:\"This option controls whether line break characters should be removed from output HTML. This option is enabled by default because there are differences between browser implementations regarding what to do with white space in the DOM. Gecko and Safari place white space in text nodes in the DOM. IE and Opera remove them from the DOM and therefore the line breaks will automatically be removed in those. This option will normalize this behavior when enabled (true) and all browsers will have a white-space-stripped DOM serialization.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:18:\"remove_script_host\";a:7:{s:4:\"name\";s:18:\"remove_script_host\";s:4:\"desc\";s:221:\"If this option is enabled the protocol and host part of the URLs returned from the file manager will be removed. This option is only used if the relative_urls option is set to false. This option is set to true by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:20:\"remove_trailing_nbsp\";a:7:{s:4:\"name\";s:20:\"remove_trailing_nbsp\";s:4:\"desc\";s:392:\"This option enables you to specify that TinyMCE should remove any traling &nbsp; characters in block elements if you start to write inside them. Paragraphs are default padded with a &nbsp; and if you write text into such paragraphs the space will remain. Setting this option to true will remove the space. This option is set to false by default since the cursor jumps a bit in Gecko browsers.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:4:\"skin\";a:7:{s:4:\"name\";s:4:\"skin\";s:4:\"desc\";s:330:\"This option enables you to specify what skin you want to use with your theme. A skin is basically a CSS file that gets loaded from the skins directory inside the theme. The advanced theme that TinyMCE comes with has two skins, these are called \"default\" and \"o2k7\". We added another skin named \"cirkuit\" that is chosen by default.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:7:\"cirkuit\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"skin_variant\";a:7:{s:4:\"name\";s:12:\"skin_variant\";s:4:\"desc\";s:403:\"This option enables you to specify a variant for the skin, for example \"silver\" or \"black\". \"default\" skin does not offer any variant, whereas \"o2k7\" default offers \"silver\" or \"black\" variants to the default one. For the \"cirkuit\" skin there\'s one variant named \"silver\". When creating a skin, additional variants may also be created, by adding ui_[variant_name].css files alongside the default ui.css.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:20:\"table_inline_editing\";a:7:{s:4:\"name\";s:20:\"table_inline_editing\";s:4:\"desc\";s:231:\"This option gives you the ability to turn on/off the inline table editing controls in Firefox/Mozilla. According to the TinyMCE documentation, these controls are somewhat buggy and not redesignable, so they are disabled by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:22:\"theme_advanced_disable\";a:7:{s:4:\"name\";s:22:\"theme_advanced_disable\";s:4:\"desc\";s:111:\"This option should contain a comma separated list of controls to disable from any toolbar row/panel in TinyMCE.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:19:\"theme_advanced_path\";a:7:{s:4:\"name\";s:19:\"theme_advanced_path\";s:4:\"desc\";s:331:\"This option gives you the ability to enable/disable the element path. This option is only useful if the theme_advanced_statusbar_location option is set to \"top\" or \"bottom\". This option is set to \"true\" by default. Setting this option to \"false\" will effectively hide the path tool, though it still takes up room in the Status Bar.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:32:\"theme_advanced_resize_horizontal\";a:7:{s:4:\"name\";s:32:\"theme_advanced_resize_horizontal\";s:4:\"desc\";s:319:\"This option gives you the ability to enable/disable the horizontal resizing. This option is only useful if the theme_advanced_statusbar_location option is set to \"top\" or \"bottom\" and when the theme_advanced_resizing is set to true. This option is set to true by default, allowing both resizing horizontal and vertical.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:23:\"theme_advanced_resizing\";a:7:{s:4:\"name\";s:23:\"theme_advanced_resizing\";s:4:\"desc\";s:216:\"This option gives you the ability to enable/disable the resizing button. This option is only useful if the theme_advanced_statusbar_location option is set to \"top\" or \"bottom\". This option is set to false by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:33:\"theme_advanced_statusbar_location\";a:7:{s:4:\"name\";s:33:\"theme_advanced_statusbar_location\";s:4:\"desc\";s:257:\"This option enables you to specify where the element statusbar with the path and resize tool should be located. This option can be set to \"top\" or \"bottom\". The default value is set to \"top\". This option can only be used when the theme is set to \"advanced\".\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:5:\"value\";s:3:\"top\";s:4:\"text\";s:3:\"Top\";}i:1;a:2:{s:5:\"value\";s:6:\"bottom\";s:4:\"text\";s:6:\"Bottom\";}}s:5:\"value\";s:6:\"bottom\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:28:\"theme_advanced_toolbar_align\";a:7:{s:4:\"name\";s:28:\"theme_advanced_toolbar_align\";s:4:\"desc\";s:187:\"This option enables you to specify the alignment of the toolbar, this value can be \"left\", \"right\" or \"center\" (the default). This option can only be used when theme is set to \"advanced\".\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:3:{i:0;a:2:{s:5:\"value\";s:6:\"center\";s:4:\"text\";s:6:\"Center\";}i:1;a:2:{s:5:\"value\";s:4:\"left\";s:4:\"text\";s:4:\"Left\";}i:2;a:2:{s:5:\"value\";s:5:\"right\";s:4:\"text\";s:5:\"Right\";}}s:5:\"value\";s:4:\"left\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:31:\"theme_advanced_toolbar_location\";a:7:{s:4:\"name\";s:31:\"theme_advanced_toolbar_location\";s:4:\"desc\";s:191:\"\nThis option enables you to specify where the toolbar should be located. This option can be set to \"top\" or \"bottom\" (the defualt). This option can only be used when theme is set to advanced.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:5:\"value\";s:3:\"top\";s:4:\"text\";s:3:\"Top\";}i:1;a:2:{s:5:\"value\";s:6:\"bottom\";s:4:\"text\";s:6:\"Bottom\";}}s:5:\"value\";s:3:\"top\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"width\";a:7:{s:4:\"name\";s:5:\"width\";s:4:\"desc\";s:32:\"The width of the TinyMCE editor.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"95%\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:33:\"template_selected_content_classes\";a:7:{s:4:\"name\";s:33:\"template_selected_content_classes\";s:4:\"desc\";s:234:\"Specify a list of CSS class names for the template plugin. They must be separated by spaces. Any template element with one of the specified CSS classes will have its content replaced by the selected editor content when first inserted.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}}',0,'',0,''),
	(2,1,0,'Image Resizer','',0,2,0,'// CHANGE THESE AS NEEDED\n$phpThumb_file_location = \"resources/libs/phpthumb/ThumbLib.inc.php\";\n\n// MEDIA SOURCE FOR LOCKED DOWN IMAGE UPLOADING\n$tv_media_source = \'assets/media/\';\n\n// MODIFY THE ARRAY STRUCTURE BELOW FOR EACH TEMPLATE VARIABLE THAT NEEDS AUTORESIZING\n$tv_array = array(\n                  \'homepage_rotator_image\' => array(\n                    \'large_size\' => \'1024x577\',\n                    \'large_crop\' => 1,\n                    \'retina\' => 1,\n                  )\n                );\n\n$tv_names = implode(\"\',\'\",array_keys($tv_array));\n\n//GET THE TVS FROM THE DB\n$sql = \"SELECT A.*\";\n$sql .= \" FROM modx_site_tmplvars A\";\n$sql .= \" WHERE A.type = \'image\' AND A.name IN (\'\" . $tv_names . \"\')\";\n\n// GET VALUES FROM DATABASE AND SET PARAMS\n$result = $modx->query($sql);\nif ($result && $result instanceof PDOStatement) {\n  $image_array = array();\n  $image_settings_array = array();\n  while ($gal_var = $result->fetch(PDO::FETCH_ASSOC)) {\n\n    if (isset($_REQUEST[\'tv\'.$gal_var[\'id\']])) {\n      // BUILD THE ARRAY OF IMAGES THAT WERE FOUND IN THE DOCUMENT FOR PROCESSING\n      $image_array[] = $_REQUEST[\'tv\'.$gal_var[\'id\']];\n      $image_settings_array[] = $tv_array[$gal_var[\'name\']];\n    }\n\n  }\n}\n\nif(!empty($image_array)) {\n\n  foreach ($image_array as $key=>$upload_file) {\n\n    $modx->log(modX::LOG_LEVEL_ERROR,\'Starting \'.$upload_file);\n\n    set_time_limit(300);\n\n    if(is_null($upload_file))\n      return false;\n\n    include_once ($modx->getOption(\'base_path\') . $phpThumb_file_location);\n\n    $full_path = $modx->getOption(\'base_path\') . $upload_file;\n    if(!is_file($full_path)) {\n      $full_path = $modx->getOption(\'base_path\') . $tv_media_source . $upload_file;\n    }\n    $file_name = basename($full_path);\n    $image_directory = dirname($full_path);\n\n    // MAKE SOME LARGE ONES\n    if(isset($image_settings_array[$key][\'large_size\'])) {\n        try\n        {\n          $large = PhpThumbFactory::create($full_path);\n        }\n        catch (Exception $e)\n        {\n    // handle error here however you\'d like\n        }\n\n\n        $large_size_array = explode(\"x\",$image_settings_array[$key][\'large_size\']);\n\n        if ($image_settings_array[$key][\'retina\']) {\n          if ($image_settings_array[$key][\'large_crop\']) {\n            $large->adaptiveResize($large_size_array[0]*2, $large_size_array[1]*2);\n          } else {\n            $large->resize($large_size_array[0]*2, $large_size_array[1]*2);\n          }\n          $extension_pos = strrpos($file_name, \'.\');\n          $file_name_amended = substr($file_name, 0, $extension_pos) . \'_large@2x\' . substr($file_name, $extension_pos);\n          $large->save($image_directory . \'/\' . $file_name_amended );\n        }\n\n        if ($image_settings_array[$key][\'large_crop\']) {\n          $large->adaptiveResize($large_size_array[0], $large_size_array[1]);\n        } else {\n          $large->resize($large_size_array[0], $large_size_array[1]);\n        }\n        $extension_pos = strrpos($file_name, \'.\');\n        $file_name_amended = substr($file_name, 0, $extension_pos) . \'_large\' . substr($file_name, $extension_pos);\n        $large->save($image_directory . \'/\' . $file_name_amended );\n    }\n\n\n    $modx->log(modX::LOG_LEVEL_ERROR,\'Finished \'.$upload_file);\n\n  }\n}',0,'a:0:{}',0,'',1,'assets/plugins/utils/resizer.php'),
	(3,1,0,'redirect','Redirect plugin for mapping old urls to new urls\n\nDOES NOT WORK IF STATIC',0,2,0,'// Add URLs and 301s to the array below\n\n$redirect_map = array(\n						\"/old.html\" => \"/page-two.html\",\n						\"/old2.url\" => \"/new2.url\"\n						);\n\n\n$key = $_SERVER[\"REQUEST_URI\"];\n\nif(!empty($redirect_map[$key])) $url = $redirect_map[$key];\nif(!empty($redirect_map[$key.\"/\"])) $url = $redirect_map[$key.\"/\"];\nif(!empty($redirect_map[rtrim($key,\"/\")])) $url = $redirect_map[rtrim($key,\"/\")];\n\nif(isset($url)) {\n header(\"HTTP/1.1 301 Moved Permanently\");\n header(\"Location: $url\");\n}',0,'a:0:{}',0,'',0,'assets/plugins/utils/redirect.php'),
	(4,0,0,'CodeMirror','CodeMirror 2.2.1-pl plugin for MODx Revolution',0,0,0,'/**\n * @var modX $modx\n * @var array $scriptProperties\n *\n * @package codemirror\n */\nif ($modx->event->name == \'OnRichTextEditorRegister\') {\n    $modx->event->output(\'CodeMirror\');\n    return;\n}\n\n$eventArray = array(\n    \'element\'=>array(\n		\'OnSnipFormPrerender\',\n		\'OnTempFormPrerender\',\n		\'OnChunkFormPrerender\',\n		\'OnPluginFormPrerender\',\n		/*\'OnTVFormPrerender\'*/\n		\'OnFileEditFormPrerender\',\n		\'OnFileEditFormPrerender\',\n		),\n	\'other\'=>array(\n		\'OnDocFormPrerender\',\n		\'OnRichTextEditorInit\',\n		\'OnRichTextBrowserInit\'\n	)\n);\nif ((in_array($modx->event->name,$eventArray[\'element\']) && $modx->getOption(\'which_element_editor\',null,\'CodeMirror\') != \'CodeMirror\') || (in_array($modx->event->name,$eventArray[\'other\']) && $modx->getOption(\'which_editor\',null,\'CodeMirror\') != \'CodeMirror\')) return;\n\nif (!$modx->getOption(\'use_editor\',null,true)) return;\nif (!$modx->getOption(\'codemirror.enable\',null,true)) return;\n\n/** @var CodeMirror $codeMirror */\n$codeMirror = $modx->getService(\'codemirror\',\'CodeMirror\',$modx->getOption(\'codemirror.core_path\',null,$modx->getOption(\'core_path\').\'components/codemirror/\').\'model/codemirror/\');\nif (!($codeMirror instanceof CodeMirror)) return \'\';\n\n$options = array(\n    \'modx_path\' => $codeMirror->config[\'assetsUrl\'],\n    \'theme\' => $modx->getOption(\'theme\',$scriptProperties,\'default\'),\n\n    \'indentUnit\' => (int)$modx->getOption(\'indentUnit\',$scriptProperties,$modx->getOption(\'indent_unit\',$scriptProperties,2)),\n    \'smartIndent\' => (boolean)$modx->getOption(\'smartIndent\',$scriptProperties,false),\n    \'tabSize\' => (int)$modx->getOption(\'tabSize\',$scriptProperties,4),\n    \'indentWithTabs\' => (boolean)$modx->getOption(\'indentWithTabs\',$scriptProperties,true),\n    \'electricChars\' => (boolean)$modx->getOption(\'electricChars\',$scriptProperties,true),\n    \'autoClearEmptyLines\' => (boolean)$modx->getOption(\'electricChars\',$scriptProperties,false),\n\n    \'lineWrapping\' => (boolean)$modx->getOption(\'lineWrapping\',$scriptProperties,true),\n    \'lineNumbers\' => (boolean)$modx->getOption(\'lineNumbers\',$scriptProperties,$modx->getOption(\'line_numbers\',$scriptProperties,true)),\n    \'firstLineNumber\' => (int)$modx->getOption(\'firstLineNumber\',$scriptProperties,1),\n    \'highlightLine\' => (boolean)$modx->getOption(\'highlightLine\',$scriptProperties,true),\n    \'matchBrackets\' => (boolean)$modx->getOption(\'matchBrackets\',$scriptProperties,true),\n    \'showSearchForm\' => (boolean)$modx->getOption(\'showSearchForm\',$scriptProperties,true),\n    \'undoDepth\' => $modx->getOption(\'undoDepth\',$scriptProperties,40),\n);\n\n$load = false;\nswitch ($modx->event->name) {\n    case \'OnSnipFormPrerender\':\n        $options[\'modx_loader\'] = \'onSnippet\';\n        $options[\'mode\'] = \'php\';\n        $load = true;\n        break;\n    case \'OnTempFormPrerender\':\n        $options[\'modx_loader\'] = \'onTemplate\';\n        $options[\'mode\'] = \'htmlmixed\';\n        $load = true;\n        break;\n    case \'OnChunkFormPrerender\':\n        $options[\'modx_loader\'] = \'onChunk\';\n        $options[\'mode\'] = \'htmlmixed\';\n        $load = true;\n        break;\n    case \'OnPluginFormPrerender\':\n        $options[\'modx_loader\'] = \'onPlugin\';\n        $options[\'mode\'] = \'php\';\n        $load = true;\n        break;\n    /* disabling TVs for now, since it causes problems with newlines\n    case \'OnTVFormPrerender\':\n        $options[\'modx_loader\'] = \'onTV\';\n        $options[\'height\'] = \'250px\';\n        $load = true;\n        break;*/\n    case \'OnFileEditFormPrerender\':\n        $options[\'modx_loader\'] = \'onFile\';\n        $options[\'mode\'] = \'php\';\n        $load = true;\n        break;\n    case \'OnDocFormPrerender\':\n    	$options[\'modx_loader\'] = \'onResource\';\n        $options[\'mode\'] = \'htmlmixed\';\n        $load = true;\n    	break;\n    /* debated whether or not to use */\n    case \'OnRichTextEditorInit\':\n        break;\n    case \'OnRichTextBrowserInit\':\n        break;\n}\n\nif ($load) {\n    $options[\'searchTpl\'] = $codeMirror->getChunk(\'codemirror.search\');\n\n    $modx->regClientStartupHTMLBlock(\'<script type=\"text/javascript\">MODx.codem = \'.$modx->toJSON($options).\';</script>\');\n    $modx->regClientCSS($codeMirror->config[\'assetsUrl\'].\'css/codemirror-compressed.css\');\n    $modx->regClientCSS($codeMirror->config[\'assetsUrl\'].\'css/cm.css\');\n    if ($options[\'theme\'] != \'default\') {\n        $modx->regClientCSS($codeMirror->config[\'assetsUrl\'].\'cm/theme/\'.$options[\'theme\'].\'.css\');\n    }\n    $modx->regClientStartupScript($codeMirror->config[\'assetsUrl\'].\'js/codemirror-compressed.js\');\n    $modx->regClientStartupScript($codeMirror->config[\'assetsUrl\'].\'js/cm.js\');\n}\n\nreturn;',0,'a:14:{s:5:\"theme\";a:7:{s:4:\"name\";s:5:\"theme\";s:4:\"desc\";s:18:\"prop_cm.theme_desc\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:14:{i:0;a:2:{s:4:\"text\";s:7:\"default\";s:5:\"value\";s:7:\"default\";}i:1;a:2:{s:4:\"text\";s:8:\"ambiance\";s:5:\"value\";s:8:\"ambiance\";}i:2;a:2:{s:4:\"text\";s:10:\"blackboard\";s:5:\"value\";s:10:\"blackboard\";}i:3;a:2:{s:4:\"text\";s:6:\"cobalt\";s:5:\"value\";s:6:\"cobalt\";}i:4;a:2:{s:4:\"text\";s:7:\"eclipse\";s:5:\"value\";s:7:\"eclipse\";}i:5;a:2:{s:4:\"text\";s:7:\"elegant\";s:5:\"value\";s:7:\"elegant\";}i:6;a:2:{s:4:\"text\";s:11:\"erlang-dark\";s:5:\"value\";s:11:\"erlang-dark\";}i:7;a:2:{s:4:\"text\";s:11:\"lesser-dark\";s:5:\"value\";s:11:\"lesser-dark\";}i:8;a:2:{s:4:\"text\";s:7:\"monokai\";s:5:\"value\";s:7:\"monokai\";}i:9;a:2:{s:4:\"text\";s:4:\"neat\";s:5:\"value\";s:4:\"near\";}i:10;a:2:{s:4:\"text\";s:5:\"night\";s:5:\"value\";s:5:\"night\";}i:11;a:2:{s:4:\"text\";s:8:\"rubyblue\";s:5:\"value\";s:8:\"rubyblue\";}i:12;a:2:{s:4:\"text\";s:11:\"vibrant-ink\";s:5:\"value\";s:11:\"vibrant-ink\";}i:13;a:2:{s:4:\"text\";s:7:\"xq-dark\";s:5:\"value\";s:7:\"xq-dark\";}}s:5:\"value\";s:7:\"default\";s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:10:\"indentUnit\";a:7:{s:4:\"name\";s:10:\"indentUnit\";s:4:\"desc\";s:23:\"prop_cm.indentUnit_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:2;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:11:\"smartIndent\";a:7:{s:4:\"name\";s:11:\"smartIndent\";s:4:\"desc\";s:24:\"prop_cm.smartIndent_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:7:\"tabSize\";a:7:{s:4:\"name\";s:7:\"tabSize\";s:4:\"desc\";s:20:\"prop_cm.tabSize_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:4;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:14:\"indentWithTabs\";a:7:{s:4:\"name\";s:14:\"indentWithTabs\";s:4:\"desc\";s:27:\"prop_cm.indentWithTabs_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:13:\"electricChars\";a:7:{s:4:\"name\";s:13:\"electricChars\";s:4:\"desc\";s:26:\"prop_cm.electricChars_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:19:\"autoClearEmptyLines\";a:7:{s:4:\"name\";s:19:\"autoClearEmptyLines\";s:4:\"desc\";s:32:\"prop_cm.autoClearEmptyLines_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:12:\"lineWrapping\";a:7:{s:4:\"name\";s:12:\"lineWrapping\";s:4:\"desc\";s:25:\"prop_cm.lineWrapping_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:11:\"lineNumbers\";a:7:{s:4:\"name\";s:11:\"lineNumbers\";s:4:\"desc\";s:24:\"prop_cm.lineNumbers_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:15:\"firstLineNumber\";a:7:{s:4:\"name\";s:15:\"firstLineNumber\";s:4:\"desc\";s:28:\"prop_cm.firstLineNumber_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:1;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:13:\"highlightLine\";a:7:{s:4:\"name\";s:13:\"highlightLine\";s:4:\"desc\";s:26:\"prop_cm.highlightLine_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:13:\"matchBrackets\";a:7:{s:4:\"name\";s:13:\"matchBrackets\";s:4:\"desc\";s:26:\"prop_cm.matchBrackets_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:14:\"showSearchForm\";a:7:{s:4:\"name\";s:14:\"showSearchForm\";s:4:\"desc\";s:27:\"prop_cm.showSearchForm_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}s:9:\"undoDepth\";a:7:{s:4:\"name\";s:9:\"undoDepth\";s:4:\"desc\";s:22:\"prop_cm.undoDepth_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:40;s:7:\"lexicon\";s:21:\"codemirror:properties\";s:4:\"area\";s:0:\"\";}}',0,'',0,''),
	(5,0,0,'MIGX','',0,5,0,'$corePath = $modx->getOption(\'migx.core_path\',null,$modx->getOption(\'core_path\').\'components/migx/\');\n$assetsUrl = $modx->getOption(\'migx.assets_url\', null, $modx->getOption(\'assets_url\') . \'components/migx/\');\nswitch ($modx->event->name) {\n    case \'OnTVInputRenderList\':\n        $modx->event->output($corePath.\'elements/tv/input/\');\n        break;\n    case \'OnTVInputPropertiesList\':\n        $modx->event->output($corePath.\'elements/tv/inputoptions/\');\n        break;\n\n        case \'OnDocFormPrerender\':\n        $modx->controller->addCss($assetsUrl.\'css/mgr.css\');\n        break; \n \n    /*          \n    case \'OnTVOutputRenderList\':\n        $modx->event->output($corePath.\'elements/tv/output/\');\n        break;\n    case \'OnTVOutputRenderPropertiesList\':\n        $modx->event->output($corePath.\'elements/tv/properties/\');\n        break;\n    \n    case \'OnDocFormPrerender\':\n        $assetsUrl = $modx->getOption(\'colorpicker.assets_url\',null,$modx->getOption(\'assets_url\').\'components/colorpicker/\'); \n        $modx->regClientStartupHTMLBlock(\'<script type=\"text/javascript\">\n        Ext.onReady(function() {\n            \n        });\n        </script>\');\n        $modx->regClientStartupScript($assetsUrl.\'sources/ColorPicker.js\');\n        $modx->regClientStartupScript($assetsUrl.\'sources/ColorMenu.js\');\n        $modx->regClientStartupScript($assetsUrl.\'sources/ColorPickerField.js\');		\n        $modx->regClientCSS($assetsUrl.\'resources/css/colorpicker.css\');\n        break;\n     */\n}\nreturn;',0,'a:0:{}',0,'',0,''),
	(6,0,0,'MIGXquip','',0,5,0,'$quipCorePath = $modx->getOption(\'quip.core_path\', null, $modx->getOption(\'core_path\') . \'components/quip/\');\n//$assetsUrl = $modx->getOption(\'migx.assets_url\', null, $modx->getOption(\'assets_url\') . \'components/migx/\');\nswitch ($modx->event->name)\n{\n\n    case \'OnDocFormPrerender\':\n\n        \n        require_once $quipCorePath . \'model/quip/quip.class.php\';\n        $modx->quip = new Quip($modx);\n\n        $modx->lexicon->load(\'quip:default\');\n        $quipconfig = $modx->toJson($modx->quip->config);\n        \n        $js = \"\n        Quip.config = Ext.util.JSON.decode(\'{$quipconfig}\');\n        console.log(Quip);\";\n\n        //$modx->controller->addCss($assetsUrl . \'css/mgr.css\');\n        $modx->controller->addJavascript($modx->quip->config[\'jsUrl\'].\'quip.js\');\n        $modx->controller->addHtml(\'<script type=\"text/javascript\">\' . $js . \'</script>\');\n        break;\n\n}\nreturn;',0,'a:0:{}',1,'',0,'');

/*!40000 ALTER TABLE `modx_site_plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_snippets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_snippets`;

CREATE TABLE `modx_site_snippets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `snippet` mediumtext,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `properties` text,
  `moduleguid` varchar(32) NOT NULL DEFAULT '',
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `moduleguid` (`moduleguid`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_snippets` WRITE;
/*!40000 ALTER TABLE `modx_site_snippets` DISABLE KEYS */;

INSERT INTO `modx_site_snippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`, `static`, `static_file`)
VALUES
	(1,0,0,'Breadcrumbs','',0,1,0,'/**\n * BreadCrumbs\n *\n * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>\n *\n * BreadCrumbs is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package breadcrumbs\n */\n/**\n * @name BreadCrumbs\n * @version 0.9f\n * @created 2006-06-12\n * @since 2009-05-11\n * @author Jared <jaredc@honeydewdesign.com>\n * @editor Bill Wilson\n * @editor wendy@djamoer.net\n * @editor grad\n * @editor Shaun McCormick <shaun@collabpad.com>\n * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>\n * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>\n * @tester Bob Ray\n * @package breadcrumbs\n *\n * This snippet was designed to show the path through the various levels of site\n * structure back to the root. It is NOT necessarily the path the user took to\n * arrive at a given page.\n *\n * @see breadcrumbs.class.php for config settings\n *\n * Included classes\n * .B_crumbBox Span that surrounds all crumb output\n * .B_hideCrumb Span surrounding the \"...\" if there are more crumbs than will be shown\n * .B_currentCrumb Span or A tag surrounding the current crumb\n * .B_firstCrumb Span that always surrounds the first crumb, whether it is \"home\" or not\n * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .\n * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or\n * hide)\n * .B_homeCrumb Class given to the home crumb\n */\nrequire_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';\n$bc = new BreadCrumbs($modx,$scriptProperties);\nreturn $bc->run();',0,'','',0,''),
	(2,0,0,'getResources','<strong>1.6.1-pl</strong> A general purpose Resource listing and summarization snippet for MODX Revolution',0,0,0,'/**\n * getResources\n *\n * A general purpose Resource listing and summarization snippet for MODX 2.x.\n *\n * @author Jason Coward\n * @copyright Copyright 2010-2013, Jason Coward\n *\n * TEMPLATES\n *\n * tpl - Name of a chunk serving as a resource template\n * [NOTE: if not provided, properties are dumped to output for each resource]\n *\n * tplOdd - (Opt) Name of a chunk serving as resource template for resources with an odd idx value\n * (see idx property)\n * tplFirst - (Opt) Name of a chunk serving as resource template for the first resource (see first\n * property)\n * tplLast - (Opt) Name of a chunk serving as resource template for the last resource (see last\n * property)\n * tpl_{n} - (Opt) Name of a chunk serving as resource template for the nth resource\n *\n * tplCondition - (Opt) Defines a field of the resource to evaluate against keys defined in the\n * conditionalTpls property. Must be a resource field; does not work with Template Variables.\n * conditionalTpls - (Opt) A JSON object defining a map of field values and the associated tpl to\n * use when the field defined by tplCondition matches the value. [NOTE: tplOdd, tplFirst, tplLast,\n * and tpl_{n} will take precedence over any defined conditionalTpls]\n *\n * tplWrapper - (Opt) Name of a chunk serving as a wrapper template for the output\n * [NOTE: Does not work with toSeparatePlaceholders]\n *\n * SELECTION\n *\n * parents - Comma-delimited list of ids serving as parents\n *\n * context - (Opt) Comma-delimited list of context keys to limit results by; if empty, contexts for all specified\n * parents will be used (all contexts if 0 is specified) [default=]\n *\n * depth - (Opt) Integer value indicating depth to search for resources from each parent [default=10]\n *\n * tvFilters - (Opt) Delimited-list of TemplateVar values to filter resources by. Supports two\n * delimiters and two value search formats. The first delimiter || represents a logical OR and the\n * primary grouping mechanism.  Within each group you can provide a comma-delimited list of values.\n * These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the\n * value, indicating you are searching for the value in any TemplateVar tied to the Resource. An\n * example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`\n * [NOTE: filtering by values uses a LIKE query and % is considered a wildcard.]\n * [NOTE: this only looks at the raw value set for specific Resource, i. e. there must be a value\n * specifically set for the Resource and it is not evaluated.]\n *\n * tvFiltersAndDelimiter - (Opt) Custom delimiter for logical AND, default \',\', in case you want to\n * match a literal comma in the tvFilters. E.g. &tvFiltersAndDelimiter=`&&`\n * &tvFilters=`filter1==foo,bar&&filter2==baz` [default=,]\n *\n * tvFiltersOrDelimiter - (Opt) Custom delimiter for logical OR, default \'||\', in case you want to\n * match a literal \'||\' in the tvFilters. E.g. &tvFiltersOrDelimiter=`|OR|`\n * &tvFilters=`filter1==foo||bar|OR|filter2==baz` [default=||]\n *\n * where - (Opt) A JSON expression of criteria to build any additional where clauses from. An example would be\n * &where=`{{\"alias:LIKE\":\"foo%\", \"OR:alias:LIKE\":\"%bar\"},{\"OR:pagetitle:=\":\"foobar\", \"AND:description:=\":\"raboof\"}}`\n *\n * sortby - (Opt) Field to sort by or a JSON array, e.g. {\"publishedon\":\"ASC\",\"createdon\":\"DESC\"} [default=publishedon]\n * sortbyTV - (opt) A Template Variable name to sort by (if supplied, this precedes the sortby value) [default=]\n * sortbyTVType - (Opt) A data type to CAST a TV Value to in order to sort on it properly [default=string]\n * sortbyAlias - (Opt) Query alias for sortby field [default=]\n * sortbyEscaped - (Opt) Escapes the field name(s) specified in sortby [default=0]\n * sortdir - (Opt) Order which to sort by [default=DESC]\n * sortdirTV - (Opt) Order which to sort by a TV [default=DESC]\n * limit - (Opt) Limits the number of resources returned [default=5]\n * offset - (Opt) An offset of resources returned by the criteria to skip [default=0]\n * dbCacheFlag - (Opt) Controls caching of db queries; 0|false = do not cache result set; 1 = cache result set\n * according to cache settings, any other integer value = number of seconds to cache result set [default=0]\n *\n * OPTIONS\n *\n * includeContent - (Opt) Indicates if the content of each resource should be returned in the\n * results [default=0]\n * includeTVs - (Opt) Indicates if TemplateVar values should be included in the properties available\n * to each resource template [default=0]\n * includeTVList - (Opt) Limits the TemplateVars that are included if includeTVs is true to those specified\n * by name in a comma-delimited list [default=]\n * prepareTVs - (Opt) Prepares media-source dependent TemplateVar values [default=1]\n * prepareTVList - (Opt) Limits the TVs that are prepared to those specified by name in a comma-delimited\n * list [default=]\n * processTVs - (Opt) Indicates if TemplateVar values should be rendered as they would on the\n * resource being summarized [default=0]\n * processTVList - (opt) Limits the TemplateVars that are processed if included to those specified\n * by name in a comma-delimited list [default=]\n * tvPrefix - (Opt) The prefix for TemplateVar properties [default=tv.]\n * idx - (Opt) You can define the starting idx of the resources, which is an property that is\n * incremented as each resource is rendered [default=1]\n * first - (Opt) Define the idx which represents the first resource (see tplFirst) [default=1]\n * last - (Opt) Define the idx which represents the last resource (see tplLast) [default=# of\n * resources being summarized + first - 1]\n * outputSeparator - (Opt) An optional string to separate each tpl instance [default=\"\\n\"]\n * wrapIfEmpty - (Opt) Indicates if the tplWrapper should be applied if the output is empty [default=0]\n *\n */\n$output = array();\n$outputSeparator = isset($outputSeparator) ? $outputSeparator : \"\\n\";\n\n/* set default properties */\n$tpl = !empty($tpl) ? $tpl : \'\';\n$includeContent = !empty($includeContent) ? true : false;\n$includeTVs = !empty($includeTVs) ? true : false;\n$includeTVList = !empty($includeTVList) ? explode(\',\', $includeTVList) : array();\n$processTVs = !empty($processTVs) ? true : false;\n$processTVList = !empty($processTVList) ? explode(\',\', $processTVList) : array();\n$prepareTVs = !empty($prepareTVs) ? true : false;\n$prepareTVList = !empty($prepareTVList) ? explode(\',\', $prepareTVList) : array();\n$tvPrefix = isset($tvPrefix) ? $tvPrefix : \'tv.\';\n$parents = (!empty($parents) || $parents === \'0\') ? explode(\',\', $parents) : array($modx->resource->get(\'id\'));\narray_walk($parents, \'trim\');\n$parents = array_unique($parents);\n$depth = isset($depth) ? (integer) $depth : 10;\n\n$tvFiltersOrDelimiter = isset($tvFiltersOrDelimiter) ? $tvFiltersOrDelimiter : \'||\';\n$tvFiltersAndDelimiter = isset($tvFiltersAndDelimiter) ? $tvFiltersAndDelimiter : \',\';\n$tvFilters = !empty($tvFilters) ? explode($tvFiltersOrDelimiter, $tvFilters) : array();\n\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$showUnpublished = !empty($showUnpublished) ? true : false;\n$showDeleted = !empty($showDeleted) ? true : false;\n\n$sortby = isset($sortby) ? $sortby : \'publishedon\';\n$sortbyTV = isset($sortbyTV) ? $sortbyTV : \'\';\n$sortbyAlias = isset($sortbyAlias) ? $sortbyAlias : \'modResource\';\n$sortbyEscaped = !empty($sortbyEscaped) ? true : false;\n$sortdir = isset($sortdir) ? $sortdir : \'DESC\';\n$sortdirTV = isset($sortdirTV) ? $sortdirTV : \'DESC\';\n$limit = isset($limit) ? (integer) $limit : 5;\n$offset = isset($offset) ? (integer) $offset : 0;\n$totalVar = !empty($totalVar) ? $totalVar : \'total\';\n\n$dbCacheFlag = !isset($dbCacheFlag) ? false : $dbCacheFlag;\nif (is_string($dbCacheFlag) || is_numeric($dbCacheFlag)) {\n    if ($dbCacheFlag == \'0\') {\n        $dbCacheFlag = false;\n    } elseif ($dbCacheFlag == \'1\') {\n        $dbCacheFlag = true;\n    } else {\n        $dbCacheFlag = (integer) $dbCacheFlag;\n    }\n}\n\n/* multiple context support */\n$contextArray = array();\n$contextSpecified = false;\nif (!empty($context)) {\n    $contextArray = explode(\',\',$context);\n    array_walk($contextArray, \'trim\');\n    $contexts = array();\n    foreach ($contextArray as $ctx) {\n        $contexts[] = $modx->quote($ctx);\n    }\n    $context = implode(\',\',$contexts);\n    $contextSpecified = true;\n    unset($contexts,$ctx);\n} else {\n    $context = $modx->quote($modx->context->get(\'key\'));\n}\n\n$pcMap = array();\n$pcQuery = $modx->newQuery(\'modResource\', array(\'id:IN\' => $parents), $dbCacheFlag);\n$pcQuery->select(array(\'id\', \'context_key\'));\nif ($pcQuery->prepare() && $pcQuery->stmt->execute()) {\n    foreach ($pcQuery->stmt->fetchAll(PDO::FETCH_ASSOC) as $pcRow) {\n        $pcMap[(integer) $pcRow[\'id\']] = $pcRow[\'context_key\'];\n    }\n}\n\n$children = array();\n$parentArray = array();\nforeach ($parents as $parent) {\n    $parent = (integer) $parent;\n    if ($parent === 0) {\n        $pchildren = array();\n        if ($contextSpecified) {\n            foreach ($contextArray as $pCtx) {\n                if (!in_array($pCtx, $contextArray)) {\n                    continue;\n                }\n                $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();\n                $pcchildren = $modx->getChildIds($parent, $depth, $options);\n                if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);\n            }\n        } else {\n            $cQuery = $modx->newQuery(\'modContext\', array(\'key:!=\' => \'mgr\'));\n            $cQuery->select(array(\'key\'));\n            if ($cQuery->prepare() && $cQuery->stmt->execute()) {\n                foreach ($cQuery->stmt->fetchAll(PDO::FETCH_COLUMN) as $pCtx) {\n                    $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();\n                    $pcchildren = $modx->getChildIds($parent, $depth, $options);\n                    if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);\n                }\n            }\n        }\n        $parentArray[] = $parent;\n    } else {\n        $pContext = array_key_exists($parent, $pcMap) ? $pcMap[$parent] : false;\n        if ($debug) $modx->log(modX::LOG_LEVEL_ERROR, \"context for {$parent} is {$pContext}\");\n        if ($pContext && $contextSpecified && !in_array($pContext, $contextArray, true)) {\n            $parent = next($parents);\n            continue;\n        }\n        $parentArray[] = $parent;\n        $options = !empty($pContext) && $pContext !== $modx->context->get(\'key\') ? array(\'context\' => $pContext) : array();\n        $pchildren = $modx->getChildIds($parent, $depth, $options);\n    }\n    if (!empty($pchildren)) $children = array_merge($children, $pchildren);\n    $parent = next($parents);\n}\n$parents = array_merge($parentArray, $children);\n\n/* build query */\n$criteria = array(\"modResource.parent IN (\" . implode(\',\', $parents) . \")\");\nif ($contextSpecified) {\n    $contextResourceTbl = $modx->getTableName(\'modContextResource\');\n    $criteria[] = \"(modResource.context_key IN ({$context}) OR EXISTS(SELECT 1 FROM {$contextResourceTbl} ctx WHERE ctx.resource = modResource.id AND ctx.context_key IN ({$context})))\";\n}\nif (empty($showDeleted)) {\n    $criteria[\'deleted\'] = \'0\';\n}\nif (empty($showUnpublished)) {\n    $criteria[\'published\'] = \'1\';\n}\nif (empty($showHidden)) {\n    $criteria[\'hidemenu\'] = \'0\';\n}\nif (!empty($hideContainers)) {\n    $criteria[\'isfolder\'] = \'0\';\n}\n$criteria = $modx->newQuery(\'modResource\', $criteria);\nif (!empty($tvFilters)) {\n    $tmplVarTbl = $modx->getTableName(\'modTemplateVar\');\n    $tmplVarResourceTbl = $modx->getTableName(\'modTemplateVarResource\');\n    $conditions = array();\n    $operators = array(\n        \'<=>\' => \'<=>\',\n        \'===\' => \'=\',\n        \'!==\' => \'!=\',\n        \'<>\' => \'<>\',\n        \'==\' => \'LIKE\',\n        \'!=\' => \'NOT LIKE\',\n        \'<<\' => \'<\',\n        \'<=\' => \'<=\',\n        \'=<\' => \'=<\',\n        \'>>\' => \'>\',\n        \'>=\' => \'>=\',\n        \'=>\' => \'=>\'\n    );\n    foreach ($tvFilters as $fGroup => $tvFilter) {\n        $filterGroup = array();\n        $filters = explode($tvFiltersAndDelimiter, $tvFilter);\n        $multiple = count($filters) > 0;\n        foreach ($filters as $filter) {\n            $operator = \'==\';\n            $sqlOperator = \'LIKE\';\n            foreach ($operators as $op => $opSymbol) {\n                if (strpos($filter, $op, 1) !== false) {\n                    $operator = $op;\n                    $sqlOperator = $opSymbol;\n                    break;\n                }\n            }\n            $tvValueField = \'tvr.value\';\n            $tvDefaultField = \'tv.default_text\';\n            $f = explode($operator, $filter);\n            if (count($f) >= 2) {\n                if (count($f) > 2) {\n                    $k = array_shift($f);\n                    $b = join($operator, $f);\n                    $f = array($k, $b);\n                }\n                $tvName = $modx->quote($f[0]);\n                if (is_numeric($f[1]) && !in_array($sqlOperator, array(\'LIKE\', \'NOT LIKE\'))) {\n                    $tvValue = $f[1];\n                    if ($f[1] == (integer)$f[1]) {\n                        $tvValueField = \"CAST({$tvValueField} AS SIGNED INTEGER)\";\n                        $tvDefaultField = \"CAST({$tvDefaultField} AS SIGNED INTEGER)\";\n                    } else {\n                        $tvValueField = \"CAST({$tvValueField} AS DECIMAL)\";\n                        $tvDefaultField = \"CAST({$tvDefaultField} AS DECIMAL)\";\n                    }\n                } else {\n                    $tvValue = $modx->quote($f[1]);\n                }\n                if ($multiple) {\n                    $filterGroup[] =\n                        \"(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) \" .\n                        \"OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) \" .\n                        \")\";\n                } else {\n                    $filterGroup =\n                        \"(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) \" .\n                        \"OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) \" .\n                        \")\";\n                }\n            } elseif (count($f) == 1) {\n                $tvValue = $modx->quote($f[0]);\n                if ($multiple) {\n                    $filterGroup[] = \"EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)\";\n                } else {\n                    $filterGroup = \"EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)\";\n                }\n            }\n        }\n        $conditions[] = $filterGroup;\n    }\n    if (!empty($conditions)) {\n        $firstGroup = true;\n        foreach ($conditions as $cGroup => $c) {\n            if (is_array($c)) {\n                $first = true;\n                foreach ($c as $cond) {\n                    if ($first && !$firstGroup) {\n                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_OR, null, $cGroup);\n                    } else {\n                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_AND, null, $cGroup);\n                    }\n                    $first = false;\n                }\n            } else {\n                $criteria->condition($criteria->query[\'where\'][0][1], $c, $firstGroup ? xPDOQuery::SQL_AND : xPDOQuery::SQL_OR, null, $cGroup);\n            }\n            $firstGroup = false;\n        }\n    }\n}\n/* include/exclude resources, via &resources=`123,-456` prop */\nif (!empty($resources)) {\n    $resourceConditions = array();\n    $resources = explode(\',\',$resources);\n    $include = array();\n    $exclude = array();\n    foreach ($resources as $resource) {\n        $resource = (int)$resource;\n        if ($resource == 0) continue;\n        if ($resource < 0) {\n            $exclude[] = abs($resource);\n        } else {\n            $include[] = $resource;\n        }\n    }\n    if (!empty($include)) {\n        $criteria->where(array(\'OR:modResource.id:IN\' => $include), xPDOQuery::SQL_OR);\n    }\n    if (!empty($exclude)) {\n        $criteria->where(array(\'modResource.id:NOT IN\' => $exclude), xPDOQuery::SQL_AND, null, 1);\n    }\n}\nif (!empty($where)) {\n    $criteria->where($where);\n}\n\n$total = $modx->getCount(\'modResource\', $criteria);\n$modx->setPlaceholder($totalVar, $total);\n\n$fields = array_keys($modx->getFields(\'modResource\'));\nif (empty($includeContent)) {\n    $fields = array_diff($fields, array(\'content\'));\n}\n$columns = $includeContent ? $modx->getSelectColumns(\'modResource\', \'modResource\') : $modx->getSelectColumns(\'modResource\', \'modResource\', \'\', array(\'content\'), true);\n$criteria->select($columns);\nif (!empty($sortbyTV)) {\n    $criteria->leftJoin(\'modTemplateVar\', \'tvDefault\', array(\n        \"tvDefault.name\" => $sortbyTV\n    ));\n    $criteria->leftJoin(\'modTemplateVarResource\', \'tvSort\', array(\n        \"tvSort.contentid = modResource.id\",\n        \"tvSort.tmplvarid = tvDefault.id\"\n    ));\n    if (empty($sortbyTVType)) $sortbyTVType = \'string\';\n    if ($modx->getOption(\'dbtype\') === \'mysql\') {\n        switch ($sortbyTVType) {\n            case \'integer\':\n                $criteria->select(\"CAST(IFNULL(tvSort.value, tvDefault.default_text) AS SIGNED INTEGER) AS sortTV\");\n                break;\n            case \'decimal\':\n                $criteria->select(\"CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV\");\n                break;\n            case \'datetime\':\n                $criteria->select(\"CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV\");\n                break;\n            case \'string\':\n            default:\n                $criteria->select(\"IFNULL(tvSort.value, tvDefault.default_text) AS sortTV\");\n                break;\n        }\n    } elseif ($modx->getOption(\'dbtype\') === \'sqlsrv\') {\n        switch ($sortbyTVType) {\n            case \'integer\':\n                $criteria->select(\"CAST(ISNULL(tvSort.value, tvDefault.default_text) AS BIGINT) AS sortTV\");\n                break;\n            case \'decimal\':\n                $criteria->select(\"CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV\");\n                break;\n            case \'datetime\':\n                $criteria->select(\"CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV\");\n                break;\n            case \'string\':\n            default:\n                $criteria->select(\"ISNULL(tvSort.value, tvDefault.default_text) AS sortTV\");\n                break;\n        }\n    }\n    $criteria->sortby(\"sortTV\", $sortdirTV);\n}\nif (!empty($sortby)) {\n    if (strpos($sortby, \'{\') === 0) {\n        $sorts = $modx->fromJSON($sortby);\n    } else {\n        $sorts = array($sortby => $sortdir);\n    }\n    if (is_array($sorts)) {\n        while (list($sort, $dir) = each($sorts)) {\n            if ($sortbyEscaped) $sort = $modx->escape($sort);\n            if (!empty($sortbyAlias)) $sort = $modx->escape($sortbyAlias) . \".{$sort}\";\n            $criteria->sortby($sort, $dir);\n        }\n    }\n}\nif (!empty($limit)) $criteria->limit($limit, $offset);\n\nif (!empty($debug)) {\n    $criteria->prepare();\n    $modx->log(modX::LOG_LEVEL_ERROR, $criteria->toSQL());\n}\n$collection = $modx->getCollection(\'modResource\', $criteria, $dbCacheFlag);\n\n$idx = !empty($idx) || $idx === \'0\' ? (integer) $idx : 1;\n$first = empty($first) && $first !== \'0\' ? 1 : (integer) $first;\n$last = empty($last) ? (count($collection) + $idx - 1) : (integer) $last;\n\n/* include parseTpl */\ninclude_once $modx->getOption(\'getresources.core_path\',null,$modx->getOption(\'core_path\').\'components/getresources/\').\'include.parsetpl.php\';\n\n$templateVars = array();\nif (!empty($includeTVs) && !empty($includeTVList)) {\n    $templateVars = $modx->getCollection(\'modTemplateVar\', array(\'name:IN\' => $includeTVList));\n}\n/** @var modResource $resource */\nforeach ($collection as $resourceId => $resource) {\n    $tvs = array();\n    if (!empty($includeTVs)) {\n        if (empty($includeTVList)) {\n            $templateVars = $resource->getMany(\'TemplateVars\');\n        }\n        /** @var modTemplateVar $templateVar */\n        foreach ($templateVars as $tvId => $templateVar) {\n            if (!empty($includeTVList) && !in_array($templateVar->get(\'name\'), $includeTVList)) continue;\n            if ($processTVs && (empty($processTVList) || in_array($templateVar->get(\'name\'), $processTVList))) {\n                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $templateVar->renderOutput($resource->get(\'id\'));\n            } else {\n                $value = $templateVar->getValue($resource->get(\'id\'));\n                if ($prepareTVs && method_exists($templateVar, \'prepareOutput\') && (empty($prepareTVList) || in_array($templateVar->get(\'name\'), $prepareTVList))) {\n                    $value = $templateVar->prepareOutput($value);\n                }\n                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $value;\n            }\n        }\n    }\n    $odd = ($idx & 1);\n    $properties = array_merge(\n        $scriptProperties\n        ,array(\n            \'idx\' => $idx\n            ,\'first\' => $first\n            ,\'last\' => $last\n            ,\'odd\' => $odd\n        )\n        ,$includeContent ? $resource->toArray() : $resource->get($fields)\n        ,$tvs\n    );\n    $resourceTpl = false;\n    if ($idx == $first && !empty($tplFirst)) {\n        $resourceTpl = parseTpl($tplFirst, $properties);\n    }\n    if ($idx == $last && empty($resourceTpl) && !empty($tplLast)) {\n        $resourceTpl = parseTpl($tplLast, $properties);\n    }\n    $tplidx = \'tpl_\' . $idx;\n    if (empty($resourceTpl) && !empty($$tplidx)) {\n        $resourceTpl = parseTpl($$tplidx, $properties);\n    }\n    if ($idx > 1 && empty($resourceTpl)) {\n        $divisors = getDivisors($idx);\n        if (!empty($divisors)) {\n            foreach ($divisors as $divisor) {\n                $tplnth = \'tpl_n\' . $divisor;\n                if (!empty($$tplnth)) {\n                    $resourceTpl = parseTpl($$tplnth, $properties);\n                    if (!empty($resourceTpl)) {\n                        break;\n                    }\n                }\n            }\n        }\n    }\n    if ($odd && empty($resourceTpl) && !empty($tplOdd)) {\n        $resourceTpl = parseTpl($tplOdd, $properties);\n    }\n    if (!empty($tplCondition) && !empty($conditionalTpls) && empty($resourceTpl)) {\n        $conTpls = $modx->fromJSON($conditionalTpls);\n        $subject = $properties[$tplCondition];\n        $tplOperator = !empty($tplOperator) ? $tplOperator : \'=\';\n        $tplOperator = strtolower($tplOperator);\n        $tplCon = \'\';\n        foreach ($conTpls as $operand => $conditionalTpl) {\n            switch ($tplOperator) {\n                case \'!=\':\n                case \'neq\':\n                case \'not\':\n                case \'isnot\':\n                case \'isnt\':\n                case \'unequal\':\n                case \'notequal\':\n                    $tplCon = (($subject != $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'<\':\n                case \'lt\':\n                case \'less\':\n                case \'lessthan\':\n                    $tplCon = (($subject < $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'>\':\n                case \'gt\':\n                case \'greater\':\n                case \'greaterthan\':\n                    $tplCon = (($subject > $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'<=\':\n                case \'lte\':\n                case \'lessthanequals\':\n                case \'lessthanorequalto\':\n                    $tplCon = (($subject <= $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'>=\':\n                case \'gte\':\n                case \'greaterthanequals\':\n                case \'greaterthanequalto\':\n                    $tplCon = (($subject >= $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'isempty\':\n                case \'empty\':\n                    $tplCon = empty($subject) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'!empty\':\n                case \'notempty\':\n                case \'isnotempty\':\n                    $tplCon = !empty($subject) && $subject != \'\' ? $conditionalTpl : $tplCon;\n                    break;\n                case \'isnull\':\n                case \'null\':\n                    $tplCon = $subject == null || strtolower($subject) == \'null\' ? $conditionalTpl : $tplCon;\n                    break;\n                case \'inarray\':\n                case \'in_array\':\n                case \'ia\':\n                    $operand = explode(\',\', $operand);\n                    $tplCon = in_array($subject, $operand) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'between\':\n                case \'range\':\n                case \'>=<\':\n                case \'><\':\n                    $operand = explode(\',\', $operand);\n                    $tplCon = ($subject >= min($operand) && $subject <= max($operand)) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'==\':\n                case \'=\':\n                case \'eq\':\n                case \'is\':\n                case \'equal\':\n                case \'equals\':\n                case \'equalto\':\n                default:\n                    $tplCon = (($subject == $operand) ? $conditionalTpl : $tplCon);\n                    break;\n            }\n        }\n        if (!empty($tplCon)) {\n            $resourceTpl = parseTpl($tplCon, $properties);\n        }\n    }\n    if (!empty($tpl) && empty($resourceTpl)) {\n        $resourceTpl = parseTpl($tpl, $properties);\n    }\n    if ($resourceTpl === false && !empty($debug)) {\n        $chunk = $modx->newObject(\'modChunk\');\n        $chunk->setCacheable(false);\n        $output[]= $chunk->process(array(), \'<pre>\' . print_r($properties, true) .\'</pre>\');\n    } else {\n        $output[]= $resourceTpl;\n    }\n    $idx++;\n}\n\n/* output */\n$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);\nif (!empty($toSeparatePlaceholders)) {\n    $modx->setPlaceholders($output, $toSeparatePlaceholders);\n    return \'\';\n}\n\n$output = implode($outputSeparator, $output);\n\n$tplWrapper = $modx->getOption(\'tplWrapper\', $scriptProperties, false);\n$wrapIfEmpty = $modx->getOption(\'wrapIfEmpty\', $scriptProperties, false);\nif (!empty($tplWrapper) && ($wrapIfEmpty || !empty($output))) {\n    $output = parseTpl($tplWrapper, array_merge($scriptProperties, array(\'output\' => $output)));\n}\n\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $output);\n    return \'\';\n}\nreturn $output;',0,'a:44:{s:3:\"tpl\";a:7:{s:4:\"name\";s:3:\"tpl\";s:4:\"desc\";s:121:\"Name of a chunk serving as a resource template. NOTE: if not provided, properties are dumped to output for each resource.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"tplOdd\";a:7:{s:4:\"name\";s:6:\"tplOdd\";s:4:\"desc\";s:100:\"Name of a chunk serving as resource template for resources with an odd idx value (see idx property).\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:8:\"tplFirst\";a:7:{s:4:\"name\";s:8:\"tplFirst\";s:4:\"desc\";s:89:\"Name of a chunk serving as resource template for the first resource (see first property).\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"tplLast\";a:7:{s:4:\"name\";s:7:\"tplLast\";s:4:\"desc\";s:87:\"Name of a chunk serving as resource template for the last resource (see last property).\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"tplWrapper\";a:7:{s:4:\"name\";s:10:\"tplWrapper\";s:4:\"desc\";s:115:\"Name of a chunk serving as wrapper template for the Snippet output. This does not work with toSeparatePlaceholders.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"wrapIfEmpty\";a:7:{s:4:\"name\";s:11:\"wrapIfEmpty\";s:4:\"desc\";s:95:\"Indicates if empty output should be wrapped by the tplWrapper, if specified. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"sortby\";a:7:{s:4:\"name\";s:6:\"sortby\";s:4:\"desc\";s:153:\"A field name to sort by or JSON object of field names and sortdir for each field, e.g. {\"publishedon\":\"ASC\",\"createdon\":\"DESC\"}. Defaults to publishedon.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:11:\"publishedon\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:8:\"sortbyTV\";a:7:{s:4:\"name\";s:8:\"sortbyTV\";s:4:\"desc\";s:65:\"Name of a Template Variable to sort by. Defaults to empty string.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"sortbyTVType\";a:7:{s:4:\"name\";s:12:\"sortbyTVType\";s:4:\"desc\";s:72:\"An optional type to indicate how to sort on the Template Variable value.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:4:{i:0;a:2:{s:4:\"text\";s:6:\"string\";s:5:\"value\";s:6:\"string\";}i:1;a:2:{s:4:\"text\";s:7:\"integer\";s:5:\"value\";s:7:\"integer\";}i:2;a:2:{s:4:\"text\";s:7:\"decimal\";s:5:\"value\";s:7:\"decimal\";}i:3;a:2:{s:4:\"text\";s:8:\"datetime\";s:5:\"value\";s:8:\"datetime\";}}s:5:\"value\";s:6:\"string\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"sortbyAlias\";a:7:{s:4:\"name\";s:11:\"sortbyAlias\";s:4:\"desc\";s:58:\"Query alias for sortby field. Defaults to an empty string.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"sortbyEscaped\";a:7:{s:4:\"name\";s:13:\"sortbyEscaped\";s:4:\"desc\";s:82:\"Determines if the field name specified in sortby should be escaped. Defaults to 0.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"sortdir\";a:7:{s:4:\"name\";s:7:\"sortdir\";s:4:\"desc\";s:41:\"Order which to sort by. Defaults to DESC.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:4:\"text\";s:3:\"ASC\";s:5:\"value\";s:3:\"ASC\";}i:1;a:2:{s:4:\"text\";s:4:\"DESC\";s:5:\"value\";s:4:\"DESC\";}}s:5:\"value\";s:4:\"DESC\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:9:\"sortdirTV\";a:7:{s:4:\"name\";s:9:\"sortdirTV\";s:4:\"desc\";s:61:\"Order which to sort a Template Variable by. Defaults to DESC.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:4:\"text\";s:3:\"ASC\";s:5:\"value\";s:3:\"ASC\";}i:1;a:2:{s:4:\"text\";s:4:\"DESC\";s:5:\"value\";s:4:\"DESC\";}}s:5:\"value\";s:4:\"DESC\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"limit\";a:7:{s:4:\"name\";s:5:\"limit\";s:4:\"desc\";s:55:\"Limits the number of resources returned. Defaults to 5.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"5\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"offset\";a:7:{s:4:\"name\";s:6:\"offset\";s:4:\"desc\";s:56:\"An offset of resources returned by the criteria to skip.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:9:\"tvFilters\";a:7:{s:4:\"name\";s:9:\"tvFilters\";s:4:\"desc\";s:778:\"Delimited-list of TemplateVar values to filter resources by. Supports two delimiters and two value search formats. THe first delimiter || represents a logical OR and the primary grouping mechanism.  Within each group you can provide a comma-delimited list of values. These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the value, indicating you are searching for the value in any TemplateVar tied to the Resource. An example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`. <br />NOTE: filtering by values uses a LIKE query and % is considered a wildcard. <br />ANOTHER NOTE: This only looks at the raw value set for specific Resource, i. e. there must be a value specifically set for the Resource and it is not evaluated.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:21:\"tvFiltersAndDelimiter\";a:7:{s:4:\"name\";s:21:\"tvFiltersAndDelimiter\";s:4:\"desc\";s:83:\"The delimiter to use to separate logical AND expressions in tvFilters. Default is ,\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\",\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:20:\"tvFiltersOrDelimiter\";a:7:{s:4:\"name\";s:20:\"tvFiltersOrDelimiter\";s:4:\"desc\";s:83:\"The delimiter to use to separate logical OR expressions in tvFilters. Default is ||\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:2:\"||\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"depth\";a:7:{s:4:\"name\";s:5:\"depth\";s:4:\"desc\";s:88:\"Integer value indicating depth to search for resources from each parent. Defaults to 10.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:2:\"10\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"parents\";a:7:{s:4:\"name\";s:7:\"parents\";s:4:\"desc\";s:57:\"Optional. Comma-delimited list of ids serving as parents.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:14:\"includeContent\";a:7:{s:4:\"name\";s:14:\"includeContent\";s:4:\"desc\";s:95:\"Indicates if the content of each resource should be returned in the results. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"includeTVs\";a:7:{s:4:\"name\";s:10:\"includeTVs\";s:4:\"desc\";s:124:\"Indicates if TemplateVar values should be included in the properties available to each resource template. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"includeTVList\";a:7:{s:4:\"name\";s:13:\"includeTVList\";s:4:\"desc\";s:96:\"Limits included TVs to those specified as a comma-delimited list of TV names. Defaults to empty.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"showHidden\";a:7:{s:4:\"name\";s:10:\"showHidden\";s:4:\"desc\";s:85:\"Indicates if Resources that are hidden from menus should be shown. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"showUnpublished\";a:7:{s:4:\"name\";s:15:\"showUnpublished\";s:4:\"desc\";s:79:\"Indicates if Resources that are unpublished should be shown. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"showDeleted\";a:7:{s:4:\"name\";s:11:\"showDeleted\";s:4:\"desc\";s:75:\"Indicates if Resources that are deleted should be shown. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:9:\"resources\";a:7:{s:4:\"name\";s:9:\"resources\";s:4:\"desc\";s:177:\"A comma-separated list of resource IDs to exclude or include. IDs with a - in front mean to exclude. Ex: 123,-456 means to include Resource 123, but always exclude Resource 456.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"processTVs\";a:7:{s:4:\"name\";s:10:\"processTVs\";s:4:\"desc\";s:117:\"Indicates if TemplateVar values should be rendered as they would on the resource being summarized. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"processTVList\";a:7:{s:4:\"name\";s:13:\"processTVList\";s:4:\"desc\";s:166:\"Limits processed TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for processing if specified. Defaults to empty.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"prepareTVs\";a:7:{s:4:\"name\";s:10:\"prepareTVs\";s:4:\"desc\";s:120:\"Indicates if TemplateVar values that are not processed fully should be prepared before being returned. Defaults to true.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"prepareTVList\";a:7:{s:4:\"name\";s:13:\"prepareTVList\";s:4:\"desc\";s:164:\"Limits prepared TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for preparing if specified. Defaults to empty.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:8:\"tvPrefix\";a:7:{s:4:\"name\";s:8:\"tvPrefix\";s:4:\"desc\";s:55:\"The prefix for TemplateVar properties. Defaults to: tv.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"tv.\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:3:\"idx\";a:7:{s:4:\"name\";s:3:\"idx\";s:4:\"desc\";s:120:\"You can define the starting idx of the resources, which is an property that is incremented as each resource is rendered.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"first\";a:7:{s:4:\"name\";s:5:\"first\";s:4:\"desc\";s:81:\"Define the idx which represents the first resource (see tplFirst). Defaults to 1.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:4:\"last\";a:7:{s:4:\"name\";s:4:\"last\";s:4:\"desc\";s:129:\"Define the idx which represents the last resource (see tplLast). Defaults to the number of resources being summarized + first - 1\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"toPlaceholder\";a:7:{s:4:\"name\";s:13:\"toPlaceholder\";s:4:\"desc\";s:85:\"If set, will assign the result to this placeholder instead of outputting it directly.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:22:\"toSeparatePlaceholders\";a:7:{s:4:\"name\";s:22:\"toSeparatePlaceholders\";s:4:\"desc\";s:130:\"If set, will assign EACH result to a separate placeholder named by this param suffixed with a sequential number (starting from 0).\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"debug\";a:7:{s:4:\"name\";s:5:\"debug\";s:4:\"desc\";s:68:\"If true, will send the SQL query to the MODX log. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"where\";a:7:{s:4:\"name\";s:5:\"where\";s:4:\"desc\";s:193:\"A JSON expression of criteria to build any additional where clauses from, e.g. &where=`{{\"alias:LIKE\":\"foo%\", \"OR:alias:LIKE\":\"%bar\"},{\"OR:pagetitle:=\":\"foobar\", \"AND:description:=\":\"raboof\"}}`\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"dbCacheFlag\";a:7:{s:4:\"name\";s:11:\"dbCacheFlag\";s:4:\"desc\";s:218:\"Determines how result sets are cached if cache_db is enabled in MODX. 0|false = do not cache result set; 1 = cache result set according to cache settings, any other integer value = number of seconds to cache result set\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"context\";a:7:{s:4:\"name\";s:7:\"context\";s:4:\"desc\";s:116:\"A comma-delimited list of context keys for limiting results. Default is empty, i.e. do not limit results by context.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"tplCondition\";a:7:{s:4:\"name\";s:12:\"tplCondition\";s:4:\"desc\";s:129:\"A condition to compare against the conditionalTpls property to map Resources to different tpls based on custom conditional logic.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"tplOperator\";a:7:{s:4:\"name\";s:11:\"tplOperator\";s:4:\"desc\";s:125:\"An optional operator to use for the tplCondition when comparing against the conditionalTpls operands. Default is == (equals).\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:10:{i:0;a:2:{s:4:\"text\";s:11:\"is equal to\";s:5:\"value\";s:2:\"==\";}i:1;a:2:{s:4:\"text\";s:15:\"is not equal to\";s:5:\"value\";s:2:\"!=\";}i:2;a:2:{s:4:\"text\";s:9:\"less than\";s:5:\"value\";s:1:\"<\";}i:3;a:2:{s:4:\"text\";s:21:\"less than or equal to\";s:5:\"value\";s:2:\"<=\";}i:4;a:2:{s:4:\"text\";s:24:\"greater than or equal to\";s:5:\"value\";s:2:\">=\";}i:5;a:2:{s:4:\"text\";s:8:\"is empty\";s:5:\"value\";s:5:\"empty\";}i:6;a:2:{s:4:\"text\";s:12:\"is not empty\";s:5:\"value\";s:6:\"!empty\";}i:7;a:2:{s:4:\"text\";s:7:\"is null\";s:5:\"value\";s:4:\"null\";}i:8;a:2:{s:4:\"text\";s:11:\"is in array\";s:5:\"value\";s:7:\"inarray\";}i:9;a:2:{s:4:\"text\";s:10:\"is between\";s:5:\"value\";s:7:\"between\";}}s:5:\"value\";s:2:\"==\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"conditionalTpls\";a:7:{s:4:\"name\";s:15:\"conditionalTpls\";s:4:\"desc\";s:121:\"A JSON map of conditional operands and tpls to compare against the tplCondition property using the specified tplOperator.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}}','',0,''),
	(3,0,0,'Wayfinder','Wayfinder for MODx Revolution 2.0.0-beta-5 and later.',0,0,0,'/**\n * Wayfinder Snippet to build site navigation menus\n *\n * Totally refactored from original DropMenu nav builder to make it easier to\n * create custom navigation by using chunks as output templates. By using\n * templates, many of the paramaters are no longer needed for flexible output\n * including tables, unordered- or ordered-lists (ULs or OLs), definition lists\n * (DLs) or in any other format you desire.\n *\n * @version 2.1.1-beta5\n * @author Garry Nutting (collabpad.com)\n * @author Kyle Jaebker (muddydogpaws.com)\n * @author Ryan Thrash (modx.com)\n * @author Shaun McCormick (modx.com)\n * @author Jason Coward (modx.com)\n *\n * @example [[Wayfinder? &startId=`0`]]\n *\n * @var modX $modx\n * @var array $scriptProperties\n * \n * @package wayfinder\n */\n$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');\n\n/* include a custom config file if specified */\nif (isset($scriptProperties[\'config\'])) {\n    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);\n    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';\n} else {\n    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';\n}\nif (file_exists($scriptProperties[\'config\'])) {\n    include $scriptProperties[\'config\'];\n}\n\n/* include wayfinder class */\ninclude_once $wayfinder_base.\'wayfinder.class.php\';\nif (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {\n    return \'error: Wayfinder class not found\';\n}\n$wf = new Wayfinder($modx,$scriptProperties);\n\n/* get user class definitions\n * TODO: eventually move these into config parameters */\n$wf->_css = array(\n    \'first\' => isset($firstClass) ? $firstClass : \'\',\n    \'last\' => isset($lastClass) ? $lastClass : \'last\',\n    \'here\' => isset($hereClass) ? $hereClass : \'active\',\n    \'parent\' => isset($parentClass) ? $parentClass : \'\',\n    \'row\' => isset($rowClass) ? $rowClass : \'\',\n    \'outer\' => isset($outerClass) ? $outerClass : \'\',\n    \'inner\' => isset($innerClass) ? $innerClass : \'\',\n    \'level\' => isset($levelClass) ? $levelClass: \'\',\n    \'self\' => isset($selfClass) ? $selfClass : \'\',\n    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'\n);\n\n/* get user templates\n * TODO: eventually move these into config parameters */\n$wf->_templates = array(\n    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',\n    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',\n    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',\n    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',\n    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',\n    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',\n    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',\n    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',\n    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',\n    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',\n    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'\n);\n\n/* process Wayfinder */\n$output = $wf->run();\nif ($wf->_config[\'debug\']) {\n    $output .= $wf->renderDebugOutput();\n}\n\n/* output results */\nif ($wf->_config[\'ph\']) {\n    $modx->setPlaceholder($wf->_config[\'ph\'],$output);\n} else {\n    return $output;\n}',0,'a:48:{s:5:\"level\";a:6:{s:4:\"name\";s:5:\"level\";s:4:\"desc\";s:25:\"prop_wayfinder.level_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"includeDocs\";a:6:{s:4:\"name\";s:11:\"includeDocs\";s:4:\"desc\";s:31:\"prop_wayfinder.includeDocs_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"excludeDocs\";a:6:{s:4:\"name\";s:11:\"excludeDocs\";s:4:\"desc\";s:31:\"prop_wayfinder.excludeDocs_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"contexts\";a:6:{s:4:\"name\";s:8:\"contexts\";s:4:\"desc\";s:28:\"prop_wayfinder.contexts_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"cacheResults\";a:6:{s:4:\"name\";s:12:\"cacheResults\";s:4:\"desc\";s:32:\"prop_wayfinder.cacheResults_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"cacheTime\";a:6:{s:4:\"name\";s:9:\"cacheTime\";s:4:\"desc\";s:29:\"prop_wayfinder.cacheTime_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:3600;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:2:\"ph\";a:6:{s:4:\"name\";s:2:\"ph\";s:4:\"desc\";s:22:\"prop_wayfinder.ph_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:5:\"debug\";a:6:{s:4:\"name\";s:5:\"debug\";s:4:\"desc\";s:25:\"prop_wayfinder.debug_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"ignoreHidden\";a:6:{s:4:\"name\";s:12:\"ignoreHidden\";s:4:\"desc\";s:32:\"prop_wayfinder.ignoreHidden_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"hideSubMenus\";a:6:{s:4:\"name\";s:12:\"hideSubMenus\";s:4:\"desc\";s:32:\"prop_wayfinder.hideSubMenus_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:13:\"useWeblinkUrl\";a:6:{s:4:\"name\";s:13:\"useWeblinkUrl\";s:4:\"desc\";s:33:\"prop_wayfinder.useWeblinkUrl_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"fullLink\";a:6:{s:4:\"name\";s:8:\"fullLink\";s:4:\"desc\";s:28:\"prop_wayfinder.fullLink_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"scheme\";a:6:{s:4:\"name\";s:6:\"scheme\";s:4:\"desc\";s:26:\"prop_wayfinder.scheme_desc\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:3:{i:0;a:2:{s:4:\"text\";s:23:\"prop_wayfinder.relative\";s:5:\"value\";s:0:\"\";}i:1;a:2:{s:4:\"text\";s:23:\"prop_wayfinder.absolute\";s:5:\"value\";s:3:\"abs\";}i:2;a:2:{s:4:\"text\";s:19:\"prop_wayfinder.full\";s:5:\"value\";s:4:\"full\";}}s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"sortOrder\";a:6:{s:4:\"name\";s:9:\"sortOrder\";s:4:\"desc\";s:29:\"prop_wayfinder.sortOrder_desc\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:4:\"text\";s:24:\"prop_wayfinder.ascending\";s:5:\"value\";s:3:\"ASC\";}i:1;a:2:{s:4:\"text\";s:25:\"prop_wayfinder.descending\";s:5:\"value\";s:4:\"DESC\";}}s:5:\"value\";s:3:\"ASC\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"sortBy\";a:6:{s:4:\"name\";s:6:\"sortBy\";s:4:\"desc\";s:26:\"prop_wayfinder.sortBy_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:9:\"menuindex\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:5:\"limit\";a:6:{s:4:\"name\";s:5:\"limit\";s:4:\"desc\";s:25:\"prop_wayfinder.limit_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"cssTpl\";a:6:{s:4:\"name\";s:6:\"cssTpl\";s:4:\"desc\";s:26:\"prop_wayfinder.cssTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:5:\"jsTpl\";a:6:{s:4:\"name\";s:5:\"jsTpl\";s:4:\"desc\";s:25:\"prop_wayfinder.jsTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"rowIdPrefix\";a:6:{s:4:\"name\";s:11:\"rowIdPrefix\";s:4:\"desc\";s:31:\"prop_wayfinder.rowIdPrefix_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"textOfLinks\";a:6:{s:4:\"name\";s:11:\"textOfLinks\";s:4:\"desc\";s:31:\"prop_wayfinder.textOfLinks_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:9:\"menutitle\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"titleOfLinks\";a:6:{s:4:\"name\";s:12:\"titleOfLinks\";s:4:\"desc\";s:32:\"prop_wayfinder.titleOfLinks_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:9:\"pagetitle\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"displayStart\";a:6:{s:4:\"name\";s:12:\"displayStart\";s:4:\"desc\";s:32:\"prop_wayfinder.displayStart_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:10:\"firstClass\";a:6:{s:4:\"name\";s:10:\"firstClass\";s:4:\"desc\";s:30:\"prop_wayfinder.firstClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:5:\"first\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"lastClass\";a:6:{s:4:\"name\";s:9:\"lastClass\";s:4:\"desc\";s:29:\"prop_wayfinder.lastClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:4:\"last\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"hereClass\";a:6:{s:4:\"name\";s:9:\"hereClass\";s:4:\"desc\";s:29:\"prop_wayfinder.hereClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:6:\"active\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"parentClass\";a:6:{s:4:\"name\";s:11:\"parentClass\";s:4:\"desc\";s:31:\"prop_wayfinder.parentClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"rowClass\";a:6:{s:4:\"name\";s:8:\"rowClass\";s:4:\"desc\";s:28:\"prop_wayfinder.rowClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:10:\"outerClass\";a:6:{s:4:\"name\";s:10:\"outerClass\";s:4:\"desc\";s:30:\"prop_wayfinder.outerClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:10:\"innerClass\";a:6:{s:4:\"name\";s:10:\"innerClass\";s:4:\"desc\";s:30:\"prop_wayfinder.innerClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:10:\"levelClass\";a:6:{s:4:\"name\";s:10:\"levelClass\";s:4:\"desc\";s:30:\"prop_wayfinder.levelClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"selfClass\";a:6:{s:4:\"name\";s:9:\"selfClass\";s:4:\"desc\";s:29:\"prop_wayfinder.selfClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"webLinkClass\";a:6:{s:4:\"name\";s:12:\"webLinkClass\";s:4:\"desc\";s:32:\"prop_wayfinder.webLinkClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"outerTpl\";a:6:{s:4:\"name\";s:8:\"outerTpl\";s:4:\"desc\";s:28:\"prop_wayfinder.outerTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"rowTpl\";a:6:{s:4:\"name\";s:6:\"rowTpl\";s:4:\"desc\";s:26:\"prop_wayfinder.rowTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"parentRowTpl\";a:6:{s:4:\"name\";s:12:\"parentRowTpl\";s:4:\"desc\";s:32:\"prop_wayfinder.parentRowTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:16:\"parentRowHereTpl\";a:6:{s:4:\"name\";s:16:\"parentRowHereTpl\";s:4:\"desc\";s:36:\"prop_wayfinder.parentRowHereTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:7:\"hereTpl\";a:6:{s:4:\"name\";s:7:\"hereTpl\";s:4:\"desc\";s:27:\"prop_wayfinder.hereTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"innerTpl\";a:6:{s:4:\"name\";s:8:\"innerTpl\";s:4:\"desc\";s:28:\"prop_wayfinder.innerTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"innerRowTpl\";a:6:{s:4:\"name\";s:11:\"innerRowTpl\";s:4:\"desc\";s:31:\"prop_wayfinder.innerRowTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"innerHereTpl\";a:6:{s:4:\"name\";s:12:\"innerHereTpl\";s:4:\"desc\";s:32:\"prop_wayfinder.innerHereTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:18:\"activeParentRowTpl\";a:6:{s:4:\"name\";s:18:\"activeParentRowTpl\";s:4:\"desc\";s:38:\"prop_wayfinder.activeParentRowTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:18:\"categoryFoldersTpl\";a:6:{s:4:\"name\";s:18:\"categoryFoldersTpl\";s:4:\"desc\";s:38:\"prop_wayfinder.categoryFoldersTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"startItemTpl\";a:6:{s:4:\"name\";s:12:\"startItemTpl\";s:4:\"desc\";s:32:\"prop_wayfinder.startItemTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"permissions\";a:6:{s:4:\"name\";s:11:\"permissions\";s:4:\"desc\";s:31:\"prop_wayfinder.permissions_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:4:\"list\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"hereId\";a:6:{s:4:\"name\";s:6:\"hereId\";s:4:\"desc\";s:26:\"prop_wayfinder.hereId_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:5:\"where\";a:6:{s:4:\"name\";s:5:\"where\";s:4:\"desc\";s:25:\"prop_wayfinder.where_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"templates\";a:6:{s:4:\"name\";s:9:\"templates\";s:4:\"desc\";s:29:\"prop_wayfinder.templates_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:18:\"previewUnpublished\";a:6:{s:4:\"name\";s:18:\"previewUnpublished\";s:4:\"desc\";s:38:\"prop_wayfinder.previewunpublished_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}}','',0,''),
	(4,0,0,'UltimateParent','Return the \"ultimate\" parent of a resource.',0,0,0,'/**\n * @name UltimateParent\n * @version 1.3\n * @author Susan Ottwell <sottwell@sottwell.com> March 2006\n * @author Al B <> May 18, 2007\n * @author S. Hamblett <shamblett@cwazy.co.uk>\n * @author Shaun McCormick <shaun@modx.com>\n * @author Jason Coward <modx@modx.com>\n *\n * @param &id The id of the document whose parent you want to find.\n * @param &top The top node for the search.\n * @param &topLevel The top level node for the search (root = level 1)\n *\n * @license Public Domain, use as you like.\n *\n * @example [[UltimateParent? &id=`45` &top=`6`]]\n * Will find the ultimate parent of document 45 if it is a child of document 6;\n * otherwise it will return 45.\n *\n * @example [[UltimateParent? &topLevel=`2`]]\n * Will find the ultimate parent of the current document at a depth of 2 levels\n * in the document hierarchy, with the root level being level 1.\n *\n * This snippet travels up the document tree from a specified document and\n * returns the \"ultimate\" parent.  Version 2.0 was rewritten to use the new\n * getParentIds function features available only in MODx 0.9.5 or later.\n *\n * Based on the original UltimateParent 1.x snippet by Susan Ottwell\n * <sottwell@sottwell.com>.  The topLevel parameter was introduced by staed and\n * adopted here.\n */\nif (!isset($modx)) return \'\';\n\n$top = isset($top) && intval($top) ? $top : 0;\n$id= isset($id) && intval($id) ? intval($id) : $modx->resource->get(\'id\');\n$topLevel= isset($topLevel) && intval($topLevel) ? intval($topLevel) : 0;\nif ($id && $id != $top) {\n    $pid = $id;\n    $pids = $modx->getParentIds($id);\n    if (!$topLevel || count($pids) >= $topLevel) {\n        while ($parentIds= $modx->getParentIds($id, 1)) {\n            $pid = array_pop($parentIds);\n            if ($pid == $top) {\n                break;\n            }\n            $id = $pid;\n            $parentIds = $modx->getParentIds($id);\n            if ($topLevel && count($parentIds) < $topLevel) {\n                break;\n            }\n        }\n    }\n}\nreturn $id;',0,NULL,'',0,''),
	(5,0,0,'FormIt','A dynamic form processing snippet.',0,4,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * FormIt\n *\n * A dynamic form processing Snippet for MODx Revolution.\n *\n * @package formit\n */\nrequire_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\',null,MODX_CORE_PATH).\'components/formit/\').\'model/formit/formit.class.php\';\n$fi = new FormIt($modx,$scriptProperties);\n$fi->initialize($modx->context->get(\'key\'));\n$fi->loadRequest();\n\n$fields = $fi->request->prepare();\nreturn $fi->request->handle($fields);',0,'a:56:{s:5:\"hooks\";a:7:{s:4:\"name\";s:5:\"hooks\";s:4:\"desc\";s:22:\"prop_formit.hooks_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"preHooks\";a:7:{s:4:\"name\";s:8:\"preHooks\";s:4:\"desc\";s:25:\"prop_formit.prehooks_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"submitVar\";a:7:{s:4:\"name\";s:9:\"submitVar\";s:4:\"desc\";s:26:\"prop_formit.submitvar_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"validate\";a:7:{s:4:\"name\";s:8:\"validate\";s:4:\"desc\";s:25:\"prop_formit.validate_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:6:\"errTpl\";a:7:{s:4:\"name\";s:6:\"errTpl\";s:4:\"desc\";s:23:\"prop_formit.errtpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:37:\"<span class=\"error\">[[+error]]</span>\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:22:\"validationErrorMessage\";a:7:{s:4:\"name\";s:22:\"validationErrorMessage\";s:4:\"desc\";s:39:\"prop_formit.validationerrormessage_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:96:\"<p class=\"error\">A form validation error occurred. Please check the values you have entered.</p>\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:22:\"validationErrorBulkTpl\";a:7:{s:4:\"name\";s:22:\"validationErrorBulkTpl\";s:4:\"desc\";s:39:\"prop_formit.validationerrorbulktpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:19:\"<li>[[+error]]</li>\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:26:\"trimValuesBeforeValidation\";a:7:{s:4:\"name\";s:26:\"trimValuesBeforeValidation\";s:4:\"desc\";s:43:\"prop_formit.trimvaluesdeforevalidation_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:16:\"customValidators\";a:7:{s:4:\"name\";s:16:\"customValidators\";s:4:\"desc\";s:33:\"prop_formit.customvalidators_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:20:\"clearFieldsOnSuccess\";a:7:{s:4:\"name\";s:20:\"clearFieldsOnSuccess\";s:4:\"desc\";s:37:\"prop_formit.clearfieldsonsuccess_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:14:\"successMessage\";a:7:{s:4:\"name\";s:14:\"successMessage\";s:4:\"desc\";s:31:\"prop_formit.successmessage_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:25:\"successMessagePlaceholder\";a:7:{s:4:\"name\";s:25:\"successMessagePlaceholder\";s:4:\"desc\";s:42:\"prop_formit.successmessageplaceholder_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:17:\"fi.successMessage\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:5:\"store\";a:7:{s:4:\"name\";s:5:\"store\";s:4:\"desc\";s:22:\"prop_formit.store_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"placeholderPrefix\";a:7:{s:4:\"name\";s:17:\"placeholderPrefix\";s:4:\"desc\";s:34:\"prop_formit.placeholderprefix_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"fi.\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"storeTime\";a:7:{s:4:\"name\";s:9:\"storeTime\";s:4:\"desc\";s:26:\"prop_formit.storetime_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:300;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:10:\"allowFiles\";a:7:{s:4:\"name\";s:10:\"allowFiles\";s:4:\"desc\";s:27:\"prop_formit.allowfiles_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:15:\"spamEmailFields\";a:7:{s:4:\"name\";s:15:\"spamEmailFields\";s:4:\"desc\";s:32:\"prop_formit.spamemailfields_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:5:\"email\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"spamCheckIp\";a:7:{s:4:\"name\";s:11:\"spamCheckIp\";s:4:\"desc\";s:28:\"prop_formit.spamcheckip_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"recaptchaJs\";a:7:{s:4:\"name\";s:11:\"recaptchaJs\";s:4:\"desc\";s:28:\"prop_formit.recaptchajs_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:2:\"{}\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:14:\"recaptchaTheme\";a:7:{s:4:\"name\";s:14:\"recaptchaTheme\";s:4:\"desc\";s:31:\"prop_formit.recaptchatheme_desc\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:4:{i:0;a:2:{s:4:\"text\";s:14:\"formit.opt_red\";s:5:\"value\";s:3:\"red\";}i:1;a:2:{s:4:\"text\";s:16:\"formit.opt_white\";s:5:\"value\";s:5:\"white\";}i:2;a:2:{s:4:\"text\";s:16:\"formit.opt_clean\";s:5:\"value\";s:5:\"clean\";}i:3;a:2:{s:4:\"text\";s:21:\"formit.opt_blackglass\";s:5:\"value\";s:10:\"blackglass\";}}s:5:\"value\";s:5:\"clean\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:10:\"redirectTo\";a:7:{s:4:\"name\";s:10:\"redirectTo\";s:4:\"desc\";s:27:\"prop_formit.redirectto_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:14:\"redirectParams\";a:7:{s:4:\"name\";s:14:\"redirectParams\";s:4:\"desc\";s:31:\"prop_formit.redirectparams_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"emailTo\";a:7:{s:4:\"name\";s:7:\"emailTo\";s:4:\"desc\";s:24:\"prop_formit.emailto_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"emailToName\";a:7:{s:4:\"name\";s:11:\"emailToName\";s:4:\"desc\";s:28:\"prop_formit.emailtoname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"emailFrom\";a:7:{s:4:\"name\";s:9:\"emailFrom\";s:4:\"desc\";s:26:\"prop_formit.emailfrom_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:13:\"emailFromName\";a:7:{s:4:\"name\";s:13:\"emailFromName\";s:4:\"desc\";s:30:\"prop_formit.emailfromname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"emailReplyTo\";a:7:{s:4:\"name\";s:12:\"emailReplyTo\";s:4:\"desc\";s:29:\"prop_formit.emailreplyto_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:16:\"emailReplyToName\";a:7:{s:4:\"name\";s:16:\"emailReplyToName\";s:4:\"desc\";s:33:\"prop_formit.emailreplytoname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"emailCC\";a:7:{s:4:\"name\";s:7:\"emailCC\";s:4:\"desc\";s:24:\"prop_formit.emailcc_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"emailCCName\";a:7:{s:4:\"name\";s:11:\"emailCCName\";s:4:\"desc\";s:28:\"prop_formit.emailccname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"emailBCC\";a:7:{s:4:\"name\";s:8:\"emailBCC\";s:4:\"desc\";s:25:\"prop_formit.emailbcc_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"emailBCCName\";a:7:{s:4:\"name\";s:12:\"emailBCCName\";s:4:\"desc\";s:29:\"prop_formit.emailbccname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"emailSubject\";a:7:{s:4:\"name\";s:12:\"emailSubject\";s:4:\"desc\";s:29:\"prop_formit.emailsubject_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:23:\"emailUseFieldForSubject\";a:7:{s:4:\"name\";s:23:\"emailUseFieldForSubject\";s:4:\"desc\";s:40:\"prop_formit.emailusefieldforsubject_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"emailHtml\";a:7:{s:4:\"name\";s:9:\"emailHtml\";s:4:\"desc\";s:26:\"prop_formit.emailhtml_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:20:\"emailConvertNewlines\";a:7:{s:4:\"name\";s:20:\"emailConvertNewlines\";s:4:\"desc\";s:37:\"prop_formit.emailconvertnewlines_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"emailMultiWrapper\";a:7:{s:4:\"name\";s:17:\"emailMultiWrapper\";s:4:\"desc\";s:34:\"prop_formit.emailmultiwrapper_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:10:\"[[+value]]\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:19:\"emailMultiSeparator\";a:7:{s:4:\"name\";s:19:\"emailMultiSeparator\";s:4:\"desc\";s:36:\"prop_formit.emailmultiseparator_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"fiarTpl\";a:7:{s:4:\"name\";s:7:\"fiarTpl\";s:4:\"desc\";s:22:\"prop_fiar.fiartpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"fiarToField\";a:7:{s:4:\"name\";s:11:\"fiarToField\";s:4:\"desc\";s:26:\"prop_fiar.fiartofield_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:5:\"email\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"fiarSubject\";a:7:{s:4:\"name\";s:11:\"fiarSubject\";s:4:\"desc\";s:26:\"prop_fiar.fiarsubject_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:30:\"[[++site_name]] Auto-Responder\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"fiarFrom\";a:7:{s:4:\"name\";s:8:\"fiarFrom\";s:4:\"desc\";s:23:\"prop_fiar.fiarfrom_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"fiarFromName\";a:7:{s:4:\"name\";s:12:\"fiarFromName\";s:4:\"desc\";s:27:\"prop_fiar.fiarfromname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"fiarReplyTo\";a:7:{s:4:\"name\";s:11:\"fiarReplyTo\";s:4:\"desc\";s:26:\"prop_fiar.fiarreplyto_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:15:\"fiarReplyToName\";a:7:{s:4:\"name\";s:15:\"fiarReplyToName\";s:4:\"desc\";s:30:\"prop_fiar.fiarreplytoname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:6:\"fiarCC\";a:7:{s:4:\"name\";s:6:\"fiarCC\";s:4:\"desc\";s:21:\"prop_fiar.fiarcc_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:10:\"fiarCCName\";a:7:{s:4:\"name\";s:10:\"fiarCCName\";s:4:\"desc\";s:25:\"prop_fiar.fiarccname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"fiarBCC\";a:7:{s:4:\"name\";s:7:\"fiarBCC\";s:4:\"desc\";s:22:\"prop_fiar.fiarbcc_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"fiarBCCName\";a:7:{s:4:\"name\";s:11:\"fiarBCCName\";s:4:\"desc\";s:26:\"prop_fiar.fiarbccname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"fiarHtml\";a:7:{s:4:\"name\";s:8:\"fiarHtml\";s:4:\"desc\";s:23:\"prop_fiar.fiarhtml_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"mathMinRange\";a:7:{s:4:\"name\";s:12:\"mathMinRange\";s:4:\"desc\";s:27:\"prop_math.mathminrange_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:10;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"mathMaxRange\";a:7:{s:4:\"name\";s:12:\"mathMaxRange\";s:4:\"desc\";s:27:\"prop_math.mathmaxrange_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:100;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"mathField\";a:7:{s:4:\"name\";s:9:\"mathField\";s:4:\"desc\";s:24:\"prop_math.mathfield_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:4:\"math\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"mathOp1Field\";a:7:{s:4:\"name\";s:12:\"mathOp1Field\";s:4:\"desc\";s:27:\"prop_math.mathop1field_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"op1\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"mathOp2Field\";a:7:{s:4:\"name\";s:12:\"mathOp2Field\";s:4:\"desc\";s:27:\"prop_math.mathop2field_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"op2\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"mathOperatorField\";a:7:{s:4:\"name\";s:17:\"mathOperatorField\";s:4:\"desc\";s:32:\"prop_math.mathoperatorfield_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:8:\"operator\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}}','',0,''),
	(6,0,0,'FormItAutoResponder','Custom hook for FormIt to handle Auto-Response emails.',0,4,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * A custom FormIt hook for auto-responders.\n *\n * @var modX $modx\n * @var array $scriptProperties\n * @var FormIt $formit\n * @var fiHooks $hook\n * \n * @package formit\n */\n/* setup default properties */\n$tpl = $modx->getOption(\'fiarTpl\',$scriptProperties,\'fiarTpl\');\n$mailFrom = $modx->getOption(\'fiarFrom\',$scriptProperties,$modx->getOption(\'emailsender\'));\n$mailFromName = $modx->getOption(\'fiarFromName\',$scriptProperties,$modx->getOption(\'site_name\'));\n$mailSender = $modx->getOption(\'fiarSender\',$scriptProperties,$modx->getOption(\'emailsender\'));\n$mailSubject = $modx->getOption(\'fiarSubject\',$scriptProperties,\'[[++site_name]] Auto-Responder\');\n$mailSubject = str_replace(array(\'[[++site_name]]\',\'[[++emailsender]]\'),array($modx->getOption(\'site_name\'),$modx->getOption(\'emailsender\')),$mailSubject);\n$isHtml = $modx->getOption(\'fiarHtml\',$scriptProperties,true);\n$toField = $modx->getOption(\'fiarToField\',$scriptProperties,\'email\');\n$multiSeparator = $modx->getOption(\'fiarMultiSeparator\',$formit->config,\"\\n\");\n$multiWrapper = $modx->getOption(\'fiarMultiWrapper\',$formit->config,\"[[+value]]\");\nif (empty($fields[$toField])) {\n    $modx->log(modX::LOG_LEVEL_ERROR,\'[FormIt] Auto-responder could not find field `\'.$toField.\'` in form submission.\');\n    return false;\n}\n\n/* handle checkbox and array fields */\nforeach ($fields as $k => &$v) {\n    if (is_array($v)) {\n        $vOpts = array();\n        foreach ($v as $vKey => $vValue) {\n            if (is_string($vKey) && !empty($vKey)) {\n                $vKey = $k.\'.\'.$vKey;\n                $fields[$vKey] = $vValue;\n            } else {\n                $vOpts[] = str_replace(\'[[+value]]\',$vValue,$multiWrapper);\n            }\n        }\n        $newValue = implode($multiSeparator,$vOpts);\n        if (!empty($vOpts)) {\n            $fields[$k] = $newValue;\n        }\n    }\n}\n\n/* setup placeholders */\n$placeholders = $fields;\n$mailTo= $fields[$toField];\n\n$message = $formit->getChunk($tpl,$placeholders);\n$modx->parser->processElementTags(\'\',$message,true,false);\n\n$modx->getService(\'mail\', \'mail.modPHPMailer\');\n$modx->mail->reset();\n$modx->mail->set(modMail::MAIL_BODY,$message);\n$modx->mail->set(modMail::MAIL_FROM,$hook->_process($mailFrom,$placeholders));\n$modx->mail->set(modMail::MAIL_FROM_NAME,$hook->_process($mailFromName,$placeholders));\n$modx->mail->set(modMail::MAIL_SENDER,$hook->_process($mailSender,$placeholders));\n$modx->mail->set(modMail::MAIL_SUBJECT,$hook->_process($mailSubject,$placeholders));\n$modx->mail->address(\'to\',$mailTo);\n$modx->mail->setHTML($isHtml);\n\n/* reply to */\n$emailReplyTo = $modx->getOption(\'fiarReplyTo\',$scriptProperties,$mailFrom);\n$emailReplyTo = $hook->_process($emailReplyTo,$fields);\n$emailReplyToName = $modx->getOption(\'fiarReplyToName\',$scriptProperties,$mailFromName);\n$emailReplyToName = $hook->_process($emailReplyToName,$fields);\nif (!empty($emailReplyTo)) {\n    $modx->mail->address(\'reply-to\',$emailReplyTo,$emailReplyToName);\n}\n\n/* cc */\n$emailCC = $modx->getOption(\'fiarCC\',$scriptProperties,\'\');\nif (!empty($emailCC)) {\n    $emailCCName = $modx->getOption(\'fiarCCName\',$scriptProperties,\'\');\n    $emailCC = explode(\',\',$emailCC);\n    $emailCCName = explode(\',\',$emailCCName);\n    $numAddresses = count($emailCC);\n    for ($i=0;$i<$numAddresses;$i++) {\n        $etn = !empty($emailCCName[$i]) ? $emailCCName[$i] : \'\';\n        if (!empty($etn)) $etn = $hook->_process($etn,$fields);\n        $emailCC[$i] = $hook->_process($emailCC[$i],$fields);\n        if (!empty($emailCC[$i])) {\n            $modx->mail->address(\'cc\',$emailCC[$i],$etn);\n        }\n    }\n}\n\n/* bcc */\n$emailBCC = $modx->getOption(\'fiarBCC\',$scriptProperties,\'\');\nif (!empty($emailBCC)) {\n    $emailBCCName = $modx->getOption(\'fiarBCCName\',$scriptProperties,\'\');\n    $emailBCC = explode(\',\',$emailBCC);\n    $emailBCCName = explode(\',\',$emailBCCName);\n    $numAddresses = count($emailBCC);\n    for ($i=0;$i<$numAddresses;$i++) {\n        $etn = !empty($emailBCCName[$i]) ? $emailBCCName[$i] : \'\';\n        if (!empty($etn)) $etn = $hook->_process($etn,$fields);\n        $emailBCC[$i] = $hook->_process($emailBCC[$i],$fields);\n        if (!empty($emailBCC[$i])) {\n            $modx->mail->address(\'bcc\',$emailBCC[$i],$etn);\n        }\n    }\n}\n\nif (!$formit->inTestMode) {\n    if (!$modx->mail->send()) {\n        $modx->log(modX::LOG_LEVEL_ERROR,\'[FormIt] An error occurred while trying to send the auto-responder email: \'.$modx->mail->mailer->ErrorInfo);\n        return false;\n    }\n}\n$modx->mail->reset();\nreturn true;',0,NULL,'',0,''),
	(7,0,0,'FormItRetriever','Fetches a form submission for a user for displaying on a thank you page.',0,4,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Retrieves a prior form submission that was stored with the &store property\n * in a FormIt call.\n *\n * @var modX $modx\n * @var array $scriptProperties\n * \n * @package formit\n */\nrequire_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\').\'components/formit/\').\'model/formit/formit.class.php\';\n$fi = new FormIt($modx,$scriptProperties);\n\n/* setup properties */\n$placeholderPrefix = $modx->getOption(\'placeholderPrefix\',$scriptProperties,\'fi.\');\n$eraseOnLoad = $modx->getOption(\'eraseOnLoad\',$scriptProperties,false);\n$redirectToOnNotFound = $modx->getOption(\'redirectToOnNotFound\',$scriptProperties,false);\n\n/* fetch data from cache and set to placeholders */\n$fi->loadRequest();\n$fi->request->loadDictionary();\n$data = $fi->request->dictionary->retrieve();\nif (!empty($data)) {\n    /* set data to placeholders */\n    foreach ($data as $k=>$v) {\n        /*checkboxes & other multi-values are stored as arrays, must be imploded*/\n        if (is_array($v)) {\n            $data[$k] = implode(\',\',$v);\n        }\n    }\n    $modx->toPlaceholders($data,$placeholderPrefix,\'\');\n    \n    /* if set, erase the data on load, otherwise depend on cache expiry time */\n    if ($eraseOnLoad) {\n        $fi->request->dictionary->erase();\n    }\n/* if the data\'s not found, and we want to redirect somewhere if so, do here */\n} else if (!empty($redirectToOnNotFound)) {\n    $url = $modx->makeUrl($redirectToOnNotFound);\n    $modx->sendRedirect($url);\n}\nreturn \'\';',0,'a:3:{s:17:\"placeholderPrefix\";a:7:{s:4:\"name\";s:17:\"placeholderPrefix\";s:4:\"desc\";s:31:\"prop_fir.placeholderprefix_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"fi.\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:20:\"redirectToOnNotFound\";a:7:{s:4:\"name\";s:20:\"redirectToOnNotFound\";s:4:\"desc\";s:34:\"prop_fir.redirecttoonnotfound_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"eraseOnLoad\";a:7:{s:4:\"name\";s:11:\"eraseOnLoad\";s:4:\"desc\";s:25:\"prop_fir.eraseonload_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}}','',0,''),
	(8,0,0,'FormItIsChecked','A custom output filter used with checkboxes/radios for checking checked status.',0,4,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Custom output filter that returns checked=\"checked\" if the value is set\n *\n * @var string $input\n * @var string $options\n * @package formit\n */\n$output = \' \';\nif ($input == $options) {\n    $output = \' checked=\"checked\"\';\n}\n$input = $modx->fromJSON($input);\nif (in_array($options,$input)) {\n  $output = \' checked=\"checked\"\';\n}\nreturn $output;',0,NULL,'',0,''),
	(9,0,0,'FormItIsSelected','A custom output filter used with dropdowns for checking selected status.',0,4,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Custom output filter that returns checked=\"checked\" if the value is set\n *\n * @var string $input\n * @var string $options\n * @package formit\n */\n$output = \' \';\nif ($input == $options) {\n    $output = \' selected=\"selected\"\';\n}\n$input = $modx->fromJSON($input);\nif (in_array($options,$input)) {\n  $output = \' selected=\"selected\"\';\n}\nreturn $output;',0,NULL,'',0,''),
	(10,0,0,'FormItCountryOptions','A utility snippet for generating a dropdown list of countries.',0,4,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Automatically generates and outputs a country list for usage in forms\n *\n * @var modX $modx\n * @var array $scriptProperties\n * @package formit\n */\nrequire_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\').\'components/formit/\').\'model/formit/formit.class.php\';\n$fi = new FormIt($modx,$scriptProperties);\n\n/** @var fiCountryOptions $co */\n$co = $fi->loadModule(\'fiCountryOptions\',\'countryOptions\',$scriptProperties);\n$co->initialize();\n$co->getData();\n$co->loadPrioritized();\n$co->iterate();\nreturn $co->output();',0,'a:9:{s:8:\"selected\";a:7:{s:4:\"name\";s:8:\"selected\";s:4:\"desc\";s:23:\"prop_fico.selected_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:3:\"tpl\";a:7:{s:4:\"name\";s:3:\"tpl\";s:4:\"desc\";s:18:\"prop_fico.tpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:6:\"option\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:10:\"useIsoCode\";a:7:{s:4:\"name\";s:10:\"useIsoCode\";s:4:\"desc\";s:25:\"prop_fico.useisocode_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"prioritized\";a:7:{s:4:\"name\";s:11:\"prioritized\";s:4:\"desc\";s:26:\"prop_fico.prioritized_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"optGroupTpl\";a:7:{s:4:\"name\";s:11:\"optGroupTpl\";s:4:\"desc\";s:26:\"prop_fico.optgrouptpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:8:\"optgroup\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:20:\"prioritizedGroupText\";a:7:{s:4:\"name\";s:20:\"prioritizedGroupText\";s:4:\"desc\";s:35:\"prop_fico.prioritizedgrouptext_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"allGroupText\";a:7:{s:4:\"name\";s:12:\"allGroupText\";s:4:\"desc\";s:27:\"prop_fico.allgrouptext_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"selectedAttribute\";a:7:{s:4:\"name\";s:17:\"selectedAttribute\";s:4:\"desc\";s:32:\"prop_fico.selectedattribute_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:20:\" selected=\"selected\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:13:\"toPlaceholder\";a:7:{s:4:\"name\";s:13:\"toPlaceholder\";s:4:\"desc\";s:28:\"prop_fico.toplaceholder_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}}','',0,''),
	(11,0,0,'FormItStateOptions','A utility snippet for generating a dropdown list of U.S. states.',0,4,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Automatically generates and outputs a U.S. state list for usage in forms\n * \n * @package formit\n */\nrequire_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\').\'components/formit/\').\'model/formit/formit.class.php\';\n$fi = new FormIt($modx,$scriptProperties);\n\n/** @var fiCountryOptions $co */\n$co = $fi->loadModule(\'fiStateOptions\',\'stateOptions\',$scriptProperties);\n$co->initialize();\n$co->getData();\n$co->iterate();\nreturn $co->output();',0,'a:6:{s:8:\"selected\";a:7:{s:4:\"name\";s:8:\"selected\";s:4:\"desc\";s:23:\"prop_fiso.selected_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:3:\"tpl\";a:7:{s:4:\"name\";s:3:\"tpl\";s:4:\"desc\";s:18:\"prop_fiso.tpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:6:\"option\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"useAbbr\";a:7:{s:4:\"name\";s:7:\"useAbbr\";s:4:\"desc\";s:22:\"prop_fiso.useabbr_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"selectedAttribute\";a:7:{s:4:\"name\";s:17:\"selectedAttribute\";s:4:\"desc\";s:32:\"prop_fiso.selectedattribute_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:20:\" selected=\"selected\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"country\";a:7:{s:4:\"name\";s:7:\"country\";s:4:\"desc\";s:22:\"prop_fiso.country_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:2:\"us\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:13:\"toPlaceholder\";a:7:{s:4:\"name\";s:13:\"toPlaceholder\";s:4:\"desc\";s:28:\"prop_fiso.toplaceholder_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}}','',0,''),
	(12,0,0,'getImageList','',0,5,0,'/**\n * getImageList\n *\n * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>\n *\n * getImageList is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package migx\n */\n/**\n * getImageList\n *\n * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution \n *\n * @version 1.4\n * @author Bruno Perner <b.perner@gmx.de>\n * @copyright Copyright &copy; 2009-2014\n * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License\n * version 2 or (at your option) any later version.\n * @package migx\n */\n\n/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src=\"[[+imageURL]]\"/><p>[[+imageAlt]]</p></li>`]]</ul>*/\n/* get default properties */\n\n\n$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');\n$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');\n$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');\n$offset = $modx->getOption(\'offset\', $scriptProperties, 0);\n$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');\n$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);\n$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images\n$where = $modx->getOption(\'where\', $scriptProperties, \'\');\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');\n$sort = !empty($sort) ? $modx->fromJSON($sort) : array();\n$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\n$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');\n$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');\n$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');\n$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);\n$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');\n$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');\n$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;\n$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');\n$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));\n$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;\n$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');\n$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');\n$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');\n$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');\n$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');\n$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;\n//split json into parts\n$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));\n$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');\n$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');\n$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from\n$inheritFrom = !empty($inheritFrom) ? explode(\',\',$inheritFrom) : \'\';\n\n$modx->setPlaceholder(\'docid\', $docid);\n\n$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);\n$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';\n\nif (!empty($tvname)) {\n    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {\n\n        /*\n        *   get inputProperties\n        */\n\n\n        $properties = $tv->get(\'input_properties\');\n        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();\n\n        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');\n        if (!empty($migx->config[\'configs\'])) {\n            $migx->loadConfigs();\n            // get tabs from file or migx-config-table\n            $formtabs = $migx->getTabs();\n        }\n        if (empty($formtabs) && isset($properties[\'formtabs\'])) {\n            //try to get formtabs and its fields from properties\n            $formtabs = $modx->fromJSON($properties[\'formtabs\']);\n        }\n\n        if (!empty($properties[\'basePath\'])) {\n            if ($properties[\'autoResourceFolders\'] == \'true\') {\n                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';\n                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';\n            } else {\n                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];\n                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];\n            }\n        }\n        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {\n            $jsonVarKey = $properties[\'jsonvarkey\'];\n            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;\n        }\n        \n        if (empty($outputvalue)){\n            $outputvalue = $tv->renderOutput($docid);\n            if (empty($outputvalue) && !empty($inheritFrom)){\n                foreach ($inheritFrom as $from){\n                    if ($from == \'parents\'){\n                        $outputvalue = $tv->processInheritBinding(\'\',$docid);\n                    }else{\n                        $outputvalue = $tv->renderOutput($from);\n                    }\n                }\n            }\n        }\n        \n        \n        /*\n        *   get inputTvs \n        */\n        $inputTvs = array();\n        if (is_array($formtabs)) {\n\n            //multiple different Forms\n            // Note: use same field-names and inputTVs in all forms\n            $inputTvs = $migx->extractInputTvs($formtabs);\n        }\n        $migx->source = $tv->getSource($migx->working_context, false);\n    }\n\n\n}\n\nif (empty($outputvalue)) {\n    return \'\';\n}\n\n//echo $outputvalue.\'<br/><br/>\';\n\n$items = $modx->fromJSON($outputvalue);\n\n// where filter\nif (is_array($where) && count($where) > 0) {\n    $items = $migx->filterItems($where, $items);\n}\n$modx->setPlaceholder($totalVar, count($items));\n\nif (!empty($reverse)) {\n    $items = array_reverse($items);\n}\n\n// sort items\nif (is_array($sort) && count($sort) > 0) {\n    $items = $migx->sortDbResult($items, $sort);\n}\n\n$summaries = array();\n$output = \'\';\n$items = $offset > 0 ? array_slice($items, $offset) : $items;\n$count = count($items);\n\nif ($count > 0) {\n    $limit = $limit == 0 || $limit > $count ? $count : $limit;\n    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;\n    //preselect important items\n    $preitems = array();\n    if ($randomize && $preselectLimit > 0) {\n        for ($i = 0; $i < $preselectLimit; $i++) {\n            $preitems[] = $items[$i];\n            unset($items[$i]);\n        }\n        $limit = $limit - count($preitems);\n    }\n\n    //shuffle items\n    if ($randomize) {\n        shuffle($items);\n    }\n\n    //limit items\n    $count = count($items);\n    $tempitems = array();\n\n    for ($i = 0; $i < $limit; $i++) {\n        if ($i >= $count) {\n            break;\n        }\n        $tempitems[] = $items[$i];\n    }\n    $items = $tempitems;\n\n    //add preselected items and schuffle again\n    if ($randomize && $preselectLimit > 0) {\n        $items = array_merge($preitems, $items);\n        shuffle($items);\n    }\n\n    $properties = array();\n    foreach ($scriptProperties as $property => $value) {\n        $properties[\'property.\' . $property] = $value;\n    }\n\n    $idx = 0;\n    $output = array();\n    $template = array();\n    $count = count($items);\n\n    foreach ($items as $key => $item) {\n        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';\n        $fields = array();\n        foreach ($item as $field => $value) {\n            if (is_array($value)) {\n                if (is_array($value[0])) {\n                    //nested array - convert to json\n                    $value = $modx->toJson($value);\n                } else {\n                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)\n                }\n            }\n\n\n            $inputTVkey = $formname . $field;\n            if ($processTVs && isset($inputTvs[$inputTVkey])) {\n                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {\n\n                } else {\n                    $tv = $modx->newObject(\'modTemplateVar\');\n                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);\n                }\n                $inputTV = $inputTvs[$inputTVkey];\n\n                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');\n                //don\'t manipulate any urls here\n                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');\n                $tv->set(\'default_text\', $value);\n                $value = $tv->renderOutput($docid);\n                //set option back\n                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);\n                //now manipulate urls\n                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {\n                    $mTypes = explode(\',\', $mTypes);\n                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {\n                        //$value = $mediasource->prepareOutputUrl($value);\n                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));\n                    }\n                }\n\n            }\n            $fields[$field] = $value;\n\n        }\n\n        if (!empty($addfields)) {\n            foreach ($addfields as $addfield) {\n                $addfield = explode(\':\', $addfield);\n                $addname = $addfield[0];\n                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';\n                $fields[$addname] = $adddefault;\n            }\n        }\n\n        if (!empty($sumFields)) {\n            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);\n            foreach ($sumFields as $sumField) {\n                if (isset($fields[$sumField])) {\n                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];\n                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];\n                }\n            }\n        }\n\n\n        if ($toJsonPlaceholder) {\n            $output[] = $fields;\n        } else {\n            $fields[\'_alt\'] = $idx % 2;\n            $idx++;\n            $fields[\'_first\'] = $idx == 1 ? true : \'\';\n            $fields[\'_last\'] = $idx == $limit ? true : \'\';\n            $fields[\'idx\'] = $idx;\n            $rowtpl = \'\';\n            //get changing tpls from field\n            if (substr($tpl, 0, 7) == \"@FIELD:\") {\n                $tplField = substr($tpl, 7);\n                $rowtpl = $fields[$tplField];\n            }\n\n            if ($fields[\'_first\'] && !empty($tplFirst)) {\n                $rowtpl = $tplFirst;\n            }\n            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {\n                $rowtpl = $tplLast;\n            }\n            $tplidx = \'tpl_\' . $idx;\n            if (empty($rowtpl) && !empty($$tplidx)) {\n                $rowtpl = $$tplidx;\n            }\n            if ($idx > 1 && empty($rowtpl)) {\n                $divisors = $migx->getDivisors($idx);\n                if (!empty($divisors)) {\n                    foreach ($divisors as $divisor) {\n                        $tplnth = \'tpl_n\' . $divisor;\n                        if (!empty($$tplnth)) {\n                            $rowtpl = $$tplnth;\n                            if (!empty($rowtpl)) {\n                                break;\n                            }\n                        }\n                    }\n                }\n            }\n\n            if ($count == 1 && isset($tpl_oneresult)) {\n                $rowtpl = $tpl_oneresult;\n            }\n\n            $fields = array_merge($fields, $properties);\n\n            if (!empty($rowtpl)) {\n                $template = $migx->getTemplate($tpl, $template);\n                $fields[\'_tpl\'] = $template[$tpl];\n            } else {\n                $rowtpl = $tpl;\n\n            }\n            $template = $migx->getTemplate($rowtpl, $template);\n\n\n            if ($template[$rowtpl]) {\n                $chunk = $modx->newObject(\'modChunk\');\n                $chunk->setCacheable(false);\n                $chunk->setContent($template[$rowtpl]);\n\n\n                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {\n                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);\n                } else {\n                    $output[] = $chunk->process($fields);\n                }\n            } else {\n                if (!empty($placeholdersKeyField)) {\n                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n                } else {\n                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n                }\n            }\n        }\n\n\n    }\n}\n\nif (count($summaries) > 0) {\n    $modx->toPlaceholders($summaries);\n}\n\n\nif ($toJsonPlaceholder) {\n    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));\n    return \'\';\n}\n\nif (!empty($toSeparatePlaceholders)) {\n    $modx->toPlaceholders($output, $toSeparatePlaceholders);\n    return \'\';\n}\n/*\nif (!empty($outerTpl))\n$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));\nelse \n*/\n\nif ($count > 0 && $splits > 0) {\n    $size = ceil($count / $splits);\n    $chunks = array_chunk($output, $size);\n    $output = array();\n    foreach ($chunks as $chunk) {\n        $o = implode($outputSeparator, $chunk);\n        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));\n    }\n    $outputSeparator = $splitSeparator;\n}\n\nif (is_array($output)) {\n    $o = implode($outputSeparator, $output);\n} else {\n    $o = $output;\n}\n\nif (!empty($o) && !empty($wrapperTpl)) {\n    $template = $migx->getTemplate($wrapperTpl);\n    if ($template[$wrapperTpl]) {\n        $chunk = $modx->newObject(\'modChunk\');\n        $chunk->setCacheable(false);\n        $chunk->setContent($template[$wrapperTpl]);\n        $properties[\'output\'] = $o;\n        $o = $chunk->process($properties);\n    }\n}\n\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $o);\n    return \'\';\n}\n\nreturn $o;',0,'a:0:{}','',0,''),
	(13,0,0,'migxGetRelations','',0,5,0,'$id = $modx->getOption(\'id\', $scriptProperties, $modx->resource->get(\'id\'));\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, \'\');\n$element = $modx->getOption(\'element\', $scriptProperties, \'getResources\');\n$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \',\');\n$sourceWhere = $modx->getOption(\'sourceWhere\', $scriptProperties, \'\');\n$ignoreRelationIfEmpty = $modx->getOption(\'ignoreRelationIfEmpty\', $scriptProperties, false);\n$inheritFromParents = $modx->getOption(\'inheritFromParents\', $scriptProperties, false);\n$parentIDs = $inheritFromParents ? array_merge(array($id), $modx->getParentIds($id)) : array($id);\n\n$packageName = \'resourcerelations\';\n\n$packagepath = $modx->getOption(\'core_path\') . \'components/\' . $packageName . \'/\';\n$modelpath = $packagepath . \'model/\';\n\n$modx->addPackage($packageName, $modelpath, $prefix);\n$classname = \'rrResourceRelation\';\n$output = \'\';\n\nforeach ($parentIDs as $id) {\n    if (!empty($id)) {\n        $output = \'\';\n                \n        $c = $modx->newQuery($classname, array(\'target_id\' => $id, \'published\' => \'1\'));\n        $c->select($modx->getSelectColumns($classname, $classname));\n\n        if (!empty($sourceWhere)) {\n            $sourceWhere_ar = $modx->fromJson($sourceWhere);\n            if (is_array($sourceWhere_ar)) {\n                $where = array();\n                foreach ($sourceWhere_ar as $key => $value) {\n                    $where[\'Source.\' . $key] = $value;\n                }\n                $joinclass = \'modResource\';\n                $joinalias = \'Source\';\n                $selectfields = \'id\';\n                $selectfields = !empty($selectfields) ? explode(\',\', $selectfields) : null;\n                $c->leftjoin($joinclass, $joinalias);\n                $c->select($modx->getSelectColumns($joinclass, $joinalias, $joinalias . \'_\', $selectfields));\n                $c->where($where);\n            }\n        }\n\n        //$c->prepare(); echo $c->toSql();\n        if ($c->prepare() && $c->stmt->execute()) {\n            $collection = $c->stmt->fetchAll(PDO::FETCH_ASSOC);\n        }\n        \n        foreach ($collection as $row) {\n            $ids[] = $row[\'source_id\'];\n        }\n        $output = implode($outputSeparator, $ids);\n    }\n    if (!empty($output)){\n        break;\n    }\n}\n\n\nif (!empty($element)) {\n    if (empty($output) && $ignoreRelationIfEmpty) {\n        return $modx->runSnippet($element, $scriptProperties);\n    } else {\n        $scriptProperties[\'resources\'] = $output;\n        $scriptProperties[\'parents\'] = \'9999999\';\n        return $modx->runSnippet($element, $scriptProperties);\n    }\n\n\n}\n\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $output);\n    return \'\';\n}\n\nreturn $output;',0,NULL,'',0,''),
	(14,0,0,'migx','',0,5,0,'$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');\n$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');\n$offset = $modx->getOption(\'offset\', $scriptProperties, 0);\n$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');\n$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);\n$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images\n$where = $modx->getOption(\'where\', $scriptProperties, \'\');\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$sortConfig = $modx->getOption(\'sortConfig\', $scriptProperties, \'\');\n$sortConfig = !empty($sortConfig) ? $modx->fromJSON($sortConfig) : array();\n$configs = $modx->getOption(\'configs\', $scriptProperties, \'\');\n$configs = !empty($configs) ? explode(\',\',$configs):array();\n$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\n$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');\n//$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');\n$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'id\');\n$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);\n$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');\n$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');\n$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;\n$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');\n$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));\n$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;\n$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');\n\n$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);\n$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n//$modx->migx = &$migx;\n$defaultcontext = \'web\';\n$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : $defaultcontext;\n\nif (!empty($tvname))\n{\n    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname)))\n    {\n\n        /*\n        *   get inputProperties\n        */\n\n\n        $properties = $tv->get(\'input_properties\');\n        $properties = isset($properties[\'configs\']) ? $properties : $tv->getProperties();\n        $cfgs = $modx->getOption(\'configs\',$properties,\'\');\n        if (!empty($cfgs)){\n            $cfgs = explode(\',\',$cfgs);\n            $configs = array_merge($configs,$cfgs);\n           \n        }\n        \n    }\n}\n\n\n\n//$migx->config[\'configs\'] = implode(\',\',$configs);\n$migx->loadConfigs(false,true,array(\'configs\'=>implode(\',\',$configs)));\n$migx->customconfigs = array_merge($migx->customconfigs,$scriptProperties);\n\n\n\n// get tabs from file or migx-config-table\n$formtabs = $migx->getTabs();\nif (empty($formtabs))\n{\n    //try to get formtabs and its fields from properties\n    $formtabs = $modx->fromJSON($properties[\'formtabs\']);\n}\n\nif ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\']))\n{\n    $jsonVarKey = $properties[\'jsonvarkey\'];\n    $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;\n}\n\n$outputvalue = $tv && empty($outputvalue) ? $tv->renderOutput($docid) : $outputvalue;\n/*\n*   get inputTvs \n*/\n$inputTvs = array();\nif (is_array($formtabs))\n{\n\n    //multiple different Forms\n    // Note: use same field-names and inputTVs in all forms\n    $inputTvs = $migx->extractInputTvs($formtabs);\n}\n\nif ($tv)\n{\n    $migx->source = $tv->getSource($migx->working_context, false);\n}\n\n//$task = $modx->migx->getTask();\n$filename = \'getlist.php\';\n$processorspath = $migx->config[\'processorsPath\'] . \'mgr/\';\n$filenames = array();\n$scriptProperties[\'start\'] = $modx->getOption(\'offset\', $scriptProperties, 0);\nif ($processor_file = $migx->findProcessor($processorspath, $filename, $filenames))\n{\n    include ($processor_file);\n    //todo: add getlist-processor for default-MIGX-TV\n}\n\n$items = isset($rows) && is_array($rows) ? $rows : array();\n$modx->setPlaceholder($totalVar, isset($count) ? $count : 0);\n\n$properties = array();\nforeach ($scriptProperties as $property => $value)\n{\n    $properties[\'property.\' . $property] = $value;\n}\n\n$idx = 0;\n$output = array();\nforeach ($items as $key => $item)\n{\n\n    $fields = array();\n    foreach ($item as $field => $value)\n    {\n        $value = is_array($value) ? implode(\'||\', $value) : $value; //handle arrays (checkboxes, multiselects)\n        if ($processTVs && isset($inputTvs[$field]))\n        {\n            if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$field][\'inputTV\'])))\n            {\n\n            } else\n            {\n                $tv = $modx->newObject(\'modTemplateVar\');\n                $tv->set(\'type\', $inputTvs[$field][\'inputTVtype\']);\n            }\n            $inputTV = $inputTvs[$field];\n\n            $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');\n            //don\'t manipulate any urls here\n            $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');\n            $tv->set(\'default_text\', $value);\n            $value = $tv->renderOutput($docid);\n            //set option back\n            $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);\n            //now manipulate urls\n            if ($mediasource = $migx->getFieldSource($inputTV, $tv))\n            {\n                $mTypes = explode(\',\', $mTypes);\n                if (!empty($value) && in_array($tv->get(\'type\'), $mTypes))\n                {\n                    //$value = $mediasource->prepareOutputUrl($value);\n                    $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));\n                }\n            }\n\n        }\n        $fields[$field] = $value;\n\n    }\n    if ($toJsonPlaceholder)\n    {\n        $output[] = $fields;\n    } else\n    {\n        $fields[\'_alt\'] = $idx % 2;\n        $idx++;\n        $fields[\'_first\'] = $idx == 1 ? true : \'\';\n        $fields[\'_last\'] = $idx == $limit ? true : \'\';\n        $fields[\'idx\'] = $idx;\n        $rowtpl = $tpl;\n        //get changing tpls from field\n        if (substr($tpl, 0, 7) == \"@FIELD:\")\n        {\n            $tplField = substr($tpl, 7);\n            $rowtpl = $fields[$tplField];\n        }\n\n        if (!isset($template[$rowtpl]))\n        {\n            if (substr($rowtpl, 0, 6) == \"@FILE:\")\n            {\n                $template[$rowtpl] = file_get_contents($modx->config[\'base_path\'] . substr($rowtpl, 6));\n            } elseif (substr($rowtpl, 0, 6) == \"@CODE:\")\n            {\n                $template[$rowtpl] = substr($tpl, 6);\n            } elseif ($chunk = $modx->getObject(\'modChunk\', array(\'name\' => $rowtpl), true))\n            {\n                $template[$rowtpl] = $chunk->getContent();\n            } else\n            {\n                $template[$rowtpl] = false;\n            }\n        }\n\n        $fields = array_merge($fields, $properties);\n\n        if ($template[$rowtpl])\n        {\n            $chunk = $modx->newObject(\'modChunk\');\n            $chunk->setCacheable(false);\n            $chunk->setContent($template[$rowtpl]);\n            if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField]))\n            {\n                $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);\n            } else\n            {\n                $output[] = $chunk->process($fields);\n            }\n        } else\n        {\n            if (!empty($placeholdersKeyField))\n            {\n                $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n            } else\n            {\n                $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n            }\n        }\n    }\n\n\n}\n\n\nif ($toJsonPlaceholder)\n{\n    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));\n    return \'\';\n}\n\nif (!empty($toSeparatePlaceholders))\n{\n    $modx->toPlaceholders($output, $toSeparatePlaceholders);\n    return \'\';\n}\n/*\nif (!empty($outerTpl))\n$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));\nelse \n*/\nif (is_array($output))\n{\n    $o = implode($outputSeparator, $output);\n} else\n{\n    $o = $output;\n}\n\nif (!empty($toPlaceholder))\n{\n    $modx->setPlaceholder($toPlaceholder, $o);\n    return \'\';\n}\n\nreturn $o;',0,NULL,'',0,''),
	(15,0,0,'migxLoopCollection','',0,5,0,'/*\ngetXpdoInstanceAndAddPackage - properties\n\n$prefix\n$usecustomprefix\n$packageName\n\n\nprepareQuery - properties:\n\n$limit\n$offset\n$totalVar\n$where\n$queries\n$sortConfig\n$groupby\n$joins\n$selectfields\n$classname\n$debug\n\nrenderOutput - properties:\n\n$tpl\n$wrapperTpl\n$toSeparatePlaceholders\n$toPlaceholder\n$outputSeparator\n$placeholdersKeyField\n$toJsonPlaceholder\n$jsonVarKey\n$addfields\n\n*/\n\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n//$modx->migx = &$migx;\n\n$xpdo = $migx->getXpdoInstanceAndAddPackage($scriptProperties);\n\n$defaultcontext = \'web\';\n$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : $defaultcontext;\n\n$c = $migx->prepareQuery($xpdo,$scriptProperties);\n$rows = $migx->getCollection($c);\n\n$output = $migx->renderOutput($rows,$scriptProperties);\n\nreturn $output;',0,NULL,'',0,''),
	(16,0,0,'migxResourceMediaPath','',0,5,0,'/**\n * @name migxResourceMediaPath\n * @description Dynamically calculates the upload path for a given resource\n * \n * This Snippet is meant to dynamically calculate your baseBath attribute\n * for custom Media Sources.  This is useful if you wish to shepard uploaded\n * images to a folder dedicated to a given resource.  E.g. page 123 would \n * have its own images that page 456 could not reference.\n *\n * USAGE:\n * [[migxResourceMediaPath? &pathTpl=`assets/businesses/{id}/`]]\n * [[migxResourceMediaPath? &pathTpl=`assets/test/{breadcrumb}`]]\n * [[migxResourceMediaPath? &pathTpl=`assets/test/{breadcrumb}` &breadcrumbdepth=`2`]]\n *\n * PARAMETERS\n * &pathTpl string formatting string specifying the file path. \n *		Relative to MODX base_path\n *		Available placeholders: {id}, {pagetitle}, {parent}\n * &docid (optional) integer page id\n * &createfolder (optional) boolean whether or not to create\n */\n$pathTpl = $modx->getOption(\'pathTpl\', $scriptProperties, \'\');\n$docid = $modx->getOption(\'docid\', $scriptProperties, \'\');\n$createfolder = $modx->getOption(\'createFolder\', $scriptProperties, false);\n$path = \'\';\n$createpath = false;\n\nif (empty($pathTpl)) {\n    $modx->log(MODX_LOG_LEVEL_ERROR, \'[migxResourceMediaPath]: pathTpl not specified.\');\n    return;\n}\n\nif (empty($docid) && $modx->getPlaceholder(\'docid\')) {\n    // placeholder was set by some script\n    // warning: the parser may not render placeholders, e.g. &docid=`[[*parent]]` may fail\n    $docid = $modx->getPlaceholder(\'docid\');\n}\nif (empty($docid)) {\n\n    //on frontend\n    if (is_object($modx->resource)) {\n        $docid = $modx->resource->get(\'id\');\n    }\n    //on backend\n    else {\n        $createpath = $createfolder;\n        // We do this to read the &id param from an Ajax request\n        $parsedUrl = parse_url($_SERVER[\'HTTP_REFERER\']);\n        parse_str($parsedUrl[\'query\'], $parsedQuery);\n\n        if (isset($parsedQuery[\'amp;id\'])) {\n            $docid = (int)$parsedQuery[\'amp;id\'];\n        } elseif (isset($parsedQuery[\'id\'])) {\n            $docid = (int)$parsedQuery[\'id\'];\n        }\n    }\n}\n\nif (empty($docid)) {\n    $modx->log(MODX_LOG_LEVEL_ERROR, \'[migxResourceMediaPath]: docid could not be determined.\');\n    return;\n}\n\nif ($resource = $modx->getObject(\'modResource\', $docid)) {\n    $path = $pathTpl;\n    $ultimateParent = \'\';\n    if (strstr($path, \'{breadcrumb}\') || strstr($path, \'{ultimateparent}\')) {\n        $parentids = $modx->getParentIds($docid);\n        $breadcrumbdepth = $modx->getOption(\'breadcrumbdepth\', $scriptProperties, count($parentids));\n        $breadcrumbdepth = $breadcrumbdepth > count($parentids) ? count($parentids) : $breadcrumbdepth;\n        if (count($parentids) > 1) {\n            $parentids = array_reverse($parentids);\n            $parentids[] = $docid;\n            $ultimateParent = $parentids[1];\n        } else {\n            $ultimateParent = $docid;\n            $parentids = array();\n            $parentids[] = $docid;\n        }\n    }\n\n    if (strstr($path, \'{breadcrumb}\')) {\n        $breadcrumbpath = \'\';\n        for ($i = 1; $i <= $breadcrumbdepth; $i++) {\n            $breadcrumbpath .= $parentids[$i] . \'/\';\n        }\n        $path = str_replace(\'{breadcrumb}\', $breadcrumbpath, $path);\n\n    } else {\n        $path = str_replace(\'{id}\', $docid, $path);\n        $path = str_replace(\'{pagetitle}\', $resource->get(\'pagetitle\'), $path);\n        $path = str_replace(\'{alias}\', $resource->get(\'alias\'), $path);\n        $path = str_replace(\'{parent}\', $resource->get(\'parent\'), $path);\n        $path = str_replace(\'{ultimateparent}\', $ultimateParent, $path);\n        if ($template = $resource->getOne(\'Template\')) {\n            $path = str_replace(\'{templatename}\', $template->get(\'templatename\'), $path);\n        }\n        if ($user = $modx->user) {\n            $path = str_replace(\'{username}\', $modx->user->get(\'username\'), $path);\n        }\n    }\n\n    $fullpath = $modx->getOption(\'base_path\') . $path;\n    \n    if ($createpath && !file_exists($fullpath)) {\n        $permissions = $modx->getOption(\'new_folder_permissions\', null, \'0755\', true);\n        if (!@mkdir($fullpath, $permissions, true)) {\n            $modx->log(MODX_LOG_LEVEL_ERROR, sprintf(\'[migxResourceMediaPath]: could not create directory %s).\', $fullpath));\n\n        }\n    }\n\n    return $path;\n} else {\n    $modx->log(MODX_LOG_LEVEL_ERROR, sprintf(\'[migxResourceMediaPath]: resource not found (page id %s).\', $docid));\n    return;\n}',0,NULL,'',0,''),
	(17,0,0,'migxImageUpload','',0,5,0,'return include $modx->getOption(\'core_path\').\'components/migx/model/imageupload/imageupload.php\';',0,NULL,'',0,''),
	(18,0,0,'migxChunklistToJson','',0,5,0,'$category = $modx->getOption(\'category\', $scriptProperties, \'\');\n$format = $modx->getOption(\'format\', $scriptProperties, \'json\');\n\n$classname = \'modChunk\';\n$rows = array();\n$output = \'\';\n\n$c = $modx->newQuery($classname);\n$c->select($modx->getSelectColumns($classname, $classname, \'\', array(\'id\', \'name\')));\n$c->sortby(\'name\');\n\nif (!empty($category)) {\n    $c->where(array(\'category\' => $category));\n}\n//$c->prepare();echo $c->toSql();\nif ($collection = $modx->getCollection($classname, $c)) {\n    $i = 0;\n\n    switch ($format) {\n        case \'json\':\n            foreach ($collection as $object) {\n                $row[\'MIGX_id\'] = (string )$i;\n                $row[\'name\'] = $object->get(\'name\');\n                $row[\'selected\'] = \'0\';\n                $rows[] = $row;\n                $i++;\n            }\n            $output = $modx->toJson($rows);\n            break;\n        \n        case \'optionlist\':\n            foreach ($collection as $object) {\n                $rows[] = $object->get(\'name\');\n                $i++;\n            }\n            $output = implode(\'||\',$rows);      \n        break;\n            \n    }\n\n\n}\n\nreturn $output;',0,NULL,'',0,''),
	(19,0,0,'migxSwitchDetailChunk','',0,5,0,'//[[migxSwitchDetailChunk? &detailChunk=`detailChunk` &listingChunk=`listingChunk`]]\n\n\n$properties[\'migx_id\'] = $modx->getOption(\'migx_id\',$_GET,\'\');\n\nif (!empty($properties[\'migx_id\'])){\n    $output = $modx->getChunk($detailChunk,$properties);\n}\nelse{\n    $output = $modx->getChunk($listingChunk);\n}\n\nreturn $output;',0,NULL,'',0,''),
	(20,0,0,'getSwitchColumnCol','',0,5,0,'$scriptProperties = $_REQUEST;\n$col = \'\';\n// special actions, for example the showSelector - action\n$tempParams = $modx->getOption(\'tempParams\', $scriptProperties, \'\');\n\nif (!empty($tempParams)) {\n    $tempParams = $modx->fromJson($tempParams);\n    $col = $modx->getOption(\'col\', $tempParams, \'\');\n}\n\nreturn $col;',0,NULL,'',0,''),
	(21,0,0,'getDayliMIGXrecord','',0,5,0,'/**\n * getDayliMIGXrecord\n *\n * Copyright 2009-2011 by Bruno Perner <b.perner@gmx.de>\n *\n * getDayliMIGXrecord is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * getDayliMIGXrecord is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * getDayliMIGXrecord; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package migx\n */\n/**\n * getDayliMIGXrecord\n *\n * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution \n *\n * @version 1.0\n * @author Bruno Perner <b.perner@gmx.de>\n * @copyright Copyright &copy; 2012\n * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License\n * version 2 or (at your option) any later version.\n * @package migx\n */\n\n/*example: [[!getDayliMIGXrecord? &tvname=`myTV`&tpl=`@CODE:<img src=\"[[+image]]\"/>` &randomize=`1`]]*/\n/* get default properties */\n\n\n$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');\n$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);\n$where = $modx->getOption(\'where\', $scriptProperties, \'\');\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');\n$sort = !empty($sort) ? $modx->fromJSON($sort) : array();\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\n$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));\n$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');\n\n$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\nif (!($migx instanceof Migx))\n    return \'\';\n$migx->working_context = $modx->resource->get(\'context_key\');\n\nif (!empty($tvname)) {\n    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {\n\n        /*\n        *   get inputProperties\n        */\n\n\n        $properties = $tv->get(\'input_properties\');\n        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();\n\n        $migx->config[\'configs\'] = $properties[\'configs\'];\n        $migx->loadConfigs();\n        // get tabs from file or migx-config-table\n        $formtabs = $migx->getTabs();\n        if (empty($formtabs)) {\n            //try to get formtabs and its fields from properties\n            $formtabs = $modx->fromJSON($properties[\'formtabs\']);\n        }\n\n        //$tv->setCacheable(false);\n        //$outputvalue = $tv->renderOutput($docid);\n        \n        $tvresource = $modx->getObject(\'modTemplateVarResource\', array(\n            \'tmplvarid\' => $tv->get(\'id\'),\n            \'contentid\' => $docid,\n            ));\n\n\n        $outputvalue = $tvresource->get(\'value\');\n        \n        /*\n        *   get inputTvs \n        */\n        $inputTvs = array();\n        if (is_array($formtabs)) {\n\n            //multiple different Forms\n            // Note: use same field-names and inputTVs in all forms\n            $inputTvs = $migx->extractInputTvs($formtabs);\n        }\n        $migx->source = $tv->getSource($migx->working_context, false);\n\n        if (empty($outputvalue)) {\n            return \'\';\n        }\n\n        $items = $modx->fromJSON($outputvalue);\n\n\n        //is there an active item for the current date?\n        $activedate = $modx->getOption(\'activedate\', $scriptProperties, strftime(\'%Y/%m/%d\'));\n        //$activedate = $modx->getOption(\'activedate\', $_GET, strftime(\'%Y/%m/%d\'));\n        $activewhere = array();\n        $activewhere[\'activedate\'] = $activedate;\n        $activewhere[\'activated\'] = \'1\';\n        $activeitems = $migx->filterItems($activewhere, $items);\n\n        if (count($activeitems) == 0) {\n\n            $activeitems = array();\n            // where filter\n            if (is_array($where) && count($where) > 0) {\n                $items = $migx->filterItems($where, $items);\n            }\n\n            $tempitems = array();\n            $count = count($items);\n            $emptycount = 0;\n            $latestdate = $activedate;\n            $nextdate = strtotime($latestdate);\n            foreach ($items as $item) {\n                //empty all dates and active-states which are older than today\n                if (!empty($item[\'activedate\']) && $item[\'activedate\'] < $activedate) {\n                    $item[\'activated\'] = \'0\';\n                    $item[\'activedate\'] = \'\';\n                }\n                if (empty($item[\'activedate\'])) {\n                    $emptycount++;\n                }\n                if ($item[\'activedate\'] > $latestdate) {\n                    $latestdate = $item[\'activedate\'];\n                    $nextdate = strtotime($latestdate) + (24 * 60 * 60);\n                }\n                if ($item[\'activedate\'] == $activedate) {\n                    $item[\'activated\'] = \'1\';\n                    $activeitems[] = $item;\n                }\n                $tempitems[] = $item;\n            }\n\n            //echo \'<pre>\' . print_r($tempitems, 1) . \'</pre>\';\n\n            $items = $tempitems;\n\n\n            //are there more than half of all items with empty activedates\n\n            if ($emptycount >= $count / 2) {\n\n                // sort items\n                if (is_array($sort) && count($sort) > 0) {\n                    $items = $migx->sortDbResult($items, $sort);\n                }\n                if (count($items) > 0) {\n                    //shuffle items\n                    if ($randomize) {\n                        shuffle($items);\n                    }\n                }\n\n                $tempitems = array();\n                foreach ($items as $item) {\n                    if (empty($item[\'activedate\'])) {\n                        $item[\'activedate\'] = strftime(\'%Y/%m/%d\', $nextdate);\n                        $nextdate = $nextdate + (24 * 60 * 60);\n                        if ($item[\'activedate\'] == $activedate) {\n                            $item[\'activated\'] = \'1\';\n                            $activeitems[] = $item;\n                        }\n                    }\n\n                    $tempitems[] = $item;\n                }\n\n                $items = $tempitems;\n            }\n\n            //$resource = $modx->getObject(\'modResource\', $docid);\n            //echo $modx->toJson($items);\n            $sort = \'[{\"sortby\":\"activedate\"}]\';\n            $items = $migx->sortDbResult($items, $modx->fromJson($sort));\n\n            //echo \'<pre>\' . print_r($items, 1) . \'</pre>\';\n\n            $tv->setValue($docid, $modx->toJson($items));\n            $tv->save();\n\n        }\n    }\n\n}\n\n\n$properties = array();\nforeach ($scriptProperties as $property => $value) {\n    $properties[\'property.\' . $property] = $value;\n}\n\n$output = \'\';\n\nforeach ($activeitems as $key => $item) {\n\n    $fields = array();\n    foreach ($item as $field => $value) {\n        $value = is_array($value) ? implode(\'||\', $value) : $value; //handle arrays (checkboxes, multiselects)\n        if ($processTVs && isset($inputTvs[$field])) {\n            if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$field][\'inputTV\']))) {\n\n            } else {\n                $tv = $modx->newObject(\'modTemplateVar\');\n                $tv->set(\'type\', $inputTvs[$field][\'inputTVtype\']);\n            }\n            $inputTV = $inputTvs[$field];\n\n            $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');\n            //don\'t manipulate any urls here\n            $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');\n            $tv->set(\'default_text\', $value);\n            $value = $tv->renderOutput($docid);\n            //set option back\n            $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);\n            //now manipulate urls\n            if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {\n                $mTypes = explode(\',\', $mTypes);\n                if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {\n                    //$value = $mediasource->prepareOutputUrl($value);\n                    $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));\n                }\n            }\n\n        }\n        $fields[$field] = $value;\n\n    }\n\n    $rowtpl = $tpl;\n    //get changing tpls from field\n    if (substr($tpl, 0, 7) == \"@FIELD:\") {\n        $tplField = substr($tpl, 7);\n        $rowtpl = $fields[$tplField];\n    }\n\n    if (!isset($template[$rowtpl])) {\n        if (substr($rowtpl, 0, 6) == \"@FILE:\") {\n            $template[$rowtpl] = file_get_contents($modx->config[\'base_path\'] . substr($rowtpl, 6));\n        } elseif (substr($rowtpl, 0, 6) == \"@CODE:\") {\n            $template[$rowtpl] = substr($tpl, 6);\n        } elseif ($chunk = $modx->getObject(\'modChunk\', array(\'name\' => $rowtpl), true)) {\n            $template[$rowtpl] = $chunk->getContent();\n        } else {\n            $template[$rowtpl] = false;\n        }\n    }\n\n    $fields = array_merge($fields, $properties);\n\n    if ($template[$rowtpl]) {\n        $chunk = $modx->newObject(\'modChunk\');\n        $chunk->setCacheable(false);\n        $chunk->setContent($template[$rowtpl]);\n        $output .= $chunk->process($fields);\n\n    } else {\n        $output .= \'<pre>\' . print_r($fields, 1) . \'</pre>\';\n\n    }\n\n\n}\n\n\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $output);\n    return \'\';\n}\n\nreturn $output;',0,NULL,'',0,''),
	(22,0,0,'filterbytag','',0,5,0,'if (!is_array($subject)) {\n    $subject = explode(\',\',str_replace(array(\'||\',\' \'),array(\',\',\'\'),$subject));\n}\n\nreturn (in_array($operand,$subject));',0,NULL,'',0,''),
	(23,0,0,'migxObjectMediaPath','',0,5,0,'$pathTpl = $modx->getOption(\'pathTpl\', $scriptProperties, \'\');\n$objectid = $modx->getOption(\'objectid\', $scriptProperties, \'\');\n$createfolder = $modx->getOption(\'createFolder\', $scriptProperties, \'1\');\n$path = \'\';\n$createpath = false;\nif (empty($objectid) && $modx->getPlaceholder(\'objectid\')) {\n    // placeholder was set by some script on frontend for example\n    $objectid = $modx->getPlaceholder(\'objectid\');\n}\nif (empty($objectid) && isset($_REQUEST[\'object_id\'])) {\n    $objectid = $_REQUEST[\'object_id\'];\n}\n\n\n\nif (empty($objectid)) {\n\n    //set Session - var in fields.php - processor\n    if (isset($_SESSION[\'migxWorkingObjectid\'])) {\n        $objectid = $_SESSION[\'migxWorkingObjectid\'];\n        $createpath = !empty($createfolder);\n    }\n\n}\n\n\n$path = str_replace(\'{id}\', $objectid, $pathTpl);\n\n$fullpath = $modx->getOption(\'base_path\') . $path;\n\nif ($createpath && !file_exists($fullpath)) {\n    mkdir($fullpath, 0755, true);\n}\n\nreturn $path;',0,NULL,'',0,''),
	(24,0,0,'exportMIGX2db','',0,5,0,'/**\n * exportMIGX2db\n *\n * Copyright 2014 by Bruno Perner <b.perner@gmx.de>\n * \n * Sponsored by Simon Wurster <info@wurster-medien.de>\n *\n * exportMIGX2db is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * exportMIGX2db is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * exportMIGX2db; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package migx\n */\n/**\n * exportMIGX2db\n *\n * export Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string to db-table \n *\n * @version 1.0\n * @author Bruno Perner <b.perner@gmx.de>\n * @copyright Copyright &copy; 2014\n * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License\n * version 2 or (at your option) any later version.\n * @package migx\n */\n\n/*\n[[!exportMIGX2db? \n&tvname=`references` \n&resources=`25` \n&packageName=`projekte`\n&classname=`Projekt` \n&migx_id_field=`migx_id` \n&renamed_fields=`{\"Firmen-URL\":\"Firmen_url\",\"Projekt-URL\":\"Projekt_URL\",\"main-image\":\"main_image\"}`\n]]\n*/\n\n\n$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');\n$resources = $modx->getOption(\'resources\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : \'\'));\n$resources = explode(\',\', $resources);\n$prefix = isset($scriptProperties[\'prefix\']) ? $scriptProperties[\'prefix\'] : null;\n$packageName = $modx->getOption(\'packageName\', $scriptProperties, \'\');\n$classname = $modx->getOption(\'classname\', $scriptProperties, \'\');\n$value = $modx->getOption(\'value\', $scriptProperties, \'\');\n$migx_id_field = $modx->getOption(\'migx_id_field\', $scriptProperties, \'\');\n$pos_field = $modx->getOption(\'pos_field\', $scriptProperties, \'\');\n$renamed_fields = $modx->getOption(\'renamed_fields\', $scriptProperties, \'\');\n\n$packagepath = $modx->getOption(\'core_path\') . \'components/\' . $packageName .\n    \'/\';\n$modelpath = $packagepath . \'model/\';\n\n$modx->addPackage($packageName, $modelpath, $prefix);\n$added = 0;\n$modified = 0;\n\nforeach ($resources as $docid) {\n    \n    $outputvalue = \'\';\n    if (count($resources)==1){\n        $outputvalue = $value;    \n    }\n    \n    if (!empty($tvname)) {\n        if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {\n\n            $outputvalue = empty($outputvalue) ? $tv->renderOutput($docid) : $outputvalue;\n        }\n    }\n\n    if (!empty($outputvalue)) {\n        $renamed = !empty($renamed_fields) ? $modx->fromJson($renamed_fields) : array();\n\n        $items = $modx->fromJSON($outputvalue);\n        $pos = 1;\n        $searchfields = array();\n        if (is_array($items)) {\n            foreach ($items as $fields) {\n                $search = array();\n                if (!empty($migx_id_field)) {\n                    $search[$migx_id_field] = $fields[\'MIGX_id\'];\n                }\n                if (!empty($resource_id_field)) {\n                    $search[$resource_id_field] = $docid;\n                }\n                if (!empty($migx_id_field) && $object = $modx->getObject($classname, $search)) {\n                    $mode = \'mod\';\n                } else {\n                    $object = $modx->newObject($classname);\n                    $object->fromArray($search);\n                    $mode = \'add\';\n                }\n                foreach ($fields as $field => $value) {\n                    $fieldname = array_key_exists($field, $renamed) ? $renamed[$field] : $field;\n                    $object->set($fieldname, $value);\n                }\n                if (!empty($pos_field)) {\n                    $object->set($pos_field,$pos) ;\n                }                \n                if ($object->save()) {\n                    if ($mode == \'add\') {\n                        $added++;\n                    } else {\n                        $modified++;\n                    }\n                }\n                $pos++;\n            }\n            \n        }\n    }\n}\n\n\nreturn $added . \' rows added to db, \' . $modified . \' existing rows actualized\';',0,NULL,'',0,''),
	(25,0,0,'preparedatewhere','',0,5,0,'$name = $modx->getOption(\'name\', $scriptProperties, \'\');\n$date = $modx->getOption($name . \'_date\', $_REQUEST, \'\');\n$dir = str_replace(\'T\', \' \', $modx->getOption($name . \'_dir\', $_REQUEST, \'\'));\n\nif (!empty($date) && !empty($dir) && $dir != \'all\') {\n    switch ($dir) {\n        case \'=\':\n            $where = array(\n            \'enddate:>=\' => strftime(\'%Y-%m-%d 00:00:00\',strtotime($date)),\n            \'startdate:<=\' => strftime(\'%Y-%m-%d 23:59:59\',strtotime($date))\n            );\n            break;\n        case \'>=\':\n            $where = array(\n            \'enddate:>=\' => strftime(\'%Y-%m-%d 00:00:00\',strtotime($date))\n            );\n            break;\n        case \'<=\':\n            $where = array(\n            \'startdate:<=\' => strftime(\'%Y-%m-%d 23:59:59\',strtotime($date))\n            );            \n            break;\n\n    }\n\n    return $modx->toJson($where);\n}',0,NULL,'',0,''),
	(26,0,0,'migxJsonToPlaceholders','',0,5,0,'$value = $modx->getOption(\'value\',$scriptProperties,\'\');\n$prefix = $modx->getOption(\'prefix\',$scriptProperties,\'\');\n\n//$modx->setPlaceholders($modx->fromJson($value),$prefix,\'\',true);\n\n$values = $modx->fromJson($value);\nif (is_array($values)){\n    foreach ($values as $key => $value){\n        $value = $value == null ? \'\' : $value;\n        $modx->setPlaceholder($prefix . $key, $value);\n    }\n}',0,NULL,'',0,''),
	(27,0,0,'migxGetCollectionTree','',0,5,0,'/**\n * migxGetCollectionTree\n *\n * Copyright 2014 by Bruno Perner <b.perner@gmx.de>\n *\n * migxGetCollectionTree is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * migxGetCollectionTree is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * migxGetCollectionTree; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package migx\n */\n/**\n * migxGetCollectionTree\n *\n *          display nested items from different objects. The tree-schema is defined by a json-property. \n *\n * @version 1.0.0\n * @author Bruno Perner <b.perner@gmx.de>\n * @copyright Copyright &copy; 2014\n * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License\n * version 2 or (at your option) any later version.\n * @package migx\n */\n\n$treeSchema = $modx->getOption(\'treeSchema\', $scriptProperties, \'\');\n$treeSchema = $modx->fromJson($treeSchema);\n\n$scriptProperties[\'current\'] = $modx->getOption(\'current\', $scriptProperties, \'\');\n$scriptProperties[\'currentClassname\'] = $modx->getOption(\'currentClassname\', $scriptProperties, \'\');\n$scriptProperties[\'currentKeyField\'] = $modx->getOption(\'currentKeyField\', $scriptProperties, \'id\');\n$return = $modx->getOption(\'return\', $scriptProperties, \'parsed\'); //parsed,json,arrayprint\n\n/*\nExamples:\n\nGet Resource-Tree, 4 levels deep\n\n[[!migxGetCollectionTree?\n&current=`57`\n&currentClassname=`modResource`\n&treeSchema=`\n{\n\"classname\": \"modResource\",\n\"debug\": \"1\",\n\"tpl\": \"mgctResourceTree\",\n\"wrapperTpl\": \"@CODE:<ul>[[+output]]</ul>\",\n\"selectfields\": \"id,pagetitle\",\n\"where\": {\n\"parent\": \"0\",\n\"published\": \"1\",\n\"deleted\": \"0\"\n},\n\"_branches\": [{\n\"alias\": \"children\",\n\"classname\": \"modResource\",\n\"local\": \"parent\",\n\"foreign\": \"id\",\n\"tpl\": \"mgctResourceTree\",\n\"debug\": \"1\",\n\"selectfields\": \"id,pagetitle,parent\",\n\"_branches\": [{\n\"alias\": \"children\",\n\"classname\": \"modResource\",\n\"local\": \"parent\",\n\"foreign\": \"id\",\n\"tpl\": \"mgctResourceTree\",\n\"debug\": \"1\",\n\"selectfields\": \"id,pagetitle,parent\",\n\"where\": {\n\"published\": \"1\",\n\"deleted\": \"0\"\n},\n\"_branches\": [{\n\"alias\": \"children\",\n\"classname\": \"modResource\",\n\"local\": \"parent\",\n\"foreign\": \"id\",\n\"tpl\": \"mgctResourceTree\",\n\"debug\": \"1\",\n\"selectfields\": \"id,pagetitle,parent\",\n\"where\": {\n\"published\": \"1\",\n\"deleted\": \"0\"\n}\n}]\n}]\n}]\n}\n`]]\n\nthe chunk mgctResourceTree:\n<li class=\"[[+_activelabel]] [[+_currentlabel]]\" ><a href=\"[[~[[+id]]]]\">[[+pagetitle]]([[+id]])</a></li>\n[[+innercounts.children:gt=`0`:then=`\n<ul>[[+innerrows.children]]</ul>\n`:else=``]]\n\nget all Templates and its Resources:\n\n[[!migxGetCollectionTree?\n&treeSchema=`\n{\n\"classname\": \"modTemplate\",\n\"debug\": \"1\",\n\"tpl\": \"@CODE:<h3>[[+templatename]]</h3><ul>[[+innerrows.resource]]</ul>\",\n\"selectfields\": \"id,templatename\",\n\"_branches\": [{\n\"alias\": \"resource\",\n\"classname\": \"modResource\",\n\"local\": \"template\",\n\"foreign\": \"id\",\n\"tpl\": \"@CODE:<li>[[+pagetitle]]([[+id]])</li>\",\n\"debug\": \"1\",\n\"selectfields\": \"id,pagetitle,template\"\n}]\n}\n`]]\n*/\n\nif (!class_exists(\'MigxGetCollectionTree\')) {\n    class MigxGetCollectionTree\n    {\n        function __construct(modX & $modx, array $config = array())\n        {\n            $this->modx = &$modx;\n            $this->config = $config;\n        }\n\n        function getBranch($branch, $foreigns = array(), $level = 1)\n        {\n\n            $rows = array();\n\n            if (count($foreigns) > 0) {\n                $modx = &$this->modx;\n\n                $local = $modx->getOption(\'local\', $branch, \'\');\n                $where = $modx->getOption(\'where\', $branch, array());\n                $where = !empty($where) && !is_array($where) ? $modx->fromJSON($where) : $where;\n                $where[] = array($local . \':IN\' => $foreigns);\n\n                $branch[\'where\'] = $modx->toJson($where);\n\n                $level++;\n                /*\n                if ($levelFromCurrent > 0){\n                $levelFromCurrent++;    \n                }\n                */\n\n                $rows = $this->getRows($branch, $level);\n            }\n\n            return $rows;\n        }\n\n        function getRows($scriptProperties, $level)\n        {\n            $migx = &$this->migx;\n            $modx = &$this->modx;\n\n            $current = $modx->getOption(\'current\', $this->config, \'\');\n            $currentKeyField = $modx->getOption(\'currentKeyField\', $this->config, \'id\');\n            $currentlabel = $modx->getOption(\'currentlabel\', $this->config, \'current\');\n            $classname = $modx->getOption(\'classname\', $scriptProperties, \'\');\n            $currentClassname = !empty($this->config[\'currentClassname\']) ? $this->config[\'currentClassname\'] : $classname;\n\n            $activelabel = $modx->getOption(\'activelabel\', $this->config, \'active\');\n            $return = $modx->getOption(\'return\', $this->config, \'parsed\');\n\n            $xpdo = $migx->getXpdoInstanceAndAddPackage($scriptProperties);\n            $c = $migx->prepareQuery($xpdo, $scriptProperties);\n            $rows = $migx->getCollection($c);\n\n            $branches = $modx->getOption(\'_branches\', $scriptProperties, array());\n\n            $collectedSubrows = array();\n            foreach ($branches as $branch) {\n                $foreign = $modx->getOption(\'foreign\', $branch, \'\');\n                $local = $modx->getOption(\'local\', $branch, \'\');\n                $alias = $modx->getOption(\'alias\', $branch, \'\');\n                //$activeonly = $modx->getOption(\'activeonly\', $branch, \'\');\n                $foreigns = array();\n                foreach ($rows as $row) {\n                    $foreigns[] = $row[$foreign];\n                }\n\n                $subrows = $this->getBranch($branch, $foreigns, $level);\n                foreach ($subrows as $subrow) {\n\n                    $collectedSubrows[$subrow[$local]][] = $subrow;\n                    $subrow[\'_active\'] = $modx->getOption(\'_active\', $subrow, \'0\');\n                    /*\n                    if (!empty($activeonly) && $subrow[\'_active\'] != \'1\') {\n                    $output = \'\';\n                    } else {\n                    $collectedSubrows[$subrow[$local]][] = $subrow;\n                    }\n                    */\n                    if ($subrow[\'_active\'] == \'1\') {\n                        //echo \'active subrow:<pre>\' . print_r($subrow,1) . \'</pre>\';\n                        $activesubrow[$subrow[$local]] = true;\n                    }\n                    if ($subrow[\'_current\'] == \'1\') {\n                        //echo \'active subrow:<pre>\' . print_r($subrow,1) . \'</pre>\';\n                        $currentsubrow[$subrow[$local]] = true;\n                    }\n\n\n                }\n                //insert subrows\n                $temprows = $rows;\n                $rows = array();\n                foreach ($temprows as $row) {\n                    if (isset($collectedSubrows[$row[$foreign]])) {\n                        $row[\'_active\'] = \'0\';\n                        $row[\'_currentparent\'] = \'0\';\n                        if (isset($activesubrow[$row[$foreign]]) && $activesubrow[$row[$foreign]]) {\n                            $row[\'_active\'] = \'1\';\n                            //echo \'active row:<pre>\' . print_r($row,1) . \'</pre>\';\n                        }\n                        if (isset($currentsubrow[$row[$foreign]]) && $currentsubrow[$row[$foreign]]) {\n                            $row[\'_currentparent\'] = \'1\';\n                            //echo \'active row:<pre>\' . print_r($row,1) . \'</pre>\';\n                        }\n\n                        //render innerrows\n                        //$output = $migx->renderOutput($collectedSubrows[$row[$foreign]],$scriptProperties);\n                        //$output = $collectedSubrows[$row[$foreign]];\n\n                        $row[\'innercounts.\' . $alias] = count($collectedSubrows[$row[$foreign]]);\n                        $row[\'_scriptProperties\'][$alias] = $branch;\n                        /*\n                        switch ($return) {\n                        case \'parsed\':\n                        $output = $migx->renderOutput($collectedSubrows[$row[$foreign]], $branch);\n                        //$subbranches = $modx->getOption(\'_branches\', $branch, array());\n                        //if there are any placeholders left with the same alias from subbranch, remove them\n                        $output = str_replace(\'[[+innerrows.\' . $alias . \']]\', \'\', $output);\n                        break;\n                        case \'json\':\n                        case \'arrayprint\':\n                        $output = $collectedSubrows[$row[$foreign]];\n                        break;\n                        }\n                        */\n                        $output = $collectedSubrows[$row[$foreign]];\n\n                        $row[\'innerrows.\' . $alias] = $output;\n\n                    }\n                    $rows[] = $row;\n                }\n\n            }\n\n            $temprows = $rows;\n            $rows = array();\n            foreach ($temprows as $row) {\n                //add additional placeholders\n                $row[\'_level\'] = $level;\n                $row[\'_active\'] = $modx->getOption(\'_active\', $row, \'0\');\n                if ($currentClassname == $classname && $row[$currentKeyField] == $current) {\n                    $row[\'_current\'] = \'1\';\n                    $row[\'_currentlabel\'] = $currentlabel;\n                    $row[\'_active\'] = \'1\';\n                } else {\n                    $row[\'_current\'] = \'0\';\n                    $row[\'_currentlabel\'] = \'\';\n                }\n                if ($row[\'_active\'] == \'1\') {\n                    $row[\'_activelabel\'] = $activelabel;\n                } else {\n                    $row[\'_activelabel\'] = \'\';\n                }\n                $rows[] = $row;\n            }\n\n            return $rows;\n        }\n\n        function renderRow($row, $levelFromCurrent = 0)\n        {\n            $migx = &$this->migx;\n            $modx = &$this->modx;\n            $return = $modx->getOption(\'return\', $this->config, \'parsed\');\n            $branchProperties = $modx->getOption(\'_scriptProperties\', $row, array());\n            $current = $modx->getOption(\'_current\', $row, \'0\');\n            $currentparent = $modx->getOption(\'_currentparent\', $row, \'0\');\n            $levelFromCurrent = $current == \'1\' ? 1 : $levelFromCurrent;\n            $row[\'_levelFromCurrent\'] = $levelFromCurrent;\n            foreach ($branchProperties as $alias => $properties) {\n                $innerrows = $modx->getOption(\'innerrows.\' . $alias, $row, array());\n                $subrows = $this->renderRows($innerrows, $properties, $levelFromCurrent, $currentparent);\n                if ($return == \'parsed\') {\n                    $subrows = $migx->renderOutput($subrows, $properties);\n                }\n                $row[\'innerrows.\' . $alias] = $subrows;\n            }\n\n            return $row;\n        }\n\n        function renderRows($rows, $scriptProperties, $levelFromCurrent = 0, $siblingOfCurrent = \'0\')\n        {\n\n            $modx = &$this->modx;\n            $temprows = $rows;\n            $rows = array();\n            if ($levelFromCurrent > 0) {\n                $levelFromCurrent++;\n            }\n            foreach ($temprows as $row) {\n                $row[\'_siblingOfCurrent\'] = $siblingOfCurrent;\n                $row = $this->renderRow($row, $levelFromCurrent);\n                $rows[] = $row;\n            }\n            return $rows;\n        }\n    }\n}\n\n$instance = new MigxGetCollectionTree($modx, $scriptProperties);\n\nif (is_array($treeSchema)) {\n    $scriptProperties = $treeSchema;\n\n    $migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);\n    if (!($migx instanceof Migx))\n        return \'\';\n\n    $defaultcontext = \'web\';\n    $migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : $defaultcontext;\n    $instance->migx = &$migx;\n\n    $level = 1;\n    $scriptProperties[\'alias\'] = \'row\';\n    $rows = $instance->getRows($scriptProperties, $level);\n    $row = array();\n    $row[\'innercounts.row\'] = count($rows);\n    $row[\'innerrows.row\'] = $rows;\n    $row[\'_scriptProperties\'][\'row\'] = $scriptProperties;\n\n    $rows = $instance->renderRow($row);\n\n    $output = \'\';\n    switch ($return) {\n        case \'parsed\':\n            $output = $modx->getOption(\'innerrows.row\', $rows, \'\');\n            break;\n        case \'json\':\n            $output = $modx->toJson($rows);\n            break;\n        case \'arrayprint\':\n            $output = \'<pre>\' . print_r($rows, 1) . \'</pre>\';\n            break;\n    }\n\n    return $output;\n\n}',0,NULL,'',0,''),
	(28,1,0,'GetPageTitle','',0,0,0,'/**\n * MY_GetPageTitle\n *\n * DESCRIPTION\n *\n * This snippet returns the title of a page matching an id\n *\n * USAGE:\n *\n * [[!MY_GetPageTitle? &id=`[[+parent]]`]]\n */\n \n$page = $modx->getObject(\'modResource\', $id);\nif(!empty($page)) {\n    return $page->get(\'pagetitle\');\n}\nreturn \'\';',0,'a:0:{}','',1,'assets/snippets/getPageTitle.html');

/*!40000 ALTER TABLE `modx_site_snippets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_templates`;

CREATE TABLE `modx_site_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `templatename` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Template',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `template_type` int(11) NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `properties` text,
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `templatename` (`templatename`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_templates` WRITE;
/*!40000 ALTER TABLE `modx_site_templates` DISABLE KEYS */;

INSERT INTO `modx_site_templates` (`id`, `source`, `property_preprocess`, `templatename`, `description`, `editor_type`, `category`, `icon`, `template_type`, `content`, `locked`, `properties`, `static`, `static_file`)
VALUES
	(1,0,0,'BaseTemplate','Template',0,0,'',0,'<html>\n<head>\n<title>[[++site_name]] - [[*pagetitle]]</title>\n<base href=\"[[++site_url]]\" />\n</head>\n<body>\n[[*content]]\n</body>\n</html>',0,NULL,0,''),
	(2,1,0,'Default','',0,0,'',0,'[[$doc_head]]\n<body>\n	[[$navigation]]\n	[[$header]]\n\n	<div class=\"container\">\n		<div class=\"starter-template\">\n			<h1>[[*pagetitle]]</h1>\n			<div class = \"lead\">\n				[[*content]]\n			</div>\n		</div>\n	</div><!-- /.container -->\n\n	[[$footer]]\n	[[$doc_footer]]',0,'a:0:{}',1,'assets/templates/default.html'),
	(3,1,0,'Homepage','',0,0,'',0,'[[$doc_head]]\n    <body class=\"home\">\n        [[$header]]\n        <section class=\"hero\" style=\"background-image:url(\'[[*header-image]]\')\">\n            <div class=\"row-fluid wrap\">\n                <div class=\"span9\">\n                    [[*promotion]]\n                </div>\n            </div>\n        </section>\n        [[$quote-bar]]\n        <section class=\"content\">\n            <div class=\"row-fluid wrap\">\n                <div>\n                    [[*content]]\n                </div>\n                <h1>Our Services</h1>\n                <div>\n                    [[*services-content]]\n                </div>\n                <div class=\"row-fluid\">\n                    <div class=\"span4\">\n                        <h2>Foundation Repair</h2>\n                        <ul>\n                        [[getResources? &parents=`2` &sortby=`{\"menuindex\":\"ASC\"}` &limit=`0` &tpl=`nav-inner-home` &includeTVs=`1` &processTVs=`1` &tvPrefix=`` &tplFirst=`nav-inner-home-first`]]\n                    </ul>\n                    </div>\n                    <div class=\"span7 offset1\">\n                        <div class=\"tab-content\">\n                            [[getResources? &parents=`2` &sortby=`{\"menuindex\":\"ASC\"}` &limit=`0` &tpl=`home-service` &includeTVs=`1` &processTVs=`1` &tvPrefix=`` &tplFirst=`home-service-first`]]\n                            \n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section>\n        <section class=\"testimonials\">\n            <div class=\"row-fluid wrap flexslider\">\n                <ul class=\"slides\">\n                        [[getResources? &parents=`4` &limit=`0` &tpl=`testimonial` &includeTVs=`1` &processTVs=`1` &tvPrefix=`` &includeContent=`1` &sortby=`RAND()`]]\n                </ul>\n            </div>\n        </section>\n        <section class=\"about\">\n            <div class=\"row-fluid wrap\">\n                <div class=\"span4\"><img src=\"[[*about-graphic]]\" alt=\"\"></div>\n                <div class=\"span7 offset1\">\n                    [[*about-content]]\n                    \n                </div>\n            </div>\n        </section>\n        [[$footer]]\n        \n        [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/homepage.html'),
	(8,1,0,'Testimonial','',0,0,'',0,'[[$doc_head]]\n    <body class=\"testimonial\">\n        [[$header]]\n        [[$intro-interior]]\n        [[$quote-bar]]\n        <section class=\"content\">\n            <div class=\"row-fluid\">\n                    <ul>\n                        [[getResources? &parents=`4` &limit=`0` &tpl=`testimonial-interior` &tplOdd=`testimonial-odd` &includeTVs=`1` &processTVs=`1` &tvPrefix=`` &includeContent=`1` &sortby=`RAND()`]]\n                    </ul>\n               \n            </div>\n        </section>\n        [[$footer]]\n        \n        [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/testimonial.html'),
	(4,1,0,'Interior','',0,0,'',0,'[[$doc_head]]\n    <body>\n        [[$header]]\n        [[$intro-interior]]\n        [[$quote-bar]]\n        <section class=\"content\">\n            <div class=\"row-fluid wrap\">\n                    [[*content]]\n               \n            </div>\n        </section>\n        [[$footer]]\n        \n        [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/interior.html'),
	(5,1,0,'Contact','',0,0,'',0,'[[$doc_head]]\n    <body>\n        [[$header]]\n        [[$intro-interior]]\n        [[$quote-bar]]\n        <section class=\"content\">\n            <div class=\"row-fluid wrap\">\n                <div class=\"span4\">\n                    [[*content]]\n                </div>\n                <div class=\"span8\">\n                    [[$contact_form]]\n                </div>\n               \n            </div>\n        </section>\n        </section>\n        [[$footer]]\n        \n        [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/contact.html'),
	(6,1,0,'Services','',0,0,'',0,'[[$doc_head]]\n    <body>\n        [[$header]]\n        [[$intro-interior]]\n        [[$quote-bar]]\n        <section class=\"content\">\n            <div class=\"row-fluid wrap\">\n                <div class=\"span4\">\n                    <h2>[[*pagetitle]]</h2>\n                [[Wayfinder? \n                &level=`0`\n                &startId=`[[*id]]`\n                &innerTpl=`nav_inner`\n                &outerTpl=`nav-outer-list`\n                &level=`1`\n                &sortBy=`menuindex`\n                ]]\n                </div>\n                <div class=\"span8\">\n                    [[*content]]\n                </div>\n               \n            </div>\n        </section>\n        </section>\n        [[$footer]]\n        \n        [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/services.html'),
	(7,1,0,'Services Inner','',0,0,'',0,'[[$doc_head]]\n    <body>\n        [[$header]]\n        [[$intro-interior]]\n        [[$quote-bar]]\n        <section class=\"content\">\n            <div class=\"row-fluid wrap\">\n                <div class=\"span4\">\n                    <h2>[[!GetPageTitle? &id=`[[*parent]]`]]</h2>\n                        [[Wayfinder? \n                        &level=`0`\n                        &startId=`[[*parent]]`\n                        &innerTpl=`nav_inner`\n                        &outerTpl=`nav-outer-list`\n                        &level=`1`\n                        &sortBy=`menuindex`\n                        ]]\n                </div>\n                <div class=\"span8\">\n                    [[*content]]\n                </div>\n               \n            </div>\n        </section>\n        </section>\n        [[$footer]]\n        \n        [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/services-inner.html');

/*!40000 ALTER TABLE `modx_site_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_tmplvar_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_tmplvar_access`;

CREATE TABLE `modx_site_tmplvar_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_site_tmplvar_contentvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_tmplvar_contentvalues`;

CREATE TABLE `modx_site_tmplvar_contentvalues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `contentid` int(10) NOT NULL DEFAULT '0',
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tmplvarid` (`tmplvarid`),
  KEY `contentid` (`contentid`),
  KEY `tv_cnt` (`tmplvarid`,`contentid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_tmplvar_contentvalues` WRITE;
/*!40000 ALTER TABLE `modx_site_tmplvar_contentvalues` DISABLE KEYS */;

INSERT INTO `modx_site_tmplvar_contentvalues` (`id`, `tmplvarid`, `contentid`, `value`)
VALUES
	(1,1,1,'<p><strong>Grip-Tite Foundation Repair</strong>, a division of Grip-Tite Manufacturing Co., LLC conveniently located in Winterset, Iowa, provides foundation wall restoration, foundation stabilization and re-leveling, new construction solutions, and interior and exterior water control systems using genuine, guaranteed Grip-Tite products installed by trained and certified Grip-Tite personnel.</p>\r\n<p>Grip-Tite Foundation Repair brings the expertise and integrity of over 90 years of anchoring know how to residential and commercial customers in Central Iowa by offering a wide variety of foundation repair solutions utilizing the same Grip-Tite products that have been synonymous with a safe, secure basement in Iowa and the nation for more than 35 years.</p>\r\n<p><a class=\"arrow\" href=\"[[~14]]\">Learn more about our 90 years of expertise </a></p>'),
	(37,8,1,'<h1>Correct <span>Foundation Repairs</span> Caused by Unstable Soils</h1>\r\n<p><span>Grip-Tite Foundation Repair of central Iowa offers the complete scope of foundation repair and settlement products that will fix or repair any foundation settlement issue.</span></p>\r\n<p><a class=\"arrow\" href=\"[[~2]]\">See all of our home foundation repair services</a></p>'),
	(2,2,1,'<p>If it involves your foundation, we fix it! Foundation problems come in many shapes and sizes; from exterior and visible foundation cracks to interior bowing basement walls. Unstable soils can also lead to foundation separation resulting in unsightly and weakened foundations as well uneven and sloping floors. Water in your basement? You can prevent it with Grip-Tite Foundation Repair. We offer both internal and external waterproofing services. Mud jacking , also known as Slab jacking, is also part of our residential services. We can lift uneven sections of your basement floor, driveway, sidewalks and more!<br /> <br />For commercial and large-scale construction services, please refer to our Commercial Foundations page or call 515-462-1313.</p>'),
	(3,3,1,'images/35years.png'),
	(4,4,7,'<p>Bowing basement can severely affect the structure, integrity, and safety of your homeâ€™s foundation. Fortunately, Grip-Tite Foundation Repair is the exclusive provider in Central Iowa of the Grip-Tite WallAnchor â€“ the industry-leading choice to restore your basement wallâ€™s integrity. Better yet? The Grip-Tite Wall Anchor is made in Iowa.</p>\r\n<p><a class=\"arrow\" href=\"[[~7]]\">Learn more about how GripTite can help with your foundation problems</a></p>'),
	(5,5,7,'<p>The Grip-TiteÂ® Wall Anchor System central Iowa utilizes proven engineering methods to secure and stabilize deteriorating basement walls.</p>'),
	(6,7,15,'Redfield, Iowa'),
	(7,7,16,'Webster City, Iowa'),
	(8,7,17,'Des Moines, Iowa'),
	(9,7,18,'Des Moines, Iowa'),
	(10,7,19,'Gladbrook, Iowa'),
	(11,7,20,'Urbandale, Iowa'),
	(12,7,21,'Ellsworth, Iowa'),
	(13,7,22,'Earlham, Iowa'),
	(14,7,23,'Russell, Iowa'),
	(15,7,24,'Ankeny, Iowa'),
	(16,7,25,'What Cheer, Iowa'),
	(17,4,11,'<p>Why let the problems continue to cause your home and property further damage; dame that will only cost you more money in repairs but dramatically lowers the value of your property.</p>\r\n<p><a class=\"arrow\" href=\"http://local.griptite.com/\">Learn mroe about how GripTite can help with your foudnation problems</a></p>'),
	(31,4,12,'<p>Why let the problems continue to cause your home and property further damage; dame that will only cost you more money in repairs but dramatically lowers the value of your property.</p>\r\n<p><a class=\"arrow\" href=\"http://local.griptite.com/\">Learn mroe about how GripTite can help with your foudnation problems</a></p>'),
	(19,6,3,'commercial.jpg'),
	(20,6,2,'images/crack4.jpg'),
	(21,6,14,'images/hero2.jpg'),
	(22,6,4,'images/hero3.jpg'),
	(23,6,6,'images/hero2.jpg'),
	(24,4,8,'<p>Fact: Iowa has expansive soils. Fact: Because of the expansion and contraction of our soils, your homeâ€™s foundation may settle over time giving way to unsightly cracks or possibly uneven floors, causing loss of property value and potentially giving way to further damage.</p>\r\n<p><a class=\"arrow\" href=\"[[~8]]\">Learn more about how GripTite can help with your foundation problems</a></p>'),
	(44,6,7,'images/bowed wall pic.jpg'),
	(26,4,9,'<p>Commonly, sinking slabs may be confused with more severe foundation problems but thatâ€™s not always the case. While exterior sidewalks, patios, and porches that have sunk in toward your home may lead to water damage, they can typically be easily fixed Grip-Tite Foundationâ€™s Repairâ€™s mud-jacking.</p>\r\n<p><a class=\"arrow\" href=\"[[~9]]\">Learn more about how GripTite can help with your foundation problems</a></p>'),
	(40,4,27,'<p>Your basement wasnâ€™t made to collect water, so keep it out with Grip-Tite Foundation Repairâ€™s external and internal waterproofing systems. Far better to take preventative measures than to deal later with long-term foundation problems caused by water damage, not to mention mold and mildew.</p>\r\n<p><a class=\"arrow\" href=\"[[~27]]\">Learn more about how GripTite can help with your foundation problems</a></p>'),
	(28,5,9,'<p>Grip-Tite Foundation Repair of central Iowa offers slabjacking to solve sinking, cracked, and uneven concrete floors.</p>'),
	(29,4,10,'<p>Why let the problems continue to cause your home and property further damage that will only cost you more money in repairs but dramatically lowers the value of your property.</p>\r\n<p><a class=\"arrow\" href=\"[[~10]]\">Learn more about how GripTite can help with your foundation problems</a></p>'),
	(30,5,10,'<p>By installing a series of Grip-Tite Of Central Iowa stabilizers that are adjustable, the structure can be lifted and supported evenly, reducing any damage to the structure.</p>'),
	(32,5,12,'<p><span>Helical Tiebacks are used in Iowa commercial constructions, tiebacks are used to fix a leaning wall, avoid a new wall from moving, hold soil back.</span></p>'),
	(33,5,11,'<p>Helical Piles can be used instead of over excavations, caissons, spread footings.</p>'),
	(34,4,13,'<p>Why let the problems continue to cause your home and property further damage; dame that will only cost you more money in repairs but dramatically lowers the value of your property.</p>\r\n<p><a class=\"arrow\" href=\"http://local.griptite.com/\">Learn mroe about how GripTite can help with your foudnation problems</a></p>'),
	(35,5,2,'<p>Grip-Tite Foundation Repair of central Iowa offers the complete scope of foundation repair and settlement products that will fix or repair any foundation settlement issue.</p>'),
	(36,5,14,'<p>Grip-Tite Foundation Repair covers all aspects of Basement repair. Bowing basement walls, cracked foundations, settlement, sinking slabs, and more in Central Iowa.</p>'),
	(41,5,27,'<p>By installing Grip-Tite\'s interior drain tile system, you can rest easy knowing you won\'t have to be anxious about a wet basement.</p>'),
	(43,6,1,'images/crack3.jpg'),
	(45,6,8,'images/crack2.jpg'),
	(46,6,10,'images/crack4.jpg'),
	(47,5,8,'<p>Grip-Tite Foundation Repair of central Iowa offers the complete scope of foundation repair and settlement products that will fix or repair any foundation settlement issue.</p>');

/*!40000 ALTER TABLE `modx_site_tmplvar_contentvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_tmplvar_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_tmplvar_templates`;

CREATE TABLE `modx_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `templateid` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmplvarid`,`templateid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_tmplvar_templates` WRITE;
/*!40000 ALTER TABLE `modx_site_tmplvar_templates` DISABLE KEYS */;

INSERT INTO `modx_site_tmplvar_templates` (`tmplvarid`, `templateid`, `rank`)
VALUES
	(1,3,0),
	(2,3,0),
	(3,3,0),
	(4,7,0),
	(5,4,0),
	(5,5,0),
	(5,6,0),
	(5,7,0),
	(6,1,0),
	(8,3,0),
	(6,4,0),
	(6,5,0),
	(6,6,0),
	(6,7,0),
	(7,1,0),
	(6,8,0),
	(6,3,0);

/*!40000 ALTER TABLE `modx_site_tmplvar_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_tmplvars
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_tmplvars`;

CREATE TABLE `modx_site_tmplvars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `caption` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `elements` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `display` varchar(20) NOT NULL DEFAULT '',
  `default_text` mediumtext,
  `properties` text,
  `input_properties` text,
  `output_properties` text,
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `rank` (`rank`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_tmplvars` WRITE;
/*!40000 ALTER TABLE `modx_site_tmplvars` DISABLE KEYS */;

INSERT INTO `modx_site_tmplvars` (`id`, `source`, `property_preprocess`, `type`, `name`, `caption`, `description`, `editor_type`, `category`, `locked`, `elements`, `rank`, `display`, `default_text`, `properties`, `input_properties`, `output_properties`, `static`, `static_file`)
VALUES
	(1,2,0,'richtext','about-content','About Content','',0,7,0,'',0,'default','','a:0:{}','a:0:{}','a:0:{}',0,''),
	(2,2,0,'richtext','services-content','Services Content','',0,8,0,'',0,'default','','a:0:{}','a:0:{}','a:0:{}',0,''),
	(3,6,0,'image','about-graphic','Graphic for about section','',0,7,0,'',0,'default','','a:0:{}','a:0:{}','a:0:{}',0,''),
	(4,2,0,'richtext','home-content','Homepage lead in','',0,6,0,'',0,'default','<p>Why let the problems continue to cause your home and property further damage; dame that will only cost you more money in repairs but dramatically lowers the value of your property.</p>\n                                <a href=\"\" class=\"arrow\">Learn mroe about how GripTite can help with your foudnation problems</a>','a:0:{}','a:0:{}','a:0:{}',0,''),
	(5,2,0,'richtext','intro-content','','',0,9,0,'<p>Sed integer aliquet augue sed habitasse, amet aliquet, augue auctor ut ultrices cum natoque tincidunt aenean!</p>',0,'default','','a:0:{}','a:0:{}','a:0:{}',0,''),
	(6,6,0,'image','header-image','','',0,9,0,'',0,'default','images/hero.jpg','a:0:{}','a:0:{}','a:0:{}',0,''),
	(7,2,0,'text','location','','',0,3,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(8,2,0,'richtext','promotion','Promotion Text','',0,6,0,'',0,'default','','a:0:{}','a:0:{}','a:0:{}',0,'');

/*!40000 ALTER TABLE `modx_site_tmplvars` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_system_eventnames
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_system_eventnames`;

CREATE TABLE `modx_system_eventnames` (
  `name` varchar(50) NOT NULL,
  `service` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `groupname` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_system_eventnames` WRITE;
/*!40000 ALTER TABLE `modx_system_eventnames` DISABLE KEYS */;

INSERT INTO `modx_system_eventnames` (`name`, `service`, `groupname`)
VALUES
	('OnPluginEventBeforeSave',1,'Plugin Events'),
	('OnPluginEventSave',1,'Plugin Events'),
	('OnPluginEventBeforeRemove',1,'Plugin Events'),
	('OnPluginEventRemove',1,'Plugin Events'),
	('OnResourceGroupSave',1,'Security'),
	('OnResourceGroupBeforeSave',1,'Security'),
	('OnResourceGroupRemove',1,'Security'),
	('OnResourceGroupBeforeRemove',1,'Security'),
	('OnSnippetBeforeSave',1,'Snippets'),
	('OnSnippetSave',1,'Snippets'),
	('OnSnippetBeforeRemove',1,'Snippets'),
	('OnSnippetRemove',1,'Snippets'),
	('OnSnipFormPrerender',1,'Snippets'),
	('OnSnipFormRender',1,'Snippets'),
	('OnBeforeSnipFormSave',1,'Snippets'),
	('OnSnipFormSave',1,'Snippets'),
	('OnBeforeSnipFormDelete',1,'Snippets'),
	('OnSnipFormDelete',1,'Snippets'),
	('OnTemplateBeforeSave',1,'Templates'),
	('OnTemplateSave',1,'Templates'),
	('OnTemplateBeforeRemove',1,'Templates'),
	('OnTemplateRemove',1,'Templates'),
	('OnTempFormPrerender',1,'Templates'),
	('OnTempFormRender',1,'Templates'),
	('OnBeforeTempFormSave',1,'Templates'),
	('OnTempFormSave',1,'Templates'),
	('OnBeforeTempFormDelete',1,'Templates'),
	('OnTempFormDelete',1,'Templates'),
	('OnTemplateVarBeforeSave',1,'Template Variables'),
	('OnTemplateVarSave',1,'Template Variables'),
	('OnTemplateVarBeforeRemove',1,'Template Variables'),
	('OnTemplateVarRemove',1,'Template Variables'),
	('OnTVFormPrerender',1,'Template Variables'),
	('OnTVFormRender',1,'Template Variables'),
	('OnBeforeTVFormSave',1,'Template Variables'),
	('OnTVFormSave',1,'Template Variables'),
	('OnBeforeTVFormDelete',1,'Template Variables'),
	('OnTVFormDelete',1,'Template Variables'),
	('OnTVInputRenderList',1,'Template Variables'),
	('OnTVInputPropertiesList',1,'Template Variables'),
	('OnTVOutputRenderList',1,'Template Variables'),
	('OnTVOutputRenderPropertiesList',1,'Template Variables'),
	('OnUserGroupBeforeSave',1,'User Groups'),
	('OnUserGroupSave',1,'User Groups'),
	('OnUserGroupBeforeRemove',1,'User Groups'),
	('OnUserGroupRemove',1,'User Groups'),
	('OnBeforeUserGroupFormSave',1,'User Groups'),
	('OnUserGroupFormSave',1,'User Groups'),
	('OnBeforeUserGroupFormRemove',1,'User Groups'),
	('OnDocFormPrerender',1,'Resources'),
	('OnDocFormRender',1,'Resources'),
	('OnBeforeDocFormSave',1,'Resources'),
	('OnDocFormSave',1,'Resources'),
	('OnBeforeDocFormDelete',1,'Resources'),
	('OnDocFormDelete',1,'Resources'),
	('OnDocPublished',5,'Resources'),
	('OnDocUnPublished',5,'Resources'),
	('OnBeforeEmptyTrash',1,'Resources'),
	('OnEmptyTrash',1,'Resources'),
	('OnResourceTVFormPrerender',1,'Resources'),
	('OnResourceTVFormRender',1,'Resources'),
	('OnResourceDelete',1,'Resources'),
	('OnResourceUndelete',1,'Resources'),
	('OnResourceBeforeSort',1,'Resources'),
	('OnResourceSort',1,'Resources'),
	('OnResourceDuplicate',1,'Resources'),
	('OnResourceToolbarLoad',1,'Resources'),
	('OnResourceRemoveFromResourceGroup',1,'Resources'),
	('OnResourceAddToResourceGroup',1,'Resources'),
	('OnRichTextEditorRegister',1,'RichText Editor'),
	('OnRichTextEditorInit',1,'RichText Editor'),
	('OnRichTextBrowserInit',1,'RichText Editor'),
	('OnWebLogin',3,'Security'),
	('OnBeforeWebLogout',3,'Security'),
	('OnWebLogout',3,'Security'),
	('OnManagerLogin',2,'Security'),
	('OnBeforeManagerLogout',2,'Security'),
	('OnManagerLogout',2,'Security'),
	('OnBeforeWebLogin',3,'Security'),
	('OnWebAuthentication',3,'Security'),
	('OnBeforeManagerLogin',2,'Security'),
	('OnManagerAuthentication',2,'Security'),
	('OnManagerLoginFormRender',2,'Security'),
	('OnManagerLoginFormPrerender',2,'Security'),
	('OnPageUnauthorized',1,'Security'),
	('OnUserFormPrerender',1,'Users'),
	('OnUserFormRender',1,'Users'),
	('OnBeforeUserFormSave',1,'Users'),
	('OnUserFormSave',1,'Users'),
	('OnBeforeUserFormDelete',1,'Users'),
	('OnUserFormDelete',1,'Users'),
	('OnUserNotFound',1,'Users'),
	('OnBeforeUserActivate',1,'Users'),
	('OnUserActivate',1,'Users'),
	('OnBeforeUserDeactivate',1,'Users'),
	('OnUserDeactivate',1,'Users'),
	('OnBeforeUserDuplicate',1,'Users'),
	('OnUserDuplicate',1,'Users'),
	('OnUserChangePassword',1,'Users'),
	('OnUserBeforeRemove',1,'Users'),
	('OnUserBeforeSave',1,'Users'),
	('OnUserSave',1,'Users'),
	('OnUserRemove',1,'Users'),
	('OnUserBeforeAddToGroup',1,'User Groups'),
	('OnUserAddToGroup',1,'User Groups'),
	('OnUserBeforeRemoveFromGroup',1,'User Groups'),
	('OnUserRemoveFromGroup',1,'User Groups'),
	('OnWebPagePrerender',5,'System'),
	('OnBeforeCacheUpdate',4,'System'),
	('OnCacheUpdate',4,'System'),
	('OnLoadWebPageCache',4,'System'),
	('OnBeforeSaveWebPageCache',4,'System'),
	('OnSiteRefresh',1,'System'),
	('OnFileManagerUpload',1,'System'),
	('OnFileCreateFormPrerender',1,'System'),
	('OnFileEditFormPrerender',1,'System'),
	('OnManagerPageInit',2,'System'),
	('OnManagerPageBeforeRender',2,'System'),
	('OnManagerPageAfterRender',2,'System'),
	('OnWebPageInit',5,'System'),
	('OnLoadWebDocument',5,'System'),
	('OnParseDocument',5,'System'),
	('OnWebPageComplete',5,'System'),
	('OnBeforeManagerPageInit',2,'System'),
	('OnPageNotFound',1,'System'),
	('OnHandleRequest',5,'System'),
	('OnSiteSettingsRender',1,'Settings'),
	('OnInitCulture',1,'Internationalization'),
	('OnCategorySave',1,'Categories'),
	('OnCategoryBeforeSave',1,'Categories'),
	('OnCategoryRemove',1,'Categories'),
	('OnCategoryBeforeRemove',1,'Categories'),
	('OnChunkSave',1,'Chunks'),
	('OnChunkBeforeSave',1,'Chunks'),
	('OnChunkRemove',1,'Chunks'),
	('OnChunkBeforeRemove',1,'Chunks'),
	('OnChunkFormPrerender',1,'Chunks'),
	('OnChunkFormRender',1,'Chunks'),
	('OnBeforeChunkFormSave',1,'Chunks'),
	('OnChunkFormSave',1,'Chunks'),
	('OnBeforeChunkFormDelete',1,'Chunks'),
	('OnChunkFormDelete',1,'Chunks'),
	('OnContextSave',1,'Contexts'),
	('OnContextBeforeSave',1,'Contexts'),
	('OnContextRemove',1,'Contexts'),
	('OnContextBeforeRemove',1,'Contexts'),
	('OnContextFormPrerender',2,'Contexts'),
	('OnContextFormRender',2,'Contexts'),
	('OnPluginSave',1,'Plugins'),
	('OnPluginBeforeSave',1,'Plugins'),
	('OnPluginRemove',1,'Plugins'),
	('OnPluginBeforeRemove',1,'Plugins'),
	('OnPluginFormPrerender',1,'Plugins'),
	('OnPluginFormRender',1,'Plugins'),
	('OnBeforePluginFormSave',1,'Plugins'),
	('OnPluginFormSave',1,'Plugins'),
	('OnBeforePluginFormDelete',1,'Plugins'),
	('OnPluginFormDelete',1,'Plugins'),
	('OnPropertySetSave',1,'Property Sets'),
	('OnPropertySetBeforeSave',1,'Property Sets'),
	('OnPropertySetRemove',1,'Property Sets'),
	('OnPropertySetBeforeRemove',1,'Property Sets'),
	('OnMediaSourceBeforeFormDelete',1,'Media Sources'),
	('OnMediaSourceBeforeFormSave',1,'Media Sources'),
	('OnMediaSourceGetProperties',1,'Media Sources'),
	('OnMediaSourceFormDelete',1,'Media Sources'),
	('OnMediaSourceFormSave',1,'Media Sources'),
	('OnMediaSourceDuplicate',1,'Media Sources'),
	('OnElementNotFound',1,'System'),
	('OnResourceAutoPublish',1,'Resources'),
	('OnFileManagerDirCreate',1,'System'),
	('OnFileManagerDirRemove',1,'System'),
	('OnFileManagerDirRename',1,'System'),
	('OnFileManagerFileRename',1,'System'),
	('OnFileManagerFileRemove',1,'System'),
	('OnFileManagerFileUpdate',1,'System'),
	('OnFileManagerFileCreate',1,'System'),
	('OnFileManagerBeforeUpload',1,'System'),
	('OnFileManagerMoveObject',1,'System'),
	('OnMODXInit',5,'System');

/*!40000 ALTER TABLE `modx_system_eventnames` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_system_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_system_settings`;

CREATE TABLE `modx_system_settings` (
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_system_settings` WRITE;
/*!40000 ALTER TABLE `modx_system_settings` DISABLE KEYS */;

INSERT INTO `modx_system_settings` (`key`, `value`, `xtype`, `namespace`, `area`, `editedon`)
VALUES
	('access_category_enabled','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('access_context_enabled','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('access_resource_group_enabled','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('allow_forward_across_contexts','','combo-boolean','core','system','0000-00-00 00:00:00'),
	('allow_manager_login_forgot_password','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('allow_multiple_emails','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('allow_tags_in_post','','combo-boolean','core','system','0000-00-00 00:00:00'),
	('archive_with','','combo-boolean','core','system','0000-00-00 00:00:00'),
	('auto_menuindex','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('auto_check_pkg_updates','1','combo-boolean','core','system','0000-00-00 00:00:00'),
	('auto_check_pkg_updates_cache_expire','15','textfield','core','system','0000-00-00 00:00:00'),
	('automatic_alias','1','combo-boolean','core','furls','2013-09-30 23:37:09'),
	('base_help_url','//rtfm.modx.com/display/revolution20/','textfield','core','manager','2015-02-05 10:50:36'),
	('blocked_minutes','60','textfield','core','authentication','0000-00-00 00:00:00'),
	('cache_action_map','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_alias_map','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_context_settings','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_db','0','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_db_expires','0','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_db_session','0','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_db_session_lifetime','','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_default','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_disabled','0','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_expires','0','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_format','0','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_handler','xPDOFileCache','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_lang_js','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_lexicon_topics','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_noncore_lexicon_topics','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_resource','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_resource_expires','0','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_scripts','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_system_settings','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('clear_cache_refresh_trees','0','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('compress_css','0','combo-boolean','core','manager','2013-09-30 23:46:07'),
	('compress_js','0','combo-boolean','core','manager','2013-09-30 23:42:07'),
	('compress_js_max_files','10','textfield','core','manager','0000-00-00 00:00:00'),
	('compress_js_groups','','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('container_suffix','/','textfield','core','furls','0000-00-00 00:00:00'),
	('context_tree_sort','','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('context_tree_sortby','rank','textfield','core','manager','0000-00-00 00:00:00'),
	('context_tree_sortdir','ASC','textfield','core','manager','0000-00-00 00:00:00'),
	('cultureKey','en','modx-combo-language','core','language','0000-00-00 00:00:00'),
	('date_timezone','','textfield','core','system','0000-00-00 00:00:00'),
	('debug','','textfield','core','system','0000-00-00 00:00:00'),
	('default_duplicate_publish_option','preserve','textfield','core','manager','0000-00-00 00:00:00'),
	('default_media_source','2','modx-combo-source','core','manager','2013-12-12 12:35:28'),
	('default_per_page','20','textfield','core','manager','0000-00-00 00:00:00'),
	('default_context','web','modx-combo-context','core','site','0000-00-00 00:00:00'),
	('default_template','1','modx-combo-template','core','site','0000-00-00 00:00:00'),
	('default_content_type','1','modx-combo-content-type','core','site','0000-00-00 00:00:00'),
	('editor_css_path','','textfield','core','editor','0000-00-00 00:00:00'),
	('editor_css_selectors','','textfield','core','editor','0000-00-00 00:00:00'),
	('emailsender','info@slashwebstudios.com','textfield','core','authentication','2013-09-30 23:29:30'),
	('emailsubject','Your login details','textfield','core','authentication','0000-00-00 00:00:00'),
	('enable_dragdrop','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('error_page','1','textfield','core','site','0000-00-00 00:00:00'),
	('failed_login_attempts','5','textfield','core','authentication','0000-00-00 00:00:00'),
	('fe_editor_lang','en','modx-combo-language','core','language','0000-00-00 00:00:00'),
	('feed_modx_news','http://feeds.feedburner.com/modx-announce','textfield','core','system','0000-00-00 00:00:00'),
	('feed_modx_news_enabled','0','combo-boolean','core','system','2013-11-04 10:51:22'),
	('feed_modx_security','http://forums.modx.com/board.xml?board=294','textfield','core','system','2015-02-05 10:50:36'),
	('feed_modx_security_enabled','0','combo-boolean','core','system','2013-11-04 10:51:26'),
	('filemanager_path','','textfield','core','file','0000-00-00 00:00:00'),
	('filemanager_path_relative','1','combo-boolean','core','file','0000-00-00 00:00:00'),
	('filemanager_url','','textfield','core','file','0000-00-00 00:00:00'),
	('filemanager_url_relative','1','combo-boolean','core','file','0000-00-00 00:00:00'),
	('forgot_login_email','<p>Hello [[+username]],</p>\n<p>A request for a password reset has been issued for your MODX user. If you sent this, you may follow this link and use this password to login. If you did not send this request, please ignore this email.</p>\n\n<p>\n    <strong>Activation Link:</strong> [[+url_scheme]][[+http_host]][[+manager_url]]?modahsh=[[+hash]]<br />\n    <strong>Username:</strong> [[+username]]<br />\n    <strong>Password:</strong> [[+password]]<br />\n</p>\n\n<p>After you log into the MODX Manager, you can change your password again, if you wish.</p>\n\n<p>Regards,<br />Site Administrator</p>','textarea','core','authentication','0000-00-00 00:00:00'),
	('form_customization_use_all_groups','','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('forward_merge_excludes','type,published,class_key','textfield','core','system','0000-00-00 00:00:00'),
	('friendly_alias_lowercase_only','1','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_max_length','0','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_restrict_chars','pattern','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_restrict_chars_pattern','/[\\0\\x0B\\t\\n\\r\\f\\a&=+%#<>\"~:`@\\?\\[\\]\\{\\}\\|\\^\'\\\\]/','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_strip_element_tags','1','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_translit','none','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_translit_class','translit.modTransliterate','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_translit_class_path','{core_path}components/','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_trim_chars','/.-_','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_word_delimiter','-','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_word_delimiters','-_','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_urls','1','combo-boolean','core','furls','2013-09-30 23:35:58'),
	('friendly_urls_strict','0','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('global_duplicate_uri_check','0','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('hidemenu_default','0','combo-boolean','core','site','0000-00-00 00:00:00'),
	('inline_help','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('locale','','textfield','core','language','0000-00-00 00:00:00'),
	('log_level','1','textfield','core','system','0000-00-00 00:00:00'),
	('log_target','FILE','textfield','core','system','0000-00-00 00:00:00'),
	('link_tag_scheme','-1','textfield','core','site','0000-00-00 00:00:00'),
	('lock_ttl','360','textfield','core','system','0000-00-00 00:00:00'),
	('mail_charset','UTF-8','modx-combo-charset','core','mail','0000-00-00 00:00:00'),
	('mail_encoding','8bit','textfield','core','mail','0000-00-00 00:00:00'),
	('mail_use_smtp','1','combo-boolean','core','mail','2015-02-26 12:28:52'),
	('mail_smtp_auth','1','combo-boolean','core','mail','2015-02-26 12:28:09'),
	('mail_smtp_helo','','textfield','core','mail','0000-00-00 00:00:00'),
	('mail_smtp_hosts','smtp.gmail.com','textfield','core','mail','2015-02-26 12:51:39'),
	('mail_smtp_keepalive','','combo-boolean','core','mail','0000-00-00 00:00:00'),
	('mail_smtp_pass','jpl57006','text-password','core','mail','2015-02-26 12:59:57'),
	('mail_smtp_port','465','textfield','core','mail','2015-02-26 12:28:28'),
	('mail_smtp_prefix','ssl','textfield','core','mail','2015-02-26 12:28:36'),
	('mail_smtp_single_to','','combo-boolean','core','mail','0000-00-00 00:00:00'),
	('mail_smtp_timeout','10','textfield','core','mail','0000-00-00 00:00:00'),
	('mail_smtp_user','gtfr.contactforms@gmail.com','textfield','core','mail','2015-02-26 13:13:51'),
	('manager_date_format','Y-m-d','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_favicon_url','','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_html5_cache','0','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('manager_js_cache_file_locking','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('manager_js_cache_max_age','3600','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_js_document_root','','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_js_zlib_output_compression','0','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('manager_time_format','g:i a','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_direction','ltr','textfield','core','language','0000-00-00 00:00:00'),
	('manager_lang_attribute','en','textfield','core','language','0000-00-00 00:00:00'),
	('manager_language','en','modx-combo-language','core','language','0000-00-00 00:00:00'),
	('manager_login_url_alternate','','textfield','core','authentication','0000-00-00 00:00:00'),
	('manager_theme','default','modx-combo-manager-theme','core','manager','2015-02-05 10:50:36'),
	('manager_week_start','0','textfield','core','manager','0000-00-00 00:00:00'),
	('modx_browser_default_sort','name','textfield','core','manager','0000-00-00 00:00:00'),
	('modx_charset','UTF-8','modx-combo-charset','core','language','0000-00-00 00:00:00'),
	('principal_targets','modAccessContext,modAccessResourceGroup,modAccessCategory,sources.modAccessMediaSource','textfield','core','authentication','0000-00-00 00:00:00'),
	('proxy_auth_type','BASIC','textfield','core','proxy','0000-00-00 00:00:00'),
	('proxy_host','','textfield','core','proxy','0000-00-00 00:00:00'),
	('proxy_password','','text-password','core','proxy','0000-00-00 00:00:00'),
	('proxy_port','','textfield','core','proxy','0000-00-00 00:00:00'),
	('proxy_username','','textfield','core','proxy','0000-00-00 00:00:00'),
	('password_generated_length','8','textfield','core','authentication','0000-00-00 00:00:00'),
	('password_min_length','8','textfield','core','authentication','0000-00-00 00:00:00'),
	('phpthumb_allow_src_above_docroot','','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_cache_maxage','30','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_cache_maxsize','100','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_cache_maxfiles','10000','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_cache_source_enabled','','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_document_root','','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_error_bgcolor','CCCCFF','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_error_textcolor','FF0000','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_error_fontsize','1','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_far','C','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_imagemagick_path','','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nohotlink_enabled','1','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nohotlink_erase_image','1','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nohotlink_valid_domains','{http_host}','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nohotlink_text_message','Off-server thumbnailing is not allowed','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_enabled','','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_erase_image','1','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_require_refer','','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_text_message','Off-server linking is not allowed','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_valid_domains','{http_host}','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_watermark_src','','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_zoomcrop','0','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('publish_default','','combo-boolean','core','site','0000-00-00 00:00:00'),
	('rb_base_dir','','textfield','core','file','0000-00-00 00:00:00'),
	('rb_base_url','','textfield','core','file','0000-00-00 00:00:00'),
	('request_controller','index.php','textfield','core','gateway','0000-00-00 00:00:00'),
	('request_method_strict','0','combo-boolean','core','gateway','0000-00-00 00:00:00'),
	('request_param_alias','q','textfield','core','gateway','0000-00-00 00:00:00'),
	('request_param_id','id','textfield','core','gateway','0000-00-00 00:00:00'),
	('resolve_hostnames','0','combo-boolean','core','system','0000-00-00 00:00:00'),
	('resource_tree_node_name','pagetitle','textfield','core','manager','0000-00-00 00:00:00'),
	('resource_tree_node_tooltip','','textfield','core','manager','0000-00-00 00:00:00'),
	('richtext_default','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('search_default','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('server_offset_time','0','textfield','core','system','0000-00-00 00:00:00'),
	('server_protocol','http','textfield','core','system','0000-00-00 00:00:00'),
	('session_cookie_domain','','textfield','core','session','0000-00-00 00:00:00'),
	('session_cookie_lifetime','604800','textfield','core','session','0000-00-00 00:00:00'),
	('session_cookie_path','','textfield','core','session','0000-00-00 00:00:00'),
	('session_cookie_secure','','combo-boolean','core','session','0000-00-00 00:00:00'),
	('session_cookie_httponly','1','combo-boolean','core','session','0000-00-00 00:00:00'),
	('session_gc_maxlifetime','604800','textfield','core','session','0000-00-00 00:00:00'),
	('session_handler_class','modSessionHandler','textfield','core','session','0000-00-00 00:00:00'),
	('session_name','','textfield','core','session','0000-00-00 00:00:00'),
	('set_header','1','combo-boolean','core','system','0000-00-00 00:00:00'),
	('show_tv_categories_header','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('signupemail_message','<p>Hello [[+uid]],</p>\n    <p>Here are your login details for the [[+sname]] MODX Manager:</p>\n\n    <p>\n        <strong>Username:</strong> [[+uid]]<br />\n        <strong>Password:</strong> [[+pwd]]<br />\n    </p>\n\n    <p>Once you log into the MODX Manager at [[+surl]], you can change your password.</p>\n\n    <p>Regards,<br />Site Administrator</p>','textarea','core','authentication','0000-00-00 00:00:00'),
	('site_name','Grip-Tite Foundation Repair','textfield','core','site','2015-02-19 05:53:37'),
	('site_start','1','textfield','core','site','0000-00-00 00:00:00'),
	('site_status','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('site_unavailable_message','The site is currently unavailable','textfield','core','site','0000-00-00 00:00:00'),
	('site_unavailable_page','0','textfield','core','site','0000-00-00 00:00:00'),
	('strip_image_paths','1','combo-boolean','core','file','0000-00-00 00:00:00'),
	('symlink_merge_fields','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('topmenu_show_descriptions','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('tree_default_sort','menuindex','textfield','core','manager','0000-00-00 00:00:00'),
	('tree_root_id','0','numberfield','core','manager','0000-00-00 00:00:00'),
	('tvs_below_content','0','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('udperms_allowroot','','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('unauthorized_page','1','textfield','core','site','0000-00-00 00:00:00'),
	('upload_files','txt,html,htm,xml,js,css,zip,gz,rar,z,tgz,tar,htaccess,mp3,mp4,aac,wav,au,wmv,avi,mpg,mpeg,pdf,doc,docx,xls,xlsx,ppt,pptx,jpg,jpeg,png,gif,psd,ico,bmp,odt,ods,odp,odb,odg,odf','textfield','core','file','0000-00-00 00:00:00'),
	('upload_flash','swf,fla','textfield','core','file','0000-00-00 00:00:00'),
	('upload_images','jpg,jpeg,png,gif,psd,ico,bmp','textfield','core','file','0000-00-00 00:00:00'),
	('upload_maxsize','1048576','textfield','core','file','0000-00-00 00:00:00'),
	('upload_media','mp3,wav,au,wmv,avi,mpg,mpeg','textfield','core','file','0000-00-00 00:00:00'),
	('use_alias_path','1','combo-boolean','core','furls','2013-09-30 23:36:11'),
	('use_browser','1','combo-boolean','core','file','0000-00-00 00:00:00'),
	('use_editor','1','combo-boolean','core','editor','0000-00-00 00:00:00'),
	('use_multibyte','1','combo-boolean','core','language','2013-09-30 23:29:30'),
	('use_weblink_target','','combo-boolean','core','site','0000-00-00 00:00:00'),
	('webpwdreminder_message','<p>Hello [[+uid]],</p>\n\n    <p>To activate your new password click the following link:</p>\n\n    <p>[[+surl]]</p>\n\n    <p>If successful you can use the following password to login:</p>\n\n    <p><strong>Password:</strong> [[+pwd]]</p>\n\n    <p>If you did not request this email then please ignore it.</p>\n\n    <p>Regards,<br />\n    Site Administrator</p>','textarea','core','authentication','0000-00-00 00:00:00'),
	('websignupemail_message','<p>Hello [[+uid]],</p>\n\n    <p>Here are your login details for [[+sname]]:</p>\n\n    <p><strong>Username:</strong> [[+uid]]<br />\n    <strong>Password:</strong> [[+pwd]]</p>\n\n    <p>Once you log into [[+sname]] at [[+surl]], you can change your password.</p>\n\n    <p>Regards,<br />\n    Site Administrator</p>','textarea','core','authentication','0000-00-00 00:00:00'),
	('welcome_screen','','combo-boolean','core','manager','2013-09-30 23:30:09'),
	('welcome_screen_url','http://misc.modx.com/revolution/welcome.22.html','textfield','core','manager','0000-00-00 00:00:00'),
	('which_editor','TinyMCE','modx-combo-rte','core','editor','2013-09-30 23:33:02'),
	('which_element_editor','CodeMirror','modx-combo-rte','core','editor','2015-02-09 22:53:01'),
	('xhtml_urls','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('settings_version','2.3.3-pl','textfield','core','system','2015-02-05 10:50:38'),
	('settings_distro','traditional','textfield','core','system','2014-04-02 19:09:51'),
	('tiny.base_url','','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.convert_fonts_to_spans','1','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.convert_newlines_to_brs','','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.css_selectors','Arrow Link=arrow','textfield','tinymce','advanced-theme','2015-02-09 23:09:01'),
	('tiny.custom_buttons1','undo,redo,selectall,separator,pastetext,pasteword,separator,search,replace,separator,nonbreaking,hr,charmap,separator,image,modxlink,unlink,anchor,media,separator,cleanup,removeformat,separator,fullscreen,print,code,help','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_buttons2','bold,italic,underline,strikethrough,sub,sup,separator,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,separator,styleprops','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_buttons3','','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_buttons4','','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_buttons5','','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_plugins','style,advimage,advlink,modxlink,searchreplace,print,contextmenu,paste,fullscreen,noneditable,nonbreaking,xhtmlxtras,visualchars,media','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.editor_theme','advanced','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.element_format','xhtml','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.entity_encoding','named','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.fix_nesting','','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.fix_table_elements','','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.font_size_classes','','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.font_size_style_values','xx-small,x-small,small,medium,large,x-large,xx-large','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.forced_root_block','p','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.indentation','30px','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.invalid_elements','','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.nowrap','','combo-boolean','tinymce','general','0000-00-00 00:00:00'),
	('tiny.object_resizing','1','combo-boolean','tinymce','general','0000-00-00 00:00:00'),
	('tiny.path_options','','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.remove_linebreaks','','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.remove_redundant_brs','1','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.removeformat_selector','b,strong,em,i,span,ins','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.skin','cirkuit','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.skin_variant','','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.table_inline_editing','','combo-boolean','tinymce','general','0000-00-00 00:00:00'),
	('tiny.template_list','','textarea','tinymce','general','0000-00-00 00:00:00'),
	('tiny.template_list_snippet','','textarea','tinymce','general','0000-00-00 00:00:00'),
	('tiny.template_selected_content_classes','','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.theme_advanced_blockformats','p,h1,h2,h3,h4,h5,h6,div,blockquote,code,pre,address','textfield','tinymce','advanced-theme','0000-00-00 00:00:00'),
	('tiny.theme_advanced_font_sizes','80%,90%,100%,120%,140%,160%,180%,220%,260%,320%,400%,500%,700%','textfield','tinymce','advanced-theme','0000-00-00 00:00:00'),
	('tiny.use_uncompressed_library','','combo-boolean','tinymce','general','0000-00-00 00:00:00'),
	('access_policies_version','1.0','textfield','core','system','0000-00-00 00:00:00'),
	('confirm_navigation','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('modx_browser_default_viewmode','grid','textfield','core','manager','0000-00-00 00:00:00'),
	('resource_tree_node_name_fallback','pagetitle','textfield','core','manager','0000-00-00 00:00:00'),
	('syncsite_default','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('welcome_action','welcome','textfield','core','manager','0000-00-00 00:00:00'),
	('welcome_namespace','core','textfield','core','manager','0000-00-00 00:00:00'),
	('enable_gravatar','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('auto_isfolder','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('codemirror.enable','1','combo-boolean','codemirror','Editor','0000-00-00 00:00:00'),
	('formit.recaptcha_public_key','','textfield','formit','reCaptcha','0000-00-00 00:00:00'),
	('formit.recaptcha_private_key','','textfield','formit','reCaptcha','0000-00-00 00:00:00'),
	('formit.recaptcha_use_ssl','','combo-boolean','formit','reCaptcha','0000-00-00 00:00:00'),
	('primary_email','gtfr.contactforms@gmail.com','textfield','core','','2015-03-04 11:49:34'),
	('facebook-link','https://www.facebook.com/pages/Grip-Tite-Foundation-Repair/478307715522393','textfield','core','','2015-02-26 12:27:21'),
	('twitter-link','https://twitter.com/GetGripTite','textfield','core','','2015-02-27 14:53:32'),
	('google_analytics','UA-60102242-1','textfield','core','','2015-02-26 12:31:26');

/*!40000 ALTER TABLE `modx_system_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_transport_packages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_transport_packages`;

CREATE TABLE `modx_transport_packages` (
  `signature` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `installed` datetime DEFAULT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `workspace` int(10) unsigned NOT NULL DEFAULT '0',
  `provider` int(10) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `source` tinytext,
  `manifest` text,
  `attributes` mediumtext,
  `package_name` varchar(255) NOT NULL,
  `metadata` text,
  `version_major` smallint(5) unsigned NOT NULL DEFAULT '0',
  `version_minor` smallint(5) unsigned NOT NULL DEFAULT '0',
  `version_patch` smallint(5) unsigned NOT NULL DEFAULT '0',
  `release` varchar(100) NOT NULL DEFAULT '',
  `release_index` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`signature`),
  KEY `workspace` (`workspace`),
  KEY `provider` (`provider`),
  KEY `disabled` (`disabled`),
  KEY `package_name` (`package_name`),
  KEY `version_major` (`version_major`),
  KEY `version_minor` (`version_minor`),
  KEY `version_patch` (`version_patch`),
  KEY `release` (`release`),
  KEY `release_index` (`release_index`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_transport_packages` WRITE;
/*!40000 ALTER TABLE `modx_transport_packages` DISABLE KEYS */;

INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`)
VALUES
	('tinymce-4.3.3-pl','2013-10-01 06:30:50','2013-09-30 23:33:09','2013-10-01 06:33:09',0,1,1,0,'tinymce-4.3.3-pl.transport.zip',NULL,'a:32:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:9:\"signature\";s:16:\"tinymce-4.3.3-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:586:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';this.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'install\',signature:r.signature,register:\'mgr\',topic:topic});var c=this.console;MODx.Ajax.request({url:MODx.config.connectors_url+\'workspace/packages.php\',params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:7:\"install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:44:\"/workspace/package/install/tinymce-4.3.3-pl/\";s:12:\"HTTP_MODAUTH\";s:52:\"modx524a4fa6425d00.99369569_1524a4fd15b7d23.78535987\";s:14:\"package_action\";i:0;}','TinyMCE','a:31:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556bc5b2b083396d0007e9\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556bc5b2b083396d0007e9\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:16:\"tinymce-4.3.3-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"4.3.3\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"4\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"3\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"3\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:102:\"<p>TinyMCE version 3.4.7 for MODx Revolution. Works with Revolution 2.2.x or later only.</p><ul>\n</ul>\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:225:\"<p>Install via Package Management.</p>\n<p>If you\'re having issues installing, make sure you have the latest ZipArchive extension for PHP, and that it\'s properly configured, or set the \"archive_with\" System Setting to Yes.</p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2332:\"<p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.3.3</b></p><ul><li>Change popup windows to more convenient modals</li><li>Have TinyMCE respect context settings of Resource being edited</li><li>Update TinyMCE to 3.5.4.1</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.3.2</b></p><ul><li>Update Czech/German translation</li><li>&#91;#74&#93; Fix inclusion of english as fallback for language</li><li>&#91;#80&#93; Make context menu use MODxLink plugin</li><li>Upgrade TinyMCE to 3.4.7</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.3.1</b></p><ul><li>Optimizations for MODX 2.2</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.3.0</b></p><ul><li>&#91;#71&#93; Update TinyMCE to v3.4.5</li><li>&#91;#70&#93; Fixes to cirkuit skin with missing CSS styles</li><li>&#91;#64&#93; Add tiny.template_list_snippet setting for grabbing template list from a Snippet</li><li>&#91;#66&#93; Fix issues with Revolution 2.2.0 code</li><li>&#91;#63&#93; Add tiny.base_url setting for managing the document_base_url tinymce setting</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.2.4</b></p><ul><li>Updated TinyMCE to 3.4.2</li><li>Fix issue where recursion detected xPDO error was showing in logs on chunk editing</li><li>&#91;#55&#93; Fix help for element_format and preformatted descriptions in plugin properties</li><li>&#91;#53&#93; Languages added/update: German, English, French, Indonesian, Japanese, Dutch, Russian, Ukrainian</li></ul><b>New in 4.2.3</b><p></p><ul><li>Fix issue that inserted wrong URL when using TinyMCE in Revolution 2.1 and later</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.2.2</b></p><ul><li>&#91;#49&#93; Added spellchecker files</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.2.1</b></p><ul><li>&#91;#45&#93; &#91;#47&#93; Fixes for front-end usage and compatibility with NewsPublisher</li><li>Add compressed JS for faster loading</li></ul>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2012-07-07 14:50:43 UTC\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:10:\"created_by\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:16;a:3:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2012-07-07 14:50:43 UTC\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"179884\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:24;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:25;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:7:\"TinyMCE\";s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:54:\"http://modx.s3.amazonaws.com/extras/459/tinymce-ss.png\";s:8:\"children\";a:0:{}}i:29;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4ff84cc6f245544fc100000c\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4ff84cc3f245544fc100000a\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:30:\"tinymce-4.3.3-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"56255\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:14:\"66.147.244.139\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=4ff84cc6f245544fc100000c\";s:8:\"children\";a:0:{}}}}i:30;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:16:\"tinymce-4.3.3-pl\";s:8:\"children\";a:0:{}}}',4,3,3,'pl',0),
	('wayfinder-2.3.3-pl','2013-10-01 06:31:45','2013-09-30 23:33:16','2013-10-01 06:33:16',0,1,1,0,'wayfinder-2.3.3-pl.transport.zip',NULL,'a:34:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:804:\"::::::::::::::::::::::::::::::::::::::::\n Snippet name: Wayfinder\n Short Desc: builds site navigation\n Version: 2.3.0 (Revolution compatible)\n Authors: \n    Kyle Jaebker (muddydogpaws.com)\n    Ryan Thrash (vertexworks.com)\n    Shaun McCormick (splittingred.com)\n ::::::::::::::::::::::::::::::::::::::::\nDescription:\n    Totally refactored from original DropMenu nav builder to make it easier to\n    create custom navigation by using chunks as output templates. By using templates,\n    many of the paramaters are no longer needed for flexible output including tables,\n    unordered- or ordered-lists (ULs or OLs), definition lists (DLs) or in any other\n    format you desire.\n::::::::::::::::::::::::::::::::::::::::\nExample Usage:\n    [[Wayfinder? &startId=`0`]]\n::::::::::::::::::::::::::::::::::::::::\";s:9:\"changelog\";s:2655:\"Changelog for Wayfinder (for Revolution).\n\nWayfinder 2.3.3\n====================================\n- [#40] Add wf.level placeholder to items for showing current depth\n- [#42] Allow authenticated mgr users with view_unpublished to use new previewUnpublished property to preview unpublished Resources in menus\n- [#41] Fix issue with Wayfinder and truncated result sets due to getIterator call\n\nWayfinder 2.3.2\n====================================\n- [#36] Fix issue with multiple Wayfinder calls using &config\n- [#35] Fix issues with TV bindings rendering\n- Add \"protected\" placeholder that is 1 if Resource is protected by a Resource Group\n- Updated documentation, snippet properties descriptions\n\nWayfinder 2.3.1\n====================================\n- [#31] Add &scheme property for specifying link schemes\n- [#27] Improve caching in Wayfinder to store cache files in resource cache so cache is synced with modx core caching\n\nWayfinder 2.3.0\n====================================\n- [#14] Fix issue with hideSubMenus when using it with a non-zero startId\n- Add all fields of a Resource to the rowTpl placeholder set, such as menutitle, published, etc\n- Properly optimize TV value grabbing to properly parse and cache TVs to improve load times when using TVs in a result set\n- Ensure that caching also caches by user ID to persist access permissions through cached result sets\n\nWayfinder 2.2.0\n====================================\n- [#23] Fix issue that generated error message in error.log due to &contexts always being processed regardless of empty state\n- [#21] Fix issue with unnecessary groupby that was breaking sorting in older mysql versions\n- [#22] Add &cacheResults parameter, which caches queries for faster loading\n- [#8] Add &contexts parameter, and &startIdContext parameter if navigating across multiple contexts and using a non-0 &startId\n\nWayfinder 2.1.3\n====================================\n- [#14] Fix hideSubMenus property\n- Add templates parameter that accepts a comma-delimited list of template IDs to filter by\n- Add where parameter that accepts a JSON object for where conditions\n- Add hereId parameter for specifying the active location\n\nWayfinder 2.1.2\n====================================\n- Fixed bug with includeDocs parameter\n\nWayfinder 2.1.1\n====================================\n- Wayfinder now properly uses MODx parsing system\n- Fixed issue with includeDocs statement\n- Fixed issues with PDO statements\n- Added the missing permissions check\n- Added wayfinder parameter \"permissions\" - default to \"list\", empty to bypass permissions check\n- [#WAYFINDER-20] TemplateVariables not rendering in Wayfinder templates.\n- Added changelog.\";s:9:\"signature\";s:18:\"wayfinder-2.3.3-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:586:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';this.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'install\',signature:r.signature,register:\'mgr\',topic:topic});var c=this.console;MODx.Ajax.request({url:MODx.config.connectors_url+\'workspace/packages.php\',params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:7:\"install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:46:\"/workspace/package/install/wayfinder-2.3.3-pl/\";s:12:\"HTTP_MODAUTH\";s:52:\"modx524a4fa6425d00.99369569_1524a4fd15b7d23.78535987\";s:14:\"package_action\";i:0;}','Wayfinder','a:31:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556be8b2b083396d0008bd\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556be8b2b083396d0008bd\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:18:\"wayfinder-2.3.3-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"2.3.3\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"3\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"3\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:230:\"<p>Wayfinder is a highly flexible navigation builder for MODx Revolution.</p><p>See the official docs here:&nbsp;<a href=\"http://rtfm.modx.com/display/ADDON/Wayfinder\">http://rtfm.modx.com/display/ADDON/Wayfinder</a></p><ul>\n</ul>\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:38:\"<p>Install via Package Management.</p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2306:\"<p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 2.3.3</b></p><ul><li>&#91;#40&#93; Add wf.level placeholder to items for showing current depth</li><li>&#91;#42&#93; Allow authenticated mgr users with view_unpublished to use new previewUnpublished property to preview unpublished Resources in menus</li><li>&#91;#41&#93; Fix issue with Wayfinder and truncated result sets due to getIterator call</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 2.3.2</b></p><ul><li>&#91;#36&#93; Fix issue with multiple Wayfinder calls using &amp;config</li><li>&#91;#35&#93; Fix issues with TV bindings rendering</li><li>Add \"protected\" placeholder that is 1 if Resource is protected by a Resource Group</li><li>Updated documentation, snippet properties descriptions</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 2.3.1</b></p><ul><li>&#91;#31&#93; Add &amp;scheme property for specifying link schemes</li><li>&#91;#27&#93; Improve caching in Wayfinder to store cache files in resource cache so cache is synced with modx core caching</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 2.3.0</b></p><ul><li>&#91;#14&#93; Fix issue with hideSubMenus when using it with a non-zero startId</li><li>Add all fields of a Resource to the rowTpl placeholder set, such as menutitle, published, etc</li><li>Properly optimize TV value grabbing to properly parse and cache TVs to improve load times when using TVs in a result set</li><li>Ensure that caching also caches by user ID to persist access permissions through cached result sets</li></ul><p><b>New in 2.2.0</b></p><ul><li>&#91;#23&#93; Fix issue that generated error message in error.log due to &amp;contexts always being processed regardless of empty state</li><li>&#91;#21&#93; Fix issue with unnecessary groupby that was breaking sorting in older mysql versions</li><li>&#91;#22&#93; Add &amp;cacheResults parameter, which caches queries for faster loading</li><li>&#91;#8&#93; Add &amp;contexts parameter, and &amp;startIdContext parameter if navigating across multiple contexts and using a non-0 &amp;startId</li></ul>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2011-10-31 16:21:50 UTC\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:10:\"created_by\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:16;a:3:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2011-10-31 16:21:50 UTC\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"166776\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:24;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:25;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:9:\"Wayfinder\";s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.0\";s:8:\"children\";a:0:{}}i:28;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:29;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4eaecb20f24554127d0000b8\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4eaecb1ef24554127d0000b6\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:32:\"wayfinder-2.3.3-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"84441\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"202.67.40.29\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=4eaecb20f24554127d0000b8\";s:8:\"children\";a:0:{}}}}i:30;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:18:\"wayfinder-2.3.3-pl\";s:8:\"children\";a:0:{}}}',2,3,3,'pl',0),
	('getresources-1.6.0-pl','2013-10-01 06:31:50','2013-09-30 23:32:56','2013-10-01 06:32:56',0,1,1,0,'getresources-1.6.0-pl.transport.zip',NULL,'a:34:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:332:\"--------------------\nSnippet: getResources\n--------------------\nVersion: 1.6.0-pl\nReleased: February 19, 2013\nSince: December 28, 2009\nAuthor: Jason Coward <jason@modx.com>\n\nA general purpose Resource listing and summarization snippet for MODX Revolution.\n\nOfficial Documentation:\nhttp://docs.modxcms.com/display/ADDON/getResources\n\";s:9:\"changelog\";s:3232:\"Changelog for getResources.\n\ngetResources 1.6.0-pl (February 19, 2013)\n====================================\n- Add tplWrapper for specifying a wrapper template\n\ngetResources 1.5.1-pl (August 23, 2012)\n====================================\n- Add tplOperator property to default properties\n- [#73] Add between tplOperator to conditionalTpls\n\ngetResources 1.5.0-pl (June 15, 2012)\n====================================\n- [#58] Add tplCondition/conditionalTpls support\n- [#67] Add odd property\n- [#60] Allow custom delimiters for tvFilters\n- [#63] Give tplFirst/tplLast precedence over tpl_X/tpl_nX\n- Automatically prepare TV values for media-source dependent TVs\n\ngetResources 1.4.2-pl (December 9, 2011)\n====================================\n- [#25] Add new operators to tvFilters\n- [#37] Consider default values with tvFilters\n- [#57] Fix tpl overrides and improve order\n\ngetResources 1.4.1-pl (December 8, 2011)\n====================================\n- [#57] Add support for factor-based tpls\n- [#54], [#55] Fix processTVList feature\n\ngetResources 1.4.0-pl (September 21, 2011)\n====================================\n- [#50] Use children of parents from other contexts\n- [#45] Add dbCacheFlag to control db caching of getCollection, default to false\n- [#49] Allow comma-delimited list of TV names as includeTVList or processTVList\n\ngetResources 1.3.1-pl (July 14, 2011)\n====================================\n- [#43] Allow 0 as idx property\n- [#9] Fix tvFilters grouping\n- [#46] Fix criteria issue with &resources property\n\ngetResources 1.3.0-pl (March 28, 2011)\n====================================\n- [#33] sortbyTVType: Allow numeric and datetime TV sorting via SQL CAST()\n- [#24] Fix typos in list property options\n- [#4] Support multiple sortby fields via JSON object\n- Use get() instead to toArray() if includeContent is false\n- [#22] Add &toSeparatePlaceholders property for splitting output\n\ngetResources 1.2.2-pl (October 18, 2010)\n====================================\n- [#19] Fix sortbyTV returning duplicate rows\n\ngetResources 1.2.1-pl (October 11, 2010)\n====================================\n- Remove inadvertent call to modX::setLogTarget(\'ECHO\')\n\ngetResources 1.2.0-pl (September 25, 2010)\n====================================\n- Fix error when &parents is not set\n- Allow empty &sortby\n- Add ability to sort by a single Template Variable value (or default value)\n\ngetResources 1.1.0-pl (July 30, 2010)\n====================================\n- Added &toPlaceholder property for assigning results to a placeholder\n- Added &resources property for including/excluding specific resources\n- Added &showDeleted property\n- Allow multiple contexts to be passed into &context\n- Added &showUnpublish property\n- Added getresources.core_path reference for easier development\n- [#ADDON-135] Make output separator configurable via outputSeparator property\n- Add where property to allow ad hoc criteria in JSON format\n\ngetResources 1.0.0-ga (December 29, 2009)\n====================================\n- [#ADDON-81] Allow empty tvPrefix property.\n- [#ADDON-89] Allow parents property to have a value of 0.\n- Changed default value of sortbyAlias to empty string and added sortbyEscaped property with default of 0.\n- Added changelog, license, and readme.\";s:9:\"signature\";s:21:\"getresources-1.6.0-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:586:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';this.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'install\',signature:r.signature,register:\'mgr\',topic:topic});var c=this.console;MODx.Ajax.request({url:MODx.config.connectors_url+\'workspace/packages.php\',params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:7:\"install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:49:\"/workspace/package/install/getresources-1.6.0-pl/\";s:12:\"HTTP_MODAUTH\";s:52:\"modx524a4fa6425d00.99369569_1524a4fd15b7d23.78535987\";s:14:\"package_action\";i:0;}','getResources','a:31:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556c3db2b083396d000abe\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556c3db2b083396d000abe\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"getresources-1.6.0-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"1.6.0\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"6\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"opengeek\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:198:\"<p>This release adds support for a tplWrapper, allowing you to more easily handle nested lists and other complex configurations where wrapping the output is required.</p><p></p><p></p><p></p><p></p>\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:37:\"<p>Install via Package Management</p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1477:\"<p></p><p>getResources 1.6.0-pl (February 19, 2013)</p><p><ul><li>Add tplWrapper for specifying a wrapper template</li></ul></p><p>getResources 1.5.1-pl (August 23, 2012)</p><p></p><ul><li>Add tplOperator property to default properties</li><li>&#91;#73&#93; Add between tplOperator to conditionalTpls</li></ul><p></p><p>getResources 1.5.0-pl (June 15, 2012)</p><p></p><ul><li>&#91;#58&#93; Add tplCondition/conditionalTpls support</li><li>&#91;#67&#93; Add odd property</li><li>&#91;#60&#93; Allow custom delimiters for tvFilters</li><li>&#91;#63&#93; Give tplFirst/tplLast precedence over tpl_X/tpl_nX</li><li>Automatically prepare TV values for media-source dependent TVs</li></ul><p></p><p></p><p>getResources 1.4.2-pl (December 9, 2011)</p><p></p><ul><li>&#91;#25&#93; Add new operators to tvFilters</li><li>&#91;#37&#93; Consider default values with tvFilters</li><li>&#91;#57&#93; Fix tpl overrides and improve order</li></ul><p></p><p></p><p>getResources 1.4.1-pl (December 8, 2011)</p><p></p><ul><li>&#91;#57&#93; Add support for factor-based tpls</li><li>&#91;#54&#93;, &#91;#55&#93; Fix processTVList feature</li></ul><p></p><p></p><p>getResources 1.4.0-pl (September 21, 2011)</p><p></p><ul><li>&#91;#50&#93; Use children of parents from other contexts</li><li>&#91;#45&#93; Add dbCacheFlag to control db caching of getCollection, default to false</li><li>&#91;#49&#93; Allow comma-delimited list of TV names as includeTVList or processTVList</li></ul><p></p><p></p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2013-02-19 22:58:37 UTC\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:10:\"created_by\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"opengeek\";s:8:\"children\";a:0:{}}i:16;a:3:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2013-02-19 22:58:37 UTC\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"115211\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:24;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:25;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"getResources\";s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.0\";s:8:\"children\";a:0:{}}i:28;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:29;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"512403a2f245547611000025\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"5124039df245547611000023\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:35:\"getresources-1.6.0-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"24931\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"67.3.222.182\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=512403a2f245547611000025\";s:8:\"children\";a:0:{}}}}i:30;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"getresources-1.6.0-pl\";s:8:\"children\";a:0:{}}}',1,6,0,'pl',0),
	('breadcrumbs-1.1.0-pl','2013-10-01 06:32:19','2013-09-30 23:32:33','2013-10-01 06:32:33',0,1,1,0,'breadcrumbs-1.1.0-pl.transport.zip',NULL,'a:34:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:412:\"--------------------\nSnippet: Breadcrumbs\n--------------------\nVersion: 1.0\nDate: 2008.10.08\nAuthor: jaredc@honeydewdesign.com\nEditor: Shaun McCormick <shaun@collabpad.com>\nHonorable mentions:\n- Bill Wilson\n- wendy@djamoer.net\n- grad\n\nThis snippet was designed to show the path through the various levels of site structure\nback to the root. It is NOT necessarily the path the user took to arrive at a given\npage.\";s:9:\"changelog\";s:497:\"Changelog file for breadcrumbs.\n\nBreadcrumbs 1.1-rc2 (August 31, 2010)\n====================================\n- Fix bug that skips the immediate children of site_start\n\nBreadcrumbs 1.1-rc1 (March 19, 2010)\n====================================\n- Updated version for Revo RC1\n- [#ADDON-84], [#ADDON-73] Fixed bug with showCrumbsAtHome and showCurrentCrumb\n- Consolidated settings into $scriptProperties, which cuts down snippet file code\n- Added initialize() function to handle default config settings\";s:9:\"signature\";s:20:\"breadcrumbs-1.1.0-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:586:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';this.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'install\',signature:r.signature,register:\'mgr\',topic:topic});var c=this.console;MODx.Ajax.request({url:MODx.config.connectors_url+\'workspace/packages.php\',params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:7:\"install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:48:\"/workspace/package/install/breadcrumbs-1.1.0-pl/\";s:12:\"HTTP_MODAUTH\";s:52:\"modx524a4fa6425d00.99369569_1524a4fd15b7d23.78535987\";s:14:\"package_action\";i:0;}','Breadcrumbs','a:31:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556a9ab2b083396d0000eb\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556a9ab2b083396d0000eb\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:20:\"breadcrumbs-1.1.0-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"1.1.0\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:246:\"<p>Creates a highly configurable and styleable breadcrumb navigation trail.</p>\n<p>\n</p><p>&nbsp;</p>\n<p>Breadcrumbs 1.1-rc2 (August 31, 2010)</p>\n<p>\n</p><ul>\n<li>Fix bug that skips the immediate children of site_start</li>\n</ul>\n<p></p>\n<p></p>\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:38:\"<p>Install via Package Management.</p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:22:\"<p>Initial content</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2011-08-22 19:29:21 UTC\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:10:\"created_by\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:16;a:3:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2011-08-22 19:29:21 UTC\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"57014\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:24;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:25;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:11:\"Breadcrumbs\";s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.0\";s:8:\"children\";a:0:{}}i:28;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:29;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4e52ae12f24554618600003e\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4e52ae11f24554618600003c\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:34:\"breadcrumbs-1.1.0-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"26113\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:14:\"66.147.244.139\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=4e52ae12f24554618600003e\";s:8:\"children\";a:0:{}}}}i:30;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:20:\"breadcrumbs-1.1.0-pl\";s:8:\"children\";a:0:{}}}',1,1,0,'pl',0),
	('ultimateparent-2.0-pl','2013-10-01 06:34:21','2013-09-30 23:35:09','2013-10-01 06:35:09',0,1,1,0,'ultimateparent-2.0-pl.transport.zip',NULL,'a:32:{s:7:\"license\";s:13:\"Public Domain\";s:9:\"signature\";s:21:\"ultimateparent-2.0-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:586:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';this.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'install\',signature:r.signature,register:\'mgr\',topic:topic});var c=this.console;MODx.Ajax.request({url:MODx.config.connectors_url+\'workspace/packages.php\',params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:7:\"install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:49:\"/workspace/package/install/ultimateparent-2.0-pl/\";s:12:\"HTTP_MODAUTH\";s:52:\"modx524a4fa6425d00.99369569_1524a4fd15b7d23.78535987\";s:14:\"package_action\";i:0;}','UltimateParent','a:31:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556bdfb2b083396d000887\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556bdfb2b083396d000887\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"ultimateparent-2.0-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"2.0.0\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:90:\"<p>Return the \"ultimate\" parent of a document. Added topLevel support to Revo version.</p>\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:38:\"<p>Install via Package Management.</p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:7:\"<p></p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2010-10-06 14:10:42 UTC\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:10:\"created_by\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:16;a:3:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"2010-10-06 14:11:10 UTC\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"24195\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:47:\"http://modxcms.com/forums/index.php?topic=55378\";s:8:\"children\";a:0:{}}i:25;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:14:\"UltimateParent\";s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.0\";s:8:\"children\";a:0:{}}i:28;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:29;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556be1b2b083396d000895\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556be1b2b083396d000894\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:35:\"ultimateparent-2.0-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"16286\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"67.3.222.182\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=4d556be1b2b083396d000895\";s:8:\"children\";a:0:{}}}}i:30;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"ultimateparent-2.0-pl\";s:8:\"children\";a:0:{}}}',2,0,0,'pl',0),
	('getresources-1.6.1-pl','2014-04-02 09:03:26','2014-04-02 19:03:35','2014-04-02 21:03:35',0,1,1,0,'getresources-1.6.1-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:336:\"--------------------\nSnippet: getResources\n--------------------\nVersion: 1.6.0-pl\nReleased: December 30, 2013\nSince: December 28, 2009\nAuthor: Jason Coward <jason@opengeek.com>\n\nA general purpose Resource listing and summarization snippet for MODX Revolution.\n\nOfficial Documentation:\nhttp://docs.modxcms.com/display/ADDON/getResources\n\";s:9:\"changelog\";s:3492:\"Changelog for getResources.\n\ngetResources 1.6.1-pl (December 30, 2013)\n====================================\n- Allow tvFilter values to contain filter operators\n- Allow 0-based idx\n- Pass scriptProperties to wrapperTpl\n- [#30][#80] Only dump properties for invalid tpl when debug enabled\n\ngetResources 1.6.0-pl (February 19, 2013)\n====================================\n- Add tplWrapper for specifying a wrapper template\n\ngetResources 1.5.1-pl (August 23, 2012)\n====================================\n- Add tplOperator property to default properties\n- [#73] Add between tplOperator to conditionalTpls\n\ngetResources 1.5.0-pl (June 15, 2012)\n====================================\n- [#58] Add tplCondition/conditionalTpls support\n- [#67] Add odd property\n- [#60] Allow custom delimiters for tvFilters\n- [#63] Give tplFirst/tplLast precedence over tpl_X/tpl_nX\n- Automatically prepare TV values for media-source dependent TVs\n\ngetResources 1.4.2-pl (December 9, 2011)\n====================================\n- [#25] Add new operators to tvFilters\n- [#37] Consider default values with tvFilters\n- [#57] Fix tpl overrides and improve order\n\ngetResources 1.4.1-pl (December 8, 2011)\n====================================\n- [#57] Add support for factor-based tpls\n- [#54], [#55] Fix processTVList feature\n\ngetResources 1.4.0-pl (September 21, 2011)\n====================================\n- [#50] Use children of parents from other contexts\n- [#45] Add dbCacheFlag to control db caching of getCollection, default to false\n- [#49] Allow comma-delimited list of TV names as includeTVList or processTVList\n\ngetResources 1.3.1-pl (July 14, 2011)\n====================================\n- [#43] Allow 0 as idx property\n- [#9] Fix tvFilters grouping\n- [#46] Fix criteria issue with &resources property\n\ngetResources 1.3.0-pl (March 28, 2011)\n====================================\n- [#33] sortbyTVType: Allow numeric and datetime TV sorting via SQL CAST()\n- [#24] Fix typos in list property options\n- [#4] Support multiple sortby fields via JSON object\n- Use get() instead to toArray() if includeContent is false\n- [#22] Add &toSeparatePlaceholders property for splitting output\n\ngetResources 1.2.2-pl (October 18, 2010)\n====================================\n- [#19] Fix sortbyTV returning duplicate rows\n\ngetResources 1.2.1-pl (October 11, 2010)\n====================================\n- Remove inadvertent call to modX::setLogTarget(\'ECHO\')\n\ngetResources 1.2.0-pl (September 25, 2010)\n====================================\n- Fix error when &parents is not set\n- Allow empty &sortby\n- Add ability to sort by a single Template Variable value (or default value)\n\ngetResources 1.1.0-pl (July 30, 2010)\n====================================\n- Added &toPlaceholder property for assigning results to a placeholder\n- Added &resources property for including/excluding specific resources\n- Added &showDeleted property\n- Allow multiple contexts to be passed into &context\n- Added &showUnpublish property\n- Added getresources.core_path reference for easier development\n- [#ADDON-135] Make output separator configurable via outputSeparator property\n- Add where property to allow ad hoc criteria in JSON format\n\ngetResources 1.0.0-ga (December 29, 2009)\n====================================\n- [#ADDON-81] Allow empty tvPrefix property.\n- [#ADDON-89] Allow parents property to have a value of 0.\n- Changed default value of sortbyAlias to empty string and added sortbyEscaped property with default of 0.\n- Added changelog, license, and readme.\n\";s:9:\"signature\";s:21:\"getresources-1.6.1-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:902:\"function (va){\n		var g = Ext.getCmp(\'modx-package-grid\');\n		if (!g) return false;\n		var r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n		var topic = \'/workspace/package/install/\'+r.signature+\'/\';\n        this.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: \'install\'\n            ,signature: r.signature\n            ,register: \'mgr\'\n            ,topic: topic\n        });\n\n		var c = this.console;\n        MODx.Ajax.request({\n            url: MODx.config.connectors_url+\'workspace/packages.php\'\n            ,params: va\n            ,listeners: {\n                \'success\': {fn:function() {\n                    this.activate();\n					Ext.getCmp(\'modx-package-grid\').refresh();\n                },scope:this}\n                ,\'failure\': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n	}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:7:\"install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:49:\"/workspace/package/install/getresources-1.6.1-pl/\";s:14:\"package_action\";i:1;}','getResources','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52c184b462cf240b35006e31\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556c3db2b083396d000abe\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"getresources-1.6.1-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"getResources\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"1.6.1\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"6\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"opengeek\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:157:\"<p>This patch release fixes several bugs, including the dumping of properties to array if the output of a tpl Chunk is empty.</p><p></p><p></p><p></p><p></p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:37:\"<p>Install via Package Management</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1742:\"<p></p><p>getResources 1.6.1-pl (December 30, 2013)</p><ul><li>Allow tvFilter values to contain filter operators</li><li><li>Allow 0-based idx</li><li>Pass scriptProperties to wrapperTpl</li><li>Only dump properties for invalid tpl when debug enabled</li></li></ul><p>getResources 1.6.0-pl (February 19, 2013)</p><p></p><ul><li>Add tplWrapper for specifying a wrapper template</li></ul><p></p><p>getResources 1.5.1-pl (August 23, 2012)</p><p></p><ul><li>Add tplOperator property to default properties</li><li>&#91;#73&#93; Add between tplOperator to conditionalTpls</li></ul><p></p><p>getResources 1.5.0-pl (June 15, 2012)</p><p></p><ul><li>&#91;#58&#93; Add tplCondition/conditionalTpls support</li><li>&#91;#67&#93; Add odd property</li><li>&#91;#60&#93; Allow custom delimiters for tvFilters</li><li>&#91;#63&#93; Give tplFirst/tplLast precedence over tpl_X/tpl_nX</li><li>Automatically prepare TV values for media-source dependent TVs</li></ul><p></p><p></p><p>getResources 1.4.2-pl (December 9, 2011)</p><p></p><ul><li>&#91;#25&#93; Add new operators to tvFilters</li><li>&#91;#37&#93; Consider default values with tvFilters</li><li>&#91;#57&#93; Fix tpl overrides and improve order</li></ul><p></p><p></p><p>getResources 1.4.1-pl (December 8, 2011)</p><p></p><ul><li>&#91;#57&#93; Add support for factor-based tpls</li><li>&#91;#54&#93;, &#91;#55&#93; Fix processTVList feature</li></ul><p></p><p></p><p>getResources 1.4.0-pl (September 21, 2011)</p><p></p><ul><li>&#91;#50&#93; Use children of parents from other contexts</li><li>&#91;#45&#93; Add dbCacheFlag to control db caching of getCollection, default to false</li><li>&#91;#49&#93; Allow comma-delimited list of TV names as includeTVList or processTVList</li></ul><p></p><p></p>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2013-12-30T14:35:32+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"opengeek\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-04-02T18:52:47+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2013-12-30T14:35:32+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"136464\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=52c184b562cf240b35006e33\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"getresources-1.6.1-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52c184b562cf240b35006e33\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52c184b462cf240b35006e31\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:35:\"getresources-1.6.1-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"12687\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:14:\"91.106.201.126\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=52c184b562cf240b35006e33\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"getresources-1.6.1-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:32:\"blogging,content,navigation,news\";s:8:\"children\";a:0:{}}i:37;a:4:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:57:\"blog,blogging,resources,getr,getresource,resource,listing\";s:8:\"children\";a:0:{}}}',1,6,1,'pl',0),
	('formit-2.2.0-pl','2015-02-10 05:46:46','2015-02-09 22:53:05','2015-02-10 05:53:05',0,1,1,0,'formit-2.2.0-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:213:\"--------------------\nSnippet: FormIt\n--------------------\nAuthor: Shaun McCormick <shaun@modx.com>\n\nA form processing Snippet for MODx Revolution.\n\nOfficial Documentation:\nhttp://rtfm.modx.com/display/ADDON/FormIt\";s:9:\"changelog\";s:10330:\"Changelog for FormIt.\n\nFormIt 2.2.0\n====================================\n- [#8382] Prevent issue with checkboxes/radios causing text-parsing problems with required validator\n- Fixed issue with custom error message for vTextPasswordConfirm not respected\n- [#9457] Fixed issue with commas in values causing errors with FormItIsChecked & FormItIsSelected\n- [#9576] Add ability to translate country options\n- Add check for preHook errors before processing postHooks\n- Add option, defaulting true, to trim spaces from sides of values before validation\n- [#8785] Fix E_STRICT error in fiDictionary\n\nFormIt 2.1.2\n====================================\n- Various language updates\n- [#7250] Fix issue with 0 not passing :required filter\n\nFormIt 2.1.1\n====================================\n- [#8204] Fix issue with FormItAutoResponder and processing of MODX tags\n\nFormIt 2.1.0\n====================================\n- [#7620] Allow for MODX tags in email templates, as well as pass-through of snippet properties to tpl\n- [#7502] Add ability to find type of hook by using $hook->type\n- [#8151] More sanity checking for FormItAutoResponder and replyTo addresses\n- Fix useIsoCode issue in FormItCountryOptions\n- Update German translation\n- Enhance validation templating for validationErrorBulkTpl\n- Add &country option to FormItStateOptions to allow loading of non-US states (currently us/de)\n\nFormIt 2.0.3\n====================================\n- Update Czech translation\n- Fix issue with French accents in translation\n- [#6021] Refactor Russian reCaptcha translations\n- [#6618] Standardize XHTML in reCaptcha usage\n\nFormIt 2.0.2\n====================================\n- [#4864] Fix issue with isNumber not allowing blank fields\n- [#5404] Fix issues with checkboxes and array fields in FormItAutoResponder\n- [#5269] Fix issues with checkboxes in various forms in emails\n- [#5792] Update reCaptcha URLs\n\nFormIt 2.0.1\n====================================\n- [#5525] Add &allowFiles property, that when set to 0, prevents file submissions on form\n- [#5484] Fix issue with double validation error spans\n- Fix issue where config was not passed to hooks\n- Update German translation\n\nFormIt 2.0.0\n====================================\n- [#3514] Add ability to customize validator error messages per FormIt form and per field\n- [#4705] Add regexp validator\n- [#5454] Fix issue with customValidators property in 2.0.0-rc2\n- Major reworking of main FormIt script to be OOP\n- Add over 150 unit tests to prevent regression\n- [#5388], [#5240] Fix issue with FormItCountryOptions and &useIsoCode\n- Fix issue with FormItStateOptions and &useAbbr\n- [#5267] Fix issue with FormItRetriever and array fields\n\nFormIt 1.7.0\n====================================\n- Add ability to have \"Frequent Visitors\" optgroup in FormItCountryOptions, moving specified countries to the top of the list in an optgroup\n- Add missing property translations for FormItStateOptions snippet\n- Fix small issue with stored values after validation of fields\n- Add FormItStateOptions snippet for easy U.S. state dropdowns\n- Add FormItCountryOptions snippet for easy country dropdowns\n- [#5101] Fix issue with emailMultiSeparator and emailMultiWrapper default values\n- Fix issue with bracketed field names being added as extra fields post-validation with . prefix\n\nFormIt 1.6.0\n====================================\n- [#4708] Add support for bracketed fields, such as contact[name]\n- [#5038] Fix uninitialized variable warnings in reCaptcha service\n- [#4993] Add Italian translation and fix recaptcha links\n- Fix issue where fields could be removed via DOM from form and be bypassed\n- Add &emailMultiSeparator and &emailMultiWrapper for handling display of checkboxes/multi-selects in email hook\n\nFormIt 1.5.6\n====================================\n- [#4564] Fix redirectTo with non-web contexts\n\nFormIt 1.5.5\n====================================\n- [#4168] Add emailConvertNewlines property for handling newlines in HTML emails\n- [#4057] Prevent math hook from generating similar numbers\n- [#4302] Cleanups to FormItAutoResponder snippet\n- [#3991] Fix issue with checkbox values in emails\n\nFormIt 1.5.4\n====================================\n- Fix issue with math hook where error placeholders were incorrect\n- Fix issue where emailHtml property was not respected in email hook\n- Fix issue where hooks were not passed customProperties array\n- [#51] Allow blank fields to be passed with :email validator\n- [#55] Allow all fields to be accessed in custom validators\n\nFormIt 1.5.3\n====================================\n- [#40] Add ability to display all error messages in bulk at top, added validationErrorBulkTpl for templating each one\n- [#52] Add a general validation error message property, validationErrorMessage, that shows when validation fails\n- [#53] Fix bug that prevented recaptcha options from working\n- Add a generic validation error placeholder in FormIt to allow for general messages\n- [#50] Trim each hook specification in hooks calls\n- [#49] Ensure reCaptcha service instance is unique for each FormIt instance\n- [#47] Ensure email validator checks for empty string\n- [#42] Can now include field names in error strings via `field` placeholder\n- [#39] Fix issue with FormItIsChecked/Selected to prevent output from occurring if not checked\n- [#37] Fix allowTags validator to work, and work with parameters encapsulated by ^\n\nFormIt 1.5.2\n====================================\n- Fixed security vulnerability\n- Added math hook, allowing anti-spam math field measure\n- Added more debugging info to email hook\n\nFormIt 1.5.1\n====================================\n- Fixed issue where &store was not respecting values set in post-hooks\n- Redirect hook now redirects *after* all other hooks execute\n\nFormIt 1.5.0\n====================================\n- Fixed bug with redirectParams not parsing placeholders in the params\n- Added redirectParams property, which allows a JSON object of params to be passed when using redirect hook\n- Added spamCheckIp property, defaults to false, to check IP as well in spam hook\n- Fixed incorrect default param for fiarSender\n- Fixed error reporting for FormItAutoResponder\n- Added sanity checks to form attachments when dealing with missing names\n- Fixed invalid offset error in checkbox validation\n- Added recaptchaJS to allow for custom JS overriding of reCaptcha options var\n\nFormIt 1.4.1\n====================================\n- Added sanity check for emailHtml property on email hook\n- Added sanity check for replyto/cc/bcc emails on email hook\n- Added ability to change language via &language parameter\n\nFormIt 1.4.0\n====================================\n- Fixed bug with recaptcha and other hooks error display messages\n- Introduced &validate parameter for more secure validation parameters to prevent POST injection\n- Added FormItIsChecked and FormItIsSelected custom output filters for easier checkbox/radio/select handling of selected values\n- Added &placeholderPrefix for FormIt snippet, defaults to `fi.`\n\nFormIt 1.3.0\n====================================\n- Fixed issue with isNumber validator\n- Added FormItRetriever snippet to get data from a FormIt submission for thank you pages\n- Added extra API methods for custom hooks for easier data grabbing\n- Added FormItAutoResponder snippet to use as a custom hook for auto-responses\n- Added &successMessage and &successMessagePlaceholder properties for easier success message handling\n- Fixed ordering for &emailFrom property\n- Added width/height for reCaptcha, however, reCaptcha APIs prevent resizing via calls\n\nFormIt 1.2.1\n====================================\n- Added recaptchaTheme property, which allows theming of reCaptcha hook\n\nFormIt 1.2.0\n====================================\n- Added preHooks property to allow for custom snippets to pre-fill fields\n- Added clearFieldsOnSuccess property to clear fields after a successful form submission without a redirect\n- Allow placeholders of fields in all email properties\n- Added customValidators property to FormIt snippet to restrict custom validators to only specified validators to prevent brute force snippet loading\n- Added fiValidator::addError for easier error loading for custom validators\n- Added German translation\n\nFormIt 1.1.7\n====================================\n- Added bcc and cc properties for email hook\n\nFormIt 1.1.6\n====================================\n- i18n of Snippet properties\n- Added emailReplyToName and emailReplyTo properties for email hook\n- Removed SMTP settings as those are now in Revo\n- Fixed bug in html emails where linebreaks were being ignored\n- Added islowercase and isuppercase validators\n- Added multibyte support to validators\n\nFormIt 1.1.5\n====================================\n- Added Russian translation\n- Updated copyright information\n\nFormIt 1.1.4\n====================================\n- Fixed bug with isDate check\n- Migrated FormIt to Git\n- Fixed bug that caused validators to not fire\n- Fixed bug where custom validators were wonky, added \'errors\' references to custom hooks/validators\n- [#ADDON-147] Added support for validation and emailing of file fields\n- Added stripTags to all fields by default (unless \'allowTags\' hook is passed\') to prevent XSS\n- Added in missing settings\n- Added reCaptcha support via the recaptcha hook\n- Adjusted copyright information to reflect current year\n\nFormIt 1.0\n====================================\n- Fixed bug with emailFrom property getting overwritten\n- [#ADDON-122] Fixed incorrect message in spam lexicon item\n- Added \'spam\' hook that utilizes StopForumSpam spam filter. Will filter the fields in the property &spamEmailFields \n- Ensure hooks errors are set as placeholders\n- Aded fi.success placeholder to be set on a successful form submission if no redirect hook is specified \n- Added default to emailTpl property so that it is now no longer required. Will send out email with just field names and values.\n- Added Dutch translation\n- Added missing formit.contains lexicon entry\n- Fixed possible loophole with $this->fields and referencing in multiple hooks\n- Fixed bug on hooks due to !== and != difference\n- Added SMTP support to FormIt email hook\n- Fixed bug with emailFrom in email hook\n- Added emailUseFieldForSubject property to FormIt snippet\n- Fixed bug on email hook where if subject was passed through form, it wouldn\'t set it as email subject\n- Added changelog\";s:9:\"signature\";s:15:\"formit-2.2.0-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:892:\"function (va){\n        var g = Ext.getCmp(\'modx-package-grid\');\n        if (!g) return false;\n        var r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        var topic = \'/workspace/package/install/\'+r.signature+\'/\';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: \'workspace/packages/install\'\n            ,signature: r.signature\n            ,register: \'mgr\'\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                \'success\': {fn:function() {\n                    this.activate();\n					Ext.getCmp(\'modx-package-grid\').refresh();\n                },scope:this}\n                ,\'failure\': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n	}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:43:\"/workspace/package/install/formit-2.2.0-pl/\";s:14:\"package_action\";i:0;}','FormIt','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"51472969f245540556000081\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556c62b2b083396d000b9c\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.0-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"FormIt\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"2.2.0\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:400:\"<p>Automatically validate, parse and email forms. Redirect to thank you pages. Add your own hooks as Snippets to handle forms dynamically. Validate with custom Snippets. Spam protection. Auto-response options. Dynamic country/state dropdown lists.</p>\n<p>See the Official Documentation here:</p>\n<p><a href=\"http://rtfm.modx.com/display/ADDON/FormIt\">http://rtfm.modx.com/display/ADDON/FormIt</a></p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:38:\"<p>Install via Package Management.</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6161:\"<p></p><p><b>New in 2.2.0</b></p><p></p><ul><li>&#91;#8382&#93; Prevent issue with checkboxes/radios causing text-parsing problems with required validator</li><li>Fixed issue with custom error message for vTextPasswordConfirm not respected</li><li>&#91;#9457&#93; Fixed issue with commas in values causing errors with FormItIsChecked &amp; FormItIsSelected</li><li>&#91;#9576&#93; Add ability to translate country options</li><li>Add check for preHook errors before processing postHooks</li><li>Add option, defaulting true, to trim spaces from sides of values before validation</li><li>&#91;#8785&#93; Fix E_STRICT error in fiDictionary</li></ul><p></p><p><b>New in 2.1.2</b></p><p></p><ul><li>Various language updates</li><li>&#91;#7250&#93; Fix issue with 0 not passing :required filter</li></ul><p></p><p><b>New in 2.1.1</b></p><p></p><ul><li>&#91;#8204&#93; Fix issue with FormItAutoResponder and processing of MODX tags</li></ul><p></p><p><b>New in 2.1.0</b></p><p></p><ul><li>&#91;#7620&#93; Allow for MODX tags in email templates, as well as pass-through of snippet properties to tpl</li><li>&#91;#7502&#93; Add ability to find type of hook by using $hook-&gt;type</li><li>&#91;#8151&#93; More sanity checking for FormItAutoResponder and replyTo addresses</li><li>Fix useIsoCode issue in FormItCountryOptions</li><li>Update German translation</li><li>Enhance validation templating for validationErrorBulkTpl</li><li>Add &amp;country option to FormItStateOptions to allow loading of non-US states (currently us/de)</li></ul><p></p><p><b>New in 2.0.3</b></p><p></p><ul><li>Update Czech translation</li><li>Fix issue with French accents in translation</li><li>&#91;#6021&#93; Refactor Russian reCaptcha translations</li><li>&#91;#6618&#93; Standardize XHTML in reCaptcha usage</li></ul><p></p><p><b>New in 2.0.2</b></p><p></p><ul><li>&#91;#4864&#93; Fix issue with isNumber not allowing blank fields</li><li>&#91;#5404&#93; Fix issues with checkboxes and array fields in FormItAutoResponder</li><li>&#91;#5269&#93; Fix issues with checkboxes in various forms in emails</li><li>&#91;#5792&#93; Update reCaptcha URLs</li></ul><p></p><p><b>New in 2.0.1</b></p><ul><li>&#91;#5525&#93; Add &amp;allowFiles property, that when set to 0, prevents file submissions on form</li><li>&#91;#5484&#93; Fix issue with double validation error spans</li><li>Fix issue where config was not passed to hooks</li><li>Update German translation</li></ul><p></p><p><b>New in 2.0.0</b></p><p></p><ul><li>&#91;#3514&#93; Add ability to customize validator error messages per FormIt form and per field</li><li>&#91;#4705&#93; Add regexp validator</li><li>&#91;#5454&#93; Fix issue with customValidators property in 2.0.0-rc2</li><li>Fix issue with reCaptcha loading in 2.0.0-rc1</li><li>Major reworking of main FormIt script to be OOP</li><li>Add over 150 unit tests to prevent regression</li><li>&#91;#5388&#93;, &#91;#5240&#93; Fix issue with FormItCountryOptions and &amp;useIsoCode</li><li>Fix issue with FormItStateOptions and &amp;useAbbr</li><li>&#91;#5267&#93; Fix issue with FormItRetriever and array fields</li></ul><p></p><p><b>New in 1.7.0</b></p><p></p><ul><li>Add ability to have \"Frequent Visitors\" optgroup in FormItCountryOptions, moving specified countries to the top of the list in an optgroup</li><li>Add missing property translations for FormItStateOptions snippet</li><li>Fix small issue with stored values after validation of fields</li><li>Add FormItStateOptions snippet for easy U.S. state dropdowns</li><li>Add FormItCountryOptions snippet for easy country dropdowns</li><li>&#91;#5101&#93; Fix issue with emailMultiSeparator and emailMultiWrapper default values</li><li>Fix issue with bracketed field names being added as extra fields post-validation with . prefix</li></ul><p></p><p><b>New in 1.6.0</b></p><p></p><ul><li>&#91;#4708&#93; Add support for bracketed fields, such as contact&#91;name&#93;</li><li>&#91;#5038&#93; Fix uninitialized variable warnings in reCaptcha service</li><li>&#91;#4993&#93; Add Italian translation and fix recaptcha links</li><li>Fix issue where fields could be removed via DOM from form and be bypassed</li><li>Add &amp;emailMultiSeparator and &amp;emailMultiWrapper properties for handling display of checkboxes/multi-selects in email hook</li></ul><p></p><p><b>New in 1.5.6</b></p><p></p><ul><li>&#91;#4564&#93; Fix redirectTo with non-web contexts</li></ul><p></p><p><b>New in 1.5.5</b></p><ul><li>&#91;#4168&#93; Add emailConvertNewlines property for handling newlines in HTML emails</li><li>&#91;#4057&#93; Prevent math hook from generating similar numbers</li><li>&#91;#4302&#93; Cleanups to FormItAutoResponder snippet</li><li>&#91;#3991&#93; Fix issue with checkbox values in emails</li></ul><p></p><p><b>New in 1.5.4</b></p><p></p>\n<ul>\n<li>Fix issue with math hook where error placeholders were incorrect</li><li>Fix issue where emailHtml property was not respected in email hook</li><li>Fix issue where hooks were not passed customProperties array</li><li>&#91;#51&#93; Allow blank fields to be passed with :email validator</li>\n<li>&#91;#55&#93; Allow all fields to be accessed in custom validators</li>\n</ul>\n<p><b>New in 1.5.3</b></p>\n<ul>\n<li>&#91;#40&#93; Add ability to display all error messages in bulk at top, added validationErrorBulkTpl for templating each one</li>\n<li>&#91;#52&#93; Add a general validation error message property, validationErrorMessage, that shows when validation fails</li>\n<li>&#91;#53&#93; Fix bug that prevented recaptcha options from working</li>\n<li>Add a generic validation error placeholder in FormIt to allow for general messages</li>\n<li>&#91;#50&#93; Trim each hook specification in hooks calls</li>\n<li>&#91;#49&#93; Ensure reCaptcha service instance is unique for each FormIt instance</li>\n<li>&#91;#47&#93; Ensure email validator checks for empty string</li>\n<li>&#91;#42&#93; Can now include field names in error strings via `field` placeholder</li>\n<li>&#91;#39&#93; Fix issue with FormItIsChecked/Selected to prevent output from occurring if not checked</li>\n<li>&#91;#37&#93; Fix allowTags validator to work, and work with parameters encapsulated by ^</li>\n</ul>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2013-03-18T14:49:13+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-02-10T04:43:31+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2013-03-18T14:49:13+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"138398\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=5147296ff245540556000083\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.0-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"5147296ff245540556000083\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"51472969f245540556000081\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:29:\"formit-2.2.0-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"54408\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"111.67.4.130\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=5147296ff245540556000083\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.0-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"forms\";s:8:\"children\";a:0:{}}i:37;a:3:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}}',2,2,0,'pl',0),
	('codemirror-2.2.1-pl','2015-02-10 05:46:57','2015-02-09 22:53:01','2015-02-10 05:53:01',0,1,1,0,'codemirror-2.2.1-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:413:\"--------------------\r\nExtra: CodeMirror\r\n--------------------\r\nVersion: 1.0.0\r\nCreated: June 23rd, 2010\r\nAuthor: Shaun McCormick <shaun+codemirror@modx.com>\r\nLicense: GNU GPLv2 (or later at your option)\r\n\r\nIntegrates CodeMirror RTE into MODx Revolution.\r\n\r\nPlease see the documentation at:\r\nhttp://rtfm.modx.com/display/ADDON/CodeMirror/\r\n\r\nThanks for using CodeMirror!\r\nShaun McCormick\r\nshaun+codemirror@modx.com\";s:9:\"changelog\";s:1573:\"Changelog for CodeMirror integration into MODx Revolution.\r\n\r\nCodeMirror 2.2.1\r\n====================================\r\n- [#1] Fix Incompatibility with SimpleSearch\r\n\r\nCodeMirror 2.2.0\r\n====================================\r\n- [#16] Add Resource editing ability (thanks Jsewill!)\r\n- [#17] Fix tabSize value conversion\r\n\r\nCodeMirror 2.1.0\r\n====================================\r\n- Add match highlighting\r\n- Add code folding for HTML\r\n- Add line wrapping\r\n- Add fullscreen mode (F6 key)\r\n- Add auto-clear empty lines option\r\n- Add smart indenting\r\n- Add over 10 various themes\r\n- Add code folding for HTML/XML\r\n- Upgrade CodeMirror to 2.3\r\n\r\nCodeMirror 2.0.0\r\n====================================\r\n- Compress css/js for faster loading\r\n- Add search/replace field via showSearchForm property\r\n- Add line highlighting via highlightLine property\r\n- Add enterMode, electricChars, firstLineNumber, indentWithTabs, matchBrackets, undoDepth properties for more configuration options\r\n- Upgrade to CodeMirror 2\r\n\r\nCodeMirror 1.1.0\r\n====================================\r\n- Added extra checks and options for ensuring changes get saved\r\n- Integrated into OnFileEditFormPrerender, now can use in file editing\r\n\r\nCodeMirror 1.0.1\r\n====================================\r\n- Now auto-assigns which_element_editor to CodeMirror\r\n\r\nCodeMirror 1.0.0\r\n====================================\r\n- Added plugin properties to adjust how CodeMirror behaves\r\n- Now works on TV default value fields\r\n- Consolidated JS files, fixed too-fast loading in Chrome issue\r\n- Prepared for rc1 release\r\n- Initial commit\";s:9:\"signature\";s:19:\"codemirror-2.2.1-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:892:\"function (va){\n        var g = Ext.getCmp(\'modx-package-grid\');\n        if (!g) return false;\n        var r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        var topic = \'/workspace/package/install/\'+r.signature+\'/\';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: \'workspace/packages/install\'\n            ,signature: r.signature\n            ,register: \'mgr\'\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                \'success\': {fn:function() {\n                    this.activate();\n					Ext.getCmp(\'modx-package-grid\').refresh();\n                },scope:this}\n                ,\'failure\': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n	}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:47:\"/workspace/package/install/codemirror-2.2.1-pl/\";s:14:\"package_action\";i:0;}','CodeMirror','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52fd20fb62cf24170a005244\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556ccab2b083396d000e08\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:19:\"codemirror-2.2.1-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:10:\"CodeMirror\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"2.2.1\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"dinocorn\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:189:\"<p>CodeMirror integration for MODx Revolution. Get custom syntax highlighting in your Elements.</p>\n<p>CodeMirror 1.1.0-pl+ only works with Revolution 2.0.1 or later.</p><ul>\n</ul>\n<p>\n</p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:38:\"<p>Install via Package Management.</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1107:\"<p></p><p><b>New in 2.2.1</b></p><p><ul><li>&#91;#1&#93; Fix Incompatibility with SimpleSearch</li></ul></p><p><b>New in 2.2.0</b></p><p></p><ul><li>&#91;#16&#93; Add Resource editing ability (thanks Jsewill!)</li><li>&#91;#17&#93; Fix tabSize value conversion</li></ul><p></p><p><b>New in 2.1.0</b></p><p></p><ul><li>Add match highlighting</li><li>Add line wrapping</li><li>Add fullscreen mode (F6 key)</li><li>Add auto-clear empty lines option</li><li>Add smart indenting</li><li>Add over 10 various themes</li><li>Add code folding for HTML/XML</li><li>Upgrade CodeMirror to 2.3</li></ul><p></p><p><b>New in 2.0.0</b></p><p></p><ul><li>Tweak of height css of editor to allow fluid heights&nbsp;</li><li>Improve styling of search/replace buttons&nbsp;</li><li>Compress css/js for faster loading</li><li>Add search/replace field via showSearchForm property</li><li>Add line highlighting via highlightLine property</li><li>Add enterMode, electricChars, firstLineNumber, indentWithTabs, matchBrackets, undoDepth properties for more configuration options</li><li>Upgrade to CodeMirror 2</li></ul><p></p><p></p>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-02-13T19:46:03+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"dinocorn\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-02-10T04:38:37+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-02-13T19:46:03+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"76652\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=52fd20fc62cf24170a005246\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:19:\"codemirror-2.2.1-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:4:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:78:\"http://modx.s3.amazonaws.com/extras/4d556ccab2b083396d000e08/codemirror-ss.png\";s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52fd20fc62cf24170a005246\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52fd20fb62cf24170a005244\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:33:\"codemirror-2.2.1-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"20330\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"111.67.4.130\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=52fd20fc62cf24170a005246\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:19:\"codemirror-2.2.1-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:28:\"integrations,richtexteditors\";s:8:\"children\";a:0:{}}i:37;a:3:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}}',2,2,1,'pl',0),
	('migx-2.9.0-pl','2015-02-10 05:47:17','2015-02-09 22:53:26','2015-02-10 05:53:26',0,1,1,0,'migx-2.9.0-pl.transport.zip',NULL,'a:10:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:1392:\"--------------------\nMIGX\n--------------------\nVersion: 2.1.0\nAuthor: Bruno Perner <b.perner@gmx.de>\n--------------------\n\n* MIGX (multiItemsGridTv for modx) is a custom-tv-input-type for adding multiple items into one TV-value and a snippet for listing this items on your frontend.\n* It has a configurable grid and a configurable tabbed editor-window to add and edit items.\n* Each item can have multiple fields. For each field you can use another tv-input-type.\n\nFeel free to suggest ideas/improvements/bugs on GitHub:\nhttp://github.com/Bruno17/multiItemsGridTV/issues\n\nInstallation:\n\ninstall by package-management.\nCreate a new menu:\nSystem -> Actions \n\nActions-tree:\nmigx -> right-click -> create Acton here\ncontroller: index\nnamespace: migx\nlanguage-topics: migx:default,file\n\nmenu-tree:\nComponents -> right-click -> place action here\nlexicon-key: migx\naction: migx - index\nparameters: &configs=migxconfigs||packagemanager||setup\n\nclear cache\ngo to components -> migx -> setup-tab -> setup\n\nIf you are upgrading from MIGX - versions before 2.0\ngo to tab upgrade. click upgrade.\nThis will add a new autoincrementing field MIGX_id to all your MIGX-TV-items\nThe getImageList-snippet needs this field to work correctly.\n\n\nAllways after upgrading MIGX of any Version:\ngo to components -> migx -> setup-tab -> setup\n\nthis will upgrade the migx-configs-table and add new fields, if necessary.\n\n\n\";s:9:\"changelog\";s:8677:\"Changelog for MIGX.\n\nMIGX 2.9\n==============\n[migxLoopCollection] allow use of foreign database\nSottwell\'s improvements on migxresourcemediapath\nnew snippet: migxGetCollectionTree\naccess to foreign database from default processors\nimprovements on multiple formtabs\nmake inline-editing for MIGX - grid possible\noption to add MIGX-items directly without modal\nlistbox-cell-editor\nmovetotop,movetobottom - buttons for MIGX-grid\ncell-editing for MIgXdb - grids\noption to add MIGXdb-items directly without modal\n[getImageList] &inheritFrom - inherit MIGX-items from parents or other resources\nsome migxredactor - fixes \n\nMIGX 2.8.1\n==============\nlets disable the \'Add Item\' - button\nnew configs gridperpage and sortconfig\nwrapperTpl for getImageList and migxLoopCollection\n\nMIGX 2.8.0\n==============\nresolve tables on install\nrender cross, also when empty string\nreusable activaterelations - processors\n[migxLoopCollection] tpl_nN\n[#154] clean TV-value, if no MIGX-items \nadditional db-storage of formtabs and fields\nget menue working in MODX 2.3\nimprove description_is_code \n\n\nMIGX 2.6.8\n==============\nsome other small fixes\nrestrictive condition by processed MIGX-tags for formfields\nFilter-Button for Reset all filters to default-value\nextend date-filter\nmake cmp main caption translatable \nMigx::prepareJoins - optional rightjoin \n\nMIGX 2.6.7\n==============\nadd date - filter \nadd handlepositionselector - processor \nadd snippet exportmigx2db\n\nMIGX 2.6.6\n==============\nfixes only\n\nMIGX 2.6.5\n==============\nfixes only\n\nMIGX 2.6.4\n==============\n[redactor-field] get and use file-properties from a redactor-inputTV\nadd renderImageFromHtml - renderer\n\nMIGX 2.6.3\n==============\nconfigurable redactor-field with configs-configuration, make redactor work in MIGXdb - CMP\n\nMIGX 2.6.2\n==============\nfix issue with imported configs-field, if not an array \n\nMIGX 2.6.1\n==============\nMake Formfields translatable\n\nMIGX 2.6\n==============\n[getImageList] output inner arrays as json-string\nadded polish translation\n[getImageList] splits, build summaries\n make grid-columns translatable, let user add custom-lexicons from custom php-config-files \n\n\nMIGX 2.5.11\n==============\nadd simple MIGXdb - validation (only required for now)\nsome fixes\n\n\nMIGX 2.5.9\n==============\nlet us create new indexes, with altered field-def from schema \noptimize input-option-values-handling, see:http://forums.modx.com/thread/79757/sortable-editable-list-of-checkboxes?page=4#dis-post-483240\n\n\nMIGX 2.5.8\n\n==============\nAdded \'showScreenshot\' (big image in popup) \nAdded template-field for direct template-input for renderChunk\nAdded position - selector for new MIGX - items\nFix for not removed rte-editors when using formswitcher\nsend current store-params to iframe-window\n\n\nMIGX 2.5.6\n\n==============\n\nAdd support for the modmore.com Redactor editor \nsome work on multiuploader for MIGXdb\nmore eNotice - fixes\n\n\nMIGX 2.5.2\n\n==============\nread input-options into MIGX-TV\nrespect filter in default - export.php\nfix for empty value in TV - configs not loading renderers etc.\nfix changed processaction-param after export2csv \nstopEvent() by onClick - event\n\nMIGX 2.5.1\n\n==============\nfix bug with renderChunk - renderer\n\nMIGX 2.5\n\n==============\nget different rtes working - support for tinymce, ckeditor \nsome settings for MIGXfe\ncs - lexicons, \nsome eNotice - fixes\nfix with to big integers on TV-id (set phptype to string)\nlimit MIGX-record-count\n\n\nMIGX 2.4.2\n\n==============\ncolumnButtons for the migx - grid \nlittle form-layout-mods\nadded the option to have the first formtab outside the other tabs above of them.\n\nadded the option to use the TV-description-field as parsed code-areas in the formtabs, modx-tags are parsed there - \nsnippets, chunks, output-filters can be used there. All fields of the record can be used as placeholders.\n\nmigxupdate for MIGXfe\ndefault-values for MIGXdb-filters\nupdate co_id in iframe-window\nadd a searchbox to MIGX-Configurator\nread configs directly from exported configs-files from custom-packages - directory by using configname:packagename - configs\n\n\nMIGX 2.4.1\n\n==============\nsome new snippets:\ngetDayliMIGXrecord\nmigxgetrelations\n\nadded beta treecombo-filter-option for example to filter resources in MIGXdb by resourcetree\nadd window-title configuration, make window-caption dynamic (its possible to use placeholders now)\nhide tabs in form, when only one tab\nadded selectposition - renderer\n\n\nMIGX 2.4.0\n\n==============\nnew renderer - switchStatusOptions\nnew renderer - renderChunk\ngetImageList - added \'contains\' and \'snippet\' - where-filters\nadd duplicate-contextmenue to MIGXdb \nnew property for getImageList: &reverse\ngive TVs in each CMP-tab a unique id\nrefresh grid after closing iframe-window\nadded tpl_n{n} tplFirst tplLast tpl_n tpl_oneresult properties to getImageList\nexport jsonarray-fields as separate fields in csv-export\nalias, breadcrumb and ultimateparent for migxREsourceMediaPath\nAdded TV - description - field to configuration\n\n\nMIGX 2.3.1\n\n==============\nsome eNotice - error - fixes\nadd type - configuration to gridcolumns, now its possible to sort also numeric on the MIGX - grid: see https://github.com/Bruno17/MIGX/issues/41\n\nMIGX 2.3.0\n\n==============\nadd multifile - uploader, upload to MIGX - mediasource\nadd load from mediasource - button to MIGX\nadd migxResourcePath - snippet\nadd iframe - support - its now possible to create chunks with snippet-calls and show the result in an iframe-window. used by multifile-uploader.\n\n\nMIGX 2.2.3\n\n==============\nconfirmation before overriding schema-files\nsome additions for childresources-management by MIGXdb\nswitch between multiple forms - configurations\nadd renderDate - renderer , thanks to Jako\nadditional send all store-baseParams when opening the form-window. This way we can have different forms depending on filters for example.\nadd parent-property for dynamic filter-comboboxes\nadd getliste-where for default getlist-processor\nexport formtabs as clean json in editraw-mode\n\n\nMIGX 2.2.2\n\n==============\nadd migxLoopCollection-snippet\nmove prepareJoins into a migx-method\nconfirmation before remove db-record, getcombo did not use idfield \nallow empty prefix \nadd possibility to use tables without \'deleted\' - field and default-getlist-processor\nfix Call-time pass-by-reference errors\nget tinyMCE to work on richtext-TV-inputs in MIGXdb - CMPs \nfix prefix not sended to writeSchema\ngrid add cls \'main-wrapper\' to give it a bit padding, thanks to jako\n\n\nMIGX 2.2.0\n\n==============\n\nexport/import configuration-objects as json to/from files in custom-package-directories \nnew configs: getlist - defaultsort, joins, gridload_mode (by button, auto) \ngrid-smarty-template now can be also in custom-package-directories\nreworked handling of joined objects in default update-php \nadd connected_object_id baseparam to migx-grid\nadded snippet migxLoopCollection\n\n\nMIGX 2.1.1\n\n==============\n\n  fix for migx-snippet not working with multiple calls on one page\n  resource_id as script-property for getlist-processor when used with migx-snippet\n\nMIGX 2.1.0\n\n==============\n\n  add &sort to the getImageList - snippet\n  add new snippet \'migx\' to get items from db-tables, can use the same configurations and getList - processors as the MIGXdb - manager\n  make it possible to have config-files for grids and processors in another package-folder for easier packaging together custom - packages\n  more MIGXdb - configurations\n\n\nMIGX 2.0.1\n\n==============\n\n  more E_NOTICE - Error - fixes\n  Fix Missing Add - Item - Replacement - String \n\nMIGX 2.0.0\n\n==============\n\n- pl\n\n  fix for Revo 2.2.2\n  fix some E_NOTICE - errors\n\n- new in beta5\n\n  Configure multiple CMP - tabs\n  packagemanager ported to extjs - tab\n  added MIGX-setup/upgrade - tab\n  added configurable text and combo - filterboxes\n\n- new in beta3\n\n  This is a preview-version of MIGXdb\n  MIGXdb can now also be used as configurable CMP\n  MIGX - configurator for tabs, columns, MIGXdb-TV and MIGXdb-CMP\n  Package-manager, create and edit schemas and package-tables\n\n- new:\n  better compatibility with revo 2.2\n  working with mediasources\n  introduced MIGXdb\n\n\nMIGX 1.2.0\n==============\n- new:\n  merge scriptProperties to Placeholders \n  basic compatibility for modx 2.2 \n  autoinc-field: MIGX_id\n  autoResourceFolders - functionality, autoCreate directory for each resource\n  \n  \n- fixed:\n  url-TV support\n  context-based base_path issues\n  remove remaining []\n  remove Tiny-instances when switching form\n  enter on textarea closes window\n  fireResourceFormChange for drag,remove,duplicate \n\nMIGX 1.1.0\n==============\n- new:\n  &docidVarKey\n  &processTVs\n  \n- fixed:\n  context-filepath-issue\n\nMIGX 1.0.0\n==============\n- Initial release.\";s:13:\"setup-options\";s:31:\"migx-2.9.0-pl/setup-options.php\";s:9:\"signature\";s:13:\"migx-2.9.0-pl\";s:14:\"menu_placement\";s:10:\"components\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:41:\"/workspace/package/install/migx-2.9.0-pl/\";s:14:\"package_action\";i:0;}','MIGX','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"549919e6dc532f2c5906524d\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4db018def24554690c000005\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:13:\"migx-2.9.0-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"MIGX\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"2.9.0\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"9\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:3:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:7:\"Bruno17\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:676:\"<p>MIGX (multiItemsGridTv for modx) is a custom-tv-input-type for adding multiple items into one TV-value and a snippet for listing this items on your frontend.</p><p>It has a cofigurable grid and a configurable tabbed editor-window to add and edit items.</p><p>Each item can have multiple fields. For each field you can use another tv-input-type.</p><p>MIGXdb can manage (resource-related) custom-db-table-items in a TV and can help to create CMPs for custom-db-tables</p><p>See the official documentation here:&nbsp;<a href=\"http://rtfm.modx.com/display/addon/MIGX\" style=\"color: rgb(15, 112, 150); \" title=\"\" target=\"\">http://rtfm.modx.com/display/addon/MIGX</a></p><p></p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5876:\"<p></p><p style=\"margin: 10px 0px 20px; padding: 0px; border-width: 0px; outline-width: 0px; font-size: 13px; vertical-align: baseline; background-color: transparent; line-height: 1.4;\">Installation:Install via Package Management.</p><p style=\"margin: 10px 0px 20px; padding: 0px; border-width: 0px; outline-width: 0px; font-size: 13px; vertical-align: baseline; background-color: transparent; line-height: 1.4;\">For MIGX and MIGXdb - Configuration - Management:</p><p style=\"margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; \">Create a new menu:System -&gt; Actions Actions-tree:migx -&gt; right-click -&gt; create Acton herecontroller: indexnamespace: migxlanguage-topics: migx:default,filemenu-tree:Components -&gt; right-click -&gt; place action herelexicon-key: migxaction: migx - indexparameters: &amp;configs=migxconfigs||packagemanager||setupclear cachego to components -&gt; migx -&gt; setup-tab -&gt; setupIf you are upgrading from MIGX - versions before 2.0go to tab upgrade. click upgrade.This will add a new autoincrementing field MIGX_id to all your MIGX-TV-itemsThe getImageList-snippet needs this field to work correctly.</p><p style=\"margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; \"><b style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; background-position: initial initial; background-repeat: initial initial; \">Note:</b>&nbsp;Make sure to remove older versions of multiItemsGridTv and the multiitemsgridTv-namespace, if you had them tried from Github.</p><p style=\"margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; \"><b style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; background-position: initial initial; background-repeat: initial initial; \">Note</b>: Input Options for the MIGX only work for Revolution 2.1.0-rc2 and later.</p><p style=\"margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; \"></p><p style=\"margin-top: 10px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 13px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; line-height: 1.4; background-position: initial initial; background-repeat: initial initial; \"></p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:10382:\"<p>Changelog for MIGX.</p><p>MIGX 2.9.0==============</p><ul><li>&#91;migxLoopCollection&#93; allow use of foreign database</li><li>Sottwell\'s improvements on migxresourcemediapath</li><li>new snippet: migxGetCollectionTree</li><li>access to foreign database from default processors</li><li>improvements on multiple formtabs</li><li>make inline-editing for MIGX - grid possible</li><li>option to add MIGX-items directly without modal</li><li>listbox-cell-editor</li><li>movetotop,movetobottom - buttons for MIGX-grid</li><li>cell-editing for MIgXdb - grids</li><li>option to add MIGXdb-items directly without modal</li><li>&#91;getImageList&#93; &amp;inheritFrom - inherit MIGX-items from parents or other resources</li><li>some migxredactor - fixes </li></ul><p></p><p>MIGX 2.8.1==============</p><ul><li>lets disable the \'Add Item\' - button</li><li>new configs gridperpage and sortconfig</li><li>wrapperTpl for getImageList and migxLoopCollection</li></ul><p>MIGX 2.8.0==============</p><ul><li>resolve tables on install</li><li>render cross, also when empty string</li><li>reusable activaterelations - processors</li><li>&#91;migxLoopCollection&#93; tpl_nN</li><li>&#91;#154&#93; clean TV-value, if no MIGX-items </li><li>additional db-storage of formtabs and fields</li><li>get menue working in MODX 2.3</li><li>improve description_is_code </li></ul><p></p><p>MIGX 2.6.8==============</p><ul><li>some other small fixes</li><li>restrictive condition by processed MIGX-tags for formfields</li><li>Filter-Button for Reset all filters to default-value</li><li>extend date-filter</li><li>make cmp main caption translatable </li><li>Migx::prepareJoins - optional rightjoin </li></ul><p></p><p>MIGX 2.6.7==============</p><ul><li>add date - filter </li><li>add handlepositionselector - processor </li><li>add snippet exportmigx2db</li></ul><p>MIGX 2.6.6==============</p><ul><li>fixes only</li></ul><p></p><p>MIGX 2.6.5==============</p><ul><li>fix bug with migxResourceMediaPath</li></ul><p>MIGX 2.6.5==============</p><ul><li>fix not working richtext-editors in MIGXdb</li><li>add emptyThrash - process to MIGXdb</li></ul><p>MIGX 2.6.4==============</p><ul><li>&#91;redactor-field&#93; get and use file-properties from a redactor-inputTV</li><li>add renderImageFromHtml - renderer</li></ul><p>MIGX 2.6.3==============</p><ul><li>configurable redactor-field with configs-configuration, make redactor work in MIGXdb - CMP</li></ul><p></p><p>MIGX 2.6.2</p><p>==============</p><ul><li>fix issue with imported configs-field, if not an array\n      </li></ul><p></p><p>MIGX 2.6.1</p><p>==============</p><ul><li>Make Formfields translatable</li></ul><p></p><p>MIGX 2.6</p><p>==============</p><ul><li>&#91;getImageList&#93; output inner arrays as json-string</li><li>added polish translation</li><li>&#91;getImageList&#93; splits, build summaries</li><li>&nbsp;make grid-columns translatable, let user add custom-lexicons from custom php-config-files </li></ul><p>MIGX 2.5.11</p><p>==============</p><ul><li>add simple MIGXdb - validation (only required for now)</li><li>some fixes</li></ul><p></p><p>MIGX 2.5.9</p><p>==============</p><ul><li>let us create new indexes, with altered field-def from schema </li><li>optimize input-option-values-handling, see:http://forums.modx.com/thread/79757/sortable-editable-list-of-checkboxes?page=4#dis-post-483240</li></ul><p></p><p>MIGX 2.5.8</p><p>==============</p><ul><li>Added \'showScreenshot\' (big image in popup) </li><li>Added template-field for direct template-input for renderChunk</li><li>Added position - selector for new MIGX - items</li><li>Fix for not removed rte-editors when using formswitcher</li><li>send current store-params to iframe-window</li></ul><p></p><p>MIGX 2.5.6</p><p>==============</p><ul><li>Add support for the modmore.com Redactor editor\n</li><li>some work on multiuploader for MIGXdb</li><li>more eNotice - fixes</li></ul><p></p><p>MIGX 2.5.2</p><p>==============</p><ul><li>read input-options into MIGX-TV</li><li>respect filter in default - export.php</li><li>fix for empty value in TV - configs not loading renderers etc.</li><li>fix changed processaction-param after export2csv </li><li>stopEvent() by onClick - event</li></ul><p></p><p>MIGX 2.5.1</p><p>==============</p><ul><li>fix bug with renderChunk - renderer</li></ul><p></p><p>MIGX 2.5</p><p>==============</p><ul><li>get different rtes working - support for tinymce, ckeditor </li><li>some settings for MIGXfe</li><li>cs - lexicons, </li><li>some eNotice - fixes</li><li>fix with to big integers on TV-id (set phptype to string)</li><li>limit MIGX-record-count</li></ul><p></p><p>MIGX 2.4.2</p><p>==============</p><ul><li>columnButtons for the migx - grid </li><li>little form-layout-mods</li><li>added the option to have the first formtab outside the other tabs above of them.</li><li>added the option to use the TV-description-field as parsed code-areas in the formtabs, modx-tags are parsed there - </li><li>snippets, chunks, output-filters can be used there. All fields of the record can be used as placeholders.</li><li>migxupdate for MIGXfe</li><li>default-values for MIGXdb-filters</li><li>update co_id in iframe-window</li><li>add a searchbox to MIGX-Configurator</li><li>read configs directly from exported configs-files from custom-packages - directory by using configname:packagename - configs</li></ul><p>MIGX 2.4.1</p><p>==============</p><p>some new snippets:</p><ul><li>getDayliMIGXrecord</li><li>migxgetrelations</li></ul><p></p><ul><li>added beta treecombo-filter-option for example to filter resources in MIGXdb by resourcetree</li><li>add window-title configuration, make window-caption dynamic (its possible to use placeholders now)</li><li>hide tabs in form, when only one tab</li><li>added selectposition - renderer</li></ul><p>MIGX 2.4.0</p><p>==============</p><ul><li>new renderer - switchStatusOptions</li><li>new renderer - renderChunk</li><li>getImageList - added \'contains\' and \'snippet\' - where-filters</li><li>add duplicate-contextmenue to MIGXdb </li><li>new property for getImageList: &amp;reverse</li><li>give TVs in each CMP-tab a unique id</li><li>refresh grid after closing iframe-window</li><li>added tpl_n{n} tplFirst tplLast tpl_n tpl_oneresult properties to getImageList</li><li>export jsonarray-fields as separate fields in csv-export</li><li>alias, breadcrumb and ultimateparent for migxREsourceMediaPath</li><li>Added TV - description - field to configuration</li></ul><p></p><p>MIGX 2.3.1</p><p>==============</p><ul><li>some eNotice - error - fixes</li><li>add type - configuration to gridcolumns, now its possible to sort also numeric on the MIGX - grid: see https://github.com/Bruno17/MIGX/issues/41</li></ul><p></p><p>MIGX 2.3.0</p><p>==============</p><ul><li>add multifile - uploader, upload to MIGX - mediasource</li><li>add load from mediasource - button to MIGX</li><li>add migxResourcePath - snippet</li><li>add iframe - support - its now possible to create chunks with snippet-calls and show the result in an iframe-window. used by multifile-uploader.</li></ul><p></p><p>MIGX 2.2.3</p><p>==============</p><ul><li>confirmation before overriding schema-files</li><li>some additions for childresources-management by MIGXdb</li><li>switch between multiple forms - configurations</li><li>add renderDate - renderer , thanks to Jako</li><li>additional send all store-baseParams when opening the form-window. This way we can have different forms depending on filters for example.</li><li>add parent-property for dynamic filter-comboboxes</li><li>add getliste-where for default getlist-processor</li><li>export formtabs as clean json in editraw-mode</li></ul><p></p><p>MIGX 2.2.2</p><p>==============</p><ul><li>add migxLoopCollection-snippet</li><li>move prepareJoins into a migx-method</li><li>confirmation before remove db-record, getcombo did not use idfield </li><li>allow empty prefix </li><li>add possibility to use tables without \'deleted\' - field and default-getlist-processor</li><li>fix Call-time pass-by-reference errors</li><li>get tinyMCE to work on richtext-TV-inputs in MIGXdb - CMPs </li><li>fix prefix not sended to writeSchema</li><li>grid add cls \'main-wrapper\' to give it a bit padding, thanks to jako</li></ul><p></p><p>MIGX 2.2.0</p><p>==============</p><ul><li>export/import configuration-objects as json to/from files in custom-package-directories </li><li>new configs: getlist - defaultsort, joins, gridload_mode (by button, auto) </li><li>grid-smarty-template now can be also in custom-package-directories</li><li>reworked handling of joined objects in default update-php </li><li>add connected_object_id baseparam to migx-grid</li><li>added snippet migxLoopCollection</li></ul><p></p><p>MIGX 2.1.1</p>\n<p>==============</p>\n<ul><li>fix for migx-snippet not working with multiple calls on one page\n      </li><li>resource_id as script-property for getlist-processor when used with migx-snippet</li></ul>\n\n      <p>MIGX 2.1.0</p><p>==============</p><ul><li>&nbsp; add &amp;sort to the getImageList - snippet</li><li>&nbsp; add new snippet \'migx\' to get items from db-tables, can use the same configurations and getList - processors as the MIGXdb - manager</li><li>&nbsp; make it possible to have config-files for grids and processors in another package-folder for easier packaging together custom - packages</li><li>&nbsp; more MIGXdb - configurations</li></ul><p>MIGX 2.0.1</p><p>==============</p><ul><li>more E_NOTICE - Error - fixes</li><li>Fix Missing Add - Item - Replacement - String </li></ul><p></p><p>MIGX 2.0.0</p><p>==============</p><p>- pl</p><ul><li>&nbsp; fix for Revo 2.2.2</li><li>&nbsp; fix some E_NOTICE - errors</li></ul><p></p><p>- new in beta5</p><ul><li>&nbsp; Configure multiple CMP - tabs</li><li>&nbsp; packagemanager ported to extjs - tab</li><li>&nbsp; added MIGX-setup/upgrade - tab</li><li>&nbsp; added configurable text and combo - filterboxes</li></ul><p></p><p>- new in beta3</p><ul><li>&nbsp; This is a preview-version of MIGXdb</li><li>&nbsp; MIGXdb can now also be used as configurable CMP</li><li>&nbsp; MIGX - configurator for tabs, columns, MIGXdb-TV and MIGXdb-CMP</li><li>&nbsp; Package-manager, create and edit schemas and package-tables</li></ul><p></p><p>- new:</p><ul><li>&nbsp; better compatibility with revo 2.2</li></ul><ul><li>&nbsp; working with mediasources</li></ul><ul><li>&nbsp; starting with MIGXdb (very dev)</li></ul>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-12-23T07:29:42+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:7:\"Bruno17\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-02-10T04:47:12+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-12-23T07:29:42+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"45157\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=549919e7dc532f2c5906524f\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:13:\"migx-2.9.0-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"mysql\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:4:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:70:\"http://modx.s3.amazonaws.com/extras/4db018def24554690c000005/migx1.JPG\";s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"549919e7dc532f2c5906524f\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"549919e6dc532f2c5906524d\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:27:\"migx-2.9.0-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"2987\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:11:\"72.9.159.37\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=549919e7dc532f2c5906524f\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:13:\"migx-2.9.0-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"content,gallery\";s:8:\"children\";a:0:{}}i:37;a:4:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:46:\"migx,multiitems,multitv,migxdb,CMP,MIGX,MIGXdb\";s:8:\"children\";a:0:{}}}',2,9,0,'pl',0);

/*!40000 ALTER TABLE `modx_transport_packages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_transport_providers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_transport_providers`;

CREATE TABLE `modx_transport_providers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `service_url` tinytext,
  `username` varchar(255) NOT NULL DEFAULT '',
  `api_key` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `api_key` (`api_key`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_transport_providers` WRITE;
/*!40000 ALTER TABLE `modx_transport_providers` DISABLE KEYS */;

INSERT INTO `modx_transport_providers` (`id`, `name`, `description`, `service_url`, `username`, `api_key`, `created`, `updated`)
VALUES
	(1,'modx.com','The official MODX transport facility for 3rd party components.','http://rest.modx.com/extras/','','','2015-01-29 14:12:19','2015-02-05 10:50:36');

/*!40000 ALTER TABLE `modx_transport_providers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_user_attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_attributes`;

CREATE TABLE `modx_user_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL,
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  `country` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `website` varchar(255) NOT NULL DEFAULT '',
  `extended` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `internalKey` (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_user_attributes` WRITE;
/*!40000 ALTER TABLE `modx_user_attributes` DISABLE KEYS */;

INSERT INTO `modx_user_attributes` (`id`, `internalKey`, `fullname`, `email`, `phone`, `mobilephone`, `blocked`, `blockeduntil`, `blockedafter`, `logincount`, `lastlogin`, `thislogin`, `failedlogincount`, `sessionid`, `dob`, `gender`, `address`, `country`, `city`, `state`, `zip`, `fax`, `photo`, `comment`, `website`, `extended`)
VALUES
	(4,4,'Admin','justin.lobaito@weloideas.com','5153602172','5153602172',0,0,0,19,1425063683,1425070379,0,'k8smu19s9hfj0ddc5fb9h4f8g0',0,0,'1431 Alderwood Dr.','','Altoona','IA Iowa','50009','','','','','[]'),
	(5,5,'Justin Lobaito','justin.lobaito@weloideas.com','5153602172','5153602172',0,0,0,14,1425140171,1425140533,0,'qn6456ke5d0f0m579erfhbcmb3',0,0,'1431 Alderwood Dr.','','Altoona','Alabama','50009','','','','','[]'),
	(6,6,'Ross','oss@griptite.com','','',0,0,0,1,0,1425063975,0,'i928dd16h68i6tkkee4sv71h66',0,0,'','','','','','','','','','[]'),
	(2,2,'Default Admin','justin.lobaito@weloideas.com','5153602172','5153602172',0,0,0,17,1425477698,1425490829,0,'16l6i7ah0328dtihthtptkp126',0,0,'1431 Alderwood Dr.','','Altoona','Alabama','50009','','','','','[]');

/*!40000 ALTER TABLE `modx_user_attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_user_group_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_group_roles`;

CREATE TABLE `modx_user_group_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `authority` (`authority`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_user_group_roles` WRITE;
/*!40000 ALTER TABLE `modx_user_group_roles` DISABLE KEYS */;

INSERT INTO `modx_user_group_roles` (`id`, `name`, `description`, `authority`)
VALUES
	(1,'Member',NULL,9999),
	(2,'Super User',NULL,0),
	(3,'Content Editor','',100),
	(4,'SubAdmin','',9);

/*!40000 ALTER TABLE `modx_user_group_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_user_group_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_group_settings`;

CREATE TABLE `modx_user_group_settings` (
  `group` int(10) unsigned NOT NULL DEFAULT '0',
  `key` varchar(50) NOT NULL,
  `value` text,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`group`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_user_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_messages`;

CREATE TABLE `modx_user_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sender` int(10) NOT NULL DEFAULT '0',
  `recipient` int(10) NOT NULL DEFAULT '0',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `date_sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_user_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_settings`;

CREATE TABLE `modx_user_settings` (
  `user` int(11) NOT NULL DEFAULT '0',
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` text,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_users`;

CREATE TABLE `modx_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `cachepwd` varchar(100) NOT NULL DEFAULT '',
  `class_key` varchar(100) NOT NULL DEFAULT 'modUser',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `remote_key` varchar(255) DEFAULT NULL,
  `remote_data` text,
  `hash_class` varchar(100) NOT NULL DEFAULT 'hashing.modPBKDF2',
  `salt` varchar(100) NOT NULL DEFAULT '',
  `primary_group` int(10) unsigned NOT NULL DEFAULT '0',
  `session_stale` text,
  `sudo` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `class_key` (`class_key`),
  KEY `remote_key` (`remote_key`),
  KEY `primary_group` (`primary_group`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_users` WRITE;
/*!40000 ALTER TABLE `modx_users` DISABLE KEYS */;

INSERT INTO `modx_users` (`id`, `username`, `password`, `cachepwd`, `class_key`, `active`, `remote_key`, `remote_data`, `hash_class`, `salt`, `primary_group`, `session_stale`, `sudo`)
VALUES
	(4,'admin2','FBz+Ku8nPtS8r7PNgl0EozM3DHzVrz6w8Yk/ybOWJ1Y=','','modUser',1,NULL,NULL,'hashing.modPBKDF2','639c1a17980fd11d6cef2ffeee9b965c',1,'a:0:{}',0),
	(5,'test','5qBh/d1dkJvA3cQVU2qHZ4qCx8qhTjTiSefEdvPnEBg=','','modUser',1,NULL,NULL,'hashing.modPBKDF2','b539080d37be102a665849f4e68d322f',0,'a:1:{i:1;s:3:\"web\";}',0),
	(2,'admin','O/zlpgWKMygWD4x7T/1vYDKsX8G+NO2okghCg7dhFSc=','','modUser',1,NULL,NULL,'hashing.modPBKDF2','cc9e2e8652726eac7d19b5d2d0ffd498',0,'a:2:{i:0;s:3:\"mgr\";i:1;s:3:\"web\";}',1),
	(6,'ross','/N28iO50FzUqSXNqSdeqK/fAQcZ8C/GlUFnJ54ZZ5DA=','','modUser',1,NULL,NULL,'hashing.modPBKDF2','eeed3357256ad75dca723373af6d3fe1',0,'a:0:{}',0);

/*!40000 ALTER TABLE `modx_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_workspaces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_workspaces`;

CREATE TABLE `modx_workspaces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`),
  KEY `name` (`name`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_workspaces` WRITE;
/*!40000 ALTER TABLE `modx_workspaces` DISABLE KEYS */;

INSERT INTO `modx_workspaces` (`id`, `name`, `path`, `created`, `active`, `attributes`)
VALUES
	(1,'Default MODX workspace','{core_path}','2013-10-01 06:29:27',1,NULL);

/*!40000 ALTER TABLE `modx_workspaces` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
